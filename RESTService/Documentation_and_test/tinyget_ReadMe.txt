TinyGet is a Microsoft tool from the IIS 6.0 Resource Kit Tools (http://support.microsoft.com/kb/840671) that can make a request like a browser. It takes as argument the method (GET, POST, PUT, DELETE, etc.) and can send content.

Examples:


Show personen
tinyget.exe -t -srv:localhost -r:1207 -uri /RESTService/Persoon -j:GET

Show persoon
tinyget.exe -t -srv:localhost -r:1207 -uri /RESTService/Persoon/1 -j:GET

Show persoon addresses
tinyget.exe -t -srv:localhost -r:1207 -uri /RESTService/Persoon/1/Adres -j:GET

Update persoon
tinyget.exe -t -srv:localhost -r:1207 -frb:E:\Kubion\Sources\RESTService\Documentation_and_test\test.xml -uri /RESTService/Persoon/1 -j:POST

Add persoon
tinyget.exe -t -srv:localhost -r:1207 -frb:E:\Kubion\Sources\RESTService\Documentation_and_test\test.xml -uri /RESTService/Persoon -j:POST

Delete persoon
tinyget.exe -t -srv:localhost -r:1207 -uri /RESTService/Persoon/2 -j:DELETE