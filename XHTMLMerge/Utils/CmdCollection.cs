using System;
using System.Collections;

namespace XHTMLMerge
{
    [Serializable]
    public class CmdCollection : CollectionBase  
	{

		public CCmd this[ int index ]  
		{
			get  
			{
				return( (CCmd) List[index] );
			}
			set  
			{
				List[index] = value;
			}
		}

		public int Add( CCmd value )  
		{
			return( List.Add( value ) );
		}

		public int IndexOf( CCmd value )  
		{
			return( List.IndexOf( value ) );
		}

		public void Insert( int index, CCmd value )  
		{
			List.Insert( index, value );
		}

		public void Remove( CCmd value )  
		{
			List.Remove( value );
		}

		public bool Contains( CCmd value )  
		{
			// If value is not of type CCmd, this will return false.
			return( List.Contains( value ) );
		}

		protected override void OnInsert( int index, Object value )  
		{
			try
			{
				CCmd temp = (CCmd) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type CCmd." );
			}
		}

		protected override void OnRemove( int index, Object value )  
		{
			try
			{
				CCmd temp = (CCmd) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type CCmd." );
			}
		}

		protected override void OnSet( int index, Object oldValue, Object newValue )  
		{
			try
			{
				CCmd temp = (CCmd) newValue;
			}
			catch
			{
				throw new ArgumentException( "value must be of type CCmd." );
			}
		}

		protected override void OnValidate( Object value )  
		{
			try
			{
				CCmd temp = (CCmd) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type CCmd." );
			}
		}

	}
}
