using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel ;

    public interface IXManager
    {
        IXTemplate NewTemplate();
        IXQuery NewQuery();
        IXConnection NewConnection();
        IDName[] GetDocumentList();
        IDName[] GetTemplatesList();
        IDName[] GetConnectionsList();
        IDName[] GetQueriesList();
        IDName[] GetQueriesList(int templateID);
        IXConnection LoadConnection(int connectionID);

    }

    public interface IXDocument
    {
        /// <summary>
        /// The ID of the document
        /// </summary>
        int ID
        {
            get;
        }

        /// <summary>
        /// The template that this document was created from
        /// </summary>
        int TemplateID
        {
            get;
            set;
        }

        /// <summary>
        /// Flag that indicates if the current version is active or not
        /// </summary>
        bool ActiveVersion
        {
            get;
        }


        /// <summary>
        /// The version of the document
        /// </summary>
        int Version
        {
            get;
        }

        /// <summary>
        /// Returns the content of the document, if it supports string representation.
        /// The document attachments are only downloaded if the DownloadAttachments property is set to true.
        /// </summary>
        string Content
        {
            get;
            set;
        }

        /// <summary>
        /// The content of the document as a byte array (the
        /// </summary>
        byte[] BinaryContent
        {
            get;
            set;
        }

        /// <summary>
        /// The file extension of the document
        /// </summary>
        string DocType
        {
            get;
            set;
        }

        /// <summary>
        /// Redirect string after redirect processing
        /// </summary>
        string Redirect
        {
            get;
            set;
        }

        /// <summary>
        /// String to be processed in order to obtain the document redirect
        /// </summary>
        string InitialRedirect
        {
            get;
            set;
        }

        /// <summary>
        /// A unique identifier used for HTML element IDs and javascript function names in order to isolate this instance of the document in a web page
        /// </summary>
        string UniqueID
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the document attachments will be downloaded when its content will be requested.
        /// </summary>
        bool DownloadAttachments
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the document can be stored in the database
        /// </summary>
        bool ToSave
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the document is created using HTML text not generated with a template
        /// </summary>
        bool ExternalHTMLBody
        {
            get;
            set;
        }

        /// <summary>
        /// Loads a document by its ID
        /// </summary>
        void Load(int ID);

        /// <summary>
        /// Loads a document by its ID and version
        /// </summary>
        void Load(int id, int version);

        /// <summary>
        /// Saves the document, performs updates if indicated and returns the new values for the provided parameters
        /// </summary>
        /// <param name="performUpdates">Indicates if updates are performed</param>
        /// <param name="parameters">Parameters to be updated</param>
        /// <returns>New parameter values</returns>
        Hashtable Save(bool promoteUpdated, Hashtable parameters);

        /// <summary>
        /// Saves the document and performs updates if indicated 
        /// </summary>
        /// <param name="performUpdates">Indicates if updates are performed</param>
        Hashtable Save(bool promoteUpdated);

    }

    public interface IXTemplate
    {
        /// <summary>
        /// The ID of the template
        /// </summary>
        int ID
        {
            get;
        }

        /// <summary>
        /// The template name
        /// </summary>
        string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The template content
        /// </summary>
        string Content
        {
            get;
            set;
        }

        /// <summary>
        /// Default redirect string to be parsed when the generated document will be saved
        /// </summary>
        string Redirect
        {
            get;
            set;
        }

        /// <summary>
        /// The file extension of the generated document
        /// </summary>
        string DocType
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the resulting document can be saved in the database
        /// </summary>
        bool ToSave
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the document resulted is directly saved, without being shown to the user
        /// </summary>
        bool Unattended
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if this is a data template.
        /// A data template's document is shown in web applications as it is, without embedding it in containers
        /// </summary>
        bool IsData
        {
            get;
            set;
        }

        /// <summary>
        /// A unique identifier used for HTML element IDs and javascript function names in order to isolate this instance of the document in a web page
        /// </summary>
        string UniqueID
        {
            get;
        }

        /// <summary>
        /// The template description
        /// </summary>
        string Description
        {
            get;
            set;
        }

        /// <summary>
        /// The roles granted to request this template
        /// </summary>
        Collection<int> Roles
        {
            get;
            set;
        }

        /// <summary>
        /// Loads a template by its name
        /// </summary>
        /// <param name="name">The name of the template</param>
        void Load(string name);

        /// <summary>
        /// Loads a template by its ID
        /// </summary>
        /// <param name="id">The ID of the template</param>
        void Load(int id);

        /// <summary>
        /// Saves the template
        /// </summary>
        void Save();

        /// <summary>
        /// Interprets all commands in the template and returns the resulted document
        /// </summary>
        /// <returns>The document resulted</returns>
        IXDocument ProcessTemplate();

        /// <summary>
        /// Interprets all commands in the template and returns the resulted document
        /// </summary>
        /// <param name="parameters">The provided template parameters and their values. Parameters are case insensitive</param>
        /// <returns>The document resulted</returns>
        IXDocument ProcessTemplate(Hashtable parameters);

        IXDocument ProcessTemplate(string queryString);

        /// <summary>
        /// Gets an array of IDName values representing the queries for the template
        /// </summary>
        IDName[] GetQueriesList();


    }

    public interface IXQuery
    {
        int ID
        {
            get;
        }

        int ConnectionID
        {
            get;
            set;
        }

        int TemplateID
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        string QueryText
        {
            get;
            set;
        }

        void Save();

    }

    public interface IXConnection
    {

        int ID
        {
            get;
        }

        string ConnectionString
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        void Save();

    }

    public class IDName
    {
        public int ID;
        public string Name;
        public Hashtable AdditionalProperties;

        public IDName(int id, string name)
        {
            ID = id;
            Name = name;
            AdditionalProperties = new Hashtable();
        }

        public override string ToString()
        {
            return Name;
        }
    }

