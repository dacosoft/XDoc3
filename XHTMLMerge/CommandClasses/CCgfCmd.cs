using KubionLogNamespace;
using System;
using System.Globalization;

namespace XHTMLMerge
{
    [Serializable]
    public class CCgfCmd : CCmd
    {
        string m_CfgClass = "";
        string m_CfgName = "";
        string m_CfgDefault = "";
        EncodeOption m_encodeOption = EncodeOption.None  ;

        public string CfgClass
        {
            get { return m_CfgClass; }
            set { m_CfgClass = value; }
        }
        public string CfgName
        {
            get { return m_CfgName; }
            set { m_CfgName = value; }
        }
        public string CfgDefault
        {
            get { return m_CfgDefault; }
            set { m_CfgDefault = value; }
        }

        public EncodeOption EncodeOption
        {
            get { return m_encodeOption; }
            set { m_encodeOption = value; }
        }

        public CCgfCmd()
            : base()
        {
            m_enType = CommandType.CFGCommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string l_CfgClass, l_CfgName, l_CfgDefault;

            l_CfgClass = m_parser.ReplaceParameters(m_CfgClass);
            l_CfgName = m_parser.ReplaceParameters(m_CfgName);
            l_CfgDefault = m_parser.ReplaceParameters(m_CfgDefault);

            string sResult = m_parser.GetCfg(l_CfgClass + "__" + l_CfgName, l_CfgDefault);
            sResult = m_parser.ReplaceParameters(sResult);
            sResult = Utils.Encode(sResult, m_encodeOption);

            return sResult;
        }

    }
}


