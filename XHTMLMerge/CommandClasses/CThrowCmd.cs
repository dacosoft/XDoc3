using System;

namespace XHTMLMerge
{
    [Serializable]
    public class CThrowCmd : CCmd
    {
        string m_ParName = "";
        public string ParName
        {
            get { return m_ParName; }
            set { m_ParName = value; }
        }

        public CThrowCmd()
            : base()
        {
            m_enType = CommandType.THROWCommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string l_ParName = m_parser.ReplaceParameters(m_ParName);

            string retVal = "";
            retVal = l_ParName;

            throw new Exception(retVal );

            return "";
        }

    }
}

