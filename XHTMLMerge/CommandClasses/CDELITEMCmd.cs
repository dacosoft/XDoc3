using System;
using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CDELITEMCmd : CCmd
    {
        string m_Indexes = "";
        string m_VarName = "";
        string m_ContextName = "";
        string m_ID = "ID";

        public string Indexes
        {
            get { return m_Indexes; }
            set { m_Indexes = value; }
        }
        public string VarName
        {
            get { return m_VarName; }
            set { m_VarName = value; }
        }
        public string ContextName
        {
            get { return m_ContextName; }
            set { m_ContextName = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public CDELITEMCmd()
            : base()
        {
            m_enType = CommandType.DELITEMCommand ;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string l_VarName, l_ContextName, l_ID, l_Indexes;

            l_VarName = m_parser.ReplaceParameters(m_VarName);
            l_ContextName = m_parser.ReplaceParameters(m_ContextName);
            l_ID = m_parser.ReplaceParameters(m_ID);
            l_Indexes = m_parser.ReplaceParameters(m_Indexes);

            string sPar = m_parser.GetSV(l_VarName, l_ContextName, l_ID, "");
            if (sPar == "missing_setting") sPar = "";
            int index2 = -1;
            if (l_Indexes.Contains(","))
            {
                index2 = int.Parse(l_Indexes.Substring(l_Indexes.IndexOf(",") + 1));
                l_Indexes = l_Indexes.Substring(0, l_Indexes.IndexOf(","));
            }
            int index1 = int.Parse(l_Indexes);
            //sPar = sPar.Replace("\\|", "<tilda>");
            //string[] aData = sPar.Split('|'); 
            string[] aData =Utils.MySplit( sPar,'|'); 
            sPar ="";
            for (int i = 0; i < aData.Length; i++)
            {
                //aData[i] = aData[i].Replace("<tilda>", "\\|");
                if (i != index1 - 1)
                    sPar += (sPar == "" ? "" : "|") + aData[i];
                else
                    if (index2 > -1)
                    {
                        //aData[i] = aData[i].Replace("\\,", "<tilda>");
                        //string[] aData2 = aData[i].Split(',');
                        string[] aData2 =Utils.MySplit (  aData[i],',');
                        string sPar2 = "";
                        for (int j = 0; j < aData2.Length; j++)
                        {
                            //aData2[j] = aData2[j].Replace("<tilda>", "\\,");
                            if (j != index2 - 1)
                                sPar2 += (sPar2 == "" ? "" : ",") + aData2[j];
                        }
                        sPar += (sPar == "" ? "" : "|") + sPar2;
                    }
            }
           m_parser.SetSV(l_VarName, l_ContextName, sPar, l_ID);

            return "";
        }

    }
}
