using System;

namespace XHTMLMerge
{
    [Serializable]
    public class CTextCmd : CCmd
	{
        string m_text = "";
        bool m_forcedText = false;
        const string
            START_BLOCK = "STARTBLOCK",
            END_BLOCK = "ENDBLOCK",
            START_COMMENT = "STARTCOMMENT",
            END_COMMENT = "ENDCOMMENT";

        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }

		public CTextCmd():base()
		{
			m_enType = CommandType.TEXTCommand;
			this.m_bIsBlockCommand = false;
		}

        public void ForceText(string text)
        {
            m_forcedText = true;
            m_text = text;
        }

        public override string Execute(CParser m_parser)
        {
            string retVal = "";
            if (m_forcedText == true)
                if ((m_text.ToUpper().StartsWith("#" + START_BLOCK + "#")) && (m_text.ToUpper().EndsWith("#" + END_BLOCK + "#")))
                    retVal = m_text.Substring(("#" + START_BLOCK + "#").Length, m_text.Length - ("#" + START_BLOCK + "#" + "#" + END_BLOCK + "#").Length);
                else
                    if ((m_text.ToUpper().StartsWith("#" + START_COMMENT + "#")) && (m_text.ToUpper().EndsWith("#" + END_COMMENT + "#")))
                        retVal = "";
                    else
                        retVal = m_text;
            else
                retVal = m_parser.TemplateText.Substring(StartIndex, EndIndex - StartIndex);

            if (!RemoveStart(ref retVal, "\r\n"))
                RemoveStart(ref retVal, "\n");

            if (m_parser.RemoveNewLine)
            {
                retVal = retVal.Replace("\r\n", " ");
                retVal = retVal.Replace("\n", " ");
            }

            if (m_parser.RemoveTab)
            {
                retVal = retVal.Replace("\t", "");
            }
            return retVal;
		}

        bool RemoveStart(ref string text, string newLine)
        {
            if (text.StartsWith(newLine))
            {
                text = text.Substring(newLine.Length);
                return true;
            }

            return false;
        }
	}
}
