using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel ;
using System.Collections.Specialized;
using System.Xml;
using XDocuments;

//using System.Data;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using XDocuments;
//using System.Collections.ObjectModel;


namespace XHTMLMerge
{
    [Serializable]
    public class CImportCmd : CCmd
    {

        const int MAX_INCLUDES = 150;

        protected CmdCollection m_paramCmds = null;
        protected bool m_IsSet = false;
        protected int m_ConnID = -99;
        protected string m_Conn = "";
        protected string m_TemplateName = "fromXML";

        public bool IsSet
        {
            get { return m_IsSet; }
            set { m_IsSet = value; }
        }

        public int ConnID
        {
            get { return m_ConnID; }
            set { m_ConnID = value; }
        }
        public string Conn
        {
            get { return m_Conn; }
            set { m_Conn = value; }
        }
        public string TemplateName
        {
            get { return m_TemplateName; }
            set { if (value == "") value = "fromXML"; m_TemplateName = value; }
        }


        public CmdCollection ParamCmds
        {
            get { return m_paramCmds; }
            set { m_paramCmds = value; }
        }

        public CImportCmd()
            : base()
        {
            m_bIsBlockCommand = false;
            m_enType = CommandType.IMPORTCommand;
            m_paramCmds = new CmdCollection();
        }


        public override string Execute(CParser m_parser)
        {
            string l_TemplateName = TemplateName, l_Conn;
            string sResult;

            try
            {
System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import started");
                CCmd cmd = new CIncludeCmd();
                cmd.Parser = m_parser;
                ((CIncludeCmd)cmd).ParamCmds = ParamCmds;
                string returnVal = cmd.Execute(m_parser);
System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import content ready");

                returnVal = returnVal.Replace("%d_export", "%d");
                returnVal = returnVal.Replace("%dash_export", "%dash");
                returnVal = returnVal.Replace("%macro_export", "%macro");
                returnVal = returnVal.Replace("%percent_export", "%percent");
                returnVal = returnVal.Replace("_percent_export", "_percent");

                if (m_IsSet)
                {
                    l_TemplateName = "ImportSet";
                    if (TemplateName == "ImportSetFull") l_TemplateName = "ImportSetFull";
                }
                else
                    l_TemplateName = m_parser.ReplaceParameters(TemplateName);
                l_Conn = m_parser.ReplaceParameters(Conn);
System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import manager start");
                if (ConnID == -99)
                    sResult = m_parser.Manager.ImportTemplate(l_Conn, l_TemplateName, returnVal);
                else
                    sResult = m_parser.Manager.ImportTemplate(ConnID, l_TemplateName, returnVal);
System.Diagnostics.Debug.WriteLine("    " + "" + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + " - " + "import ready");

                return sResult;
            }
            catch (Exception ex)
            {
                string commandName = "IMPORT";
                string message = "Error executing " + commandName + " comand at line " + m_parser.GetLine(this.StartIndex).ToString() + " for Template = " + l_TemplateName + ";" + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
            finally
            {
                if (m_parser.PreviousTemplateIDs.Count > 0 &&
                    Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]) == m_parser.TemplateID)
//                    m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
//                        m_parser.PreviousTemplateIDs.Remove(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]);
                    m_parser.PreviousTemplateIDs.RemoveAt(m_parser.PreviousTemplateIDs.Count - 1);
            }
        }

        //        public  string Execute1()
        //        {
        //            string templateIDstr = "[unknown]";
        //            string sResult = "";
        //            try
        //            {
        //                string parameterList = "";
        //                foreach (CCmd cmd in m_paramCmds)
        //                    parameterList += cmd.Execute();

        //                char sep = '&';
        //                if (parameterList.IndexOf(sep) == -1)
        //                    sep = ';';

        //                Hashtable hashParams = CollectionsUtil.CreateCaseInsensitiveHashtable();

        //                string[] parameters = parameterList.Split(sep);
        //                foreach (string parameter in parameters)
        //                {
        //                    string[] parts = parameter.Split('=');
        //                    string paramName = parts[0];
        //                    string paramValue = parts[1];

        //                    if (hashParams.Contains(paramName))
        //                        throw new Exception("Parameter '" + paramName + "' is specified more than once;");
        //                    //hashParams[paramName] = System.Web.HttpUtility.UrlDecode(paramValue, Encoding.Default);
        //                          hashParams[paramName] = Utils.XDocUrlDecode(paramValue);
        //              }


        //                m_parser.PreviousTemplateIDs.Add(m_parser.TemplateID);

        //                Hashtable logIncludeParams = hashParams.Clone() as Hashtable;

        //                if (hashParams.Contains("templateid"))
        //                {
        //                    int idForInclude;
        //                    if (!int.TryParse(hashParams["templateid"].ToString(), out idForInclude))
        //                    {
        //                        throw new Exception("Parameter TemplateID is incorrectly specified : '" + hashParams["templateid"].ToString() + "';");
        //                    }
        //                    else
        //                    {
        //                        m_parser.IDForInclude = idForInclude;
        //                        hashParams.Remove("templateid");
        //                    }
        //                }
        //                else if (hashParams.Contains("templatename"))
        //                {
        //                    string templateName = hashParams["templatename"].ToString();
        //                    hashParams.Remove("templatename");
        //                    m_parser.RequestTemplateIDForInclude(templateName);
        //                }

        //                if (m_parser.IDForInclude != -1)
        //                {
        //                    templateIDstr = m_parser.IDForInclude.ToString();

        //                    if (m_parser.PreviousTemplateIDs.Count > MAX_INCLUDES)
        //                    {
        //                        string message = "Template inclusions reached the maximum number of " + MAX_INCLUDES.ToString();
        //                        throw new Exception(message);
        //                    }
        //                }
        //                else
        //                    throw new Exception("Parameter TemplateID or TemplateName is missing or is not correctly specified");


        //                m_parser.RequestTemplate(m_parser.IDForInclude);


        //                if (m_parser.TextForInclude == null)
        //                    throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());

        //                if (m_parser.AdditionalIncludeParams != null)
        //                    foreach (DictionaryEntry de in m_parser.AdditionalIncludeParams)
        //                        hashParams[de.Key] = de.Value;
        //                hashParams["INCLUDE"] = 1;
        //                string sSessionID = "";
        //                object retVal = null;
        //                m_parser.Evaluator.GetValue("SessionID", out retVal);
        //                if (retVal != null) sSessionID = retVal.ToString();
        //                hashParams["SESSIONID"] = sSessionID;

        ////                CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.DataProvider, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
        //                CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.Evaluator , m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
        //                parser.GlobalParams = m_parser.GlobalParams; 
        //                parser.PreviousTemplateIDs = m_parser.PreviousTemplateIDs;
        //                parser.RequestTemplateText += new CParser.RequestTemplateText_Handler(parser_RequestTemplateText);
        //                parser.RequestTemplateID += new CParser.RequestTemplateID_Handler(parser_RequestTemplateID);
        //                parser.LogInclude += new CParser.LogInclude_Handler(parser_LogInclude);
        //                parser.IncludeLogIndentation = m_parser.IncludeLogIndentation + 1;
        //                //parser.SessionVariables = m_parser.SessionVariables; 


        //                m_parser.RequestLogInclude(logIncludeParams, m_parser.IncludeLogIndentation);

        //                string returnVal = parser.Parse();

        //                if (ConnID == -99) 
        //                    sResult = parser.Manager.ImportTemplate(Conn, TemplateName, returnVal);
        //                else 
        //                    sResult = parser.Manager.ImportTemplate(ConnID, TemplateName, returnVal);

        //                return sResult;
        //            }
        //            catch (Exception ex)
        //            {
        //                string commandName = "IMPORT";
        //                string message = "Error executing " + commandName + " comand at line " + m_parser.GetLine(this.StartIndex).ToString() + " for TemplateID = " + templateIDstr + ";" + Environment.NewLine + ex.Message;
        //                throw new Exception(message);
        //            }
        //            finally
        //            {
        //                if (m_parser.PreviousTemplateIDs.Count > 0 &&
        //                    Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]) == m_parser.TemplateID)
        //                    m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
        //            }
        //        }

        //public override string Execute()
        //{
        //    string templateIDstr = "[unknown]";
        //    string sResult = "";
        //    try
        //    {
        //        string parameterList = "";
        //        foreach (CCmd cmd in m_paramCmds)
        //            parameterList += cmd.Execute();

        //        char sep = '&';
        //        if (parameterList.IndexOf(sep) == -1)
        //            sep = ';';

        //        Hashtable hashParams = CollectionsUtil.CreateCaseInsensitiveHashtable();

        //        string[] parameters = parameterList.Split(sep);
        //        foreach (string parameter in parameters)
        //        {
        //            string[] parts = parameter.Split('=');
        //            string paramName = parts[0];
        //            string paramValue = parts[1];

        //            if (hashParams.Contains(paramName))
        //                throw new Exception("Parameter '" + paramName + "' is specified more than once;");
        //            hashParams[paramName] = System.Web.HttpUtility.UrlDecode(paramValue);
        //        }

        //        if (!hashParams.Contains("importtemplatename"))
        //        {
        //            string message = "Missing ImportTemplateName";
        //            throw new Exception(message);
        //        }
        //        string sImportTemplateName = hashParams["importtemplatename"].ToString();
        //        int iImportConnID;
        //        if (!hashParams.Contains("importconnid"))
        //            iImportConnID = -1;
        //        else if (!int.TryParse(hashParams["importconnid"].ToString(), out iImportConnID))
        //        {
        //            throw new Exception("Parameter ImportConnID is incorrectly specified : '" + hashParams["importconnid"].ToString() + "';");
        //        }
        //        //if (hashParams.Contains("export"))
        //        //{
        //        //    sResult = m_parser.Manager.ExportTemplate (iGenerateConnID, sGenerateTemplateName);
        //        //}
        //        //else
        //        //{


        //        m_parser.PreviousTemplateIDs.Add(m_parser.TemplateID);

        //        Hashtable logIncludeParams = hashParams.Clone() as Hashtable;

        //        if (hashParams.Contains("templateid"))
        //        {
        //            int idForInclude;
        //            if (!int.TryParse(hashParams["templateid"].ToString(), out idForInclude))
        //            {
        //                throw new Exception("Parameter TemplateID is incorrectly specified : '" + hashParams["templateid"].ToString() + "';");
        //            }
        //            else
        //            {
        //                m_parser.IDForInclude = idForInclude;
        //                hashParams.Remove("templateid");
        //            }
        //        }
        //        else if (hashParams.Contains("templatename"))
        //        {
        //            string templateName = hashParams["templatename"].ToString();
        //            hashParams.Remove("templatename");
        //            m_parser.RequestTemplateIDForInclude(templateName);
        //        }

        //        if (m_parser.IDForInclude != -1)
        //        {
        //            templateIDstr = m_parser.IDForInclude.ToString();

        //            if (m_parser.PreviousTemplateIDs.Count > MAX_INCLUDES)
        //            {
        //                string message = "Template inclusions reached the maximum number of " + MAX_INCLUDES.ToString();
        //                throw new Exception(message);
        //            }
        //        }
        //        else
        //            throw new Exception("Parameter TemplateID or TemplateName is missing or is not correctly specified");


        //        m_parser.RequestTemplate(m_parser.IDForInclude);


        //        if (m_parser.TextForInclude == null)
        //            throw new Exception("Could not retrieve template content for TemplateID=" + m_parser.IDForInclude.ToString());

        //        if (m_parser.AdditionalIncludeParams != null)
        //            foreach (DictionaryEntry de in m_parser.AdditionalIncludeParams)
        //                hashParams[de.Key] = de.Value;
        //        hashParams["INCLUDE"] = 1;
        //        string sSessionID = "";
        //        object retVal = null;
        //        m_parser.Evaluator.GetValue("SessionID", out retVal);
        //        if (retVal != null) sSessionID = retVal.ToString();
        //        hashParams["SESSIONID"] = sSessionID;

        //        CParser parser = new CParser(m_parser.Manager, m_parser.IDForInclude, m_parser.DataProvider, m_parser.TextForInclude, hashParams, m_parser.VirtualPath, m_parser.OutputPath);
        //        parser.PreviousTemplateIDs = m_parser.PreviousTemplateIDs;
        //        parser.RequestTemplateText += new CParser.RequestTemplateText_Handler(parser_RequestTemplateText);
        //        parser.RequestTemplateID += new CParser.RequestTemplateID_Handler(parser_RequestTemplateID);
        //        parser.LogInclude += new CParser.LogInclude_Handler(parser_LogInclude);
        //        parser.IncludeLogIndentation = m_parser.IncludeLogIndentation + 1;

        //        m_parser.RequestLogInclude(logIncludeParams, m_parser.IncludeLogIndentation);

        //        string returnVal = parser.Parse();

        //        sResult = parser.Manager.ImportTemplate(iImportConnID, sImportTemplateName, returnVal);

        //        return sResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        string commandName = "IMPORT";
        //        string message = "Error executing " + commandName + " comand at line " + m_parser.GetLine(this.StartIndex).ToString() + " for TemplateID = " + templateIDstr + ";" + Environment.NewLine + ex.Message;
        //        throw new Exception(message);
        //    }
        //    finally
        //    {
        //        if (m_parser.PreviousTemplateIDs.Count > 0 &&
        //            Convert.ToInt32(m_parser.PreviousTemplateIDs[m_parser.PreviousTemplateIDs.Count - 1]) == m_parser.TemplateID)
        //            m_parser.PreviousTemplateIDs.Remove(m_parser.TemplateID);
        //    }
        //}

        //void parser_LogInclude(CParser sender, LogIncludeEventArgs e)
        //{
        //    l_parser.RequestLogInclude(e.IncludeParams, e.Indentation);
        //}

        //void parser_RequestTemplateID(CParser sender, string templateName)
        //{
        //    l_parser.RequestTemplateIDForInclude(templateName);
        //    sender.IDForInclude = l_parser.IDForInclude;
        //}

        //void parser_RequestTemplateText(CParser sender, int templateID)
        //{
        //    l_parser.RequestTemplate(templateID);
        //    sender.TextForInclude = l_parser.TextForInclude;
        //    if (l_parser.AdditionalIncludeParams != null)
        //        foreach (DictionaryEntry de in l_parser.AdditionalIncludeParams)
        //            sender.AdditionalIncludeParams[de.Key] = de.Value;
        //}
    }
}

