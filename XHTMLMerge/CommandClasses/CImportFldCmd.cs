using KubionLogNamespace;
using System;
using System.Globalization;
using XDataSourceModule;

namespace XHTMLMerge
{
    [Serializable]
    public class CImportFldCmd : CCmd
    {
        string m_strQueryName = "";
        string m_strFieldName = "";
        protected int m_ConnID = -99;
        protected string m_Conn = "";
        protected string m_TemplateName = "fromXML";
        string m_Format = "";
        string m_EncodeOption = "";

        //protected CContext m_context = null;
        //protected XDataSourceModule.IXDataSource m_evaluator = null;
        protected Array m_parameters = null;
        protected CCmd m_parent = null;
        protected bool m_isFetchID = false;
        protected bool m_isFetchID1 = false;
        protected bool m_oddEven = false;
        protected bool m_isCount = false;
        protected bool m_isFirstRow = false;
        protected bool m_isLastRow = false;

        public int ConnID
        {
            get { return m_ConnID; }
            set { m_ConnID = value; }
        }
        public string Conn
        {
            get { return m_Conn; }
            set { m_Conn = value; }
        }
        public string TemplateName
        {
            get { return m_TemplateName; }
            set { if (value == "") value = "fromXML"; m_TemplateName = value; }
        }

        public string QueryName
        {
            get { return m_strQueryName; }
            set { m_strQueryName = value; }
        }
        public string FieldName
        {
            get { return m_strFieldName; }
            set { m_strFieldName = value; }
        }
        public string Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string EncodeOption
        {
            get { return m_EncodeOption; }
            set { m_EncodeOption = value; }
        }

        public bool IsCount
        {
            get { return m_isCount; }
            set { m_isCount = value; }
        }
        public bool IsFirstRow
        {
            get { return m_isFirstRow; }
            set { m_isFirstRow = value; }
        }
        public bool IsLastRow
        {
            get { return m_isLastRow; }
            set { m_isLastRow = value; }
        }

        public bool OddEven
        {
            get { return m_oddEven; }
            set { m_oddEven = value; }
        }
        public bool IsFetchID
        {
            get { return m_isFetchID; }
            set { m_isFetchID = value; }
        }
        public bool IsFetchID1
        {
            get { return m_isFetchID1; }
            set { m_isFetchID1 = value; }
        }
        //public CContext Context
        //{
        //    get { return m_context; }
        //    set { m_context = value; }
        //}

        //public XDataSourceModule.IXDataSource Evaluator
        //{
        //    get { return m_evaluator; }
        //    set { m_evaluator = value; }
        //}

        public Array Parameters
        {
            get { return m_parameters; }
            set { m_parameters = value; }
        }

        public CCmd Parent
        {
            get { return m_parent; }
            set { m_parent = value; }
        }

        public CImportFldCmd()
            : base()
        {
            m_enType = CommandType.SETFLDCommand;
            this.m_bIsBlockCommand = false;
        }


        public override string Execute(CParser m_parser)
        {
            string  l_strFieldName, l_Val, l_EncodeOption, l_Format;
            string l_TemplateName = TemplateName, l_Conn;
            string sResult;

            //cannot get in runtime queryname: m_strQueryName = m_parser.ReplaceParameters(m_strQueryName);
            l_strFieldName = m_parser.ReplaceParameters(m_strFieldName);

            SourceResult parentResult = null;
            int queryIndex = m_parser.Context.OGetQueryIndex(m_parent);
            //int queryIndex = m_context.GetQueryIndex(m_strQueryName);
			object oVal = "";
			string retVal = "";
            try
            {
                int parentResultsCount = -1;
                if (m_parent is CREPCmd)
                {
                    parentResult = ((CREPCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CREPCmd)m_parent).ResultsCount;
                }
                else if (m_parent is CXPATHCmd)
                {
                    parentResult = ((CXPATHCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CXPATHCmd)m_parent).ResultsCount;
                }
                else if (m_parent is CQRYCmd)
                {
                    parentResult = ((CQRYCmd)m_parent).GetResult(m_parser);
                    parentResultsCount = ((CQRYCmd)m_parent).ResultsCount;
                }

                string error = "";



                if (m_isFetchID)
                    retVal = queryIndex.ToString();
                else if (m_isFetchID1)
                    retVal = (queryIndex + 1).ToString();
                else if (m_isFirstRow)
                    retVal = ((queryIndex == 0) && (parentResultsCount >0)? 1 : 0).ToString();
                else if (m_isLastRow)
                    retVal = ((queryIndex == parentResultsCount -1)?1:0).ToString();
                else if (m_oddEven)
                    retVal = (queryIndex % 2).ToString();
                else if (m_isCount)
                {
                    int retResCount = parentResultsCount;
                    retVal = retResCount.ToString();
                }
                else
                {
                    if (parentResult.GetFieldValue(l_strFieldName, queryIndex, out oVal))
                        retVal = oVal.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing FLD command for query " + m_strQueryName + " and field " 
                    + m_strFieldName + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + ex.Message);
            }


            l_EncodeOption = m_parser.ReplaceParameters(m_EncodeOption);
            l_Format = m_parser.ReplaceParameters(m_Format);

            retVal = m_parser.ApplyFormat(retVal, l_Format);

            EncodeOption t_encodeOption = CParser.GetEncodeOption(l_EncodeOption);
            retVal = Utils.Encode(retVal, t_encodeOption);

            l_Val = retVal;

            l_TemplateName = m_parser.ReplaceParameters(TemplateName);
            l_Conn = m_parser.ReplaceParameters(Conn);

            if (ConnID == -99)
                sResult = m_parser.Manager.ImportTemplate(l_Conn, l_TemplateName, l_Val);
            else
                sResult = m_parser.Manager.ImportTemplate(ConnID, l_TemplateName, l_Val);
            return sResult;
		}

    }
}

