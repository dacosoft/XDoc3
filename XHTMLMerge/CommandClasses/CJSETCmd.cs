﻿using System;
using System.Diagnostics;
using XDataSourceModule;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using KubionLogNamespace;

namespace XHTMLMerge
{
    [Serializable]
    public class CJSETCmd : CCmd
    {
        # region Protected members

        protected string m_ParamName = "";
        protected string m_Source = "";
        protected string m_JPath = "";
        protected string m_JObj = "";
        protected string m_JAttr = "";
        protected string m_JOP = "";
        protected string m_ErrorParamName = "ERROR";
        //protected string m_isParSource = "0";
        //public override void GetJson(StringBuilder sJSON, StringBuilder sbWarning)
        //{
        //    sJSON.Append("{");
        //    sJSON.Append("\"Type\":\"JSET\"");
        //    sJSON.Append(",\"Line\":\""); sJSON.Append(Line.ToString()); sJSON.Append("\"");
        //    sJSON.Append(",\"ParamName\":\""); sJSON.Append(Utils._JsonEscape(m_ParamName)); sJSON.Append("\"");
        //    sJSON.Append(",\"Name\":\""); sJSON.Append(Utils._JsonEscape(m_Source)); sJSON.Append("\"");
        //    sJSON.Append(",\"Value\":\""); sJSON.Append(Utils._JsonEscape(m_JPath)); sJSON.Append("\"");
        //    sJSON.Append(",\"Opt1\":\""); sJSON.Append(Utils._JsonEscape(m_JOP)); sJSON.Append("\"");
        //    sJSON.Append(",\"Value2\":\""); sJSON.Append(Utils._JsonEscape(m_JObj)); sJSON.Append("\"");
        //    sJSON.Append(",\"Value3\":\""); sJSON.Append(Utils._JsonEscape(m_JAttr)); sJSON.Append("\"");
        //    sJSON.Append("}");
        //}

        #endregion Protected members

        #region Public properties

        public string ParameterName
        {
            get { return m_ParamName; }
            set { m_ParamName = value; }
        }
        public string Source
        {
            get { return m_Source; }
            set { m_Source = value; }
        }
        public string JPath
        {
            get { return m_JPath; }
            set { m_JPath = value; }
        }
        public string JObj
        {
            get { return m_JObj; }
            set { m_JObj = value; }
        }
        public string JAttr
        {
            get { return m_JAttr; }
            set { m_JAttr = value; }
        }
        public string JOP
        {
            get { return m_JOP; }
            set { m_JOP = value; }
        }
        public string ErrorParamName
        {
            get { return m_ErrorParamName; }
            set { m_ErrorParamName = value; }
        }
        //public string isParSource
        //{
        //    get { return m_isParSource; }
        //    set { m_isParSource = value; }
        //}


        #endregion Public properties

        public CJSETCmd(): base()
        {
            this.m_enType = CommandType.JSETCommand ;
            m_bIsBlockCommand = false;
        }

        public override string Execute(CParser m_parser)
        {
            //JSON.Net 3.5
            //XmlNote myXmlNode = JsonConvert.DeserializeXmlNode(myJsonString);
            //// or .DeserilizeXmlNode(myJsonString, "root"); // if myJsonString does not have a root
            //string jsonString = JsonConvert.SerializeXmlNode(myXmlNode);

            //// To convert an XML node contained in string xml into a JSON string   
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);
            //string jsonText = JsonConvert.SerializeXmlNode(doc);

            //// To convert JSON text contained in string json into an XML node
            //XmlDocument doc = JsonConvert.DeserializeXmlNode(json);

            //#JDATA.QData.%SEL%.API_Data.Err#



            string l_ParamName, l_Source, l_JPath, l_ErrorParamName;
            l_ParamName = m_parser.ReplaceParameters(m_ParamName);
            l_Source = m_parser.ReplaceParameters(m_Source);
            l_JPath = m_parser.ReplaceParameters(m_JPath);
            l_ErrorParamName = m_parser.ReplaceParameters(m_ErrorParamName);

            //if (m_isParSource == "1") l_Source = (string)m_parser.TemplateParams[l_Source];

            string val = "";
            string sError = "";
            string sErrorVerbose = "";

            try
            {
                JObject o = JObject.Parse(l_Source);
                if (l_ParamName == "")
                    return o.SelectToken(l_JPath).ToString();
                else
                {
                    JToken jt = o.SelectToken(l_JPath);
                    val = jt.ToString();
                    if(jt is JArray )
                        m_parser.TemplateParams[l_ParamName + "__count"] = ((JArray)jt).Count.ToString();

                    //m_parser.ParamDefaults[l_ParamName] = val;
                    m_parser.TemplateParams[l_ParamName] = val;
                    foreach (JProperty jp in jt.Children<JProperty>())
                    {
                        if (jp.Value.Type == JTokenType.Date)
                        {
                            DateTime d = (DateTime)jp.Value;
                            m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = d.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        }
                        else
                            m_parser.TemplateParams[l_ParamName + "_" + jp.Name] = jp.Value.ToString ();
                    }
                    return "";
                }
            }
            catch (Exception ex)
            {
                sError = "Error executing JPAR command";
                sErrorVerbose = ex.Message;
                sErrorVerbose = "Error executing JPAR command in template " + m_parser.TemplateID + " at line " + m_parser.GetLine(m_nStartIndex).ToString() + "; " + newLine + sErrorVerbose;
            }

            if (l_ErrorParamName != "")
            {
                //m_parser.ParamDefaults[l_ErrorParamName] = sError;
                m_parser.TemplateParams[l_ErrorParamName] = sError;
                //m_parser.ParamDefaults[l_ErrorParamName + "Verbose"] = sErrorVerbose;
                m_parser.TemplateParams[l_ErrorParamName + "Verbose"] = sErrorVerbose;
            }

            return "";
        }
 
    }
}
