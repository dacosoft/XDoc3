using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using KubionLogNamespace;


public class XSqlData
{
    SqlConnection m_conn = null;
    SqlTransaction m_trans = null;
    bool bUseLocalTransaction = false;

    public XSqlData(string connectionString)
    {

        try
        {
            m_conn = new SqlConnection(connectionString);
        }
        catch (Exception ex)
        {
            //no trace message because we try to open SQL connection first, then OLEDB
            throw (ex);
        }
    }

    public void Dispose()
    {
        if (m_conn != null && m_conn.State != ConnectionState.Closed)
        {
            try
            {
                m_conn.Close();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw (ex);
            }
        }
    }


    public bool ConnOpen()
    {
        OpenConnection();
        return (m_conn.State == ConnectionState.Open);
    }

    private void OpenConnection()
    {
        if (m_conn.State != ConnectionState.Open)
        {
            try
            {
                m_conn.Open();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw (ex);
            }
        }
    }

    public DataTable GetDataTable(string sqlString)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;
        SqlCommand command = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            command.Transaction = m_trans;

        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(command);
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>GetDataTable(\"" + sqlString + "\")");
        try
        {
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        DataTable dt = null;
        if (ds.Tables.Count > 0)
            dt = ds.Tables[0];
        return dt;
    }


    public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return null;

        bool bLocalTrans = false;
        DataSet ds = new DataSet();

        SqlCommand comm = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            comm.Transaction = m_trans;
        DataTable dt = null;
        string traceString = ">>GetDataTable(\"" + sqlString + "\"";
        try
        {
            foreach (IDataParameter paramValue in arrParams)
            {
                if (paramValue != null)
                {
                    if (paramValue == null)
                        throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                    comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                    string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                    traceString += ", " + val;
                }
            }

            SqlDataAdapter da = new SqlDataAdapter(comm);
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (new Exception(ex.Message + ";" + Environment.NewLine + sqlString));
        }
        finally
        {
            bLocalTrans = false;
        }

        return dt;
    }


    public int ExecuteNonQuery(string sqlString)
    {
        if (!ConnOpen())
            return -1;
        bool bLocalTrans = false;
        SqlCommand comm = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            comm.Transaction = m_trans;
        int rowsAffected = -1;
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>ExecuteNonQuery (\"" + sqlString + "\")");
        try
        {
            rowsAffected = comm.ExecuteNonQuery();
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }
        return rowsAffected;
    }

    public int ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return -1;
        bool bLocalTrans = false;
        SqlCommand comm = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            comm.Transaction = m_trans;
        int rowsAffected = -1;
        string traceString = ">>ExecuteNonQuery (\"" + sqlString + "\"";

        try
        {
            foreach (IDataParameter paramValue in arrParams)
            {
                if (paramValue == null)
                    throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                traceString += ", " + val;
            }
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);
            rowsAffected = comm.ExecuteNonQuery();
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        return rowsAffected;
    }


    public DataSet GetDataSet(string sqlString)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;
        DataSet ds = new DataSet();
        SqlCommand command = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            command.Transaction = m_trans;

        SqlDataAdapter da = new SqlDataAdapter(command);
        if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
            KubionLog.WriteLine(">>GetDataSet(\"" + sqlString + "\")");
        try
        {
            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }
        return ds;
    }


    public DataSet GetDataSet(string sqlString, IDataParameter[] arrParams)
    {
        if (!ConnOpen())
            return null;
        bool bLocalTrans = false;
        DataSet ds = new DataSet();
        SqlCommand comm = new SqlCommand(sqlString, m_conn);
        if (bUseLocalTransaction)
            if (m_trans == null)
            {
                BeginTransaction();
                bLocalTrans = true;
            }
        if (m_trans != null)
            comm.Transaction = m_trans;
        string traceString = ">>GetDataSet(\"" + sqlString + "\"";

        try
        {
            foreach (IDataParameter paramValue in arrParams)
            {
                if (paramValue == null)
                    throw new ApplicationException("Parsing for unnamed parameters returned null parameters");
                comm.Parameters.AddWithValue(paramValue.ParameterName, paramValue.Value);
                string val = paramValue.Value == null ? "<NULL>" : paramValue.Value.ToString();
                traceString += ", " + val;
            }
            SqlDataAdapter da = new SqlDataAdapter(comm);
            traceString += ")";
            if (KubionLog.TraceLevel >= TraceLevelEnum.TemplateQueries)
                KubionLog.WriteLine(traceString);

            da.Fill(ds);
            if (bLocalTrans)
                Commit();
        }
        catch (Exception ex)
        {
            KubionLog.WriteLine(ex);
            if (bLocalTrans)
                Rollback();
            throw (ex);
        }
        finally
        {
            bLocalTrans = false;
        }

        return ds;
    }


    public void BeginTransaction()
    {
        OpenConnection();
        m_trans = null;
        //m_trans = m_conn.BeginTransaction(IsolationLevel.ReadUncommitted);

    }
    public void Commit()
    {
        if(m_trans != null) m_trans.Commit();
        m_trans = null;
    }
    public void Rollback()
    {
        if (m_trans != null) m_trans.Rollback();
        m_trans = null;
    }

}
