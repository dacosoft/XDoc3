using System;
using System.Collections;

namespace XDataSourceModule
{
	
	/// <summary>
	/// Summary description for UpdateSourceCollection.
	/// </summary>
	public class UpdateSourceCollection : CollectionBase  
	{
		
		public SourceUpdate this[ int index ]  
		{
			get  
			{
				return( (SourceUpdate) List[index] );
			}
			set  
			{
				List[index] = value;
			}
		}

		public SourceUpdate GetSourceUpdateByKey(string key)
		{
			for(int index = 0; index < this.Count; index++)
			{
				SourceUpdate upd = this[index];
				if(key.ToUpper() == upd.Key.ToUpper())
					return upd;
			}

			return null;
		}

		public int Add( SourceUpdate value )  
		{
			return( List.Add( value ) );
		}

		public int IndexOf( SourceUpdate value )  
		{
			return( List.IndexOf( value ) );
		}

		public void Insert( int index, SourceUpdate value )  
		{
			List.Insert( index, value );
		}

		public void Remove( SourceUpdate value )  
		{
			List.Remove( value );
		}

		public bool Contains( SourceUpdate value )  
		{
			// If value is not of type SourceUpdate, this will return false.
			return( List.Contains( value ) );
		}

		protected override void OnInsert( int index, Object value )  
		{
			try
			{
				SourceUpdate temp = (SourceUpdate) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type SourceUpdate." );
			}
		}

		protected override void OnRemove( int index, Object value )  
		{
			try
			{
				SourceUpdate temp = (SourceUpdate) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type SourceUpdate." );
			}
		}

		protected override void OnSet( int index, Object oldValue, Object newValue )  
		{
			try
			{
				SourceUpdate temp = (SourceUpdate) newValue;
			}
			catch
			{
				throw new ArgumentException( "value must be of type SourceUpdate." );
			}
		}

		protected override void OnValidate( Object value )  
		{
			try
			{
				SourceUpdate temp = (SourceUpdate) value;
			}
			catch
			{
				throw new ArgumentException( "value must be of type SourceUpdate." );
			}
		}

	}
}
