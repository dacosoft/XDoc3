using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;
using KubionDataNamespace;

namespace XDataSourceModule
{
    public class XProviderData:IDisposable 
    {
        ClientData m_clientData=null;
        bool m_inTransaction = false;
        public ProviderTypes  ProviderType()
        {
            if (m_clientData==null) return ProviderTypes.none  ;
            return m_clientData.ProviderType();
        }
        public bool InTransaction
        {
            get { return m_inTransaction; }
            set { m_inTransaction = value; }
        }
        public XProviderData(string connectionString)
        {
            try
            {
                m_clientData = new ClientData(connectionString);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void Dispose()
        {
            if (m_clientData != null)
                m_clientData.Dispose();
        }
        public bool ConnOpen()
        {
            if (m_clientData != null)
                return m_clientData.ConnOpen();
            return false;
        }

        public string GetResponse(string sqlString)
        {
            if (m_clientData != null)
                return m_clientData.GetResponse (sqlString);
            return null;
        }
        public DataTable GetDataTable(string sqlString)
        {
            if (m_clientData != null)
                return m_clientData.DTExecuteQuery(sqlString);
            return null;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            if (m_clientData != null)
                return m_clientData.DTExecuteQuery(sqlString, ref m_Response, ref m_OuterXml, ref m_InnerXml);
            return null;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            if (m_clientData != null)
                return m_clientData.DTExecuteQuery(sqlString, arrParams);
            return null;
        }
        //public int ExecuteNonQuery(string sqlString)
        //{
        //    if (m_clientData != null)
        //        return m_clientData.ExecuteNonQuery(sqlString);
        //    return -1;
        //}
        public string ExecuteNonQuery(string sqlString)
        {
            if (m_clientData != null)
                return m_clientData.ExecuteNonQuery(sqlString);
            return "Error: no data object";
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            if (m_clientData != null)
                return m_clientData.ExecuteNonQuery(sqlString, ref m_Response, ref m_OuterXml, ref m_InnerXml);
            return "Error: no data object";
        }
        //public int ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        //{
        //    if (m_clientData != null)
        //        return m_clientData.ExecuteNonQuery(sqlString, arrParams);
        //    return -1;
        //}
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            if (m_clientData != null)
                return m_clientData.ExecuteNonQuery(sqlString, arrParams);
            return "Error: no data object";
        }

        public void BeginTransaction()
        {
            if (m_clientData != null)
                m_clientData.BeginTransaction();

            m_inTransaction = true;
        }
        public void Commit()
        {
            if (m_clientData != null)
                m_clientData.CommitTransaction ();

            m_inTransaction = false;
        }
        public void Rollback()
        {
            if (m_clientData != null)
                m_clientData.RollbackTransaction();

            m_inTransaction = false;
        }


    }
    //public class XProviderData_old
    //{
    //    XSqlData m_connSql = null;
    //    XOleDbData m_connOleDb = null;
    //    XOdbcData m_connOdbc = null;

    //    bool m_inTransaction = false;

    //    public bool InTransaction
    //    {
    //        get { return m_inTransaction; }
    //        set { m_inTransaction = value; }
    //    }

    //    public XProviderData_old(string connectionString)
    //    {
    //        try
    //        {
    //            m_connSql = new XSqlData(connectionString);
    //        }
    //        catch (Exception)
    //        {
    //            try
    //            {
    //                m_connOleDb = new XOleDbData(connectionString);
    //            }
    //            catch (Exception)
    //            {
    //                try
    //                {
    //                    m_connOdbc = new XOdbcData (connectionString);
    //                }
    //                catch (Exception ex)
    //                {
    //                    throw;
    //                }
    //            }
    //        }
    //    }

    //    public void Dispose()
    //    {
    //        if (m_connSql != null)
    //            m_connSql.Dispose();
    //        if (m_connOleDb != null)
    //            m_connOleDb.Dispose();
    //        if (m_connOdbc != null)
    //            m_connOdbc.Dispose();
    //    }


    //    public bool ConnOpen()
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.ConnOpen();
    //        if (m_connOleDb != null)
    //            return m_connOleDb.ConnOpen();
    //        if (m_connOdbc != null)
    //            return m_connOdbc.ConnOpen();
    //        return false;
    //    }

    //    public DataTable GetDataTable(string sqlString)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.GetDataTable(sqlString);
    //        if (m_connOleDb != null)
    //            return m_connOleDb.GetDataTable(sqlString);
    //        if (m_connOdbc != null)
    //            return m_connOdbc.GetDataTable(sqlString);
    //        return null;
    //    }
    //    public DataSet GetDataSet(string sqlString)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.GetDataSet(sqlString);
    //        return null;
    //    }
    //    public int ExecuteNonQuery(string sqlString)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.ExecuteNonQuery(sqlString);
    //        if (m_connOleDb != null)
    //            return m_connOleDb.ExecuteNonQuery(sqlString);
    //        if (m_connOdbc != null)
    //            return m_connOdbc.ExecuteNonQuery(sqlString);
    //        return -1;
    //    }

    //    public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.GetDataTable(sqlString, arrParams);
    //        if (m_connOleDb != null)
    //            return m_connOleDb.GetDataTable(sqlString, arrParams);
    //        if (m_connOdbc != null)
    //            return m_connOdbc.GetDataTable(sqlString, arrParams);
    //        return null;
    //    }
    //    public DataSet GetDataSet(string sqlString, IDataParameter[] arrParams)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.GetDataSet(sqlString, arrParams);
    //        return null;
    //    }
    //    public int ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
    //    {
    //        if (m_connSql != null)
    //            return m_connSql.ExecuteNonQuery(sqlString, arrParams);
    //        if (m_connOdbc != null)
    //            return m_connOdbc.ExecuteNonQuery(sqlString, arrParams);
    //        if (m_connOleDb != null)
    //            return m_connOleDb.ExecuteNonQuery(sqlString, arrParams);
    //        return -1;
    //    }

    //    public void BeginTransaction()
    //    {
    //        if (m_connSql != null)
    //            m_connSql.BeginTransaction();
    //        if (m_connOleDb != null)
    //            m_connOleDb.BeginTransaction();
    //        if (m_connOdbc != null)
    //            m_connOdbc.BeginTransaction();

    //        m_inTransaction = true;
    //    }
    //    public void Commit()
    //    {
    //        if (m_connSql != null)
    //            m_connSql.Commit();
    //        if (m_connOleDb != null)
    //            m_connOleDb.Commit();
    //        if (m_connOdbc != null)
    //            m_connOdbc.Commit();

    //        m_inTransaction = false;
    //    }
    //    public void Rollback()
    //    {
    //        if (m_connSql != null)
    //            m_connSql.Rollback();
    //        if (m_connOleDb != null)
    //            m_connOleDb.Rollback();
    //        if (m_connOdbc != null)
    //            m_connOdbc.Rollback();

    //        m_inTransaction = false;
    //    }
    //}
}