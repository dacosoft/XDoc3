using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XDataSourceModule;
using System.Data.SqlClient;
using KubionLogNamespace;

namespace XDocuments
{
    internal class Connection : IXConnection
    {
        #region Private members

        private int 
            m_ID = -1;
        private string 
            m_connectionString = "",
            m_name = "";

        private XProviderData m_dataProvider = null;

        #endregion Private members


        #region Public properties

        public int ID
        {
            get { return m_ID; }
        }

        public string ConnectionString
        {
            get { return m_connectionString; }
            set 
            {
                m_connectionString = value;
                
            }
        }

        public string Name
        {
            get { return m_name; }
            set 
            {
                m_name = value; 
            }
        }

        #endregion Public properties


        #region Constructors

        internal Connection(XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
        }

        internal Connection(int id, XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
            Load(id);
        }

        internal Connection(string name, XProviderData dataProvider)
        {
            m_dataProvider = dataProvider;
            Load(name);
        }

        #endregion Constructors


        #region Internal methods

        internal void Load(DataRow row)
        {
            m_ID = Connectors.GetInt32Result(row[S_CONNECTIONS.CONNID]);
            m_name = Connectors.GetStringResult(row[S_CONNECTIONS.CONNNAME]);
            m_connectionString = Connectors.GetStringResult(row[S_CONNECTIONS.CONNSTRING]);
        }

        #endregion Internal methods


        #region Public methods

        public void Load(int id)
        {
            //string sQueryStatement;
            try
            {
                //sQueryStatement = SQL.LoadConnectionByID;
                //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNID, id.ToString());
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadConnectionByID, new IDataParameter[] { new SqlParameter("@" + S_CONNECTIONS.CONNID, id) });
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Could not find connection");
                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load connection with ID = " + id.ToString() + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        public void Load(string name)
        {
            try
            {
                //string sQueryStatement= SQL.LoadConnectionByName;
                //sQueryStatement= sQueryStatement.Replace("@" + S_CONNECTIONS.CONNNAME, "'" + name + "'");
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadConnectionByName, new IDataParameter[] { new SqlParameter("@" + S_CONNECTIONS.CONNNAME, name) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception(string.Format("Could not find connection"));
                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not load connection '" + name + "'" + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        public void Save()
        {
            //string sQueryStatement;
            try
            {
                IDataParameter[] pars = new IDataParameter[]
                {
                    new SqlParameter("@" + S_CONNECTIONS.CONNID, m_ID),
                    new SqlParameter("@" + S_CONNECTIONS.CONNNAME, m_name),
                    new SqlParameter("@" + S_CONNECTIONS.CONNSTRING, m_connectionString)
                };
                if (m_ID == -1)
                {
                    //sQueryStatement = SQL.CreateNewConnection;
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNID, m_ID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNNAME, "'" + m_name + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNSTRING, "'" + m_connectionString + "'");
                    DataTable dtResult = m_dataProvider.GetDataTable(SQL.CreateNewConnection, pars);
                    //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                    if (dtResult == null || dtResult.Rows.Count == 0)
                        throw new Exception("Could not save new connection");
                    m_ID = Connectors.GetInt32Result(dtResult.Rows[0][S_CONNECTIONS.CONNID]);
                }
                else
                {
                    //sQueryStatement = SQL.UpdateConnection;
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNID, m_ID.ToString());
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNNAME, "'" + m_name + "'");
                    //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNSTRING, "'" + m_connectionString + "'");
                    m_dataProvider.ExecuteNonQuery(SQL.UpdateConnection, pars);
                    //m_dataProvider.ExecuteNonQuery(sQueryStatement);
                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = "Could not save connection '" + m_name + "'" + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }

        }

        public static void Delete(int id, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                dataProvider.BeginTransaction();
                //sQueryStatement = SQL.DeleteConnection;
                //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNID + "1", id.ToString());
                //sQueryStatement = sQueryStatement.Replace("@" + S_CONNECTIONS.CONNID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteConnection, new IDataParameter[] { new SqlParameter("@" + S_CONNECTIONS.CONNID + "1", id), new SqlParameter("@" + S_CONNECTIONS.CONNID, id) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
                dataProvider.Commit();
            }
            catch (Exception ex)
            {
                if (dataProvider != null && dataProvider.InTransaction)
                    dataProvider.Rollback();

                string message = "Could not delete connection with ID = " + id.ToString() + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        #endregion Public methods
    }
}
