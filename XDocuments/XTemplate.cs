using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Diagnostics;
using XHTMLMerge;
using XDataSourceModule;
using System.Data.SqlClient;
using KubionLogNamespace;
using System.Globalization;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace XDocuments
{
    internal class XTemplate : IXTemplate
    {

        #region Private members

        private int m_id = -1;

        private string
            m_content = "",
            m_contentexpanded = "",
            m_name = "",
            m_redirect = "",
            m_description = "",
            m_docType = XDocManager.HTMLDocType,

            m_uniqueID = "";

        private bool
            m_toSave = true,
            m_unattended = false,
            m_isData = false,
            m_hasRights = false,
            m_contentLoaded = false,
            m_contentAltered;

        private Collection<int> m_roles = new Collection<int>();
        private Hashtable m_parameters = XDocManager.CaseInsensitiveHashtable();
        private XProviderData m_dataProvider = null;

        private XDocManager m_manager = null;

        #endregion Private members


        #region Constructors

        public XTemplate(XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
        }

        public XTemplate(int id, XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
            Load(id);
        }

        public XTemplate(string name, XDocManager manager)
        {
            m_manager = manager;
            m_dataProvider = manager.CurrentDataProvider;
            Load(name);
        }

        #endregion Constructors


        #region Public properties

        /// <summary>
        /// The ID of the template
        /// </summary>
        public int ID
        {
            get { return m_id; }
        }

        /// <summary>
        /// The template content
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public string Content
        {
            get
            {
                LoadContent();
                return m_content;
            }
            set
            {
                m_content = value;
                m_contentAltered = true;
            }
        }


        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public string ContentExpanded
        {
            get
            {
                string m_contentexpanded = LoadContentExpanded(m_id);
                //if (m_contentexpanded == "")
                //{
                //    this.Save();
                //    m_contentexpanded = LoadContentExpanded(m_id);
                //}
                return m_contentexpanded;
            }
        }

        /// <summary>
        /// The template name
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        /// <summary>
        /// The template description
        /// </summary>
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        /// <summary>
        /// Default redirect string to be parsed when the generated document will be saved
        /// </summary>
        public string Redirect
        {
            get { return m_redirect; }
            set { m_redirect = value; }
        }

        /// <summary>
        /// The file extension of the generated document
        /// </summary>
        public string DocType
        {
            get { return m_docType; }
            set { m_docType = value; }
        }

        /// <summary>
        /// Indicates if this is a data template.
        /// A data template's document is shown in web applications as it is, without embedding it in containers
        /// </summary>
        public bool IsData
        {
            get { return m_isData; }
            set { m_isData = value; }
        }

        /// <summary>
        /// Indicates if the document resulted is directly saved, without being shown to the user
        /// </summary>
        public bool Unattended
        {
            get { return m_unattended; }
            set { m_unattended = value; }
        }

        /// <summary>
        /// Indicates if the resulting document can be saved in the database
        /// </summary>
        public bool ToSave
        {
            get { return m_toSave; }
            set { m_toSave = value; }
        }

        /// <summary>
        /// Indicates if the current user has the right to request this template
        /// </summary>
        public bool HasRights
        {
            get { return m_hasRights; }
            set { m_hasRights = value; }
        }

        /// <summary>
        /// A unique identifier used for HTML element IDs and javascript function names in order to isolate this instance of the document in a web page
        /// </summary>
        public string UniqueID
        {
            get { return m_uniqueID; }
        }

        /// <summary>
        /// The roles granted to request this template
        /// </summary>
        public Collection<int> Roles
        {
            get { return m_roles; }
            set { m_roles = value; }
        }

        public XDocCache DocCache
        {
            get { if (m_manager != null) return m_manager.DocCache; else return null; }
            set { if (m_manager != null) m_manager.DocCache = value; }
        }
        public XDocCacheTemplates DocCacheTemplates
        {
            get { if (m_manager != null) return m_manager.DocCacheTemplates; else return null; }
            set { if (m_manager != null) m_manager.DocCacheTemplates = value; }
        }


        #endregion Public properties


        #region Public methods

        /// <summary>
        /// Loads a template by its name
        /// </summary>
        /// <param name="name">The name of the template</param>
        public void Load(string name)
        {
            //string sQueryStatement;
            try
            {
                //System.Diagnostics.Debug.WriteLine("LD1:" + name + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
                //sQueryStatement = SQL.LoadTemplateByName;
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATENAME, "'" + name + "'");
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateByName, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, name) });
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Template not found");
                //System.Diagnostics.Debug.WriteLine("LD2:" + name + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
                Load(dtResult.Rows[0]);
                //System.Diagnostics.Debug.WriteLine("LD3:" + name + " : " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff"));
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = FormatError(ex.Message, "Could not load template with name \"" + name + "\"");
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Loads a template by its ID
        /// </summary>
        /// <param name="id">The ID of the template</param>
        public void Load(int id)
        {
            //string sQueryStatement;
            try
            {
                //sQueryStatement = SQL.LoadTemplateByID;
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATEID, id.ToString());
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateByID, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, id) });
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception("Template not found");

                Load(dtResult.Rows[0]);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = FormatError(ex.Message, "Could not load template with ID=" + id.ToString());
                throw new Exception(message);
            }
        }

        /// <summary>
        /// Gets an array of IDName values representing the queries for the template
        /// </summary>
        public IDName[] GetQueriesList()
        {
            if (m_id != -1)
                return m_manager.GetQueriesList(m_id);

            return new IDName[] { };
        }

        /// <summary>
        /// Saves the template
        /// </summary>
        public void Save()
        {
            try
            {
                LoadContent();
                m_dataProvider.BeginTransaction();

                if (m_docType != null && m_docType != "" && !m_docType.StartsWith("."))
                    m_docType = "." + m_docType;
                string rights = "";
                foreach (int roleID in m_roles)
                    rights += roleID.ToString() + ";";
                if (rights.EndsWith(";"))
                    rights = rights.Substring(0, rights.Length - 1);
                if (m_contentexpanded == "") m_contentexpanded = ExpandContent(m_content);
                if (m_contentexpanded == m_content) m_contentexpanded = "{no_macros}";
                if (m_contentexpanded == "error") m_contentexpanded = "";

                IDataParameter[] pars = new IDataParameter[]
                    {
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, m_name),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATEDESCRIPTION, m_description),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENT, m_content),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATETOREDIRECT, m_redirect),
                        new SqlParameter("@" + S_TEMPLATES.DATATEMPLATE, m_isData),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATETOSAVE, m_toSave),
                        new SqlParameter("@" + S_TEMPLATES.UNATTENDED, m_unattended),
                        new SqlParameter("@" + S_TEMPLATES.RIGHTS, rights),
                        new SqlParameter("@" + S_TEMPLATES.DOCTYPE, m_docType),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENTEXPANDED, m_contentexpanded),
                        new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
                    };

                if (m_id == -1) // new template
                {
                    DataTable dtResult = m_dataProvider.GetDataTable(SQL.SaveNewTemplateExRights , pars);
                    if (dtResult == null || dtResult.Rows.Count == 0)
                        throw new Exception("Could not save new template");
                    m_id = Connectors.GetInt32Result(dtResult.Rows[0][S_TEMPLATES.TEMPLATEID]);
                }
                else // existing template
                {
                    m_dataProvider.ExecuteNonQuery(SQL.UpdateTemplateExRights, pars);
                }

                m_dataProvider.Commit();
                m_contentLoaded = false;
                m_contentAltered = false;
            }
            catch (Exception ex)
            {
                m_dataProvider.Rollback();
                KubionLog.WriteLine(ex);
                string message = FormatError(ex.Message, "Template cannot be saved");
                throw new Exception(message);
            }
        }
        //public void Save()
        //{
        //    try
        //    {
        //        LoadContent();
        //        m_dataProvider.BeginTransaction();

        //        if (m_docType != null && m_docType != "" && !m_docType.StartsWith("."))
        //            m_docType = "." + m_docType;

        //        IDataParameter[] pars = new IDataParameter[]
        //            {
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, m_name),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEDESCRIPTION, m_description),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENT, m_content),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATETOREDIRECT, m_redirect),
        //                new SqlParameter("@" + S_TEMPLATES.DATATEMPLATE, m_isData),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATETOSAVE, m_toSave),
        //                new SqlParameter("@" + S_TEMPLATES.UNATTENDED, m_unattended),
        //                //new SqlParameter("@" + S_TEMPLATES.DOCTYPE, m_docType),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
        //            };

        //        if (m_id == -1) // new template
        //        {
        //            DataTable dtResult = m_dataProvider.GetDataTable(SQL.SaveNewTemplate, pars);
        //            if (dtResult == null || dtResult.Rows.Count == 0)
        //                throw new Exception("Could not save new template");
        //            m_id = Connectors.GetInt32Result(dtResult.Rows[0][S_TEMPLATES.TEMPLATEID]);
        //        }
        //        else // existing template
        //        {
        //            m_dataProvider.ExecuteNonQuery(SQL.UpdateTemplate, pars);
        //        }

        //        try
        //        {
        //            if (m_contentexpanded == "") m_contentexpanded = ExpandContent(m_content);
        //            if (m_contentexpanded == m_content) m_contentexpanded = "{no_macros}";
        //            if (m_contentexpanded == "error") m_contentexpanded = "";

        //            IDataParameter[] parsEx = new IDataParameter[]
        //            {
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENTEXPANDED, m_contentexpanded),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
        //            };
        //            m_dataProvider.ExecuteNonQuery(SQL.UpdateTemplateContentEx, parsEx);

        //        }
        //        catch (Exception) { }

        //        try
        //        {
        //            //insert template rights
        //            string rights = "";
        //            foreach (int roleID in m_roles)
        //                rights += roleID.ToString() + ";";
        //            if (rights.EndsWith(";"))
        //                rights = rights.Substring(0, rights.Length - 1);

        //            DataTable dtAllColumns = m_dataProvider.GetDataTable(SQL.GetAllTemplateColumns);
        //            if (dtAllColumns != null && dtAllColumns.Columns.Contains(S_TEMPLATES.RIGHTS))
        //            {
        //                IDataParameter[] rightsParams = new IDataParameter[]
        //            {
        //                new SqlParameter("@" + S_TEMPLATES.RIGHTS, rights),
        //                new SqlParameter("@" + S_TEMPLATES.DOCTYPE, m_docType),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
        //            };
        //                m_dataProvider.ExecuteNonQuery(SQL.InsertRightsDocType, rightsParams);
        //            }
        //        }
        //        catch (Exception) { };

        //        m_dataProvider.Commit();
        //        m_contentLoaded = false;
        //        m_contentAltered = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        m_dataProvider.Rollback();
        //        KubionLog.WriteLine(ex);
        //        string message = FormatError(ex.Message, "Template cannot be saved");
        //        throw new Exception(message);
        //    }
        //}



        //public void Save1()
        //{
        //    try
        //    {
        //        LoadContent();
        //        m_contentexpanded = ExpandContent(m_content);
        //        if (m_contentexpanded == m_content) m_contentexpanded = "{no_macros}";
        //        m_dataProvider.BeginTransaction();

        //        if (m_docType != null && m_docType != "" && !m_docType.StartsWith("."))
        //            m_docType = "." + m_docType;

        //        IDataParameter[] pars = new IDataParameter[]
        //            {
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, m_name),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEDESCRIPTION, m_description),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENT, m_content),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATECONTENTEXPANDED, m_contentexpanded),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATETOREDIRECT, m_redirect),
        //                new SqlParameter("@" + S_TEMPLATES.DATATEMPLATE, m_isData),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATETOSAVE, m_toSave),
        //                new SqlParameter("@" + S_TEMPLATES.UNATTENDED, m_unattended),
        //                //new SqlParameter("@" + S_TEMPLATES.DOCTYPE, m_docType),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
        //            };

        //        if (m_id == -1) // new template
        //        {
        //            DataTable dtResult = m_dataProvider.GetDataTable(SQL.SaveNewTemplateEx , pars);
        //            if (dtResult == null || dtResult.Rows.Count == 0)
        //                throw new Exception("Could not save new template");
        //            m_id = Connectors.GetInt32Result(dtResult.Rows[0][S_TEMPLATES.TEMPLATEID]);
        //        }
        //        else // existing template
        //        {
        //            m_dataProvider.ExecuteNonQuery(SQL.UpdateTemplateEx , pars);
        //        }

        //        try
        //        {
        //            //insert template rights
        //            string rights = "";
        //            foreach (int roleID in m_roles)
        //                rights += roleID.ToString() + ";";
        //            if (rights.EndsWith(";"))
        //                rights = rights.Substring(0, rights.Length - 1);

        //            DataTable dtAllColumns = m_dataProvider.GetDataTable(SQL.GetAllTemplateColumns);
        //            if (dtAllColumns != null && dtAllColumns.Columns.Contains(S_TEMPLATES.RIGHTS))
        //            {
        //                IDataParameter[] rightsParams = new IDataParameter[]
        //            {
        //                new SqlParameter("@" + S_TEMPLATES.RIGHTS, rights),
        //                new SqlParameter("@" + S_TEMPLATES.DOCTYPE, m_docType),
        //                new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id)
        //            };
        //                m_dataProvider.ExecuteNonQuery(SQL.InsertRightsDocType, rightsParams);
        //            }
        //        }
        //        catch (Exception) { };

        //        m_dataProvider.Commit();
        //        m_contentLoaded = false;
        //        m_contentAltered = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        m_dataProvider.Rollback();
        //        KubionLog.WriteLine(ex);
        //        string message = FormatError(ex.Message, "Template cannot be saved");
        //        throw new Exception(message);
        //    }
        //}

        /// <summary>
        /// Interprets all commands in the template and returns the resulted document
        /// </summary>
        /// <returns>The document resulted</returns>
        public IXDocument ProcessTemplate()
        {
            if (m_id == -1)
                return null;

            string processedContent = "";
            IXDocument doc = m_manager.NewDocumentBasedOnTemplate(this);

            m_manager.LogTemplateCall(m_parameters, 0);

            // Add the UserID and UserName default parameters
            m_parameters[XDocManager.UserIDKey] = m_manager.UserID.ToString(CultureInfo.InvariantCulture);
            m_parameters[XDocManager.UserLoginKey] = m_manager.UserLogin;
            m_parameters[XDocManager.UserNameKey] = m_manager.UserName;
            m_parameters[XDocManager.ServerKey] = m_manager.ServerName;
            m_parameters[XDocManager.DatabaseKey] = m_manager.DatabaseName;
            m_parameters["INCLUDE"] = 0;

            try
            {
                m_contentexpanded = LoadContentExpanded(m_id);
                //if (m_contentexpanded == "")
                //{
                //    m_contentexpanded = LoadContentExpanded(m_id);
                //    this.Save();
                //}
                CParser parser = new CParser((IXManagerImportExport)m_manager, m_id, m_dataProvider, m_contentexpanded, m_parameters, m_manager.AttachmentsVirtualFolder, m_manager.AttachmentsFolder);
                if (m_manager != null) if (m_manager.DocCache != null) parser.DocCache = m_manager.DocCache;
                if (m_manager != null) if (m_manager.DocCacheTemplates != null) parser.DocCacheTemplates = m_manager.DocCacheTemplates;
                parser.GlobalParams = m_parameters;
                parser.RequestTemplateText += new CParser.RequestTemplateText_Handler(parser_RequestTemplateText);
                parser.RequestTemplateID += new CParser.RequestTemplateID_Handler(parser_RequestTemplateID);
                parser.RequestHierarchy += new SourceResult.RequestHierarcyEventHandler(parser_RequestHierarchy);
                parser.LogInclude += new CParser.LogInclude_Handler(parser_LogInclude);
                //System.Diagnostics .Debug .WriteLine ("*** Start parse  " + m_name  + " Time: " + System.DateTime.Now.ToString ()   ); 
                try
                {
                    //parser.DocCacheTemplates.AddTemplateTree(m_id, null); 
                    processedContent = parser.Parse();
                }
                catch (Exception ex)
                {
                    DocCache  = parser.DocCache;
                    DocCacheTemplates = parser.DocCacheTemplates;
                    parser.Evaluator.CommitSV();
                    throw ;
                }
                //System.Diagnostics.Debug.WriteLine("*** End parse  " + m_name + " Time: " + System.DateTime.Now.ToString()); 
                DocCache  = parser.DocCache;
                DocCacheTemplates = parser.DocCacheTemplates;
                parser.Evaluator.CommitSV();

                string providedUniqueID = null;
                if (m_parameters[XDocManager.UniqueParameter] != null)
                    providedUniqueID = m_parameters[XDocManager.UniqueParameter].ToString();

                processedContent = XDocManager.ReplaceUniqueID(processedContent, providedUniqueID, out m_uniqueID);
                processedContent = m_manager.ReplaceAttPath(processedContent);

                if (m_unattended)
                {
                    //process unattended template (create document and process redirect)
                    doc.Content = processedContent;
                    doc.Save(true, m_parameters);
                }
                else
                {
                    //NO onchange event
                    //if (!m_isData)
                    //    processedContent = InsertOnChange(processedContent, true);
                    if (m_parameters["noblocks"] != null)
                        doc.Content = processedContent;
                    else
                    {
                        string retVal = "";
                        ContentPart[] parts = XDocManager.SplitDocument(processedContent, false);
                        foreach (ContentPart cp in parts)
                            retVal += cp.Content;
                        doc.Content = retVal;
                        //doc.Content = processedContent;
                    }
                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                string message = FormatError(ex.Message, "Template cannot be processed");
                throw new Exception(message);
            }

            return doc;
        }



        /// <summary>
        /// Interprets all commands in the template and returns the resulted document
        /// </summary>
        /// <param name="parameters">The provided template parameters and their values</param>
        /// <returns>The document resulted</returns>
        public IXDocument ProcessTemplate(Hashtable parameters)
        {
            m_parameters = XDocManager.CaseInsensitiveHashtable(parameters);
            return ProcessTemplate();
        }

        /// <summary>
        /// Interprets all commands in the template and returns the resulted document
        /// </summary>
        /// <param name="parameters">The provided template parameters and their values as a URL-like formed string</param>
        /// <returns>The document resulted</returns>
        public IXDocument ProcessTemplate(string queryString)
        {
            m_parameters = XDocManager.HashtableFromQueryString(queryString);
            return ProcessTemplate();
        }

        /// <summary>
        /// Deletes a template
        /// </summary>
        /// <param name="id">The ID of the template to be deleted</param>
        public static void Delete(string name, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                dataProvider.BeginTransaction();
                Query.DeleteForTemplate(name, dataProvider);
                //sQueryStatement = SQL.DeleteTemplate;
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATEID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteTemplateN, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME , name) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
                dataProvider.Commit();
            }
            catch (Exception ex)
            {
                if (dataProvider != null)
                    dataProvider.Rollback();
                string message = "Could not delete template " + name + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }
        public static void Delete(int id, XProviderData dataProvider)
        {
            //string sQueryStatement;
            try
            {
                dataProvider.BeginTransaction();
                Query.DeleteForTemplate(id, dataProvider);
                //sQueryStatement = SQL.DeleteTemplate;
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATEID, id.ToString());
                dataProvider.ExecuteNonQuery(SQL.DeleteTemplate, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, id) });
                //dataProvider.ExecuteNonQuery(sQueryStatement);
                dataProvider.Commit();
            }
            catch (Exception ex)
            {
                if (dataProvider != null)
                    dataProvider.Rollback();
                string message = "Could not delete template ID = " + id.ToString() + Environment.NewLine + ex.Message;
                throw new Exception(message);
            }
        }

        #endregion Public methods


        #region Private methods

        private void Load(DataRow row)
        {
            m_id = Connectors.GetInt32Result(row[S_TEMPLATES.TEMPLATEID]);
            m_name = Connectors.GetStringResult(row[S_TEMPLATES.TEMPLATENAME]);
            m_redirect = Connectors.GetStringResult(row[S_TEMPLATES.TEMPLATETOREDIRECT]);
            m_isData = Connectors.GetBoolResult(row[S_TEMPLATES.DATATEMPLATE]);
            m_toSave = Connectors.GetBoolResult(row[S_TEMPLATES.TEMPLATETOSAVE], true);
            m_unattended = Connectors.GetBoolResult(row[S_TEMPLATES.UNATTENDED]);
            m_description = Connectors.GetStringResult(row[S_TEMPLATES.TEMPLATEDESCRIPTION]);
            m_docType = XDocManager.HTMLDocType;
            try
            {
                m_docType = Connectors.GetStringResult(row[S_TEMPLATES.DOCTYPE], XDocManager.HTMLDocType);
            }
            catch (Exception) { }
            if (m_docType == "")
                m_docType = XDocManager.HTMLDocType;

            m_roles.Clear();
            if (row.Table.Columns.Contains(S_TEMPLATES.RIGHTS))
            {
                string rights = Connectors.GetStringResult(row[S_TEMPLATES.RIGHTS], "");
                foreach (string part in rights.Split(';'))
                {
                    int roleID;
                    if (!int.TryParse(part, out roleID))
                        continue;
                    m_roles.Add(roleID);
                }
            }

            m_hasRights = GetTemplateRight();

            if (!m_hasRights)
                throw new System.Security.SecurityException("The user does not have the right to request the template ID=" + m_id.ToString());

        }

        private void LoadContent()
        {
            //string sQueryStatement;
            if (m_contentLoaded || m_id == -1 || m_contentAltered)
                return;
            try
            {
                //sQueryStatement = SQL.LoadTemplateContent;
                //sQueryStatement = sQueryStatement.Replace("@" + S_TEMPLATES.TEMPLATEID, m_id.ToString());
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContent, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, m_id) });
                //DataTable dtResult = m_dataProvider.GetDataTable(sQueryStatement);
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_content = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
                m_contentLoaded = true;
            }
            catch (Exception ex)
            {
                string message = "Could not load content for template ID = " + m_id + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
        }
        private string LoadContentExpanded(int iTemplateID)
        {
            //string 
            m_contentexpanded = "invalid";
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContentEx, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, iTemplateID) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENTEXPANDED]);
                if (m_contentexpanded == "{no_macros}") m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
            }
            catch (Exception ex)
            {
                //string message = "Could not load content for template ID = " + iTemplateID.ToString () + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                //throw new Exception(message);
            }
            if (m_contentexpanded == "invalid")
            {
                m_content = LoadContent(m_id);
                m_contentexpanded = ExpandContent(m_content);
                if (m_contentexpanded == "error") m_contentexpanded = m_content;
            }
            else if (m_contentexpanded == "")
            {
                m_content = LoadContent(m_id);
                m_contentexpanded = ExpandContent(m_content);
                if (m_contentexpanded != "error") this.Save();
                if (m_contentexpanded == "{no_macros}") 
                    m_contentexpanded = m_content;
                if (m_contentexpanded == "error")
                    m_contentexpanded = m_content;
            }
            return m_contentexpanded;
        }
        private string LoadContentExpanded(string sTemplateName)
        {
            //string 
            m_contentexpanded = "invalid";
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContentExName, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, sTemplateName) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENTEXPANDED]);
                if (m_contentexpanded == "{no_macros}") m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
            }
            catch (Exception ex)
            {
                //string message = "Could not load content for template name = " + sTemplateName + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                //throw new Exception(message);
            }
            if (m_contentexpanded == "invalid")
            {
                m_content = LoadContent(m_id);
                m_contentexpanded = ExpandContent(m_content);
                if (m_contentexpanded == "error") m_contentexpanded = m_content;
            }
            else if (m_contentexpanded == "")
            {
                m_content = LoadContent(m_id);
                m_contentexpanded = ExpandContent(m_content);
                if (m_contentexpanded != "error") this.Save();
                if (m_contentexpanded == "{no_macros}")
                    m_contentexpanded = m_content;
                if (m_contentexpanded == "error")
                    m_contentexpanded = m_content;
            }
            return m_contentexpanded;
        }
        private string LoadContent(int iTemplateID)
        {
            string m_content = "";
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContent, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATEID, iTemplateID) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_content = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
            }
            catch (Exception ex)
            {
                string message = "Could not load content for template ID = " + m_id + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
            return m_content;

        }
        private string LoadContent(string sTemplateName)
        {
            string m_contentexpanded = "";
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContentName, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, sTemplateName) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
            }
            catch (Exception ex)
            {
                string message = "Could not load content for template name = " + sTemplateName + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
            return m_contentexpanded;
        }
        private static string LoadContent(XProviderData  m_dataProvider, string sTemplateName)
        {
            string m_contentexpanded = "";
            try
            {
                DataTable dtResult = m_dataProvider.GetDataTable(SQL.LoadTemplateContentName, new IDataParameter[] { new SqlParameter("@" + S_TEMPLATES.TEMPLATENAME, sTemplateName) });
                if (dtResult == null || dtResult.Rows.Count == 0)
                    throw new Exception();
                m_contentexpanded = Connectors.GetStringResult(dtResult.Rows[0][S_TEMPLATES.TEMPLATECONTENT]);
            }
            catch (Exception ex)
            {
                string message = "Could not load content for template name = " + sTemplateName + (ex.Message == "" ? "" : Environment.NewLine + ex.Message);
                throw new Exception(message);
            }
            return m_contentexpanded;
        }

        private string FormatError(string error, string message)
        {
            if (m_id != -1)
                message = "(TemplateID=" + m_id.ToString() + ") " + message;
            return message + Environment.NewLine + error + "  (" + DateTime.Now.ToString("HH:mm:ss.fff") + ")";
        }

        /// <summary>
        /// Inserts the onchange attribute on every input not contained in a STARTBLOCK - ENDBLOCK section.
        /// The method can also remove the two keywords
        /// </summary>
        /// <param name="docHtml">The text to insert in</param>
        /// <param name="preserveBlockKeyWord">Keeps the STARTBLOCK and ENDBLOCK keywords</param>
        /// <returns></returns>
        private string InsertOnChange(string docHtml, bool preserveBlockKeyWord)
        {
            //NO onchange event
            return docHtml;

            string retVal = "";
            ContentPart[] parts = XDocManager.SplitDocument(docHtml, preserveBlockKeyWord);
            foreach (ContentPart cp in parts)
            {
                if (cp.IsParsable)
                {
                    CSubmitParser submitParser = new CSubmitParser();
                    cp.Content = submitParser.InsertOnChange(cp.Content);
                }
                retVal += cp.Content;
            }

            return retVal;
        }


        private bool GetTemplateRight()
        {

            if (m_roles.Count == 0)
                return true;

            ArrayList currentUserRoles = m_manager.UserRoles;
            int currentUserID = m_manager.UserID;

            foreach (int roleID in currentUserRoles)
                if (m_roles.Contains(roleID))
                    return true;

            return false;
        }

        void parser_RequestTemplateText(CParser sender, int templateID)
        {
            IXTemplate templ = m_manager.LoadTemplate(templateID);
            if (templ != null)
            {
                //sender.TextForInclude = templ.Content;
                sender.TextForInclude = ((XTemplate)templ).ContentExpanded;
                sender.AdditionalIncludeParams[XDocManager.UserIDKey] = m_manager.UserID.ToString(CultureInfo.InvariantCulture);
                sender.AdditionalIncludeParams[XDocManager.UserLoginKey] = m_manager.UserLogin;
                sender.AdditionalIncludeParams[XDocManager.UserNameKey] = m_manager.UserName;
                sender.AdditionalIncludeParams[XDocManager.ServerKey] = m_manager.ServerName;
                sender.AdditionalIncludeParams[XDocManager.DatabaseKey] = m_manager.DatabaseName;
            }
            else
                sender.TextForInclude = null;
        }

        void parser_RequestTemplateID(CParser sender, string templateName)
        {
            IXTemplate templ = m_manager.LoadTemplate(templateName);
            if (templ != null)
                sender.IDForInclude = templ.ID;
        }

        void parser_RequestHierarchy(SourceResult sourceResult, string parameters)
        {
            sourceResult.RequestedList = m_manager.GetList(parameters);
        }

        void parser_LogInclude(CParser sender, LogIncludeEventArgs e)
        {
            m_manager.LogTemplateCall(e.IncludeParams, e.Indentation);
        }

        public string TemplateTree(string sContent)
        {
            string s_text = sContent;
            return s_text;
        }

        public  string ExpandContent(string sContent)
        {
            string s_text = sContent;
            try
            {
                s_text = ReplaceSuperMacro(s_text);
                s_text = ReplaceGlobalMacro(s_text);
                s_text = ProcessMacros(s_text);
                s_text = ReplaceOverMacro(s_text);
            }
            catch (Exception ex)
            {
                s_text = "error";// + ex.Message ;
            }
            return s_text;
        }

        private  Hashtable m_MacroTemplates = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private  string sGlobalMacroName = "";
        public  string GlobalMacroName
        {
            get
            {
                if (sGlobalMacroName == "")
                {
                    string strGlobalMacroName = System.Configuration.ConfigurationManager.AppSettings["GlobalMacroName"];
                    if (strGlobalMacroName != null)
                        sGlobalMacroName = strGlobalMacroName;
                    else sGlobalMacroName = "MACRO";
                }
                return sGlobalMacroName;
            }
        }

        private  string escapeMacroPar(string sPar)
        {
            string sPar1 = sPar;
            sPar1 = sPar1.Replace(",", "\\,");
            sPar1 = sPar1.Replace(")", "\\)");
            return sPar1;
        }
        private  string GetGlobalMacro(string sMacroTemplate, string sMacroName, string[] aMacroPars)
        {
            string s_text = "";

            if (m_MacroTemplates.ContainsKey(sMacroTemplate))
            {
                s_text = m_MacroTemplates[sMacroTemplate].ToString();
            }
            else
            {
                s_text = LoadContent(sMacroTemplate);
                m_MacroTemplates[sMacroTemplate] = s_text;
            }

            string sMacroContent = "";
            int i1 = s_text.IndexOf("#STARTMACRO." + sMacroName, StringComparison.OrdinalIgnoreCase);
            int i21 = s_text.IndexOf("#", i1 + 1);
            int i22 = s_text.IndexOf("(", i1 + 1);
            int i2;
            if (i21 == -1) i21 = 100000;
            if (i22 == -1) i22 = 100000;
            i2 = (i21 < i22) ? i21 : i22;
            if (i2 == 100000) i2 = -1;
            if ((i1 != -1) && (i2 != -1))
            {
                s_text = s_text.Replace("\r\n#endmacro." + sMacroName + "#", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("#endmacro." + sMacroName + "#\r\n", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("\r\n#ENDMACRO." + sMacroName + "#", "#ENDMACRO." + sMacroName + "#");
                s_text = s_text.Replace("#ENDMACRO." + sMacroName + "#\r\n", "#ENDMACRO." + sMacroName + "#");
                int i3 = s_text.IndexOf("#ENDMACRO." + sMacroName + "#", i1 + 1, StringComparison.OrdinalIgnoreCase);
                int i4 = i3 + "#ENDMACRO.".Length + sMacroName.Length + "#".Length;
                if (i3 == -1)
                    throw new Exception("invalid definition for ENDMACRO " + sMacroName);
                string sMacroText = s_text.Substring(i1 + ("#STARTMACRO." + sMacroName + "").Length, i3 - i1 - ("#STARTMACRO." + sMacroName + "").Length);
                string sMacroDefParameters = "";
                string[] aMacroDefParameters = new string[0];
                if (sMacroText.StartsWith("("))
                {
                    int i = sMacroText.IndexOf(")");
                    if (i == -1) throw new Exception("invalid parameters for MACRO " + sMacroName);
                    sMacroDefParameters = sMacroText.Substring(1, i - 1);
                    //aMacroDefParameters = sMacroDefParameters.Split(',');
                    aMacroDefParameters = Utils.MySplit(sMacroDefParameters, ','); 
                    sMacroContent = sMacroText.Substring(i + 1 + 1); // )#
                }
                else
                    sMacroContent = sMacroText.Substring(1); //#
                while (sMacroContent.StartsWith("\r\n")) sMacroContent = sMacroContent.Substring(2);
                string sMacroPar = "";
                for (int i = 0; i < aMacroDefParameters.Length; i++)
                {
                    if (aMacroPars.Length < i + 1)
                        sMacroPar = "";
                    else
                        sMacroPar = aMacroPars[i].Replace("<tilda>", ",");
                    sMacroContent = sMacroContent.Replace("%" + aMacroDefParameters[i] + ".esc%", escapeMacroPar(sMacroPar));
                    sMacroContent = sMacroContent.Replace("%" + aMacroDefParameters[i] + "%", sMacroPar);
                }

            }
            return sMacroContent;
        }
        //private string ReplaceSuperMacro(string s_text)
        //{
        //    string s_text1 = s_text.Replace("\\)", "<tilda>");
        //    //Regex regexMacro = new Regex(@"@@[^\.\(\)]*?\(([^\)]*?)\)", RegexOptions.IgnoreCase);
        //    Regex regexMacro = new Regex(@"@@[^\.\(\)]*?\\((([^\\)]*\\\\\\))*[^\\)]*|[^\\)]*)\\)", RegexOptions.IgnoreCase);
        //    // \\((([^\\)]*\\\\\\))*[^\\)]*|[^\\)]*)\\)
        //    MatchCollection mc = regexMacro.Matches(s_text1);
        //    while (mc.Count > 0)
        //    {
        //        for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //        {
        //            Match m = mc[matchIndex];
        //            string sMacro = m.Value.Replace("<tilda>", ")");
        //            int i1 = 1;
        //            string sMacroTemplate = GlobalMacroName;
        //            int i2 = sMacro.IndexOf('(', i1 + 1);
        //            string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
        //            string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2);
        //            sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
        //            string[] aMacroPars = sMacroParameters.Split(',');
        //            string sMacroPar;
        //            string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
        //            s_text = s_text.Replace(m.Value, sResult);
        //            s_text1 = s_text1.Replace(m.Value, sResult);
        //        }
        //        s_text1 = s_text.Replace("\\)", "<tilda>");
        //        mc = regexMacro.Matches(s_text1);
        //    }
        //    return s_text;
        //}
        //private string ReplaceGlobalMacro(string s_text)
        //{
        //    string s_text1 = s_text.Replace("\\)", "<tilda>");
        //    //Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);
        //    Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);
        //    MatchCollection mc = regexMacro.Matches(s_text1);
        //    while (mc.Count > 0)
        //    {
        //        for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //        {
        //            Match m = mc[matchIndex];
        //            string sMacro = m.Value.Replace("<tilda>", ")");
        //            int i1 = sMacro.IndexOf('.', "#MACRO.".Length);
        //            string sMacroTemplate = sMacro.Substring("#MACRO.".Length, i1 - "#MACRO.".Length);
        //            int i2 = sMacro.IndexOf('(', i1 + 1);
        //            string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
        //            string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2 - 1);
        //            sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
        //            string[] aMacroPars = sMacroParameters.Split(',');
        //            string sMacroPar;
        //            string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
        //            s_text = s_text.Replace(m.Value, sResult);
        //        }
        //        s_text1 = s_text.Replace("\\)", "<tilda>");
        //        mc = regexMacro.Matches(s_text1);
        //    }
        //    return s_text;
        //}
        //private string ReplaceOverMacro(string s_text)
        //{
        //    string s_text1 = s_text.Replace("\\)", "<tilda>");
        //    Regex regexMacro = new Regex(@"\#MACRO\.*?\((\.*?)\)\#", RegexOptions.IgnoreCase);
        //    //good
        //    //Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);

        //    MatchCollection mc = regexMacro.Matches(s_text1);
        //    for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //    {
        //        Match m = mc[matchIndex];
        //        string sResult = "";
        //        s_text = s_text.Replace(m.Value, sResult);
        //    }
        //    return s_text;
        //}
        //private string ReplaceMacro(string s_text, string sMacroName, string sMacroContent, string[] aMacroDefParameters)
        //{
        //    string s_text1 = s_text.Replace("\\)", "<tilda>");
        //    Regex regexMacro = new Regex(@"\#MACRO\." + sMacroName + @"\((.*?)\)\#", RegexOptions.IgnoreCase);
        //    MatchCollection mc = regexMacro.Matches(s_text1);
        //    for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
        //    {
        //        Match m = mc[matchIndex];
        //        string sMacro = m.Value.Replace("<tilda>", ")");
        //        string sMacroParameters = sMacro.Substring("#MACRO.".Length + sMacroName.Length + 1, sMacro.Length - "#MACRO.".Length - sMacroName.Length - 1 - ")#".Length);
        //        sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
        //        string[] aMacroPars = sMacroParameters.Split(',');
        //        string sMacroPar;
        //        string sMacroContent1 = sMacroContent;
        //        for (int i = 0; i < aMacroDefParameters.Length; i++)
        //        {
        //            if (aMacroPars.Length < i + 1)
        //                sMacroPar = "";
        //            else
        //                sMacroPar = aMacroPars[i].Replace("<tilda>", ",");
        //            sMacroContent1 = sMacroContent1.Replace("%" + aMacroDefParameters[i] + "%", sMacroPar);
        //        }
        //        s_text = s_text.Replace(m.Value, sMacroContent1);
        //    }
        //    return s_text;
        //}
        private  string ReplaceSuperMacro(string s_text)
        {
            //string s_text1 = s_text.Replace("\\)", "<tilda>");
            //Regex regexMacro = new Regex(@"@@[^\.\(\)]*?\(([^\)]*?)\)", RegexOptions.IgnoreCase);
//no . in macro name            Regex regexMacro = new Regex(@"@@[^\.\(\)]*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)", RegexOptions.IgnoreCase);
            //Regex regexMacro_what_about_punt = new Regex(@"@@[^\.\( \)]*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)", RegexOptions.IgnoreCase);
            Regex regexMacro = new Regex(@"@@[^\(\)]*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)", RegexOptions.IgnoreCase);
            

            // \\((([^\\)]*\\\\\\))*[^\\)]*|[^\\)]*)\\)
            MatchCollection mc = regexMacro.Matches(s_text);
            while (mc.Count > 0)
            {
                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                {
                    Match m = mc[matchIndex];
                    string sMacro = m.Value;//.Replace("<tilda>", ")");
                    int i1 = 1;
                    string sMacroTemplate = GlobalMacroName;
                    int i2 = sMacro.IndexOf('(', i1 + 1);
                    string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
                    if (sMacroName.Contains("."))
                    {
                        sMacroTemplate = sMacroName.Substring(0, sMacroName.IndexOf("."));
                        sMacroName = sMacroName.Substring(sMacroName.IndexOf(".") + 1);
                    }
                    string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2);
                    //sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                    sMacroParameters = sMacroParameters.Replace ("\\)",")");
//                    string[] aMacroPars = sMacroParameters.Split(',');
                    string[] aMacroPars =Utils.MySplit (  sMacroParameters,',');
                    string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
                    s_text = s_text.Replace(m.Value, sResult);
                }
                mc = regexMacro.Matches(s_text);
            }
            return s_text;
        }
        private  string ReplaceGlobalMacro(string s_text)
        {
            //string s_text1 = s_text.Replace("\\)", "<tilda>");
            //Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);
            Regex regexMacro = new Regex(@"\#MACRO\.[^\.\( \)]*?\.[^\.\(\)]*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
            MatchCollection mc = regexMacro.Matches(s_text);
            while (mc.Count > 0)
            {
                for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
                {
                    Match m = mc[matchIndex];
                    string sMacro = m.Value;//.Replace("<tilda>", ")");
                    int i1 = sMacro.IndexOf('.', "#MACRO.".Length);
                    string sMacroTemplate = sMacro.Substring("#MACRO.".Length, i1 - "#MACRO.".Length);
                    int i2 = sMacro.IndexOf('(', i1 + 1);
                    string sMacroName = sMacro.Substring(i1 + 1, i2 - i1 - 1);
                    string sMacroParameters = sMacro.Substring(i2 + 1, sMacro.Length - i2 - 2 - 1);
                    //sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                    sMacroParameters = sMacroParameters.Replace("\\)", ")");
//                  string[] aMacroPars = sMacroParameters.Split(',');
                    string[] aMacroPars = Utils.MySplit(sMacroParameters, ',');
                    string sResult = GetGlobalMacro(sMacroTemplate, sMacroName, aMacroPars);
                    s_text = s_text.Replace(m.Value, sResult);
                }
                //s_text1 = s_text.Replace("\\)", "<tilda>");
                mc = regexMacro.Matches(s_text);
            }
            return s_text;
        }
        private  string ReplaceOverMacro(string s_text)
        {
            //string s_text1 = s_text.Replace("\\)", "<tilda>");
            //Regex regexMacro = new Regex(@"\#MACRO\.*?\((\.*?)\)\#", RegexOptions.IgnoreCase);
            Regex regexMacro = new Regex(@"\#MACRO\..*?\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
            //good
            //Regex regexMacro = new Regex(@"\#MACRO\.[^\.\(\)]*?\(([^\)]*?)\)\#", RegexOptions.IgnoreCase);

            MatchCollection mc = regexMacro.Matches(s_text);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                string sResult = "";
                s_text = s_text.Replace(m.Value, sResult);
            }
            return s_text;
        }
        private  string ReplaceMacro(string s_text, string sMacroName, string sMacroContent, string[] aMacroDefParameters)
        {
            //string s_text1 = s_text.Replace("\\)", "<tilda>");
            //Regex regexMacro = new Regex(@"\#MACRO\." + sMacroName + @"\((.*?)\)\#", RegexOptions.IgnoreCase);
            Regex regexMacro = new Regex(@"\#MACRO\." + sMacroName + @"\((([^\)]*\\\))*[^\)]*|[^\)]*)\)\#", RegexOptions.IgnoreCase);
            //  
            MatchCollection mc = regexMacro.Matches(s_text);
            for (int matchIndex = 0; matchIndex < mc.Count; matchIndex++)
            {
                Match m = mc[matchIndex];
                string sMacro = m.Value.Replace("<tilda>", ")");
                string sMacroParameters = sMacro.Substring("#MACRO.".Length + sMacroName.Length + 1, sMacro.Length - "#MACRO.".Length - sMacroName.Length - 1 - ")#".Length);
                sMacroParameters = sMacroParameters.Replace("\\)", ")");
                //sMacroParameters = sMacroParameters.Replace("\\,", "<tilda>");
                //string[] aMacroPars = sMacroParameters.Split(',');
                string[] aMacroPars =Utils.MySplit (  sMacroParameters,',');
                string sMacroPar;
                string sMacroContent1 = sMacroContent;
                for (int i = 0; i < aMacroDefParameters.Length; i++)
                {
                    if (aMacroPars.Length < i + 1)
                        sMacroPar = "";
                    else
                        sMacroPar = aMacroPars[i];//.Replace("<tilda>", ",");
                    sMacroContent1 = sMacroContent1.Replace("%" + aMacroDefParameters[i] + ".esc%", escapeMacroPar(sMacroPar));
                    sMacroContent1 = sMacroContent1.Replace("%" + aMacroDefParameters[i] + "%", sMacroPar);
                }
                s_text = s_text.Replace(m.Value, sMacroContent1);
            }
            return s_text;
        }
        private  string ProcessMacros(string s_text)
        {
            int i1 = s_text.IndexOf("#STARTMACRO.", StringComparison.OrdinalIgnoreCase);
            int i21 = s_text.IndexOf("#", i1 + 1);
            int i22 = s_text.IndexOf("(", i1 + 1);
            int i2;
            if (i21 == -1) i21 = 100000;
            if (i22 == -1) i22 = 100000;
            i2 = (i21 < i22) ? i21 : i22;
            if (i2 == 100000) i2 = -1;
            while ((i1 != -1) && (i2 != -1))
            {
                string sMacroName = s_text.Substring(i1 + "#STARTMACRO.".Length, i2 - i1 - "#STARTMACRO.".Length);
                s_text = s_text.Replace("\r\n#endmacro." + sMacroName + "#", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("#endmacro." + sMacroName + "#\r\n", "#endmacro." + sMacroName + "#");
                s_text = s_text.Replace("\r\n#ENDMACRO." + sMacroName + "#", "#ENDMACRO." + sMacroName + "#");
                s_text = s_text.Replace("#ENDMACRO." + sMacroName + "#\r\n", "#ENDMACRO." + sMacroName + "#");
                int i3 = s_text.IndexOf("#ENDMACRO." + sMacroName + "#", i1 + 1, StringComparison.OrdinalIgnoreCase);
                int i4 = i3 + "#ENDMACRO.".Length + sMacroName.Length + "#".Length;
                if (i3 == -1)
                    throw new Exception("invalid definition for ENDMACRO " + sMacroName);
                string sMacroText = s_text.Substring(i1 + ("#STARTMACRO." + sMacroName + "").Length, i3 - i1 - ("#STARTMACRO." + sMacroName + "").Length);
                s_text = s_text.Substring(0, i1) + s_text.Substring(i4);
                string sMacroContent = "";
                string sMacroDefParameters = "";
                string[] aMacroDefParameters = new string[0];
                if (sMacroText.StartsWith("("))
                {
                    int i = sMacroText.IndexOf(")");
                    if (i == -1) throw new Exception("invalid parameters for MACRO " + sMacroName);
                    sMacroDefParameters = sMacroText.Substring(1, i - 1);
                    //aMacroDefParameters = sMacroDefParameters.Split(',');
                    aMacroDefParameters =Utils.MySplit ( sMacroDefParameters,',');
                    sMacroContent = sMacroText.Substring(i + 1 + 1); // )#
                }
                else
                    sMacroContent = sMacroText.Substring(1); //#
                while (sMacroContent.StartsWith("\r\n")) sMacroContent = sMacroContent.Substring(2);
                s_text = ReplaceMacro(s_text, sMacroName, sMacroContent, aMacroDefParameters);

                i1 = s_text.IndexOf("#STARTMACRO.", StringComparison.OrdinalIgnoreCase);
                i21 = s_text.IndexOf("#", i1 + 1);
                i22 = s_text.IndexOf("(", i1 + 1);
                if (i21 == -1) i21 = 100000;
                if (i22 == -1) i22 = 100000;
                i2 = (i21 < i22) ? i21 : i22;
                if (i2 == 100000) i2 = -1;
            }
            return s_text;
        }

        #endregion Private methods

    }
}
