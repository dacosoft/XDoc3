<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" enableEventValidation="false" CodeFile="MakeXDoc.aspx.cs" Inherits="MakeXDoc" %>
<%@ Register Assembly="XDocumentsWebControls" Namespace="XDocumentsWebControls" TagPrefix="cc1" %>
 
<asp:Literal runat="server" id="dt"></asp:Literal>
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >--%>
<html xmlns="http://www.w3.org/1999/xhtml" style="width: 100%; height: 100%; overflow :hidden; margin: 0px; padding: 0px;">
<head id="Head1" runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <asp:Literal runat="server" id="pagehead"></asp:Literal> 
    <title ><asp:Literal runat="server" id="pagetitle"></asp:Literal></title>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
</head>
<body style="margin: 0px; padding: 0px; height: 100%; overflow :hidden;">
    <form id="FormXDoc" runat="server" style="width: 100%; height: 100%; overflow :auto" >
    <div style="width: 100%; height: 100%; overflow :auto">
        <asp:Label runat="server" ID="lbError" Visible="false" ForeColor="red"></asp:Label>
        <cc1:XDoc ID="ctrlXDoc" runat="server" style="width: 100%; height: 100%; display:block; " />
    </div>
    </form>
</body>
</html>
