<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="AXDoc.aspx.cs" Inherits="AXDoc" %>
<%@ Register Assembly="XDocumentsWebControls" Namespace="XDocumentsWebControls" TagPrefix="cc1" %>
 
<asp:literal runat="server" id="dt"></asp:literal>
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >--%>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title ><asp:literal runat="server" id="pagetitle"></asp:literal></title> 
    <asp:literal runat="server" id="pagehead"></asp:literal> 
</head>
<body id="PageBody">
<cc1:AXDoc ID="ctrlXDoc" runat="server" />
</body>
</html>
