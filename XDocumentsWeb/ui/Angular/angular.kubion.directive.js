app
  .directive("mainApplication", function (ItemObject) {
      return {
          restrict: 'A',
          controller: function ($scope, $element) {
              $scope.init = function (attrs) {
                  $scope.id = 'mainApplication';
                  $scope.ItemObject = new ItemObject('R_Portal_2_Main', $scope.id);
                  $scope.loadData();
              };
              $scope.loadData = function () {
                  $scope.isLoading = true;
                  $scope.ItemObject.refreshItem().then(function (item) { $scope.isLoading = false; }, function (ex) { console.error('Error in mainApplication.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
          },
          link: function (scope, elem, attrs) {
              scope.init(attrs);
          }
      };
  })
  .directive("acGooglemap", function ($compile, GoogleMap) {
      return {
          restrict: "AE",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  attrs.$observe('address', function (val) {
                      if (val.replace(" ", "").length > 0) $scope.loadMap(val);
                  });
                  $scope.loadMap(attrs.address);
              };
              $scope.loadMap = function (address) {
                  $scope.GoogleMap = new GoogleMap('AC_Googlemap', $scope.id, []);
                  $scope.GoogleMap.showAddress(address);
              };
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive("acDropdownSingle", function ($compile, ListObject) {
      return {
          restrict: "E",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.columnDefs = $scope.$eval(attrs.columnDefs);
                  $scope.isLoading = false;
                  $scope.expanded = false;
                  $scope.dataLoaded = false;
                  $scope.emptytext = attrs.emptytext;
                  $scope.itemID = attrs.itemid;
                  $scope.itemText = attrs.itemtext;

                  $scope.List = new ListObject('AC_DropdownSingle', attrs.id, true, '', '', 10, 1);
              }
              $scope.loadData = function (useCache) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache).then(function () { $scope.isLoading = false; $scope.dataLoaded = true; }, function (ex) { console.error('Error in AC_DropdownSingle.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.toggle = function ($event, expand) {
                  if (expand && !$scope.dataLoaded) {
                      $scope.loadData(false);
                  }
                  $scope.expanded = expand;
                  $event.preventDefault();
              }
              $scope.rowclick = function ($event, itemID, itemText) {
                  $scope.itemID = itemID;
                  $scope.itemText = itemText;
                  $scope.expanded = false;
              };
              $scope.pageClick = function ($event, pagenr) {
                  $scope.isLoading = true;
                  $scope.List.Pagination.toPageNumber(pagenr).then(function () { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_DropdownSingle.pageClick (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive("acForm", function ($compile, ItemObject) {
      return {
          restrict: "AE",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.isLoading = false;
                  $scope.isSaving = false;
                  $scope.mode = parseInt(attrs.mode);
                  $scope.id = attrs.id;
                  $scope.container = attrs.container;
                  $scope.containerid = attrs.containerid;
                  $scope.submitMethod = attrs.submitmethod;
                  $scope.ItemObject = new ItemObject('AC_Form', $scope.id, $scope.container, $scope.containerid,$scope.submitMethod);
                  $scope.loadData();
              }
              $scope.loadData = function () {
                  $scope.$apply();
                  $scope.isLoading = true;
                  $scope.ItemObject.refreshItem().then(function (item) { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_Form.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.submitForm = function ($event) {                  
                  $scope.isSaving = true;
                  $scope.ItemObject.submitItem().then(function () { $scope.isSaving = false;}, function (ex) { console.error('Error in AC_Form.submitForm (' + $scope.id + '): ' + ex.message); $scope.isSaving = false; });
              }
              $scope.switchMode = function ($event, mode) {
                  $scope.mode = mode;
              };
              $scope.inputHasError = function (input) {                  
                  if (input && input.$error && input.$dirty) {                      
                      for(var propt in input.$error){
                          if(input.$error[propt])
                              return true;
                      }                      
                  }
                  return false;
              };
              $scope.passwordEncode = function () {
                  if ($scope.ItemObject.item.password && $scope.ItemObject.item.password.length > 0) {
                      $scope.ItemObject.item.passwordEncoded = hex_md5($scope.ItemObject.item.password + "KubionPredefinedSeed");
                  }
                  else {
                      $scope.ItemObject.item.passwordEncoded = "";
                  }
              };
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive("acGrid", function ($compile, ListObject) {
      return {
          restrict: "AE",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.isLoading = false;
                  $scope.columnDefs = $scope.$eval(attrs.columnDefs);
                  $scope.noRecordsContent = attrs.norecordscontent;
                  $scope.autoSelect = (parseInt(attrs.autoselect) == 1);
                  $scope.autoSelectIfOne = (parseInt(attrs.autoselectifone) == 1);
                  $scope.id = attrs.id;

                  var loadalldata = (parseInt(attrs.loadalldata) == 1);
                  var pagesize = parseInt(attrs.pagesize);
                  var pagenr = parseInt(attrs.pagenr);
                  $scope.List = new ListObject('AC_Grid', attrs.id, loadalldata, attrs.orderdir, attrs.ordercol, pagesize, pagenr);
                  $scope.loadData(false, true);
              };
              $scope.loadData = function (useCache, fromInit) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache)
                      .then(function () {
                          $scope.isLoading = false;
                          if (fromInit) {
                              var count = $scope.List.totalsize;
                              if ((count == 1 && $scope.autoSelectIfOne) || $scope.autoSelect)
                                  $scope.rowclick(null, 1);
                          }
                      }, function (ex) { console.error('Error in AC_Grid.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.sort = function ($event, columnDef) {
                  if (columnDef.order) {
                      $scope.isLoading = true;
                      $scope.List.changeOrder(columnDef.field).then(function () { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_Grid.sort (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
                  }
                  $event.preventDefault();
              };
              $scope.rowclick = function ($event, index) {
                  _sendEvent("AC_Grid", $scope.id, "RowClick", index);
              };
              $scope.pageClick = function ($event, pagenr) {
                  $scope.isLoading = true;
                  $scope.List.Pagination.toPageNumber(pagenr).then(function () { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_Grid.pageClick (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive("acToolbar", function ($compile, ListObject) {
      return {
          restrict: "AE",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.List = new ListObject('AC_Toolbar', attrs.id, true, "", "ASC", 10, 1);
                  $scope.loadData(false);
              }
              $scope.loadData = function (useCache) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache).then(function () { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_Toolbar.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.itemClick = function ($event, index) {
                  _sendEvent("AC_Toolbar", $scope.id, "ItemClick", index);
              };
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive('acDatepicker', function () {
      return {
          restrict: "A",
          controller: function ($scope, $element) {
              $scope.init = function (attrs) {
                  $(function () {
                      $element.fdatepicker({ format: attrs.dateformat, language: 'nl' });
                  });
              };
          },
          link: function (scope, element, attrs) {
              scope.init(attrs);
          }
      }
  })
  .directive('acTab', function ($compile, ListObject) {
      return {
          restrict: 'AE',
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.currentIndex = 0;
                  $scope.noRecordsContent = attrs.norecordscontent;
                  $scope.List = new ListObject('AC_Tab', attrs.id, true, "", "ASC", 1000, 1);
                  $scope.loadData(false);
              };
              $scope.loadData = function (useCache) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache).then(function () { $scope.isLoading = false; }, function (ex) { console.error('Error in AC_Tab.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.itemClick = function ($event, index) {
                  _sendEvent("AC_Tab", $scope.id, "ItemClick", index);
                  $scope.currentIndex = index;
              };

          },
          link: function (scope, element, attrs) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive('acSlider', function ($compile, ListObject, $interval) {
      return {
          restrict: 'AE',
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              var intervalObj = null;
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.duration = parseInt(attrs.interval);
                  $scope.intervalRunning = false;
                  $scope.currentIndex = 0;
                  $scope.noRecordsContent = attrs.norecordscontent;
                  $scope.List = new ListObject('AC_Slider', attrs.id, true, "", "ASC", 1000, 1);
                  $scope.loadData(false);
              };
              $scope.loadData = function (useCache) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache).then(function () { $scope.navigateTo(null, $scope.currentIndex); $scope.isLoading = false; $scope.startInterval(); }, function (ex) { console.error('Error in AC_Slider.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.startInterval = function () {
                  if (!intervalObj && $scope.duration > 1000) {
                      intervalObj = $interval(function () {
                          var currIndex = $scope.currentIndex;
                          currIndex++;
                          $scope.navigateTo(null, currIndex);
                      }, $scope.duration);
                      $scope.intervalRunning = true;
                  }
              }
              $scope.stopInterval = function () {
                  if (intervalObj) {
                      $interval.cancel(intervalObj);
                      intervalObj = null;
                      $scope.intervalRunning = false;
                  }
              }
              $scope.itemClick = function ($event, index) {
                  _sendEvent("AC_Slider", $scope.id, "ItemClick", index);
              };
              $scope.navigateTo = function (event, index) {
                  if (index > $scope.List.totalsize - 1) index = 0;
                  if (index < 0) index = $scope.List.totalsize - 1;
                  $scope.currentIndex = index;

                  $scope.List.list.forEach(function (item) { item.visible = false; });
                  $scope.List.list[$scope.currentIndex].visible = true;
                  $scope.currentIndexShow = $scope.currentIndex + 1;
                  if (event) event.preventDefault();
              }
          },
          link: function (scope, elem, attrs) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })
  .directive("acImageupload", function ($compile) {
      return {
          restrict: 'AE',
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.pictures = [];
              }
              $scope.fileNameChanged = function (element) {
                  var photofile = element.files[0];
                  var reader = new FileReader();
                  reader.onload = function (e) {
                      $scope.$apply(function () {
                          $scope.pictures.push({ src: e.target.result, name: element.value });
                      });
                  };
                  reader.readAsDataURL(photofile);
              }
              $scope.removePicture = function (index) {
                  $scope.pictures.splice(index, 1);
              }
          },
          link: function (scope, elem, attrs) {
              scope.compile();
              scope.init(attrs);
          }
      }
  })  
  .directive("acSearchS", function ($compile, ListObject) {
      return {
          restrict: "E",
          replace: true,
          transclude: true,
          template: "<div data-ng-transclude></div>",
          scope: {},
          controller: function ($scope, $element) {
              $scope.compile = function () {
                  var htmlString = $element.children()[0].innerHTML;
                  var el = $compile(htmlString)($scope);
                  $element.html(el);
              };
              $scope.init = function (attrs) {
                  $scope.id = attrs.id;
                  $scope.searchvalue = "";
                  $scope.container = attrs.container;
                  $scope.containerid = attrs.containerid;
                  $scope.searchclickMethod = attrs.searchclickmethod;
                  $scope.emptytext = attrs.emptytext;                  
                  $scope.columnDefs = $scope.$eval(attrs.columnDefs);
                  $scope.List = new ListObject('AC_SearchS', attrs.id, true, '', '', 10, 1);
                  $scope.loadData(false);                  
              }
              $scope.loadData = function (useCache) {
                  $scope.isLoading = true;
                  $scope.List.refreshList(useCache).then(function () { $scope.initTypeAhead($scope.List.list); }, function (ex) { console.error('Error in AC_SearchS.loadData (' + $scope.id + '): ' + ex.message); $scope.isLoading = false; });
              };
              $scope.initTypeAhead = function (list) {
                  var data = new Bloodhound({
                      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                      queryTokenizer: Bloodhound.tokenizers.whitespace,                      
                      local: $.map(list, function (item) {
                          return {
                              value: item[$scope.columnDefs[1].field],
                              idvalue: item[$scope.columnDefs[0].field],
                          };
                      })
                  });                  
                  data.initialize();
                  var typeahead = $($element).find(".typeahead").typeahead({
                      hint: true,
                      highlight: true,
                      minLength: 1
                  },
                  {                      
                      source: data.ttAdapter()                      
                  });
                  typeahead.on("typeahead:selected", $scope.onItemSelected);                  
              }
              $scope.onItemSelected = function (event, suggestion, dataset) {
                  _sendEvent($scope.container, $scope.containerid, $scope.searchclickMethod, suggestion.idvalue, "", "");
              }
              $scope.clearAll = function () {                  
                  $scope.searchvalue = "";
                  $scope.$apply();
              }
          },
          link: function (scope, element, attrs, ctrl) {
              scope.compile();
              scope.init(attrs);             
          }
      }
  });
       