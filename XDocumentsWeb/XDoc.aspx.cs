using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using XDocUtilNamespace;
using System.IO;
using System.Threading;
using System.Text;
using XDocuments;
using XDataSourceModule;
using System.Xml;

public partial class XDoc : System.Web.UI.Page
{
    //XDocManager xdoc;

    #region Events

//private void Page_PreRender(object sender, System.EventArgs e) 
//{ 
//Response.Write("Page_PreRender was called<br>"); 
//} 

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
    protected override void Render(HtmlTextWriter writer)
    {
        string toWrite = "";

        StringBuilder sb = new StringBuilder();
        StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        base.Render(hw);

        hw.Close();
        tw.Close();

        toWrite = sb.ToString();

        if (!toWrite.Contains("ctrlXDoc$hiddenUnique"))
        {
            int i = toWrite.IndexOf("<!DOCTYPE");
            if (i < 1) toWrite = "";
            else             toWrite = toWrite.Substring(0, i - 1);
        }
        Page.Controls.Clear();
        Page.Controls.Add(new LiteralControl(toWrite));

        base.Render(writer);
    }

    /* ALTERNATIVE GetTemplateContentUC
     
    public class FormlessPage : Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
    }


    //private string GetTemplateContentUC(string strParameters)
    private string GetTemplateContentUC(string strTemplateName, string strParameters)
    {
        //Page pageHolder = new Page();
        Page pageHolder = new FormlessPage();
        XDocumentsWebControls.XDoc xdocwebcontrol = new XDocumentsWebControls.XDoc();
        xdocwebcontrol.Parameters = "TemplateName=" + strTemplateName + "&" + strParameters;
        xdocwebcontrol.BuildPage();
        pageHolder.Controls.Add(xdocwebcontrol);
        StringWriter output = new StringWriter();
        try
        {
            HttpContext.Current.Server.Execute(pageHolder, output, false);
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return output.ToString();
    }
*/
    private string GetTemplateContentUC(string strTemplateName, string strParameters, ref string strDocType)
    {
        XDocumentsWebControls.XDoc xdocwebcontrol = new XDocumentsWebControls.XDoc();
        return xdocwebcontrol.GetHTML(strTemplateName, strParameters);
    }
    private string GetTemplateContent(string strTemplateName, string strParameters)
    {
        //IXTemplate xt = this.xdoc.LoadTemplate(strTemplate);
        //IXDocument xd = xt.ProcessTemplate(strParameters);
        //return xd.Content;
        XDocManager xm = new XDocManager();
        XDocCacheTemplates m_xdocCacheTemplates = null;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
        if (m_xdocCacheTemplates != null) { xm.DocCacheTemplates = m_xdocCacheTemplates; }
        IXTemplate xt = xm.LoadTemplate(strTemplateName);
        IXDocument xd = xt.ProcessTemplate(strParameters);
        m_xdocCacheTemplates = xm.DocCacheTemplates;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
        xm.Dispose();
        return xd.Content;

    }
    private string GetTemplateContentUC(string strTemplateName, Hashtable htParameters, ref string strDocType)
    {
        XDocumentsWebControls.XDoc xdocwebcontrol = new XDocumentsWebControls.XDoc();
        XDocManager xm = new XDocManager();
        XDocCacheTemplates m_xdocCacheTemplates = null;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
        if (m_xdocCacheTemplates != null) { xm.DocCacheTemplates = m_xdocCacheTemplates; }
        strDocType = xm.LoadTemplate(strTemplateName).DocType;
        string result = xdocwebcontrol.GetHTML(strTemplateName, htParameters, HttpContext.Current);
        m_xdocCacheTemplates = xm.DocCacheTemplates;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
        xm.Dispose();
        return result;
    }
    private string GetTemplateContent(string strTemplateName, Hashtable htParameters, ref string strDocType)
    {
        XDocManager xm = new XDocManager();
        XDocCacheTemplates m_xdocCacheTemplates = null;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) m_xdocCacheTemplates = (XDocCacheTemplates)HttpContext.Current.Application["XDocCacheTemplates"];
        if (m_xdocCacheTemplates != null) { xm.DocCacheTemplates = m_xdocCacheTemplates; }
        IXTemplate xt = xm.LoadTemplate(strTemplateName);
        IXDocument xd = xt.ProcessTemplate(htParameters);
        strDocType = xt.DocType;
        m_xdocCacheTemplates = xm.DocCacheTemplates;
        if (HttpContext.Current != null) if (HttpContext.Current.Application != null) HttpContext.Current.Application["XDocCacheTemplates"] = m_xdocCacheTemplates;
        xm.Dispose();
        return xd.Content;
    }

     protected void Page_Load(object sender, EventArgs e)
    {
        //this.dt.Text = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        //this.dt.Text = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
        string strDocType = ConfigurationManager.AppSettings["DOCTYPE"];
        if (strDocType == null)
        {
            string strDocType1 = ConfigurationManager.AppSettings["DOCTYPE1"];
            string strDocType2 = ConfigurationManager.AppSettings["DOCTYPE2"];
            if (strDocType1 == null) strDocType1 = "-//W3C//DTD HTML 4.01 Transitional//EN";
            if (strDocType2 == null) strDocType2 = "";
            else strDocType2 = " \"" + strDocType2 + "\"";
            //            strDocType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            strDocType = "<!DOCTYPE HTML PUBLIC \"" + strDocType1 + "\"" + strDocType2 + ">";
        }
        this.dt.Text = strDocType;
        string strPageTitle = ConfigurationManager.AppSettings["PageTitle"];
        if (strPageTitle == null) strPageTitle = "IRIS";
        this.pagetitle.Text = strPageTitle;

        string strPageHead = ConfigurationManager.AppSettings["PageHead"];
        if (strPageHead == null) strPageHead = "";
        this.pagehead.Text = strPageHead;
         
         ctrlXDoc.AnswerEvent += new XDocumentsWebControls.XDoc1.XDocCtrlEventHandler(ctrlXDoc_AnswerEvent);
        string parameters = GetQueryString();
        ctrlXDoc.Parameters = parameters;

        //bool processControls = false;

        //if (Request.QueryString["uc"] != null)
        //{
        //    string uc = Request.QueryString["uc"];
        //    if (uc.ToLower() == "true" || uc == "1")
        //        processControls = true;
        //}

        ////MIMI
        //processControls = false;

        //if (!IsPostBack || processControls)
        //    if (Request.QueryString["postbackGuid"] == null)
        //        ctrlXDoc.BuildPage();

        if (!IsPostBack)
            if (Request.QueryString["postbackGuid"] == null)
            {
                DateTime dt1 = DateTime.Now  ;
                ctrlXDoc.BuildPage();
                DateTime dt2 = DateTime.Now ;
                //System.Diagnostics.Debug.WriteLine(dt2.Subtract(dt1));  
            }
    }

    void ctrlXDoc_AnswerEvent(object sender, string newParameters)
    {
        try
        {
            Response.Redirect(newParameters, true);
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
        }
    }

    #endregion Events

    #region Private Methods


    void ShowError(string message)
    {
        lbError.Visible = true;
        lbError.Text = message;
    }

    //private string GetPostString()
    //{
    //    /* Get the parameters based on the posted template content */
    //    byte[] bts = new byte[HttpContext.Current.Request.InputStream.Length];
    //    HttpContext.Current.Request.InputStream.Seek(0, SeekOrigin.Begin);
    //    HttpContext.Current.Request.InputStream.Read(bts, 0, bts.Length);
    //    HttpContext.Current.Request.InputStream.Seek(0, SeekOrigin.Begin);
    //    string s = HttpContext.Current.Request.ContentEncoding.GetString(bts);

    //    XmlDocument doc = new XmlDocument();
    //    try { doc.LoadXml(s); }
    //    catch (XmlException) { doc = null; }
    //    if (doc.ChildNodes.Count != 0 && doc.ChildNodes[0].Name == "xDocPostData")
    //        return (doc.ChildNodes[0] as XmlElement).GetElementsByTagName("content")[0].InnerText + "&";

    //    return "";
    //}
    private string GetParamValue(string strParameters, string strParameterName, string strDefaultValue)
    {
        strParameters = "&" + strParameters.Replace('?', '&') + "&";
        string strFind = "&" + strParameterName + "=";

        // Get starting position
        int intStart = strParameters.IndexOf(strFind, StringComparison.CurrentCultureIgnoreCase);
        if (intStart == -1) return strDefaultValue;
        intStart += strFind.Length;

        // Get end position
        int intLength = strParameters.IndexOf("&", intStart) - intStart;
        if (intLength < 0) return strDefaultValue;

        // Parameter found, return value
        return strParameters.Substring(intStart, intLength);
    }

    /// <summary>
    /// Gets a string representation of the QueryString NameValueCollection used to pass parameters to the user control
    /// </summary>
    string GetQueryString_old()
    {
        string ret = "";
        foreach (string key in Request.QueryString.AllKeys)
        {
            ret += key + "=" + HttpUtility.UrlEncode(Request.QueryString[key], HttpContext.Current.Request.ContentEncoding) + "&";
        }
        string sSessionID = (string) Session["SessionID"];
        if (sSessionID == null) { sSessionID = System.Guid.NewGuid().ToString("N"); Session["SessionID"] = sSessionID; }
        ret += "SessionID=" + sSessionID ;
        if (ret.EndsWith("&"))
            ret = ret.Remove(ret.Length - 1);
        return ret;
    }
    string GetQueryString()
    {
        string cliUUID = "";
        string ret = "";
        foreach (string key in Request.QueryString.AllKeys)
        {
            ret += key + "=" + HttpUtility.UrlEncode(Request.QueryString[key], HttpContext.Current.Request.ContentEncoding) + "&";
            if (key.ToUpper() == "UUID")
                cliUUID = Request.QueryString[key];
        }

        string srvUUID = (string)Session["UUID"];
        if (cliUUID == "")
        {
            srvUUID = System.Guid.NewGuid().ToString("N"); Session["UUID"] = srvUUID;
        }
        else
            if (srvUUID == null) { srvUUID = cliUUID; Session["UUID"] = srvUUID; }
            else
                if (cliUUID != srvUUID)
                {
                    ret += "cliUUID=" + cliUUID + "&srvUUID=" + srvUUID + "&";
                    Session.Remove("UUID");
                }
        string sSessionID = (string)Session["SessionID"];
        if (sSessionID == null) { sSessionID = System.Guid.NewGuid().ToString("N"); Session["SessionID"] = sSessionID; }
        ret += "SessionID=" + sSessionID;
        if (ret.EndsWith("&"))
            ret = ret.Remove(ret.Length - 1);
        return ret;
    }

    #endregion Private Methods

}
