using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using KubionLogNamespace;

namespace KubionDataNamespace
{
    public class RestData : IData
    {
        private string sURI;
        private string sCommand;
        private RESTConsumer objRestConsumer;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        //Provider=REST;
        //URI=http://software.kubion.nl/iris_repository/;
        //UserName=sa;
        //Password=www;
        //Domain=Kubion;
        //ProxyURI=isa.wgt.local:8080;
        //ProxyUserName=proxyuser;
        //ProxyPassword=1234;
        //ProxyDomain=Domijn;
        private string ParseURI(string sURI)
        {
            string strURI = GetURIValue(sURI, "URI");
            string strUserName = GetURIValue(sURI, "UserName");
            string strPassword = GetURIValue(sURI, "Password");
            string strDomain = GetURIValue(sURI, "Domain");
            string strProxyURI = GetURIValue(sURI, "ProxyURI");
            string strProxyUserName = GetURIValue(sURI, "ProxyUserName");
            string strProxyPassword = GetURIValue(sURI, "ProxyPassword");
            string strProxyDomain = GetURIValue(sURI, "ProxyDomain");
            string strCommand = "";
            strCommand += "&url=" + System.Web.HttpUtility.UrlEncode(strURI, System.Text.Encoding.Default);
            strCommand += "&username=" + System.Web.HttpUtility.UrlEncode(strUserName, System.Text.Encoding.Default);
            strCommand += "&password=" + System.Web.HttpUtility.UrlEncode(strPassword, System.Text.Encoding.Default);
            strCommand += "&domain=" + System.Web.HttpUtility.UrlEncode(strDomain, System.Text.Encoding.Default);
            strCommand += "&proxyurl=" + System.Web.HttpUtility.UrlEncode(strProxyURI, System.Text.Encoding.Default);
            strCommand += "&proxyusername=" + System.Web.HttpUtility.UrlEncode(strProxyUserName, System.Text.Encoding.Default);
            strCommand += "&proxypassword=" + System.Web.HttpUtility.UrlEncode(strProxyPassword, System.Text.Encoding.Default);
            strCommand += "&proxydomain=" + System.Web.HttpUtility.UrlEncode(strProxyDomain, System.Text.Encoding.Default);
            return strCommand;
        }
        private string GetURIValue(string strURI, string strKey)
        {
            int intStart = (";" + strURI).IndexOf(";" + strKey + "=", StringComparison.CurrentCultureIgnoreCase);
            if (intStart == -1) return "";
            intStart += (strKey + "=").Length;
            int intEnd = (strURI + ";").IndexOf(";", intStart);
            return (strURI + ";").Substring(intStart, (intEnd - intStart));
        }

        public RestData(string strURI)
        {
            try
            {
				sURI = strURI;
                sCommand = ParseURI(sURI);
                string strSession = GetURIValue(sURI, "Session");
                if (strSession == "1")
                    objRestConsumer = new RESTConsumer(true);
                else
                    objRestConsumer = new RESTConsumer();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public void Dispose()
        {
            //objRestConsumer = null;
        }

        private void OpenConnection()
        {
        }

        public bool ConnOpen()
        {
            return true;
        }

        public DataTable GetSchema(string sqlString)
        {
            DataSet ds = new DataSet();
            //            OdbcDataAdapter da = new OdbcDataAdapter(command);
            try
            {
                //                da.FillSchema(ds, SchemaType.Source);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public IDataReader GetDataReader(string sqlString)
        {
            return null;
        }

        private bool parseRestSQL(string sqlString, ref string sCmd, ref string sRootNode, ref string sXPath, ref  string sContentType,  ref string sMethode, ref string sXML)
        {
            sCmd = "";
            sRootNode = "";
            sXPath = "\\";
            sContentType = "";
            sMethode = "GET";
            sXML = "";
            if (sqlString.IndexOf("\r\n") != -1)
            {
                sCmd = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sCmd = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sRootNode = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sRootNode = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sXPath = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sXPath = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sContentType = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sContentType = sqlString;
                sqlString = "";
            }

            if (sqlString.IndexOf("\r\n") != -1)
            {
                sMethode  = sqlString.Substring(0, sqlString.IndexOf("\r\n"));
                sqlString = sqlString.Substring(sqlString.IndexOf("\r\n") + 2);
            }
            else
            {
                sMethode  = sqlString;
                sqlString = "";
            }

            sXML = (sqlString == "\r\n")?"":sqlString ;
            return true;
        }

        public string GetResponse(string sqlString)
        {
            string sCmd = "";
            string sRootNode = "";
            string sXPath = "";
            string sContentType = "";
            string sMethode = "";
            string sXML = "";
            try
            {
                if (parseRestSQL(sqlString, ref sCmd, ref sRootNode, ref sXPath, ref sContentType, ref sMethode, ref sXML))
                {
                    string strCommand = sCommand;
                    strCommand += "&cmd=" + System.Web.HttpUtility.UrlEncode(sCmd, System.Text.Encoding.Default);
                    strCommand += "&rootnode=" + System.Web.HttpUtility.UrlEncode(sRootNode, System.Text.Encoding.Default);
                    strCommand += "&xpath=" + System.Web.HttpUtility.UrlEncode(sXPath, System.Text.Encoding.Default);
                    strCommand += "&contenttype=" + System.Web.HttpUtility.UrlEncode(sContentType, System.Text.Encoding.Default);
                    strCommand += "&method=" + System.Web.HttpUtility.UrlEncode(sMethode, System.Text.Encoding.Default);
                    strCommand += "&xml=" + System.Web.HttpUtility.UrlEncode(sXML, System.Text.Encoding.Default);
                    string ret = objRestConsumer.GetResponse(strCommand);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return null;
        }
        public DataTable GetDataTable(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return GetDataTable(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }

        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {

            string sCmd = "";
            string sRootNode = "";
            string sXPath = "";
            string sContentType = "";
            string sMethode = "";
            string sXML = "";
            try
            {
                if (parseRestSQL(sqlString, ref sCmd, ref sRootNode, ref sXPath, ref sContentType, ref sMethode, ref sXML))
                {
                    string strCommand = sCommand ;
                    strCommand += "&cmd=" + System.Web.HttpUtility.UrlEncode(sCmd , System.Text.Encoding.Default);
                    strCommand += "&rootnode=" + System.Web.HttpUtility.UrlEncode(sRootNode, System.Text.Encoding.Default);
                    strCommand += "&xpath=" + System.Web.HttpUtility.UrlEncode(sXPath, System.Text.Encoding.Default);
                    strCommand += "&contenttype=" + System.Web.HttpUtility.UrlEncode(sContentType, System.Text.Encoding.Default);
                    strCommand += "&method=" + System.Web.HttpUtility.UrlEncode(sMethode, System.Text.Encoding.Default);
                    strCommand += "&xml=" + System.Web.HttpUtility.UrlEncode(sXML, System.Text.Encoding.Default);
                    DataTable objDataTable = objRestConsumer.GetDataTable(strCommand, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                    return objDataTable;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return null;
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            return GetDataTable(sqlString);
        }

        public string ExecuteNonQuery(string sqlString)
        {
            string strResponse = "";
            string strOuterXml = "";
            string strInnerXml = "";

            return ExecuteNonQuery(sqlString, ref strResponse, ref strOuterXml, ref strInnerXml);
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            try
            {
                DataTable dt = GetDataTable(sqlString, ref m_Response, ref m_OuterXml, ref m_InnerXml);
                try { return dt.Rows[0][0].ToString().TrimEnd(); }
                catch { };

                //int iResult = -1;
                //try { iResult = System.Convert.ToInt32(dt.Rows[0][0]); }
                //catch { };
                //return iResult;
            }
            catch (Exception ex)
            {
                throw;
            }
            return "";
            //int iResult = 0;
            //try { iResult = System.Convert.ToInt32(m_InnerXml); }
            //catch { };
            //return iResult;
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            return ExecuteNonQuery(sqlString);
        }

        public void BeginTransaction()
        {
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
        }
        public void CommitTransaction()
        {
        }
        public void RollbackTransaction()
        {
        }

    }
}


