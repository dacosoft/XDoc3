using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using KubionLogNamespace;

namespace KubionDataNamespace
{
    public class SQLData : IData, IDisposable
    {
        #region data access
        SqlConnection m_conn = null;
        SqlTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        public SQLData()
        {
            string strConn = GetConnectionString();
            try
            {
                m_conn = new SqlConnection(strConn);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public SQLData(string connectionString)
        {

            try
            {
                m_conn = new SqlConnection(connectionString);
                OpenConnection();
            }
            catch (Exception ex)
            {
                //no trace message because we try to open SQL connection first, then OLEDB
                throw;
            }
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                try
                {
                    m_conn.Close();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }

        public SqlConnection Connection
        {
            get { return m_conn; }
        }

        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                try
                {
                    m_conn.Open();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    //Debug.WriteLine(ex);
                    throw;
                }
            }
        }

        string GetConnectionString()
        {
            string connectionString = ConfigurationManager.AppSettings["KBConn.ConnectionString"];
            if (connectionString == null || connectionString == "")
            {
                string strServer = ConfigurationManager.AppSettings["KBConn.Server"];
                string strDatabase = ConfigurationManager.AppSettings["KBConn.Database"];
                string strUser = ConfigurationManager.AppSettings["KBConn.User"];
                string strPassword = ConfigurationManager.AppSettings["KBConn.Password"];
                if (strServer == null || strDatabase == null || strUser == null || strPassword == null)
                {
                    Exception ex = new Exception("Connection settings are not specified in the application config file");
                    KubionLog.WriteLine(ex);
                    throw (ex);
                }
                connectionString = "Data source=" + strServer + "; Initial Catalog=" + strDatabase + "; User ID=" + strUser + "; Password=" + strPassword;
            }

            return connectionString;
        }

        public DataTable GetSchema(string sqlString)
        {
            OpenConnection();
            SqlCommand command = new SqlCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                command.Transaction = m_trans;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                //DateTime dt1 = DateTime.Now;
                da.FillSchema(ds, SchemaType.Source);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }

        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            SqlCommand command = new SqlCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans ;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }


        public string GetResponse(string sqlString)
        {
            string sTables = "";
            if (sqlString.LastIndexOf("--tables:") > 0)
            {

                sTables = sqlString.Substring(sqlString.LastIndexOf("--tables:") + "--tables:".Length);
                sqlString = sqlString.Substring(0, sqlString.LastIndexOf("--tables:"));
            }
            OpenConnection();
            SqlCommand command = new SqlCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                command.Transaction = m_trans;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                da.Fill(ds);
                if (sTables != "")
                {
                    string[] aTables = sTables.Split (',');
                    for (int i = 0; i < aTables.Length; i++)
                        if (aTables[i].Trim()!="") ds.Tables[i].TableName = aTables[i].Trim();
                }
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {
            }
            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }
        public DataTable GetDataTable(string sqlString)
        {
            OpenConnection();
            SqlCommand command = new SqlCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                command.Transaction = m_trans;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }
 //           string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            DataTable dt = null;
            dt = GetDataTable(sqlString);
            if (dt != null)
                if (dt.Rows.Count>0)
                    if (dt.Rows[0] != null)
                            if (dt.Rows[0][0] != null)
                            m_InnerXml = dt.Rows[0][0].ToString();
            return dt;
            //return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(SqlCommand command)
        {
            OpenConnection();
            if (m_trans != null)
                command.Transaction = m_trans;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString,  IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            SqlCommand comm = new SqlCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
            try
            {
                foreach (SqlParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                SqlDataAdapter da = new SqlDataAdapter(comm);
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return dt;
        }


        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            SqlCommand comm = new SqlCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;
            try
            {
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }


            return rowsAffected.ToString ();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString );
        }
        public string ExecuteNonQuery(SqlCommand comm)
        {
            OpenConnection();
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;
            try
            {
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }


            return rowsAffected.ToString ();
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            SqlCommand comm = new SqlCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            int rowsAffected = -1;

            try
            {
                foreach (SqlParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return rowsAffected.ToString ();
        }


        public DataSet GetDataSet(string sqlString)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(sqlString, m_conn);
            try
            {
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine("executed query:  " + duration + " query:" + sqlString);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }
            return ds;
        }
        public DataSet GetDataSet(SqlCommand command)
        {
            OpenConnection();
            if (m_trans != null)
                command.Transaction = m_trans;
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }
            return ds;
        }
        public DataSet GetDataSet(string sqlString, SqlParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            SqlCommand comm = new SqlCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;


            try
            {
                foreach (SqlParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                SqlDataAdapter da = new SqlDataAdapter(comm);

                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return ds;
        }


        #region static members
        public static string GetTransactionKey(ref SqlConnection conn)
        {
            string key = conn.WorkstationId.ToString() + conn.DataSource.ToString() + conn.Database.ToString();
            return key;
        }


        #endregion





        #endregion


        //#region GetResult

        //public static bool GetBoolResult(object obj)
        //{
        //    return GetBoolResult(obj, false);
        //}
        //public static bool GetBoolResult(object obj, bool defaultValue)
        //{
        //    if (obj != null && obj is bool)
        //        return (bool)obj;
        //    return defaultValue;
        //}

        //public static string GetStringResult(object obj, string defaultValue)
        //{
        //    if (obj != null)
        //        if (obj is string)
        //            return ((string)obj).TrimEnd();
        //        else
        //            return obj.ToString();
        //    return defaultValue;
        //}
        //public static string GetStringResult(object obj)
        //{
        //    return GetStringResult(obj, "");
        //}

        //public static int GetInt32Result(object obj, int defaultValue)
        //{
        //    if (obj == null) return defaultValue;
        //    if (obj is int) return (int)obj;
        //    if (obj is byte) return (int)((byte)obj);
        //    return defaultValue;
        //}
        
        ////public static int GetInt32Result(object obj, int defaultValue)
        ////{
        ////    if (obj != null && obj is int)
        ////        return (int)obj;

        ////    return defaultValue;
        ////}
        //public static int GetInt32Result(object obj)
        //{
        //    return GetInt32Result(obj, -1);
        //}

        //public static double GetDoubleResult(object obj, double defaultValue)
        //{
        //    if (obj != null && obj is double)
        //        return (double)obj;
        //    return defaultValue;
        //}
        //public static double GetDoubleResult(object obj)
        //{
        //    return GetDoubleResult(obj, -1);
        //}

        //public static DateTime GetDateTimeResult(object obj, DateTime defaultValue)
        //{
        //    if (obj != null && obj is DateTime)
        //        return (DateTime)obj;
        //    return defaultValue;
        //}
        //public static DateTime GetDateTimeResult(object obj)
        //{
        //    return GetDateTimeResult(obj, (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue);
        //}

        //#endregion GetResult

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {
                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }
        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }
        }

    }
}