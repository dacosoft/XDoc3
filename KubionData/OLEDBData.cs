using System;
using System.Data;
using System.Data.OleDb ;
using System.Configuration;
using System.Diagnostics;
using KubionLogNamespace;
using System.Collections;

namespace KubionDataNamespace
{
    public class OLEDBData : IData, IDisposable
    {
        OleDbConnection m_conn = null;
        OleDbTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }

        public OLEDBData(string connectionString)
        {

            try
            {
                m_conn = new OleDbConnection(connectionString);
                OpenConnection();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                try
                {
                    m_conn.Close();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }
        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                try
                {
                    m_conn.Open();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }


        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        public DataTable GetSchema(string sqlString)
        {
            if (!ConnOpen()) return null;
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.FillSchema(ds, SchemaType.Source);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }
        public string GetResponse(string sqlString)
        {
            if (!ConnOpen()) return null;
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {
            }
            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }
        public DataTable GetDataTable(string sqlString)
        {
            if (!ConnOpen()) return null;
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
            try
            {
                foreach (OleDbParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                OleDbDataAdapter da = new OleDbDataAdapter(comm);
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return dt;
        }
        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null) comm.Transaction = m_trans;

            int rowsAffected = -1;
            try
            {
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return rowsAffected.ToString();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null) comm.Transaction = m_trans;

            int rowsAffected = -1;
            try
            {
                foreach (OleDbParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return rowsAffected.ToString();
        }

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {
                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }
        }

    }
    public class OLEDBParData : IData, IDisposable
    {
        OleDbConnection m_conn = null;
        OleDbTransaction m_trans = null;
        private int iCommandTimeout = 30;
        public int CommandTimeout { get { return iCommandTimeout; } set { iCommandTimeout = value; } }
        public static string[] MySplit(string sVal, string sSeparator)
        {
            char cSeparator = '~';
            //sVal = sVal.Replace("\\\\", "<_backslash_>");
            sVal = sVal.Replace(cSeparator.ToString(), "<_tilda_>");
            sVal = sVal.Replace(sSeparator, cSeparator.ToString());
            string[] aData = sVal.Split(cSeparator);
            for (int i = 0; i < aData.Length; i++)
            {
                aData[i] = aData[i].Replace("<_tilda_>", cSeparator.ToString());
            }
            return aData;
        }

        public OLEDBParData(string connectionString)
        {

            try
            {
                m_conn = new OleDbConnection(connectionString);
                OpenConnection();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
        }

        public void Dispose()
        {
            if (m_conn != null && m_conn.State != ConnectionState.Closed)
            {
                try
                {
                    m_conn.Close();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }
        private void OpenConnection()
        {
            if (m_conn.State != ConnectionState.Open)
            {
                try
                {
                    m_conn.Open();
                }
                catch (Exception ex)
                {
                    KubionLog.WriteLine(ex);
                    throw;
                }
            }
        }


        public bool ConnOpen()
        {
            OpenConnection();
            return (m_conn.State == ConnectionState.Open);
        }

        public DataTable GetSchema(string sqlString)
        {
            if (!ConnOpen()) return null;
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.FillSchema(ds, SchemaType.Source);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public IDataReader GetDataReader(string sqlString)
        {
            OpenConnection();
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            command.CommandTimeout = CommandTimeout;
            if (m_trans == null) BeginTransaction(IsolationLevel.ReadUncommitted);
            command.Transaction = m_trans;
            return command.ExecuteReader(CommandBehavior.SequentialAccess);
        }
        public string GetResponse(string sqlString)
        {
            if (!ConnOpen()) return null;
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OleDbParameter oParameter = new OleDbParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OleDbParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {
            }
            string ret = Newtonsoft.Json.JsonConvert.SerializeObject(ds);
            return ret;
        }
        public DataTable GetDataTable(string sqlString)
        {
            if (!ConnOpen()) return null;
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OleDbParameter oParameter = new OleDbParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OleDbParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            try
            {
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }

            DataTable dt = null;
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];
            return dt;
        }
        public DataTable GetDataTable(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return GetDataTable(sqlString);
        }
        public DataTable GetDataTable(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            DataSet ds = new DataSet();
            OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null)
                comm.Transaction = m_trans;
            DataTable dt = null;
            try
            {
                foreach (OleDbParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                OleDbDataAdapter da = new OleDbDataAdapter(comm);
                //DateTime dt1 = DateTime.Now;
                da.Fill(ds);
                //DateTime dt2 = DateTime.Now;
                //TimeSpan ts = dt2.Subtract(dt1);
                //string duration = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString();
                //System.Diagnostics.Debug.WriteLine(dt1.ToString("HH:mm:ss.fff") + " executed query:  " + duration + " query:" + sqlString); 
                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return dt;
        }
        public string ExecuteNonQuery(string sqlString)
        {
            OpenConnection();
            //parameter sniffing :: ::
            ArrayList aParams = null;
            if (sqlString.Contains("::"))
            {
                aParams = new ArrayList();
                string[] aData = MySplit(sqlString, "::");
                sqlString = "";
                int i = 0;
                for (i = 0; i < aData.GetLength(0) - 1; i += 2)
                {
                    //sqlString = sqlString + aData[i] + "@P" + i.ToString();
                    sqlString = sqlString + aData[i] + "?";
                    string v = aData[i + 1];
                    if (v.StartsWith("'") && v.EndsWith("'"))
                        v = v.Substring(1, v.Length - 2);
                    OleDbParameter oParameter = new OleDbParameter("@P" + i.ToString(), v);
                    aParams.Add(oParameter);
                }
                if (i < aData.GetLength(0))
                    sqlString = sqlString + aData[i];
            }
            OleDbCommand command = new OleDbCommand(sqlString, m_conn);
            if (aParams != null)
                foreach (OleDbParameter oParameter in aParams)
                    command.Parameters.Add(oParameter);
            command.CommandTimeout = CommandTimeout;
            if (m_trans != null) command.Transaction = m_trans;

            int rowsAffected = -1;
            try
            {
                rowsAffected = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
            return rowsAffected.ToString();
        }
        public string ExecuteNonQuery(string sqlString, ref string m_Response, ref string m_OuterXml, ref string m_InnerXml)
        {
            return ExecuteNonQuery(sqlString);
        }
        public string ExecuteNonQuery(string sqlString, IDataParameter[] arrParams)
        {
            OpenConnection();
            OleDbCommand comm = new OleDbCommand(sqlString, m_conn);
            comm.CommandTimeout = CommandTimeout;
            if (m_trans != null) comm.Transaction = m_trans;

            int rowsAffected = -1;
            try
            {
                foreach (OleDbParameter paramValue in arrParams)
                {
                    comm.Parameters.Add(paramValue);
                }
                rowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                KubionLog.WriteLine(ex);
                throw;
            }
            finally
            {

            }

            return rowsAffected.ToString();
        }

        public void BeginTransaction()
        {
            OpenConnection();
            try
            {
                m_trans = m_conn.BeginTransaction();
            }
            catch (Exception)
            {
            }
        }
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            OpenConnection();
            try
            {
                if (m_trans != null) RollbackTransaction();
                m_trans = m_conn.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
            }
        }
        public void CommitTransaction()
        {
            try
            {
                m_trans.Commit();
                m_trans = null;
            }
            catch (Exception)
            {
            }

        }
        public void RollbackTransaction()
        {
            try
            {
                m_trans.Rollback();
                m_trans = null;
            }
            catch (Exception)
            {
            }
        }

    }
}