using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using XDocuments;
using XHTMLMerge;
using IDynamic;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Collections.Specialized;
using System.Reflection;
using KubionLogNamespace;

namespace XDocumentsWebControls
{
    [ToolboxData("<{0}:UpdateXDoc runat=server></{0}:XDoc>")]
    public class UpdateXDoc : WebControl, INamingContainer
    {

        public delegate void UpdateXDocEventHandler(object sender, string newParameters);
        public event UpdateXDocEventHandler AnswerEvent;

        #region Controls

        Panel 
            pTop = null, 
            DivXDoc = null;
        PlaceHolder 
            DocumentBody = null, 
            phControls = null;
        Button
            btnSaveDocument = null;
        HiddenField 
            hiddenDivContent = null, 
            hiddenAnswer = null, 
            hiddenUnique = null;


        #endregion Controls

        #region Members

        protected XDocManager xdm = null;
        protected IXDocument xdoc = null;
        protected string parameters = null;

        #endregion Members

        #region Public Properties

        public string Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        #endregion Public Properties

        #region Private methods

        void ToggleButtonVisible(Button button, bool visible)
        {
            string displayStyle = visible ? "inline" : "none";
            button.Style["display"] = displayStyle;
        }


        string RequestQueryString(string item)
        {
            if (Parameters != null)
            {
                string[] aParams = Parameters.Split('&');
                foreach (string sParam in aParams)
                {
                    if (sParam.ToUpper().StartsWith(item.ToUpper() + "="))
                    {
                        return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1));
                    }
                }
            }
            return null;
        }

        void ShowDocument(Hashtable parameters)
        {
            if (xdoc.DocType.ToUpper() == XDocManager.HTMLDocType)
            {
                AddHtmlToDocumentBody(xdoc.Content, xdoc.UniqueID);
            }
            else
                ShowError("The document cannot be updated because it is not an HTML document");

            string showSave = RequestQueryString("ShowSave");

            bool buttonVisible = (showSave != null && (showSave.ToUpper() == "TRUE" || showSave == "1"));
            ToggleButtonVisible(btnSaveDocument, buttonVisible);

            //register scripts
            string script = string.Format("function {0}_GetDivContent(){{document.getElementById(\"{1}\").value = document.getElementById(\"{2}\").innerHTML;}}{3}",
                        this.ClientID, hiddenDivContent.ClientID, DivXDoc.ClientID, Environment.NewLine);

            script += string.Format(
                "{1}" +
                "var {4}_callbackFnName = \"\";{3}" +
                "var {4}_req = null;{3}" +
                "function {4}_save(){3}" +
                "{{{3}" +
                "{4}_save(1);{3}" +
                "}}{3}" +
                "function {4}_save(withValidation){3}" +
                "{{{3}" +
                "_save(\"{4}\", withValidation, \"{0}\");{3}" +
                "}}{3}" +
                "function {4}_setAnswer(answer){3}" +
                "{{{3}" +
                "document.getElementById(\"{2}\").value = answer;{3}" +
                "}}{3}" +
                "function {4}_callInitialize(){3}" +
                "{{{3}" +
                "_callInitialize(\"{4}\");{3}" +
                "}}{3}" +
                "function {4}_makeCallback(url, callbackFn){3}" +
                "{{{3}" +
                "_makeCallback(\"{4}\", url, callbackFn){3}" +
                "}}{3}" +
                "function {4}_processResponse(){3}" +
                "{{{3}" +
                "_processResponse(\"{4}\");{3}" +
                "}}{3}",
                btnSaveDocument.ClientID, "", hiddenAnswer.ClientID, " ", xdoc.UniqueID);


            Page.ClientScript.RegisterClientScriptInclude("XDocumentsJS", Page.ClientScript.GetWebResourceUrl(typeof(XDocumentsWebControls.XDoc), "XDocumentsWebControls.Resources.XDocuments.js"));

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString("N"), script, true);

//            string initializeScript = string.Format("_addEvent(window, \"load\", _callInitialize);", xdoc.UniqueID);
            string initializeScript = "_addEvent(window, \"load\", _callInitialize);";
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString("N"), initializeScript, true);

        }

        void AddHtmlToDocumentBody(string html, string uniqueID)
        {
            if (html != null && html != "")
            {
                btnSaveDocument.Attributes["onclick"] = this.ClientID + "_GetDivContent();";
                btnSaveDocument.Attributes["onclick"] += "document.forms[0].setAttribute('onsubmit', '');";
            }
            DocumentBody.Controls.Clear();
            Literal lit = new Literal();
            lit.Text = html;
            DocumentBody.Controls.Add(lit);

            AddUniqueMarkup(phControls, uniqueID);
        }

        /// <summary>
        /// Adds controls accessible to the document and used to save the document.
        /// Only for backward compatibility.
        /// </summary>
        void AddUniqueMarkup(PlaceHolder placeHolder, string uniqueID)
        {
            string html4buttons = "<INPUT type=\"button\" style=\"display:none\" id=\"{0}\" onclick=\"document.getElementById('{1}').click();\" />";
            string btnHtmlSave = string.Format(html4buttons, uniqueID + btnSaveDocument.ID, btnSaveDocument.ClientID);
            placeHolder.Controls.Add(new LiteralControl(btnHtmlSave));
        }


        private void SetAnswer(string sAnswer)
        {
            if (AnswerEvent != null)
                AnswerEvent(this, sAnswer);
        }

        private void SaveDocument()
        {
            try
            {
                xdm = new XDocManager();

                if (xdoc == null)
                {
                    int docID = Convert.ToInt32(RequestQueryString("DocID"));
                    int version = Convert.ToInt32(RequestQueryString("Version"));

                    if (version > 0)
                        xdoc = xdm.LoadDocument(docID, version);
                    else
                        xdoc = xdm.LoadDocument(docID);

                    if (xdoc == null)
                    {
                        string err = "Cannot load document (ID = " + docID.ToString();
                        if (version != -1)
                            err += ", Version = " + version.ToString();
                        err += ")";

                        throw (new Exception(err));
                    }
                }

                Hashtable providedParameters = GetHashtableFromQueryString();

                xdoc.Content = hiddenDivContent.Value;
                xdoc.InitialRedirect = hiddenAnswer.Value;

                xdoc.Save(false, providedParameters);

                hiddenDivContent.Value = "";

                AddHtmlToDocumentBody("", xdoc.UniqueID);

                PerformRedirect(xdoc.Redirect, providedParameters);

                return;
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        void PerformRedirect(string redirect, Hashtable newRedirectParameters)
        {
            if ((redirect == "") || (redirect.ToUpper().Contains("RELOAD")))
            {
                Parameters = GetURLParameterList(newRedirectParameters);
                BuildPage();
            }
            else
                SetAnswer(redirect);

        }


        private void ShowError(string sAnswer)
        {
            DocumentBody.Controls.Clear();
            Literal lit = new Literal();
            sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
            lit.Text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
            DocumentBody.Controls.Add(lit);
        }


        Hashtable GetHashtableFromQueryString()
        {
            Hashtable ret = XDocuments.XDocManager.HashtableFromQueryString(parameters);
            return ret;
        }


        string GetURLParameterList(Hashtable receivedParams)
        {
            string ret = "";
            if (receivedParams == null)
                return "";

            IDictionaryEnumerator en = receivedParams.GetEnumerator();
            while (en.MoveNext())
            {
                ret += en.Key.ToString() + "=" + Utils.Encode(en.Value.ToString(), EncodeOption.URLEncode) + "&";
            }

            if (ret.EndsWith("&"))
                ret = ret.Substring(0, ret.Length - 1);

            return ret;
        }


        #endregion Private methods

        #region Public methods

        public void BuildPage()
        {
            try
            {
                DocumentBody.Controls.Clear();
                int docID = Convert.ToInt32(RequestQueryString("DocID"));
                int version = Convert.ToInt32(RequestQueryString("Version"));
                string attachments = RequestQueryString("Attachments");


                xdm = new XDocManager();

                if (version > 0)
                    xdoc = xdm.LoadDocument(docID, version);
                else
                    xdoc = xdm.LoadDocument(docID);

                if (attachments != null && (attachments == "1" || attachments.ToUpper() == "TRUE"))
                    xdoc.DownloadAttachments = true;



                if (xdoc == null)
                {
                    string err = "Cannot load document (ID = " + docID.ToString();
                    if (version != -1)
                        err += ", Version = " + version.ToString();
                    err += ")";
                    throw (new Exception(err));
                }
                Hashtable providedParameters = GetHashtableFromQueryString();
                ShowDocument(providedParameters);
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        #endregion Public methods

        #region Event handlers

        protected override void OnLoad(EventArgs e)
        {
            if (!ChildControlsCreated)
                EnsureChildControls();
        }

        protected override void CreateChildControls()
        {
            pTop = new Panel();
            pTop.ID = "panelTop";
            pTop.Style[HtmlTextWriterStyle.PaddingLeft] = "1px";
            pTop.Style[HtmlTextWriterStyle.PaddingRight] = "1px";

            DivXDoc = new Panel();
            DivXDoc.ID = "DivXDoc";

            hiddenDivContent = new HiddenField();
            hiddenDivContent.ID = "hiddenDivContent";

            hiddenAnswer = new HiddenField();
            hiddenAnswer.ID = "hiddenAnswer";

            hiddenUnique = new HiddenField();
            hiddenUnique.ID = "hiddenUnique";

            phControls = new PlaceHolder();
            phControls.ID = "phControls";

            btnSaveDocument = new Button();
            btnSaveDocument.ID = "btnSaveDocument";
            btnSaveDocument.Text = "Save Document";
            btnSaveDocument.Style[HtmlTextWriterStyle.Display] = "none";
            btnSaveDocument.Click += new EventHandler(btnSaveDocument_Click);


            DocumentBody = new PlaceHolder();
            DocumentBody.ID = "DocumentBody";

            DivXDoc.Controls.Add(DocumentBody);

            pTop.Controls.Add(btnSaveDocument);
            pTop.Controls.Add(hiddenAnswer);
            pTop.Controls.Add(hiddenDivContent);
            pTop.Controls.Add(hiddenUnique);
            pTop.Controls.Add(phControls);
            pTop.Controls.Add(DivXDoc);

            this.Controls.Add(pTop);

            base.CreateChildControls();
        }

        public void btnSaveDocument_Click(object sender, System.EventArgs e)
        {
            SaveDocument();
        }

        #endregion Event handlers

    }
}
