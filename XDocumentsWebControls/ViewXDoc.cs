using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using XDocuments;
using XHTMLMerge;
using IDynamic;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Collections.Specialized;
using System.Reflection;

namespace XDocumentsWebControls
{
    [ToolboxData("<{0}:ViewXDocXDoc runat=server></{0}:XDoc>")]
    public class ViewXDoc : WebControl, INamingContainer
    {
        #region Controls

        Panel 
            pTop = null, 
            DivXDoc = null;
        PlaceHolder 
            DocumentBody = null, 
            phControls = null;


        #endregion Controls

        #region Members

        protected XDocManager xdm = null;
        protected IXDocument xdoc = null;
        protected string parameters = null;

        #endregion Members

        #region Properties

        public string Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        #endregion Properties

        #region Public methods

        public void BuildPage()
        {
            try
            {
                if (!ChildControlsCreated)
                    EnsureChildControls();

                DocumentBody.Controls.Clear();
                int docID = Convert.ToInt32(RequestQueryString("DocID"));
                int version = Convert.ToInt32(RequestQueryString("Version"));

                xdm = new XDocManager();

                if (version > 0)
                    xdoc = xdm.LoadDocument(docID, version);
                else
                    xdoc = xdm.LoadDocument(docID);


                if (xdoc == null)
                {
                    string err = "Cannot load document (ID = " + docID.ToString();
                    if (version != -1)
                        err += ", Version = " + version.ToString();
                    err += ")";
                    throw (new Exception(err));
                }
                Hashtable providedParameters = GetHashtableFromQueryString();
                ShowDocument(providedParameters);
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }

        }

        #endregion Public methods

        #region Protected methods

        string RequestQueryString(string item)
        {
            if (Parameters != null)
            {
                string[] aParams = Parameters.Split('&');
                foreach (string sParam in aParams)
                {
                    if (sParam.ToUpper().StartsWith(item.ToUpper() + "="))
                    {
                        return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1));
                    }
                }
            }
            return null;
        }

        void ShowDocument(Hashtable parameters)
        {
            if (xdoc.DocType.ToUpper() == XDocManager.HTMLDocType.ToUpper())
            {
                AddHtmlToDocumentBody(xdoc.Content);

            }
            else
            {
                // binary file
                AddHtmlToDocumentBody("");
                string path = xdm.AttachmentsFolder;
                string fileName = "Document" + xdoc.ID.ToString() + "v" + xdoc.Version.ToString();
                if (!xdoc.DocType.StartsWith("."))
                    fileName += ".";
                fileName += xdoc.DocType;

                string filePath = Path.Combine(path, fileName);

                if (!File.Exists(filePath))
                {
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(filePath, FileMode.Create);
                        fs.Write(xdoc.BinaryContent, 0, (int)xdoc.BinaryContent.Length);
                    }
                    catch (Exception ex)
                    {
                        throw ;
                    }
                    finally
                    {
                        if (fs != null)
                            fs.Close();
                    }
                }

                string url = xdm.AttachmentsVirtualFolder;
                if (!url.EndsWith("/"))
                    url += "/";
                url += fileName;
                //string script = "window.open(\"" + url + "\");";

                //Page.ClientScript.RegisterClientScriptInclude("XDocumentsJS", Page.ClientScript.GetWebResourceUrl(typeof(XDocumentsWebControls.XDoc), "XDocumentsWebControls.Resources.XDocuments.js"));

                //Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_ViewDocument", script, true);
                Page.Response.Redirect(url);
            }
        }

        void AddHtmlToDocumentBody(string html)
        {
            DocumentBody.Controls.Clear();
            Literal lit = new Literal();
            lit.Text = html;
            DocumentBody.Controls.Add(lit);
        }

        void ShowError(string sAnswer)
        {
            DocumentBody.Controls.Clear();
            Literal lit = new Literal();
            sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
            lit.Text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
            DocumentBody.Controls.Add(lit);
        }

        Hashtable GetHashtableFromQueryString()
        {
            Hashtable ret = XDocuments.XDocManager.HashtableFromQueryString(parameters);
            return ret;
        }

        #endregion Protected methods

        #region Event handlers

        protected override void OnLoad(EventArgs e)
        {
            if (!ChildControlsCreated)
                EnsureChildControls();
        }

        protected override void CreateChildControls()
        {
            pTop = new Panel();
            pTop.ID = "panelTop";
            pTop.Style[HtmlTextWriterStyle.PaddingLeft] = "1px";
            pTop.Style[HtmlTextWriterStyle.PaddingRight] = "1px";

            DivXDoc = new Panel();
            DivXDoc.ID = "DivXDoc";

            phControls = new PlaceHolder();
            phControls.ID = "phControls";


            DocumentBody = new PlaceHolder();
            DocumentBody.ID = "DocumentBody";

            DivXDoc.Controls.Add(DocumentBody);


            pTop.Controls.Add(phControls);
            pTop.Controls.Add(DivXDoc);

            this.Controls.Add(pTop);

            base.CreateChildControls();
        }


        #endregion Event handlers

    }
}
