using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using XDocuments;
using XHTMLMerge;
using IDynamic;
using System.Threading;
using System.Configuration;
using System.IO;
using System.Collections.Specialized;
using System.Reflection;
using System.Xml;
using System.Text.RegularExpressions;

namespace XDocumentsWebControls
{
    [ToolboxData("<{0}:XDocA runat=server></{0}:XDocA>")]
    public class XDocA : WebControl, INamingContainer
    {
        #region Controls

        Panel
            pTop = null,
            DivXDoc = null;
        PlaceHolder
            DocumentBody = null,
            phControls = null;
        Button
            btnSaveDocument = null,
            btnSaveDocumentNoUpdates = null;
        HiddenField
            hiddenDivContent = null,
            hiddenAnswer = null,
            hiddenUnique = null,
            hiddenParameters = null;


        #endregion Controls

        #region Members

        protected XDocuments.XDocManager xdm = null;
        protected XDocuments.IXTemplate xt = null;
        protected string parameters = null;
        protected bool m_postedDataChecked = false;
        protected bool m_isDataPosted = false;

        Regex rgExSrc = new Regex("(src(\\s)*=(\\s)*\"[^\"]+\")|(src(\\s)*=(\\s)*'[^']+')", RegexOptions.IgnoreCase);
        Regex rgExUrlSrc = new Regex("('[^']+')|(\"[^\"]+\")");
        Regex rgExLineComment = new Regex(@"//(.)*");

        string
            m_scriptToRegister = "";

        #endregion Members

        #region Public events

        public delegate void XDocACtrlEventHandler(object sender, string newParameters);
        public event XDocACtrlEventHandler AnswerEvent;

        #endregion Public events

        #region Properties

        public string Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public string XDocUniqueID
        {
            get { return hiddenUnique.Value; }
        }


        #endregion Properties

        #region Event handlers


        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        protected override object SaveControlState()
        {
            return parameters != "" ? (object)parameters : null;
        }

        protected override void LoadControlState(object state)
        {
            if (state != null)
            {
                parameters = (string)state;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!ChildControlsCreated)
                EnsureChildControls();
            if (Page.IsPostBack == true ) RegisterScripts();
            CheckPostData();
        }

        protected override void CreateChildControls()
        {
            pTop = new Panel();
            pTop.ID = "panelTop";
            pTop.Style[HtmlTextWriterStyle.PaddingLeft] = "1px";
            pTop.Style[HtmlTextWriterStyle.PaddingRight] = "1px";

            DivXDoc = new Panel();
            DivXDoc.ID = "DivXDoc";

            hiddenDivContent = new HiddenField();
            hiddenDivContent.ID = "hiddenDivContent";

            hiddenAnswer = new HiddenField();
            hiddenAnswer.ID = "hiddenAnswer";

            hiddenUnique = new HiddenField();
            hiddenUnique.ID = "hiddenUnique";

            hiddenParameters = new HiddenField();
            hiddenParameters.ID = "hiddenParameters";

            phControls = new PlaceHolder();
            phControls.ID = "phControls";

            btnSaveDocument = new Button();
            btnSaveDocument.ID = "btnSaveDocument";
            btnSaveDocument.Text = "Save Document";
            btnSaveDocument.Style[HtmlTextWriterStyle.Display] = "none";
            btnSaveDocument.Click += new EventHandler(btnSaveDocument_Click);


            btnSaveDocumentNoUpdates = new Button();
            btnSaveDocumentNoUpdates.ID = "btnSaveDocumentNoUpdates";
            btnSaveDocumentNoUpdates.Text = "Save Document No Updates";
            btnSaveDocumentNoUpdates.Style[HtmlTextWriterStyle.Display] = "none";
            btnSaveDocumentNoUpdates.Click += new EventHandler(btnSaveDocumentNoUpdates_Click);

            DocumentBody = new PlaceHolder();
            DocumentBody.ID = "DocumentBody";
            //MIMI
            checkBuildPage();

            DivXDoc.Controls.Add(DocumentBody);

            pTop.Controls.Add(btnSaveDocument);
            pTop.Controls.Add(btnSaveDocumentNoUpdates);
            pTop.Controls.Add(hiddenAnswer);
            pTop.Controls.Add(hiddenDivContent);
            pTop.Controls.Add(hiddenUnique);
            pTop.Controls.Add(hiddenParameters);
            pTop.Controls.Add(phControls);
            pTop.Controls.Add(DivXDoc);

            this.Controls.Add(pTop);

            base.CreateChildControls();
        }
        void checkBuildPage()
        {
            if (parameters == "") return;
            if (Page.IsPostBack == false) return;

            bool processControls = false;

            if (Page.Request.QueryString["uc"] != null)
            {
                string uc = Page.Request.QueryString["uc"];
                if (uc.ToLower() == "true" || uc == "1")
                    processControls = true;
            }


            if (!Page.IsPostBack || processControls)
                if (Page.Request.QueryString["postbackGuid"] == null)
                    BuildPage();

        }

        public void btnSaveDocument_Click(object sender, System.EventArgs e)
        {
            SaveDocument(true);
        }

        public void btnSaveDocumentNoUpdates_Click(object sender, System.EventArgs e)
        {
            SaveDocument(false);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (m_scriptToRegister != "")
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), m_scriptToRegister, true);

        }
        
        protected override void Render(HtmlTextWriter writer)
        {
            if ((false ) && (xt != null) && (xt.IsData))
            {
                string toWrite = "";


                StringBuilder sb = new StringBuilder();
                StringWriter tw = new StringWriter(sb);
                HtmlTextWriter hw = new HtmlTextWriter(tw);

                DocumentBody.RenderControl(hw);

                hw.Close();
                tw.Close();

                toWrite = sb.ToString();

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(toWrite);
                try
                {
                    HttpContext.Current.Response.End();
                }
                catch (ThreadAbortException)
                {
                }


            }
            else
            {
                base.Render(writer);
            }
        }

        #endregion Event handlers

        #region Public methods
        bool inBuildPage = false;
        public void BuildPage()
        {
            if (inBuildPage) return;
            inBuildPage = true;
            try
            {
                if (!this.ChildControlsCreated)
                    EnsureChildControls();
                DocumentBody.Controls.Clear();
                int templateID = -1;
                string templateName = "";

                IdentifyTemplate(out templateID, out templateName);

                xdm = new XDocuments.XDocManager();

                string error = "";
                try
                {
                    if (templateID > -1)
                        xt = xdm.LoadTemplate(templateID);
                    else
                        xt = xdm.LoadTemplate(templateName);

                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }

                if (xt == null)
                {
                    if (templateID == 0)
                        throw new Exception("Neither the TemplateID nor the TemplateName parameter has been specified!");
                    string err = "Cannot find template";
                    if (templateName != "")
                        err += "(TemplateName = " + templateName + ")";
                    else
                        err += "(TemplateID = " + templateID.ToString() + ")";
                    err += Environment.NewLine + error;
                    throw (new Exception(err));
                }
                Hashtable providedParameters = GetHashtableFromQueryString();
                ShowTemplate(providedParameters);
            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                bool dataTemplate = (xt != null && xt.IsData) || new WebUtils().ShowErrorAsData();
                ShowError(ex.Message, dataTemplate);
            }
            inBuildPage = false;
        }


        #endregion Public methods

        #region Private methods

        void CheckPostData()
        {
            if (m_postedDataChecked)
                return;

            m_postedDataChecked = true;

            HttpRequest request = HttpContext.Current.Request;

            XmlDocument doc = new WebUtils().CheckPostData();
            if (doc != null)
            {
                string xmlParams = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_PARAMS_NODE)[0].InnerText;
                string xmlTemplateContent = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_CONTENT_NODE)[0].InnerText;
                string xmlAnswer = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_ANSWER_NODE)[0].InnerText;
                string xmlUniqueID = (doc.ChildNodes[0] as XmlElement).GetElementsByTagName(WebUtils.POST_UNIQUEID_NODE)[0].InnerText;

                hiddenDivContent.Value = xmlTemplateContent;

                if (!string.IsNullOrEmpty(xmlParams))
                    Parameters = xmlParams;

                if (!string.IsNullOrEmpty(xmlAnswer))
                    hiddenAnswer.Value = xmlAnswer;

                if (!string.IsNullOrEmpty(xmlUniqueID))
                    hiddenUnique.Value = xmlUniqueID;

                m_isDataPosted = true;

                SaveDocument(true);
            }
        }

        void IdentifyTemplate(out int templateID, out string templateName)
        {
            templateID = -1;
            templateName = "";
            string oTemplate = RequestQueryString("TemplateID");
            if (oTemplate != null)
            {
                if (!int.TryParse(oTemplate.ToString(), out templateID))
                    templateID = -1;
            }
            else
            {
                oTemplate = RequestQueryString("TemplateName");
                if (oTemplate != null)
                    templateName = oTemplate.ToString();
                else
                    templateID = 0;
            }
        }

        void ToggleButtonVisible(Button button, bool visible)
        {
            string displayStyle = visible ? "inline" : "none";
            button.Style["display"] = displayStyle;
        }

        string RequestQueryString(string item)
        {
            if (Parameters != null)
            {
                string[] aParams = Parameters.Split('&');
                foreach (string sParam in aParams)
                {
                    if (sParam.ToUpper().StartsWith(item.ToUpper() + "="))
                    {
                        return HttpUtility.UrlDecode(sParam.Substring(item.Length + 1));
                    }
                }
            }
            return null;
        }

        void ShowTemplate(Hashtable parameters)
        {
            bool unattended = false;

            IXDocument doc = xt.ProcessTemplate(parameters);

            //perform redirect 
            if (xt.Unattended)
            {
                PerformRedirect(doc.Redirect, parameters);
                return;
            }

            if (hiddenAnswer.Value == "")
                hiddenAnswer.Value = xt.Redirect;

            if (RequestQueryString("UC") != null && (RequestQueryString("UC").ToUpper() == "TRUE" || RequestQueryString("UC") == "1"))
                ProcessUC(doc.Content);
            else
            {
                AddHtmlToDocumentBody(doc.Content, xt.UniqueID);

                hiddenUnique.Value = xt.UniqueID;

                string showSave = RequestQueryString("ShowSave");

                bool buttonVisible = !unattended && (showSave != null && (showSave.ToUpper() == "TRUE" || showSave == "1"));
                ToggleButtonVisible(btnSaveDocument, buttonVisible);
                ToggleButtonVisible(btnSaveDocumentNoUpdates, buttonVisible);

                hiddenParameters.Value = XDocManager.QueryStringFromHashtable(parameters);
            }
            //Page.ClientScript.RegisterClientScriptInclude("XDocumentsJS", Page.ClientScript.GetWebResourceUrl(typeof(XDocumentsWebControls.XDoc), "XDocumentsWebControls.Resources.XDocuments.js"));
            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "XDocumentsJS", Page.ClientScript.GetWebResourceUrl(typeof(XDocumentsWebControls.XDocA ), "XDocumentsWebControls.Resources.XDocuments.js"));

            RegisterScripts();
        }

        void RegisterScripts()
        {
            if (xt == null) return;
            //register scripts
            string script = string.Format("function {0}_GetDivContent(){{document.getElementById(\"{1}\").value = document.getElementById(\"{2}\").innerHTML;}}{3}",
                        this.ClientID, hiddenDivContent.ClientID, DivXDoc.ClientID, Environment.NewLine);

            //function %UNIQUE%_save() { %UNIQUE%_save(1); } 
            //function %UNIQUE%_saveNoUpdates() { %UNIQUE%_saveNoUpdates(1); } 
            //function %UNIQUE%_save(withValidation) { _save("%UNIQUE%", withValidation, "ctrlXDoc_btnSaveDocument"); } 
            //function %UNIQUE%_saveNoUpdates(withValidation) { _saveNoUpdates("%UNIQUE%", withValidation, "ctrlXDoc_btnSaveDocumentNoUpdates"); } 
            //function %UNIQUE%_setAnswer(answer) { document.getElementById("ctrlXDoc_hiddenAnswer").value = answer; } 
            //function %UNIQUE%_callInitialize() { _callInitialize("%UNIQUE%"); } 
            //function %UNIQUE%_callInitializeA() { _callInitializeA("%UNIQUE%"); } 
            //function %UNIQUE%_makePostback(callbackFn) { url = window.location.href; content = document.getElementById("ctrlXDoc_DivXDoc").innerHTML; parameters = document.getElementById("ctrlXDoc_hiddenParameters").value; answer = document.getElementById("ctrlXDoc_hiddenAnswer").value; targetUniqueID = document.getElementById("ctrlXDoc_hiddenUnique").value; _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID) } 

            script += string.Format(
                "function {4}_save(){3}" +
                "{{{3}" +
                "{4}_save(1);{3}" +
                "}}{3}" +
                "function {4}_saveNoUpdates(){3}" +
                "{{{3}" +
                "{4}_saveNoUpdates(1);{3}" +
                "}}{3}" +
                "function {4}_save(withValidation){3}" +
                "{{{3}" +
                "_save(\"{4}\", withValidation, \"{0}\");{3}" +
                "}}{3}" +
                "function {4}_saveNoUpdates(withValidation){3}" +
                "{{{3}" +
                "_saveNoUpdates(\"{4}\", withValidation, \"{1}\");{3}" +
                "}}{3}" +
                "function {4}_setAnswer(answer){3}" +
                "{{{3}" +
                "document.getElementById(\"{2}\").value = answer;{3}" +
                "}}{3}" +
                "function {4}_makePostback(callbackFn){3}" +
                "{{{3}" +
                "url = window.location.href;{3}" +
                "content = document.getElementById(\"{5}\").innerHTML;{3}" +
                "parameters = document.getElementById(\"{6}\").value;{3}" +
                "answer = document.getElementById(\"{7}\").value;{3}" +
                "targetUniqueID = document.getElementById(\"{8}\").value;{3}" +
                " _makePostback(url, callbackFn, content, parameters, answer, targetUniqueID);{3}" +
                "}}{3}",
                btnSaveDocument.ClientID, btnSaveDocumentNoUpdates.ClientID, hiddenAnswer.ClientID, " ", xt.UniqueID,
                DivXDoc.ClientID, hiddenParameters.ClientID, hiddenAnswer.ClientID, hiddenUnique.ClientID);


//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString("N"), script, true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString("N"), script, true);

//            string initializeScript = string.Format("_addEvent(window, \"load\", _callInitialize);");
            //string initializeScript = string.Format("if (typeof({0}_callInitialize) !== \"undefined\") window.attachEvent(\"onload\",{0}_callInitialize);", xt.UniqueID);
            string initializeScript = string.Format("if (typeof(Sys) !== \"undefined\" && typeof(Sys.Application) !== \"undefined\") _callInitialize();");

//            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString("N"), initializeScript, true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), initializeScript, true);

        }

        void AddHtmlToDocumentBody(string html, string uniqueID)
        {
            if (html != null && html != "")
            {
                btnSaveDocument.Attributes["onclick"] = this.ClientID + "_GetDivContent();";
                btnSaveDocumentNoUpdates.Attributes["onclick"] = this.ClientID + "_GetDivContent();";
                AddScript(html);
            }

            DocumentBody.Controls.Clear();
            LiteralControl lit = new LiteralControl();
            lit.Text = html;
            DocumentBody.Controls.Add(lit);

            if (!xt.IsData) AddUniqueMarkup(phControls, uniqueID);
        }

        void AddScript(string html)
        {
            string script = GetScript(html, true);
            if (script != "")
            {
                script = ReplaceComments(script);
                m_scriptToRegister = script;
            }
        }

        /// <summary>
        /// Replaces single line comments with block comments
        /// </summary>
        /// <param name="script">The script to be modified</param>
        /// <returns></returns>
        string ReplaceComments(string script)
        {
            if (script == null || script == "")
                return script;

            SortedList insertStrings = new SortedList();
            SortedList removeLengths = new SortedList();

            MatchCollection mc = rgExLineComment.Matches(script);
            foreach (Match m in mc)
            {

                string newComment = "/*" + m.Value.Substring(2);
                string newLineStr = RemoveEndNewLine(ref newComment);
                newComment += "*/" + newLineStr;

                removeLengths.Add(m.Index, m.Value.Length);
                insertStrings.Add(m.Index, newComment);
            }

            IDictionaryEnumerator en = removeLengths.GetEnumerator();
            int offset = 0;

            StringBuilder sbScript = new StringBuilder(script);
            while (en.MoveNext())
            {
                int index = offset + (int)en.Key;

                string strInsert = (string)insertStrings[en.Key];
                int remLen = (int)en.Value;
                int insLen = strInsert.Length;

                sbScript = sbScript.Remove(index, remLen);
                sbScript = sbScript.Insert(index, strInsert);

                offset += insLen - remLen;
            }

            return sbScript.ToString();
        }

        string RemoveEndNewLine(ref string match)
        {
            if (match == null || match == "")
                return match;

            if (match.EndsWith("\r"))
            {
                match = match.Substring(0, match.Length - 1);
                return "\r";
            }
            if (match.EndsWith("\r\n"))
            {
                match = match.Substring(0, match.Length - 2);
                return "\r\n";
            }

            return "";


        }

        string GetScript(string html, bool parseIncludes)
        {
            string
                retScript = "",
                firstKey = "<script",
                secondKey = "</script>";

            if (html != null)
            {
                int startSearchIndex = 0;
                int ioStartTag = -1;

                do
                {
                    ioStartTag = html.ToLower().IndexOf(firstKey, startSearchIndex);
                    if (ioStartTag != -1)
                    {
                        int ioEndTag = html.ToLower().IndexOf(secondKey, ioStartTag);
                        if (ioEndTag != -1)
                        {
                            int ioStartScript = html.IndexOf(">", ioStartTag);
                            if (ioStartScript != -1)
                            {
                                bool included = false;
                                if (parseIncludes)
                                {
                                    string scriptTag = html.Substring(ioStartTag, ioStartScript - ioStartTag);
                                    string includedScript = GetIncludedScriptFromTag(scriptTag);
                                    if (includedScript != "")
                                    {
                                        //retScript += includedScript;
                                        included = true;
                                    }
                                }
                                if (!included)
                                {
                                    string script = html.Substring(ioStartScript + 1, ioEndTag - ioStartScript - 1);
                                    retScript += script;
                                }
                                startSearchIndex = ioEndTag;
                            }
                        }
                    }
                } while (ioStartTag != -1);
            }
            return retScript;
        }

        string GetIncludedScriptFromTag(string tag)
        {
            string ret = "";
            if (tag == "" || tag == null)
                return ret;

            tag = tag.ToLower();

            string url = GetIncludedURL(tag);
            if (url == "")
                return ret;


            return GetIncludedScript(url);
        }

        string GetIncludedScript(string url)
        {
            string ret = "";

            ScriptManager.RegisterClientScriptInclude(this, this.GetType(), Guid.NewGuid().ToString("N"), url);

            return ret;
            /*
            string filePath = "";
            try
            {
                filePath = MapPathFromRoot(ref url, false);
            }
            catch { return ""; }

            if (filePath == "")
                return "";

            StreamReader sr = null;
            try
            {
                sr = new StreamReader(filePath);
                ret = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                KubionLogNamespace.KubionLog.WriteLine(ex);
                string message = "Could not include script for template with parameters " + this.parameters + " " + ex.Message;
                throw new Exception(message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
            return ret;*/
        }


        string GetIncludedURL(string tag)
        {
            MatchCollection mc = rgExSrc.Matches(tag);
            if (mc.Count > 0)
            {
                Match m = rgExUrlSrc.Match(mc[0].Value);
                if (m.Success)
                    return m.Value.Replace("'", "").Replace("\"", "");
            }
            return "";
        }

        string MapPathFromRoot(ref string virtualPath, bool adjustVirtualPath)
        {
            int validPos = -1;
            for (int i = 0; i < virtualPath.ToCharArray().Length; i++)
            {
                if (char.IsSymbol(virtualPath[i]) || char.IsPunctuation(virtualPath[i]))
                    continue;
                else
                {
                    validPos = i;
                    break;
                }
            }
            if (validPos == -1)
                throw new Exception("The virtual path is not correctly specified!");

            string temp = virtualPath;
            temp = temp.Substring(validPos);

            if (adjustVirtualPath)
                virtualPath = temp;

            return System.Web.HttpContext.Current.Server.MapPath("~/" + temp);
        }


        /// <summary>
        /// Adds controls accessible to the template and used to save the document.
        /// Only for backward compatibility.
        /// </summary>
        void AddUniqueMarkup(PlaceHolder placeHolder, string uniqueID)
        {
            string html4buttons = "<INPUT type=\"button\" style=\"display:none\" id=\"{0}\" onclick=\"document.getElementById('{1}').click();\" />";

            string btnHtmlSave = string.Format(html4buttons, uniqueID + btnSaveDocument.ID, btnSaveDocument.ClientID);
            string btnHtmlSaveNoUpd = string.Format(html4buttons, uniqueID + btnSaveDocumentNoUpdates.ID, btnSaveDocumentNoUpdates.ClientID);

            placeHolder.Controls.Add(new LiteralControl(btnHtmlSave));
            placeHolder.Controls.Add(new LiteralControl(btnHtmlSaveNoUpd));
        }

        private void SetAnswer(string sAnswer)
        {
            if (AnswerEvent != null)
            {
                AnswerEvent(this, sAnswer);
            }
        }

        private void SaveDocument(bool promoteUpdates)
        {
            if (!this.ChildControlsCreated)
                EnsureChildControls();
            try
            {
                int templateID;
                string templateName;
                IdentifyTemplate(out templateID, out templateName);

                xdm = new XDocManager();

                //recover the uniqueID generated at xt.ProcessTemplate(..) at the previous postback and copied in the hidden by GetDivContent()
                string uniqueID = hiddenUnique.Value;

                Hashtable providedParameters = GetHashtableFromQueryString();
                IXDocument doc = null;
                if (templateID > -1)
                    doc = xdm.NewDocumentBasedOnTemplate(templateID);
                else
                    doc = xdm.NewDocumentBasedOnTemplate(templateName);

                if (doc == null)
                    throw new Exception("Could not load document based on TemplateID = " + templateID.ToString() + " or TemplateName = " + templateName);

                doc.Content = hiddenDivContent.Value;
                doc.UniqueID = uniqueID;
                if (hiddenAnswer.Value != "")
                    doc.InitialRedirect = hiddenAnswer.Value;

                Hashtable newRedirectParameters = doc.Save(promoteUpdates, providedParameters);

                hiddenDivContent.Value = ""; // otherwise the document body will remain copied in this hidden

                PerformRedirect(doc.Redirect, newRedirectParameters);

                return;
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }

        void PerformRedirect(string redirect, Hashtable newRedirectParameters)
        {
            if ((m_isDataPosted) && (!redirect.ToUpper().StartsWith("RELOAD")))
                //if (m_isDataPosted)
            {
                ReturnAnswerToPostClient(redirect);
            }
            else
            {

                if (redirect.ToUpper().Contains("RELOAD"))
                {
                    Parameters = GetURLParameterList(newRedirectParameters);
                    BuildPage();
                }
                else
                {
                    SetAnswer(redirect);
                }
            }
        }

        void ReturnAnswerToPostClient(string answer)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(answer);
            try
            {
                HttpContext.Current.Response.End();
            }
            catch (ThreadAbortException)
            {

            }
        }

        private void ShowError(string sAnswer, bool dataTemplate)
        {
            if (dataTemplate)
            {
                System.Web.HttpContext.Current.Response.Clear();
                sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
                string text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
                System.Web.HttpContext.Current.Response.Write(text);
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                try
                {
                    System.Web.HttpContext.Current.Response.End();
                }
                catch (ThreadAbortException)
                {

                }

            }
            else
            {
                DocumentBody.Controls.Clear();
                Literal lit = new Literal();
                sAnswer = sAnswer.Replace(Environment.NewLine, "<br/>");
                lit.Text = "<font color=\"red\" size=\"2\">" + sAnswer + "</font>";
                DocumentBody.Controls.Add(lit);
            }
        }

        private void ShowError(string sAnswer)
        {
            bool showAsData = new WebUtils().ShowErrorAsData();
            ShowError(sAnswer, showAsData);
        }


        Hashtable GetHashtableFromQueryString()
        {
            Hashtable ret = XDocManager.HashtableFromQueryString(parameters);
            return ret;
        }

        string GetUniqueID()
        {
            /*string ret = btnSaveDocument.ClientID.Replace(btnSaveDocument.ID, "");
            if (ret.EndsWith("_"))
                ret = ret.Remove(ret.Length - 1);
            return ret; */

            return this.ClientID;
        }

        string GetURLParameterList(Hashtable receivedParams)
        {
            string ret = "";
            if (receivedParams == null)
                return "";

            Hashtable h = XDocManager.HashtableFromQueryString(Parameters);
            if (h != null)
                foreach (DictionaryEntry de in h)
                    if (!receivedParams.ContainsKey(de.Key))
                        receivedParams[de.Key] = de.Value;

            IDictionaryEnumerator en = receivedParams.GetEnumerator();
            while (en.MoveNext())
            {
                ret += en.Key.ToString() + "=" + CParser.Encode(en.Value.ToString(), EncodeOption.URLEncode) + "&";
            }

            if (ret.EndsWith("&"))
                ret = ret.Substring(0, ret.Length - 1);

            return ret;
        }

        #endregion Private methods

        #region Dynamic Usercontrols

        Hashtable GetUCParameters(string tag)
        {
            int fio = tag.IndexOf('(');
            Hashtable ret = CollectionsUtil.CreateCaseInsensitiveHashtable();
            if (fio != -1)
            {
                int lio = tag.LastIndexOf(')');
                //string strRawParams = tag.Substring(fio + 1, lio - fio - 1);

                string[] rawParams = ParseUtils.StringArrayFromParamList(ParseUtils.GetParameterList(tag, ("'").ToCharArray()[0], ',', true));

                foreach (string param in rawParams)
                {
                    int io = param.IndexOf("=");
                    if (io != -1)
                    {
                        string name = param.Substring(0, io).Trim();
                        string val = param.Substring(io + 1).Trim();
                        if (val.StartsWith("'") && val.Length > 0)
                            val = val.Substring(1);
                        if (val.EndsWith("'") && val.Length > 0)
                            val = val.Substring(0, val.Length - 1);

                        ret[name] = val;
                    }
                    else
                        throw new ApplicationException("The parameter " + param + " does not contain the '=' character");

                }
            }
            return ret;
        }

        string GetUCName(string tag)
        {
            int fio = tag.IndexOf('(');
            if (fio == -1)
                return tag.Substring(4, tag.Length - (4 + 2));
            return tag.Substring(4, fio - 4);
        }

        string GetWebControlName(string tag)
        {
            if (tag == null || tag == "")
                return "";
            string key = "ctrl:";
            int fio = tag.ToLower().IndexOf(key);
            if (fio != -1)
            {
                int lio = tag.IndexOf(" ", fio + 1);
                return tag.Substring(fio + key.Length, lio - fio - key.Length).TrimEnd().TrimStart();
            }
            return "";

        }

        SortedList<int, UCMatch> GetUCMatches(string originalText)
        {

            SortedList<int, UCMatch> ret = new SortedList<int, UCMatch>();
            if (originalText == null)
                return ret;

            string ucId = "<uc:";
            string ctrlId = "<ctrl:";
            string closingBr = "/>";

            string text = originalText.ToLower();

            int firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ucId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new ApplicationException("User control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, true);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            firstIndex = 0;

            while (firstIndex != -1)
            {
                int idx = text.IndexOf(ctrlId, firstIndex);
                if (idx != -1)
                {
                    int idx2 = text.IndexOf(closingBr, idx + 1);
                    if (idx2 == -1)
                        throw new ApplicationException("Web control tag is missing the closing angular bracket");
                    string tag = originalText.Substring(idx, idx2 - idx + closingBr.Length);
                    UCMatch match = new UCMatch(tag, idx, false);
                    ret[match.Index] = match;
                    firstIndex = idx + match.Length;
                }
                else
                    break;
            }

            return ret;

        }

        void ProcessUC(string docHtml)
        {

            SortedList<int, UCMatch> allMatches = GetUCMatches(docHtml);


            //add first part of the template, before the first UC
            if (allMatches.Count > 0)
            {
                int firstOccurence = allMatches.Keys[0];
                if (allMatches[firstOccurence].Index > 0)
                {
                    string firstPart = docHtml.Substring(0, allMatches[firstOccurence].Index);
                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(firstPart));
                }
            }

            for (int k = 0; k < allMatches.Count; k++)
            {
                UCMatch m = allMatches[allMatches.Keys[k]];

                // add the middle template parts (between controls)
                if (k > 0)
                {
                    UCMatch previousMatch = allMatches[allMatches.Keys[k - 1]];
                    int start = previousMatch.Index + previousMatch.Length;
                    int end = m.Index - 1;
                    string templateText = docHtml.Substring(start, end - start + 1);
                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(templateText));
                }

                if (m.IsUC) // this is an usercontrol
                {
                    string tag = m.Value;

                    string ucName = GetUCName(tag);
                    Control uc = TemplateControl.LoadControl(ucName + ".ascx");

                    string ucID = System.IO.Path.GetFileNameWithoutExtension(ucName);
                    int count = 0;
                    bool found = true;
                    while (found)
                    {
                        if (DocumentBody.FindControl(ucID + "_" + count.ToString()) != null)
                        {
                            found = true;
                            count++;
                        }
                        else
                            found = false;
                    }
                    ucID += "_" + count.ToString();
                    uc.ID = ucID;

                    DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, uc);

                    Hashtable controlParameters = GetUCParameters(tag);

                    (uc as IDynamicUC).SetParameters(controlParameters);
                    (uc as IDynamicUC).DynamicUCEvent += new DynamicUCEventHandler(XDocUC_DynamicUCEvent);
                    (uc as IDynamicUC).RegisterScript += new RegisterScriptEventHandler(XDocUCA_RegisterScript);
                }
                else // this is a web control
                {
                    string typeName = GetWebControlName(m.Value);
                    if (typeName == "")
                        throw new Exception("The web control " + m.Value + " does not have a type name");

                    Hashtable tagAttributes = ParseUtils.TagAttributes(m.Value);

                    string path = ParseUtils.GetTagAttribute(tagAttributes, "assembly");
                    if (path == "")
                        throw new Exception("The web control " + m.Value + " does not have an assembly attribute");

                    path = System.IO.Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Bin"), path);

                    Assembly assmbl = Assembly.LoadFrom(path);

                    Control ctrl = assmbl.CreateInstance(typeName) as Control;

                    if (ctrl != null)
                    {
                        string ctrlID = tagAttributes["id"] as String;
                        if (ctrlID != "")
                        {
                            ctrl.ID = ctrlID;

                            IDictionaryEnumerator en = tagAttributes.GetEnumerator();
                            while (en.MoveNext())
                            {
                                if (en.Key.ToString().ToLower() == "assembly" || en.Key.ToString().ToLower() == "id")
                                    continue;

                                PropertyInfo pi = assmbl.GetType(typeName).GetProperty(en.Key.ToString());
                                pi.SetValue(ctrl, en.Value.ToString(), null);

                            }

                            DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, ctrl);
                            ////MIMI
                            //m_ctrl = m.Value;
                        }
                    }
                }
            }

            // add the last template part

            if (allMatches.Count > 0)
            {
                int lastOccurence = allMatches.Keys[allMatches.Count - 1];
                if (allMatches[lastOccurence].Index > 0)
                {
                    int index = allMatches[lastOccurence].Index + allMatches[lastOccurence].Length;
                    if (index < docHtml.Length)
                    {
                        string lastPart = docHtml.Substring(index);
                        DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(lastPart));
                    }
                }
            }
            else // there is no user control or web control
            {
                DocumentBody.Controls.AddAt(DocumentBody.Controls.Count, new LiteralControl(docHtml));
            }

            string separator = this.ClientID == "" ? "" : "_";
            btnSaveDocument.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            btnSaveDocumentNoUpdates.Attributes["onclick"] = this.ClientID + separator + "GetDivContent();";
            if (!xt.IsData) AddUniqueMarkup(DocumentBody, xt.UniqueID);
            AddScript(docHtml);
        }

        void XDocUC_DynamicUCEvent(object sender, DynamicUCEventArgs e)
        {
            SetAnswer(e.EventParameter.ToString());
        }

        void XDocUCA_RegisterScript(object sender, RegisterScriptEventArgs e)
        {
            string script = e.ScriptBlock;
            foreach (string urlInclude in e.Includes)
                script += " " + GetIncludedScript(urlInclude);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), script, e.AddScriptTags);

        }

        #endregion Dynamic Usercontrols

        private class UCMatch
        {
            public int Index = -1;
            public string Value = "";
            public bool IsUC = true;

            public int Length
            {
                get
                {
                    if (Value != null)
                        return Value.Length;
                    return 0;

                }
            }

            public UCMatch(string value, int index, bool isUC)
            {
                Index = index;
                Value = value;
                IsUC = isUC;
            }

            public override string ToString()
            {
                if (Value != null)
                    return Value;
                return "";
            }
        }

    }
}
