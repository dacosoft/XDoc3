using System.Collections;
using System.Collections.ObjectModel;
using System.Web;

namespace XDocuments
{

    public interface IXManagerImportExport
    {
        string ImportTemplate(int ConnID, string TemplateName, string TemplateXML);
        string ImportTemplate(int ConnID, int TemplateID, string TemplateXML);
        string ImportTemplate(string Conn, string TemplateName, string TemplateXML);
        string ImportTemplate(string Conn, int TemplateID, string TemplateXML);
        string ExportTemplatesLike(int ConnID, string sTemplatePattern,string sPostfix);
        string ExportTemplate(int ConnID, string TemplateName);
        string ExportTemplate(int ConnID, int TemplateID);
        string ExportTemplatesLike(string sConn, string sTemplatePattern, string sPostfix);
        string ExportTemplate(string Conn, string TemplateName);
        string ExportTemplate(string Conn, int TemplateID);
        string RemoveTemplate(int ConnID, string TemplateName);
        string RemoveTemplate(int ConnID, int TemplateID);
        string RemoveTemplate(string Conn, string TemplateName);
        string RemoveTemplate(string Conn, int TemplateID);
        string UserLogin { get; }
        string UserName { get; }
        int UserID { get; }
        string ServerName { get; }
        string DatabaseName { get; }
        object myHttpContext { get; }
        void ReadSFile(string sKey, out string sFileName, out  string sFileExtension, out byte[] aFileContent, out string sFileContentType);
        string WriteSFile(string sKey, string sFileName, string sFileExtension, byte[] aFileContent, string sFileContentType);
    }

}