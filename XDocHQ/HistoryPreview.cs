using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using KubionDataNamespace;
using KubionLogNamespace;
using XDocuments;

namespace XDocHQ
{
    public partial class HistoryPreview : Form
    {
        #region Members

            private int _TempHistID;
            private int _tempID;
            private int _oriHeight;
            private int _oriSplitHeigt;
            private IDName[] _connections;
            private bool _blnDocTypeRights;
            private int _posLeft;           //Waarde die aangeeft waar het historieoverzicht staat, zodat dit formulier ernaast geplaatst kan worden.
            private int _posTop;            //Waarde die aangeeft waar het historieoverzicht staat, zodat dit formulier ernaast geplaatst kan worden.            
            private bool _blnIsSQLite = false;

        #endregion Members

        #region Event Handlers

            private void HistoryPreview_ResizeEnd(object sender, System.EventArgs e)
            {
                int iNcrease = this.Height - _oriHeight;

                if (iNcrease > 0)
                {
                    splitContainer1.SplitterDistance = _oriSplitHeigt + iNcrease;
                }
            }

            private void HistoryPreview_Load(object sender, System.EventArgs e)
            {
                _oriHeight = this.Height;
                _oriSplitHeigt = splitContainer1.SplitterDistance;

                GetHistoryData();                       //Vul alle velden met de historiegegevens
                txtID.Text = _tempID.ToString();
                txtHistoryID.Text = _TempHistID.ToString();
                this.Text = "Preview template history   -   TemplateID = " + _tempID + "   -   TemplateHistoryID = " + _TempHistID; //Titel van het form aanpassen

                rtbContent.Focus();
                rtbContent.SelectionStart = 0;          //Cursor aan het begin
                rtbContent.SelectionLength = 0;         //We willen dat er niks geselecteerd is

                FillQueryGrid();

                PositionRight();
            }

            private void rtbContent_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.Control && e.KeyCode == Keys.A)
                {
                    rtbContent.SelectAll();
                }
            }
        
            private void dgvQueries_DoubleClick(object sender, System.EventArgs e)
            {
                if (dgvQueries.SelectedRows.Count > 0)
                {   //Hulp: QueryID as ID, QueryName as Name, QueryText as Text, ConnID 
                    int Qid;
                    int.TryParse(dgvQueries.SelectedRows[0].Cells["ID"].Value.ToString(),out Qid);                    
                    string Qname = (string)dgvQueries.SelectedRows[0].Cells["Name"].Value;
                    string Qtext = (string)dgvQueries.SelectedRows[0].Cells["Text"].Value;
                    int intConn;
                    int.TryParse(dgvQueries.SelectedRows[0].Cells["ConnID"].Value.ToString(), out intConn);
                    string strConn = "";
                    foreach (IDName conn in _connections)
                    {
                        if (conn.ID == intConn)
                        {
                            strConn = conn.Name;
                        }
                    }
                    
                    QueryForm frm = new QueryForm(Qid,Qname,Qtext,_tempID,_TempHistID,strConn);

                    frm.Show(this);
                }
            }

            private void HistoryPreview_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Escape)
                {
                    this.Close();
                }
            }

            private void HistoryPreview_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
            {
                this.Owner.Focus();     //Dit was nodig, omdat na het openen van de preview van de queryhistorie, de focus niet meer op het historiescherm stond, maar op het mainform
                HistoryForm histFrm = (HistoryForm)this.Owner;
                histFrm.CountChildForms -= 1;   //Verlaag het aantal op met 1, in het moederformulier, omdat er nu een gesloten is!
            }
             
        #endregion Event Handlers
        
        #region Constructor

            public HistoryPreview(int HistTempID, int tempID, IDName[] connections, bool blnDocTypeRights, int position_left, int position_top, bool blnIsSQLite)
            {
                _TempHistID = HistTempID;
                _tempID = tempID;
                _connections = connections;
                _blnDocTypeRights = blnDocTypeRights;
                _posLeft = position_left;
                _posTop = position_top;
                _blnIsSQLite = blnIsSQLite;

                InitializeComponent();                
            }

        #endregion Constructor

        #region Methods

            private void PositionRight()
            {
                HistoryForm histFrm = (HistoryForm)this.Owner;

                _posTop = _posTop + histFrm.CountChildForms * 30;  //Schuif naar beneden evenredig met het aantal PreviewForms, zodat het duidelijk is dat er meerdere geopend zijn!                
                this.Location = new Point(_posLeft, _posTop);  //Zet het formulier netjes naast het bovenliggende formulier, zodat er een goed overzicht is en er makkelijk een volgend template gekozen kan worden.                     

                histFrm.CountChildForms += 1;   //Hoog het aantal op met 1, in het moederformulier
            }

            private void GetHistoryData()
            {                
                ClientData cd = null;
                DateTime times;
                string strBln;
                DataTable dt;
                string strTemp;

                try
                {
                    base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                                    

                    if (System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                    {
                        cd = new ClientData(System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                                                                    

                        StringBuilder sbSQL = new StringBuilder("SELECT TemplateName, TemplateDescription, TemplateContent, TemplateToSave, TemplateToRedirect, Unattended, DataTemplate, ");
                        if (_blnDocTypeRights)
                        {
                            sbSQL.Append("Rights, DocType, ");
                        }
                        if (_blnIsSQLite)
                        {
                            sbSQL.Append("DateTime(StartDate) as StartDate, DateTime(EndDate) as EndDate, ");
                        }
                        else
                        {
                            sbSQL.Append("StartDate, EndDate, ");
                        }
                        sbSQL.Append("StartUser, EndUser, Actual " + 
                                      "FROM S_Templates_Hist " +
                                      "WHERE (TemplateHistID = " + _TempHistID.ToString() + ") ");
                      
                        dt = cd.DTExecuteQuery(sbSQL.ToString());
                        
                        txtName.Text = dt.Rows[0]["TemplateName"].ToString();
                        txtDescription.Text = dt.Rows[0]["TemplateDescription"].ToString();
                        rtbContent.Text = dt.Rows[0]["TemplateContent"].ToString();
                        txtRedirect.Text = dt.Rows[0]["TemplateToRedirect"].ToString();
                        txtStartUser.Text = dt.Rows[0]["StartUser"].ToString();
                        txtEndUser.Text = dt.Rows[0]["EndUser"].ToString();

                        if (DateTime.TryParse(dt.Rows[0]["StartDate"].ToString(), out times))
                        {
                            txtStartDate.Text = (times).ToString("d MMMM yyyy, HH:mm:ss");
                        }
                        else
                        {
                            txtStartDate.Text = "error retrieving correct date";
                        }

                        strTemp = dt.Rows[0]["EndDate"].ToString();
                        if (string.IsNullOrEmpty(strTemp))
                        {
                            txtEndDate.Text = "Actuele template; niet beeindigd";
                        }
                        else
                        {
                            if (DateTime.TryParse(strTemp, out times))
                            {
                                txtEndDate.Text = (times).ToString("d MMMM yyyy, HH:mm:ss");
                            }
                            else
                            {
                                txtEndDate.Text = "error retrieving correct date";
                            }
                        }

                        strBln = dt.Rows[0]["TemplateToSave"].ToString().ToLower();
                        if (strBln == "1" || strBln == "true")
                        {
                            chkToSave.Checked = true;
                        }
                        else
                        {
                            chkToSave.Checked = false;
                        }

                        strBln = dt.Rows[0]["Unattended"].ToString().ToLower();
                        if (strBln == "1" || strBln == "true")
                        {
                            chkUnattended.Checked = true;
                        }
                        else
                        {
                            chkUnattended.Checked = false;
                        }

                        strBln = dt.Rows[0]["DataTemplate"].ToString().ToLower();
                        if (strBln == "1" || strBln == "true")
                        {
                            chkDataTemplate.Checked = true;
                        }
                        else
                        {
                            chkDataTemplate.Checked = false;
                        }

                        strBln = dt.Rows[0]["Actual"].ToString().ToLower();
                        if (strBln == "1" || strBln == "true")
                        {
                            chkActual.Checked = true;
                        }
                        else
                        {
                            chkActual.Checked = false;
                        }

                        if (_blnDocTypeRights)
                        {   //Hebben we met een s_templates te maken met Rights en DocType?
                            txtRights.Text = dt.Rows[0]["Rights"].ToString();
                            txtDocType.Text = dt.Rows[0]["DocType"].ToString();
                        }
                        else
                        {   //Maak duidelijk dat Right en DocType niet gevuld kan worden, omdat de kolommen niet voor komen
                            txtRights.Text = "Not available";
                            txtDocType.Text = "Not available";
                        }

                        //Before this form was filled using a datareader cd.GetDataReader
                    //    if (reader.HasRows)
                    //    {
                    //        while (reader.Read())
                    //        {
                    //            txtName.Text = reader["TemplateName"].ToString();
                    //            txtDescription.Text = reader["TemplateDescription"].ToString();
                    //            rtbContent.Text = reader["TemplateContent"].ToString();

                    //            o = reader["TemplateToSave"];   //Als ik de waarde niet eerste toeken, dan krijg ik een Error omdat ik een verkeerde Ordinal van de datareader aanspreek, dus nu even zo!
                    //            if (o != DBNull.Value)
                    //            {   
                    //                chkToSave.Checked = (bool)o;
                    //            }

                    //            txtRedirect.Text = reader["TemplateToRedirect"].ToString();

                    //            o = reader["Unattended"];
                    //            if (o != DBNull.Value)
                    //            {
                    //                chkUnattended.Checked = (bool)o;
                    //            }

                    //            o = reader["DataTemplate"];
                    //            if (o != DBNull.Value)
                    //            {
                    //                chkDataTemplate.Checked = (bool)o;
                    //            }

                    //            if (_blnDocTypeRights)
                    //            {   //Hebben we met een s_templates te maken met Rights en DocType?
                    //                txtRights.Text = reader["Rights"].ToString();
                    //                txtDocType.Text = reader["DocType"].ToString();
                    //            }
                    //            else
                    //            {   //Maak duidelijk dat Right en DocType niet gevuld kan worden, omdat de kolommen niet voor komen
                    //                txtRights.Text = "Not available";
                    //                txtDocType.Text = "Not available";
                    //            }
                    //            txtStartUser.Text = reader["StartUser"].ToString();

                    //            o = reader["StartDate"];
                    //            if (o != DBNull.Value)
                    //            {
                    //                txtStartDate.Text = ((DateTime)o).ToString("d MMMM yyyy, HH:mm:ss");
                    //            }
                                
                    //            txtEndUser.Text =  reader["EndUser"].ToString();

                    //            o = reader["EndDate"];
                    //            if (o != DBNull.Value)
                    //            {
                    //                txtEndDate.Text = ((DateTime)o).ToString("d MMMM yyyy, HH:mm:ss");                                    
                    //            }

                    //            o = reader["Actual"];
                    //            if (o != DBNull.Value)
                    //            {
                    //                chkActual.Checked = (bool)o;
                    //            }
                    //        }
                    //    }
                    //    reader.Close();
                    //
                    }
                    else
                    {
                        string message = "Error finding the the connection string XDocConnectionString.";
                        MessageBox.Show(message, "Error in the config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        KubionLog.WriteLine(message);
                    }

                }
                catch (Exception ex)
                {
                    string message = "An error occured while retrieving the history data.\n\n" + ex.Message;
                    KubionLog.WriteLine(message);
                    MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (cd != null)
                    {
                        cd.Dispose();
                    }
                    base.Cursor = Cursors.Arrow;                    
                }
            }

            private void FillQueryGrid()    //Haal de queries op
            {
                string strSQL;

                ClientData cd = null;

                try
                {
                    base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                                    

                    if (System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                    {
                        cd = new ClientData(System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                    

                        strSQL =    "SELECT QueryID as ID, QueryName as Name, QueryText as Text, ConnID " +
                                        "FROM S_Queries_Hist " +
                                        "WHERE (TemplateHistID = " + _TempHistID + ") ";

                        dgvQueries.DataSource = cd.DTExecuteQuery(strSQL);

                        dgvQueries.Columns["ID"].Width = 60;
                        dgvQueries.Columns["ConnID"].Width = 60;
                        dgvQueries.Columns["Text"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                    }
                    else
                    {
                        MessageBox.Show("Cannont find connection string XDocConnectionString", "Error in config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    string message = "An error occured while retrieving the history data.\n\n" + ex.Message;
                    KubionLog.WriteLine(message);
                    MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);                    
                }
                finally
                {
                    if (cd != null)
                    {
                        cd.Dispose();
                    }
                    base.Cursor = Cursors.Arrow;                    
                }
            }

            public void Reload(int HistTempID)  //Er is in het moederscherm een ander template geselecteerd, dat nu getoond moet worden.
            {   
                //Alles even leeg maken, anders zien we data van het vorige formulier staan, wanneer de query niks invult:
                txtName.Text = string.Empty;
                txtDescription.Text = string.Empty;
                rtbContent.Text = string.Empty;
                chkToSave.Checked = false;
                txtRedirect.Text = string.Empty;              
                chkUnattended.Checked = false;
                chkDataTemplate.Checked = false;       
                txtRights.Text = string.Empty; 
                txtDocType.Text = string.Empty;                  
                txtStartUser.Text = string.Empty; 
                txtStartDate.Text = string.Empty;              
                txtEndUser.Text = string.Empty; 
                txtEndDate.Text = string.Empty; 
                chkActual.Checked = false;
                txtHistoryID.Text  = string.Empty;                
                ((DataTable)dgvQueries.DataSource).Clear();

                _TempHistID = HistTempID;
                
                GetHistoryData();                       //Vul alle velden met de historiegegevens                
                txtHistoryID.Text = _TempHistID.ToString();
                this.Text = "Preview template history   -   TemplateID = " + _tempID + "   -   TemplateHistoryID = " + _TempHistID; //Titel van het form aanpassen
                                
                rtbContent.SelectionStart = 0;          //Cursor aan het begin
                rtbContent.SelectionLength = 0;         //We willen dat er niks geselecteerd is

                FillQueryGrid();

            }

        #endregion Methods         
   }

}