using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XDocuments;
using KubionLogNamespace;


namespace XDocHQ
{
    public partial class QueryForm : Form
    {
        #region Members

            IDName[] m_connections = null;
            IXQuery m_query = null;
            SearchReplaceForm _SearchReplaceForm;
            StringObject _strobjVervangterm;
            ToolStripTextBox _tbxZoektermMain;
            bool _dirty = false;
            bool _OkClick = false;

        #endregion Members

        #region Constructor

            public QueryForm(IDName[] connections, IXQuery query, ref StringObject strobjVervangterm, ref ToolStripTextBox tbxZoektermMain)
            {
                InitializeComponent();
                
                m_connections = connections;
                m_query = query;

                cmbConnections.Items.Add("<choose>");
                foreach (IDName conn in m_connections)
                {
                    cmbConnections.Items.Add(conn);
                }
                cmbConnections.SelectedIndex = 0;


                txtName.Text = m_query.Name;
                txtName.SelectionStart = 0;
                txtName.SelectionLength = 0;    //Ik had problemen met het geselecteerd zijn van de tekst in txtName, vandaar deze twee regels.
                tbxText.Text = m_query.QueryText;
                if (m_query.TemplateID != -1)
                    this.Text += "  TemplateID = " + m_query.TemplateID.ToString();
                foreach (object item in cmbConnections.Items)
                {
                    if (item is IDName && ((IDName)item).ID == m_query.ConnectionID)
                    {
                        cmbConnections.SelectedItem = item;
                        break;
                    }
                }

                _strobjVervangterm = strobjVervangterm; //We krijgen uit het MainForm de laatstgebruikte Vervangterm mee. Die moeten we weer door kunnen geven aan het zoek-en-vervangscherm
                _tbxZoektermMain = tbxZoektermMain;     //De referentie naar de zoektermbox in het MainForm wordt vastgelegd, zodat hij doorgegeven kan worden aan het zoek-en-vervangscherm

                _dirty = false;     //tekstvelen en cmb zijn ingevuld, dus waren dirty, nu weer even op niet-dirty zetten
            }

            public QueryForm(int qid, string qname, string qtext, int templateID, int tempHistID, string conn)
            {
                InitializeComponent();
                                
                cmbConnections.Items.Add(conn);
                cmbConnections.SelectedIndex = 0;

                txtName.Text = qname;
                tbxText.Text = qtext;
                this.Text += "     QueryID = " + qid.ToString() + "  -  TemplateID = " + templateID.ToString() + "  -  TemplateHistID = " + tempHistID.ToString();                

                txtName.ReadOnly = true;
                txtName.SelectionLength = 0;
                tbxText.ReadOnly = true;
                cmbConnections.Enabled = false;

                btnOK.Visible = false;
                btnCancel.Text = "OK";
                btnZoekVervang.Visible = false;

                _dirty = false;     //We willen geen melding dat er iets gewijzigd is
            }

        #endregion Constructor

        #region Eventhandlers
         
            private void QueryForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
            {
                if (_dirty)
                {
                    DialogResult dlgRes = MessageBox.Show("The query has been changed.\nAre you sure you want to discard changes?",
                                                            "Discard changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (dlgRes == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    if (_OkClick && string.IsNullOrEmpty(m_query.QueryText))    //Empty text is no acceptable.
                    {
                        MessageBox.Show("The query text is empty.\nThis is not allowed. Saving of the template will fail.\n\nPlease add a text or cancel the query editing.",
                                                            "Text cannot be empty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        _OkClick = false;   //Forget that Ok was clicked.
                    }
                }
            }

            private void txtName_TextChanged(object sender, System.EventArgs e)
            {
                _dirty = true;  //Waarschuwing bij annuleren wijzigingen... 
            }

            private void tbxText_TextChanged(object sender, System.EventArgs e)
            {
                _dirty = true;  //Waarschuwing bij annuleren wijzigingen...   
            }

            private void cmbConnections_SelectedIndexChanged(object sender, System.EventArgs e)
            {
                _dirty = true;  //Waarschuwing bij annuleren wijzigingen...
            }
                
            private void btnOK_Click(object sender, EventArgs e)
            {
                _OkClick = true;
                m_query.QueryText = tbxText.Text;
                m_query.Name = txtName.Text;

                if (cmbConnections.SelectedIndex == -1 || cmbConnections.SelectedIndex == 0)
                {
                    m_query.ConnectionID = -1;
                }
                else
                {
                    m_query.ConnectionID = ((IDName)cmbConnections.SelectedItem).ID;
                }

                _dirty = false;     //We willen geen melding meer dat wijzigingen verloren gaan.
            }

            private void QueryForm_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.Control == true)
                {
                    if (e.KeyCode == Keys.H)
                    {
                        SandRKeys(ref sender,ref e);
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        FindKeys(ref sender,ref e);
                    }
                }

            }

            private void tbxText_KeyDown(object sender, KeyEventArgs e)
            {                
                if (e.Control == true)
                {
                    if (e.KeyCode == Keys.A)
                    {
                        tbxText.SelectAll();   
                    }
                    else if (e.KeyCode == Keys.H)
                    {
                        SandRKeys(ref sender,ref e);
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        FindKeys(ref sender,ref e);
                    }
                }
                else if(e.KeyCode == Keys.F3)
                {
                    if ((_SearchReplaceForm == null) || (_SearchReplaceForm.IsDisposed))
                    {
                        btnZoek_Click(sender, e);
                    }
                    else
                    {
                        _SearchReplaceForm.btnVolgende_Click(sender, e); 
                    }
                }
            }

            private void txtName_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
            {
                if (e.Control == true)
                {
                    if (e.KeyCode == Keys.H)
                    {
                        SandRKeys(ref sender, ref e);
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        FindKeys(ref sender, ref e);
                    }
                }
            }

            private void btnZoekVervang_Click(object sender, EventArgs e)
            {
                try
                {
                    if ((_SearchReplaceForm == null) || (_SearchReplaceForm.IsDisposed))
                    {
                        _SearchReplaceForm = new SearchReplaceForm(ref _strobjVervangterm, ref tbxText, ref _tbxZoektermMain, false);   //Geef Zoekterm hoofdlettergevoelig mee, en geef de huidige tekstbox mee
                        _SearchReplaceForm.Show(this);
                    }
                    else
                    {                   
                        _SearchReplaceForm.Focus(false);
                    }

                    if (tbxText.SelectionLength > 0)
                    {
                        _SearchReplaceForm.SetZoekTerm(tbxText.SelectedText);
                    }
                }
                catch (Exception ex)
                {
                    string message = "An error occured while showing the Search-and-Replace form.\n\n" + ex.Message;
                    MessageBox.Show(message, "Error opening Search-and-Replace form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                }
            }

            private void btnZoek_Click(object sender, EventArgs e)
            {
                try
                {
                    if ((_SearchReplaceForm == null) || (_SearchReplaceForm.IsDisposed))
                    {
                        _SearchReplaceForm = new SearchReplaceForm(ref _strobjVervangterm, ref tbxText, ref _tbxZoektermMain, true);   //Geef Zoekterm hoofdlettergevoelig mee, en geef de huidige tekstbox mee
                        _SearchReplaceForm.Show(this);
                    }
                    else
                    {
                        _SearchReplaceForm.Focus(true); //True means Searchonly=true
                    }

                    if (tbxText.SelectionLength > 0)
                    {
                        _SearchReplaceForm.SetZoekTerm(tbxText.SelectedText);
                    }
                }
                catch (Exception ex)
                {
                    string message = "An error occured while showing the Search-and-Replace form.\n\n" + ex.Message;
                    MessageBox.Show(message, "Error opening Search-and-Replace form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                }
            }

            private void btnCancel_Click(object sender, EventArgs e)
            {
                this.Close();
            }
        
        #endregion  Eventhandlers

        #region Methods

            private void SandRKeys(ref object sender,ref KeyEventArgs e)
            {
                //Ctrl+H = zoek-en-vervang
                btnZoekVervang_Click(sender, EventArgs.Empty);
                e.SuppressKeyPress = true;
                e.Handled = true;               //De Ctrl+H-toets mag niet verder behandeld worden, want dan veroorzaakt hij een backspace in de huidige tekstbox
            }

            private void FindKeys(ref object sender,ref KeyEventArgs e)
            {
                //Ctrl+H = zoek-en-vervang
                btnZoek_Click(sender, EventArgs.Empty);
            }

        #endregion Methods
    }
}