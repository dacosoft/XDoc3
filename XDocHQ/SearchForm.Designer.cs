namespace XDocHQ
{
    public partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.btnZoek = new System.Windows.Forms.Button();
            this.tbxZoekterm = new System.Windows.Forms.TextBox();
            this.lblZoekTerm = new System.Windows.Forms.Label();
            this.dgvFoundTemplates = new System.Windows.Forms.DataGridView();
            this.lblResultaten = new System.Windows.Forms.Label();
            this.lblNiksGevonden = new System.Windows.Forms.Label();
            this.cbxTelTreffers = new System.Windows.Forms.CheckBox();
            this.imgInfo = new System.Windows.Forms.PictureBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.ContextMenuItemNewWindow = new System.Windows.Forms.MenuItem("Open template in new window."); //Zelf toegevoegd voor de right-mouse-click op het grid
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoundTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnZoek
            // 
            this.btnZoek.Image = ((System.Drawing.Image)(resources.GetObject("btnZoek.Image")));
            this.btnZoek.Location = new System.Drawing.Point(15, 406);
            this.btnZoek.Name = "btnZoek";
            this.btnZoek.Size = new System.Drawing.Size(127, 25);
            this.btnZoek.TabIndex = 3;
            this.btnZoek.Text = "     Search";
            this.btnZoek.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnZoek.UseVisualStyleBackColor = true;
            this.btnZoek.Click += new System.EventHandler(this.btnZoek_Click);
            // 
            // tbxZoekterm
            // 
            this.tbxZoekterm.Location = new System.Drawing.Point(12, 25);
            this.tbxZoekterm.Name = "tbxZoekterm";
            this.tbxZoekterm.Size = new System.Drawing.Size(286, 20);
            this.tbxZoekterm.TabIndex = 1;
            this.tbxZoekterm.DoubleClick += new System.EventHandler(this.tbxZoekterm_DoubleClick);
            this.tbxZoekterm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbxZoekterm_KeyUp);
            // 
            // lblZoekTerm
            // 
            this.lblZoekTerm.AutoSize = true;
            this.lblZoekTerm.Location = new System.Drawing.Point(12, 9);
            this.lblZoekTerm.Name = "lblZoekTerm";
            this.lblZoekTerm.Size = new System.Drawing.Size(55, 13);
            this.lblZoekTerm.TabIndex = 4;
            this.lblZoekTerm.Text = "Searchterm:";
            // 
            // dgvFoundTemplates
            // 
            this.dgvFoundTemplates.AllowUserToAddRows = false;
            this.dgvFoundTemplates.AllowUserToDeleteRows = false;
            this.dgvFoundTemplates.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvFoundTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFoundTemplates.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvFoundTemplates.Location = new System.Drawing.Point(12, 74);
            this.dgvFoundTemplates.MultiSelect = false;
            this.dgvFoundTemplates.Name = "dgvFoundTemplates";
            this.dgvFoundTemplates.RowHeadersVisible = false;
            this.dgvFoundTemplates.ShowEditingIcon = false;
            this.dgvFoundTemplates.Size = new System.Drawing.Size(286, 326);
            this.dgvFoundTemplates.TabIndex = 2;
            this.dgvFoundTemplates.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgvFoundTemplates_MouseDoubleClick);
            this.dgvFoundTemplates.MouseClick += new System.Windows.Forms.MouseEventHandler(dgvFoundTemplates_MouseClick);
            // 
            // lblResultaten
            // 
            this.lblResultaten.AutoSize = true;
            this.lblResultaten.Location = new System.Drawing.Point(9, 58);
            this.lblResultaten.Name = "lblResultaten";
            this.lblResultaten.Size = new System.Drawing.Size(116, 13);
            this.lblResultaten.TabIndex = 6;
            this.lblResultaten.Text = "Searchterm found in:";
            // 
            // lblNiksGevonden
            // 
            this.lblNiksGevonden.AutoSize = true;
            this.lblNiksGevonden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNiksGevonden.Location = new System.Drawing.Point(12, 238);
            this.lblNiksGevonden.Name = "lblNiksGevonden";
            this.lblNiksGevonden.Size = new System.Drawing.Size(271, 15);
            this.lblNiksGevonden.TabIndex = 2;
            this.lblNiksGevonden.Text = "De zoekterm komt in geen enkele template voor!";
            this.lblNiksGevonden.Visible = false;
            // 
            // cbxTelTreffers
            // 
            this.cbxTelTreffers.AutoSize = true;
            this.cbxTelTreffers.Location = new System.Drawing.Point(192, 411);
            this.cbxTelTreffers.Name = "cbxTelTreffers";
            this.cbxTelTreffers.Size = new System.Drawing.Size(91, 17);
            this.cbxTelTreffers.TabIndex = 7;
            this.cbxTelTreffers.Text = "Count the hits";
            this.cbxTelTreffers.UseVisualStyleBackColor = true;
            // 
            // imgInfo
            // 
            this.imgInfo.Cursor = System.Windows.Forms.Cursors.Help;
            this.imgInfo.Image = ((System.Drawing.Image)(resources.GetObject("imgInfo.Image")));
            this.imgInfo.InitialImage = ((System.Drawing.Image)(resources.GetObject("imgInfo.InitialImage")));
            this.imgInfo.Location = new System.Drawing.Point(278, 51);
            this.imgInfo.Name = "imgInfo";
            this.imgInfo.Size = new System.Drawing.Size(20, 20);
            this.imgInfo.TabIndex = 8;
            this.imgInfo.TabStop = false;
            this.imgInfo.Tag = "xfghxfghxth";
            this.imgInfo.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Cursor = System.Windows.Forms.Cursors.Help;
            this.lblInfo.Location = new System.Drawing.Point(244, 58);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(28, 13);
            this.lblInfo.TabIndex = 9;
            this.lblInfo.Text = "Info:";
            this.lblInfo.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 439);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.imgInfo);
            this.Controls.Add(this.cbxTelTreffers);
            this.Controls.Add(this.btnZoek);
            this.Controls.Add(this.dgvFoundTemplates);
            this.Controls.Add(this.lblNiksGevonden);
            this.Controls.Add(this.tbxZoekterm);
            this.Controls.Add(this.lblZoekTerm);
            this.Controls.Add(this.lblResultaten);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchForm";
            this.Text = "Search overview";
            this.Load += new System.EventHandler(this.SearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoundTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
            //            
            // ContextMenuItemNewWindow     // Zelf toegevoegd contextmenu voor de rechter muisknop op het dgvTemplates
            //            
            this.ContextMenuItemNewWindow.Click += new System.EventHandler(ContextMenuItemNewWindow_Click);           

        }
        #endregion

        private System.Windows.Forms.Button btnZoek;
        internal System.Windows.Forms.TextBox tbxZoekterm;
        private System.Windows.Forms.Label lblZoekTerm;
        private System.Windows.Forms.DataGridView dgvFoundTemplates;
        private System.Windows.Forms.Label lblResultaten;
        private System.Windows.Forms.Label lblNiksGevonden;
        private System.Windows.Forms.CheckBox cbxTelTreffers;
        private System.Windows.Forms.PictureBox imgInfo;        
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.MenuItem ContextMenuItemNewWindow;
    }
}