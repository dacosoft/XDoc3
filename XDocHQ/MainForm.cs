using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XDocuments;
using System.Collections;
using System.IO;
using KubionLogNamespace;
using System.Diagnostics;
using System.Configuration;
using System.Threading;
using KubionDataNamespace;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
//using System.Reflection;
//using System.Security;
//using System.Security.Principal;
//using System.DirectoryServices;
 
namespace XDocHQ
{
    public partial class MainForm : Form
    {

        #region Members

        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));    //Om dynamisch plaatje van buttons te veranderen
        DataTable m_dtTemplates = null;
        XDocManager m_manager = null;
        public IXTemplate m_template = null;    //Dit is het geopende template
        public IDName[] m_connections = null;
        ArrayList m_queries = new ArrayList();
        ArrayList m_modifiedQueries = new ArrayList();
        ArrayList m_deletedQueries = new ArrayList();
        private int _iPos = -1; //Dit is de huidige positie waarop de geselecteerde tekst staat die door de tekstzoekfunctie gevonden is.
        private string _strZoekterm = "";
        private bool _blnZoekAllRecursive = false;
        SearchForm _SearchForm; //Het zoekformulier        
        SearchReplaceForm _SearchReplaceForm;   //Het zoek-en-vervangformulier
        public int _SearchIdClicked = -1; //Het ID dat in het zoekformulier is aangeklikt
        private int iSortingPos = 0; //Na sorting moet de cursor weer hetzelfde als voorheen gezet worden
        private int iSortingID = -1; //Na sorting werd er een ander template geselecteerd; nu gaan we de oude herstellen op deze waarde
        private bool _blnAfterSave = false; //Als we na het saven een template opnieuw selecteren, dan moet een stukje logica overgeslagen worden
        string m_caption = "";
        string m_connectiontext = "";
        public bool m_dirty = false;   //Er zijn gegevens gewijzigd, de template moet gesaved worden
        bool m_supressUIEvents = false;
        string _strXDocPath = (string)Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\XDocHQ\"; //De map (%USER%\Application Data\XDocHQ\) waarin tijdelijke bestanden voor Visual Studio worden opgeslagen
        int _BewaarDuur = 17;
        string _ReadMeFileName = "ReadMe.txt";  //Deze file wordt onder %USER%\Application Data\XDocHQ\ neergezet voor een beetje toelichting
        IntPtr _VSWindowHandle = IntPtr.Zero;   //Dit is de MainWindowHandle van VS als deze gevonden is 
        int _VSProcessID = -1;                  //Dit is het ProcessID van de Visual Studioinstantie die we gebruiken als externe editor
        bool _blnAlternativeEditor = false;     //Wordt er een andere editor dan VS gebruikt om .Xdoc's te openen?
        bool _blnAlterWarningShown = false;     //De waarschuwing hoeft maar een keer per sessie getoond te worden.                
        int _iOriginalTemplateID = -1;          //De waarde van het templateID dat in de tekstbox ID stond, voordat deze gewijzigd werd
        string _iOriginalTemplateName = "";     //De waarde van de templatenaam die in de tekstbox Name stond, voordat deze gewijzigd werd
        bool _BeingSaved = false;               //Als we aan in SaveTemplate zitten, dan moet er wel een nieuwe template geladen worden, ondanks dat m_suppressUIEvents aan staat
        public StringObject _strobjVervangterm = new StringObject();       //We willen onthouden welke term de laatstgebruikte vervangeterm was in het zoek-en-vervangscherm. Deze kunnen van verschillende plaatsen opgestart worden, dus op dit niveau opslaan. We hebben een object nodig, omdat we een referentie willen sturen die vanuit dieper liggende forms een waarde ingesteld krijgt.
        string _URLTryPars;                     //Een URL waarop een MakeXDocument.aspx draait, zodat het templateID geparsed kan worden.        
        bool _blnDocTypeRights;         //Hebben we te maken met een s_templatestabel die de kolommen DocType en Rights heeft?
        int _iComArgTempID = -2;                //Dit is het templateid dat als Commandline Argument is meegegeven.
        NameValueCollection _DynReportsColl;    //Deze verzameling bevat de key-valuepairs van de dynamische rapporten; we hoeven maar 1 keer de DocHQ.exe.config uit te lezen.
        NameValueCollection _DynProceduresColl;    //Deze verzameling bevat de key-valuepairs van de dynamische procedures; we hoeven maar 1 keer de DocHQ.exe.config uit te lezen.
        private FontFamily _fontFamily;         //Dit het is het standaardfont van de templatetextbox
        private float _fontSize;                //De huidige grootte van het font van de templatetekstbox        
        private DataTable m_dtRecTemplates;     //Dit is de datatable die het datagrid van de recente templates vult
        private int _oriGridSplitSize = 200;    //Dit is de originele grootte waarop de de splitbalk van het grid stond; nodig om bij een Expand het templatesgrid weer zichtbaar te maken op de oude grootte.        
        private NameValueCollection _lstRecTemplates;       //Dit is de chronologische lijst met alle, unieke, geselecteerde templates.        
        private int _iLstRecTempCapacity;                   //Dit is de startcapaciteit van de lijst. We zorgen dat hij niet overschreden wordt voor performance. Hij is gelijk aan de waarde in configfile + 10
        private int _IDinNewW = -2;             //Het ID waarop geklikt is in een grid met de rechter muisknop.        
        private ToolTip _GeneralToolTip = new ToolTip();    //This is aan tooltipmessage that can be reused by any control.
        private int _iPosSearchStart;           //This is the position we first started searching from                
        private bool _blnSearchEndReached = false;  //Have we reached the end of the document while searching?
        private bool _blnInSearchMode = false;  //Are we in searchmode? If so, we will search round from the starting point of the search, otherwise, reset starting point.        
        private bool _blnSearchEndReachedBackwards = false; //If it is true, searching backwards from the bottom will start in the queries. 
        public string _strMainConnString = ""; //The connection string will be read from the config file. It is efficient and neat to only read it once and put it in a global variable.
        private int _oriQuerySplitSize = -1;    //This is the original splitterdistance of the splitter between the Template text and the queries
        private string _strHideQueriesHoverText = "Click to hide";    //This is the hover-over text shown when hovering over the button to hide the queries
        private bool _BeingSorted = false;      //If the templatelist is being sorted, some events must be suppressed.
        private bool _blnIsSQLite = false;

        #endregion Members

        #region Constructor

        public MainForm(string Argument)
        {
            CheckXDocAttPath();   //Settings in de configfile refering to the Kubion namespace will be checked and corrected            
            CheckKubionTraceLevel();
            CheckKubionLogFile();

            string[] ArgParts = Argument.Split(new char[] { '=' });
            int iArgument = -2;

            if (ArgParts[0].ToLower() == "templateid")
            {
                if (Int32.TryParse(ArgParts[1], out iArgument))
                {
                    _iComArgTempID = iArgument;
                }
            }

            InitializeComponent();

            Version ver = new Version(Application.ProductVersion);
            m_caption += this.Text + "  " + ver.ToString() + "";
            //m_caption += this.Text + "  " + ver.Major.ToString() + ".";
            //if (ver.Minor.ToString().Length < 2)
            //{   //Toon x.0x
            //    m_caption += "0" + ver.Minor.ToString();
            //}
            //else
            //{   //Toon x.xx
            //    m_caption += ver.Minor.ToString();
            //}
            //if (ver.Build != 0)
            //{   //Toon revisionnummer als het bestaat; x.xx.x
            //    m_caption += "." + ver.Build.ToString();
            //}
            ConnectionText(); //Vul connectiontekst in achter titel van MainForm;            

            try
            {
                m_manager = new XDocManager(_strMainConnString);

                string s = m_manager.AdminDataProvider.ProviderType().ToString();

                if (m_manager.AdminDataProvider.ProviderType() == ProviderTypes.SQLite)
                {
                    _blnIsSQLite = true;
                }
            }
            catch (Exception ex)
            {
                string message = "There was a fatal error in MainForm()!\nThe application will be closed!\nAre there errors in the config file?\n\n" +
                                    ex.Message + "Stacktrace:\n" + ex.StackTrace;
                MessageBox.Show(message, "Error in MainForm()", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
                Environment.Exit(0);
            }

            _fontFamily = rtbContent.Font.FontFamily;   //Leg het standaardfont vast; nodig om de fontgrote te veranderen
            if (!float.TryParse(ConfigurationManager.AppSettings["DefaultFontSize"], out _fontSize))    //Haal standaard fontsize op 
            {
                _fontSize = 11.25F;   //If wrong value in config, then 11.25;
            }
            if (_fontSize < 7 || _fontSize > 14)
            {
                _fontSize = 11.25F;   //If wrong value in config, then 11.25;
            }
            rtbContent.Font = new Font(_fontFamily, _fontSize);
            rtbContent.SetCaretSize = Convert.ToInt16(_fontSize);   //Laat de caret meekrimpen met het font

            rtbContent.SetCaretSize = Convert.ToInt32(_fontSize);    //De maat voor de caret (tekstcursor) moet ook vastgelegd worden.                      

            //Om onduidelijke redenen wil de designview van het MainForm niet meer laden in VS wanneer ik deze images in MainForm.designer.cs zet, dus nu hier:
            //handle naar plaatje voor Checkbox aan voor customobject Checkbox in Toolstrip
            IntPtr hbmpTrue = ((System.Drawing.Bitmap)(resources.GetObject("Checkbox_true.Image"))).GetHbitmap();
            //handle naar plaatje voor Checkbox uit voor customobject Checkbox in Toolstrip
            IntPtr hbmpFalse = ((System.Drawing.Bitmap)(resources.GetObject("doorzoekAllesToolStripMenuItem.Image"))).GetHbitmap();

            btnDoorzoekAlles.hImageChecked = hbmpTrue;
            btnDoorzoekAlles.hImageUnchecked = hbmpFalse;
            doorzoekAllesToolStripMenuItem.hImageChecked = hbmpTrue;
            doorzoekAllesToolStripMenuItem.hImageUnchecked = hbmpFalse;

            //Deze wordt af-en-toe door de designer overschreven als hij in Main.Designer.cs staat, dus nu maar even hier gezet.
            btnDoorzoekAlles.Image = ((System.Drawing.Image)(resources.GetObject("doorzoekAllesToolStripMenuItem.Image")));

            CheckDataModel(); //We have some datamodel, with and without Right and DocType and with an without a TemplateContentExpanded column; Fill some boolean for later logic.

            //Leg het contextmenu (rechter muisknop) van dgvTemplates en dgvRecTemplates vast
            System.Windows.Forms.MenuItem[] ContextMenuNewWindowArray = { ContextMenuItemNewWindow, ContextMenuItemRefresh };
            dgvTemplates.ContextMenu = new System.Windows.Forms.ContextMenu(ContextMenuNewWindowArray);

            //Might be overwritten by designer if it is in Designer.CS, because the image is manually added to resource file.
            btnHideQueries.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHideQueries.Down")));
        }

        #endregion Constructor

        #region Methods

        #region Standaardfunctionaliteit van XDocHQ

        private void ClearForm(bool keepNewChecked)
        {
            m_supressUIEvents = true;
            m_deletedQueries.Clear();
            m_modifiedQueries.Clear();
            m_queries.Clear();

            btnSave.Enabled = false;
            saveTemplateToolStripMenuItem.Enabled = false;
            btnAddQuery.Enabled = grpDetails.Enabled = grpQueries.Enabled = keepNewChecked;
            rtbContent.ReadOnly = !keepNewChecked;
            rtbContent.Invalidate();
            rtbContent.HideCaret();     //We willen geen caret (tekstcursor) zien.

            txtDescription.Text = txtName.Text = txtRedirect.Text = txtRights.Text = txtID.Text = "";
            chkDataTemplate.Checked = chkToSave.Checked = chkUnattended.Checked = false;
            rtbContent.Text = "";
            if (dgvQueries.DataSource as DataTable != null)
                (dgvQueries.DataSource as DataTable).Clear();

            m_template = null;
            if (!keepNewChecked)

                this.Text = m_caption + m_connectiontext;
            lbColumn.Text = lbLine.Text = "0";

            m_supressUIEvents = false;
        }

        private void SetDirty(bool dirty)
        {
            if (m_template != null)
            {
                m_dirty = dirty;
                btnSave.Enabled = dirty;
                saveTemplateToolStripMenuItem.Enabled = dirty;
            }
        }

        private bool BindTemplatesGrid(int selectedTemplateID)
        {
            bool blnReturnSucces = true;    //Only in the MainForm_Load we have to know if this succeeded

            try
            {
                DataGridViewColumn sortColumn = dgvTemplates.SortedColumn;
                ListSortDirection sortOrder = (ListSortDirection)(((int)dgvTemplates.SortOrder) - 1); //Rare constructie: Twee enumerarions voor sortorder die niet gelijk lopen...

                List<int> lstExsistingIDs = new List<int>(_iLstRecTempCapacity);    //Tijdelijk lijstje met templateids die voorkomen in. List is nodig voor methode .Contains 
                string[] iListDeletedIDs = new string[_iLstRecTempCapacity];              //array met te deleten templates uit de recente lijst; Array is lichter, .Contains is niet nodig

                bool supressEvts = m_supressUIEvents;
                m_supressUIEvents = true;

                IDName[] templates = m_manager.GetTemplatesList();

                if (m_dtTemplates != null)
                {   //Deze methode wordt herhaald bij het verversen van het grid, dus dan alleen clear:
                    m_dtTemplates.Clear();
                }
                else
                {   //Deze loopt maar een keer; bij het opstarten:
                    m_dtTemplates = new DataTable();
                    m_dtTemplates.Columns.Add("ID", typeof(int));
                    m_dtTemplates.Columns.Add("Name", typeof(string));
                }

                foreach (IDName idName in templates)
                {
                    m_dtTemplates.Rows.Add(idName.ID, idName.Name); //Voeg het gevonden template toe aan de te tonen lijst

                    if (_lstRecTemplates[idName.ID.ToString()] != null)
                    {   //De templatelijst wordt ververst, dus  
                        lstExsistingIDs.Add(idName.ID);
                    }
                }

                if (lstExsistingIDs.Count < _lstRecTemplates.Count)
                {   //Deze itteratie kost wat tijd, dus alleen uitvoeren als er ook werkelijk templates zijn verwijderd.                    
                    int i = 0;
                    foreach (string s in _lstRecTemplates.Keys)
                    {
                        if (!lstExsistingIDs.Contains(Convert.ToInt32(s)))
                        {
                            iListDeletedIDs[i] = s;
                            i++;
                            if (i == (_lstRecTemplates.Count - lstExsistingIDs.Count))
                            {
                                break;  //We itteren niet meer dan nodig: Ga eruit als alle rotte appels bekend zijn!
                            }
                        }
                    }
                    for (int j = 0; j <= i - 1; j++)
                    {   //We halen de templates uit de recente lijst, welke niet meer bestaan
                        _lstRecTemplates.Remove(iListDeletedIDs[j]);    //Helaas mogen we deze methode niet uitvoeren in de foreach hierboven, omdat we dan tussentijds de enumerator aanpassen!
                    }
                }

                dgvTemplates.SuspendLayout();
                dgvTemplates.DataSource = m_dtTemplates;
                dgvTemplates.Columns["ID"].Width = 60;
                dgvTemplates.Columns["Name"].Width = 300;
                dgvTemplates.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                if (sortColumn != null)
                {   //sortOrder is nooit null (omdat het een enumerator is waarschijnlijk...)
                    dgvTemplates.Sort(sortColumn, sortOrder);
                }
                else
                {   //Sorteer simpelweg op de eerste kolom: ID
                    dgvTemplates.Sort(dgvTemplates.Columns[0], ListSortDirection.Ascending);
                }

                if (selectedTemplateID != -1)
                {
                    foreach (DataGridViewRow row in dgvTemplates.Rows)
                    {
                        int id;
                        int.TryParse(row.Cells["ID"].Value.ToString(), out id);
                        if (id == selectedTemplateID)
                        {
                            row.Selected = true;
                            dgvTemplates.FirstDisplayedCell = row.Cells[0];
                            break;
                        }
                    }
                }
                else
                {
                    dgvTemplates.ClearSelection();
                }

                dgvTemplates.ResumeLayout();

                m_supressUIEvents = supressEvts;
            }
            catch (Exception ex)
            {
                string message = "An error occured while filling the templates grid.\nThis might be due to an incorrect connection string.\n\n" +
                                    ex.Message + "\n\nStacktrace:\n" + ex.StackTrace;
                MessageBox.Show(message, "Error filling templates grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
                blnReturnSucces = false;        //Only place it can be set to false.
            }

            return blnReturnSucces;
        }

        private void BindQueriesGrid()
        {
            if ((dgvQueries.DataSource as DataTable) != null)
                (dgvQueries.DataSource as DataTable).Clear();
            DataTable dtQueries = new DataTable();
            dtQueries.Columns.Add("ID", typeof(int));
            dtQueries.Columns.Add("Name", typeof(string));
            dtQueries.Columns.Add("Text", typeof(string));
            dtQueries.Columns.Add("ConnID", typeof(int));
            dtQueries.Columns.Add("HashCode", typeof(int));

            foreach (IXQuery query in m_queries)
                dtQueries.Rows.Add(query.ID, query.Name, query.QueryText, query.ConnectionID, query.GetHashCode());

            dgvQueries.DataSource = dtQueries;

            dgvQueries.Columns["ID"].Width = 60;
            dgvQueries.Columns["ConnID"].Width = 60;
            dgvQueries.Columns["Text"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvQueries.Columns["HashCode"].Visible = false;

            dgvQueries.ClearSelection();    //Geen selectie op de querylijst, want we hebben niet gezocht
            //iQueryHit = -1;                 //Zoeken door de queries moet van voren af aan beginnen
        }

        private void LoadConnections()
        {
            m_connections = m_manager.GetConnectionsList();
        }

        private void ShowTemplate(int templateID)
        {
            bool blnSaveSucces = true;      //Het laden van het nieuwe template moet afgebroken worden als het saven mislukt is.                
            int iTemp = -1;

            try //We gaan deze hele methode even in een Try-Catch zetten. De belangrijkste reden is dat het mogelijk is dat er een template geladen gaat worden, op basis van ID, die door een andere applicatie reeds verwijderd is.
            {
                if (!(_blnAfterSave)) //Als deze na het Saven wordt aangeroepen, dan willen we niet de cursospositie op -1 zetten en willen we ook niet nogmaals saven (voorkomt ook oneindige recursie)
                {
                    _iPos = -1; //We willen vanaf het begin van de nieuwe tekst zoeken

                    if (m_dirty)
                    {
                        DialogResult dlgRes = MessageBox.Show("Save current template?", "Confirm save", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dlgRes == DialogResult.Yes)
                        {
                            blnSaveSucces = SaveTemplate();     //Save het template en wijzig uitvoering van onderstaande code als het niet geslaagd is

                            if (!blnSaveSucces)
                            {
                                templateID = m_template.ID;     //Als het saven mislukt is, dan weer de oude selectie in de templatelijst maken en niet de aangeklikte laten staan.
                            }

                            m_supressUIEvents = true;           //SelectionChangedEvent moet niet reageren!
                            dgvTemplates.ClearSelection();
                            foreach (DataGridViewRow row in dgvTemplates.Rows)
                            {
                                int.TryParse(row.Cells[0].Value.ToString(), out iTemp);
                                if (iTemp == templateID)
                                {

                                    row.Selected = true;
                                    dgvTemplates.FirstDisplayedCell = row.Cells[0];
                                    break;
                                }
                            }
                            m_supressUIEvents = false;
                        }

                    }
                }

                if (blnSaveSucces)  //Als het saven gelukt is (of er hoefde niet gesaved te worden), dan gaan we een nieuw template laden, anders doen we niks meer
                {
                    ClearForm(false);

                    m_supressUIEvents = true;
                    grpQueries.Enabled = grpDetails.Enabled = btnAddQuery.Enabled = true;
                    rtbContent.ReadOnly = false;

                    m_template = m_manager.LoadTemplate(templateID);

                    rtbContent.Text = m_template.Content;
                    if (rtbContent.Focused) //Er is een template in het rtbContent geladen, terwijl de focus er nog op stond, 
                    {   //dus CreateNewCaret zal niet door de 'rtbContent_GotFocus' uitgevoerd worden, dus zelf hier doen!
                        rtbContent.CreateNewCaret(); //Ervaring leert dat na b.v. Ctrl+S de caret weg is!
                    }

                    txtDescription.Text = m_template.Description;
                    txtName.Text = m_template.Name;
                    txtID.IntValue = m_template.ID;
                    _iOriginalTemplateID = m_template.ID;   //Als deze gelijk is aan txtIDName.IntValue, dan wordt de controle of het ingetype ID al bestaat overgeslagen
                    txtRedirect.Text = m_template.Redirect;
                    txtDocType.Text = m_template.DocType;
                    chkDataTemplate.Checked = m_template.IsData;
                    chkToSave.Checked = m_template.ToSave;
                    chkUnattended.Checked = m_template.Unattended;

                    string rights = "";
                    foreach (int roleID in m_template.Roles)
                        rights += roleID.ToString() + ";";
                    if (rights.EndsWith(";"))
                        rights = rights.Substring(0, rights.Length - 1);
                    txtRights.Text = rights;

                    ShowQueries(templateID);

                    this.Text = m_caption + " (TemplateID = " + m_template.ID.ToString() + ")" + m_connectiontext;

                    UpdateRecTemplates();

                    m_dirty = false;
                }
            }
            catch (Exception ex)
            {
                string message = "An error occured loading the template with ID = " + templateID.ToString() +
                    "\n\nIs the template list out of date and does this template still exist?\n" +
                    "Do you have enough rights to view the document (see column Rights in s_templates)?\n\n" + ex.Message;
                MessageBox.Show(message, "Error loading template", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
                btnRefresh_Click(null, null);   //Laten we het grid opnieuw vullen, omdat er waarschijnlijk een template in staat die niet meer voor komt.
                UpdateRecTemplates();           //Het grid met alle templates is ververst door BindTemplatesGrid. Recente templates mogen niet achterlopen, dus update
                ClearForm(false);
            }
            finally
            {
                m_supressUIEvents = false;
            }
        }

        private void ShowQueries(int templateID)
        {
            IDName[] queries = m_manager.GetQueriesList(templateID);
            m_queries.Clear();
            foreach (IDName qIDName in queries)
            {
                IXQuery query = m_manager.LoadQuery(qIDName.ID);
                m_queries.Add(query);
            }

            BindQueriesGrid();
        }

        private void DeleteTemplates()
        {
            if (dgvTemplates.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Delete templates?", "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    int id;
                    foreach (DataGridViewRow row in dgvTemplates.SelectedRows)
                    {
                        int.TryParse(row.Cells["ID"].Value.ToString(), out id);
                        m_manager.DeleteTemplate(id);               //Laat de datamanager het template werkelijk verwijderen
                        _lstRecTemplates.Remove(id.ToString());     //Haal het template uit de lijst met recente templates
                    }
                    ClearForm(false);
                    int selTemplateID = m_template != null ? m_template.ID : -1;
                    //BindTemplatesGrid(selTemplateID);             // Do not refresh templates 
                    UpdateRecTemplates();                           //De lijst met recente templates is veranderd, dus updaten
                }
            }
        }

        IXQuery FindQuery(int queryID)
        {
            foreach (IXQuery query in m_queries)
                if (query.ID == queryID)
                    return query;
            return null;
        }

        private void GetCaretPos(TextBox txtBox, out int line, out int column)
        {
            Point pt;
            int index;

            index = txtBox.SelectionStart;
            line = txtBox.GetLineFromCharIndex(index);
            pt = txtBox.GetPositionFromCharIndex(index);
            pt.X = 0;
            column = index - txtBox.GetCharIndexFromPosition(pt);
        }

        private void UpdateCaretPositionLabels()     //Zet de Line- en Columnnumber op de juiste waarden
        {
            int line, column;
            GetCaretPos(rtbContent, out line, out column);
            lbLine.Text = line.ToString();
            lbColumn.Text = column.ToString();
        }

        public void SelecteerRijOpID(int templateID) //Hier wordt de rij in het DataGrid geselecteerd met de opgegeven ID;
        {
            int iRij;

            foreach (DataGridViewRow row in dgvTemplates.Rows)
            {
                if (Convert.ToInt32(row.Cells[0].Value) == templateID)
                {
                    iRij = row.Index; //De rij waar we naar toe gaan scrollen met FirstDisplayedCell
                    dgvTemplates.ClearSelection();
                    row.Selected = true;
                    dgvTemplates.FirstDisplayedCell = dgvTemplates.Rows[iRij].Cells[0]; //Let op! Hier geen row.Cells[0] gebruiken, want dit kan fouten opleveren als tussendoor, door row.Selected, het Grid is gerefreshed!
                    rtbContent.SelectionStart = 0;
                    break;
                }
            }
        }

        public bool SaveTemplate()  //Deze routine bewaart het template. 
        {   //Het is een bool, zodat als er iets fout gaat, de caller ook stop, b.v. met het laden van een nieuw template. Anders zou er werk verloren gaan.
            this.Cursor = Cursors.WaitCursor;
            m_supressUIEvents = true;
            bool blnSaveSucces = false;     //Pas goedkeuren als het saven gelukt is.
            bool blnDoExecute = true;   //In bepaalde gevallen willen we niks saven.

            if (m_template == null)
            {
                blnDoExecute = false;
            }
            //else if (string.IsNullOrEmpty(txtName.Text.Trim()))
            //{
            //    blnDoExecute = false;
            //    MessageBox.Show("Vul een unieke templatenaam in!\n\n", "Name required", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    txtName.Focus();
            //}
            else if (m_template.Name.Trim().ToLower() != txtName.Text.Trim().ToLower())
            {   //The templatename was changed
                if (!IsNewTemplateNameOK()) //Geen melding nodig, wordt in IsNewTemplateNameOK al gegeven
                {
                    blnDoExecute = false;
                }
            }

            if (blnDoExecute)
            {
                try //Ik zet hier nog even een try-catch omheen, omdat er vrij veel databaseacties plaats vinden in deze routine
                {
                    _blnAfterSave = true; //We slaan een stukje van de logica van het openen van een nieuwe template over, wanneer we aan het saven waren

                    if (m_template == null)
                    {
                        return blnSaveSucces;       //Niet netjes! Meerdere exitpoints uit een functie.
                    }

                    _iPos = rtbContent.SelectionStart; //We slaan de positie op waar de cursor stond, omdat de template straks opnieuw geladen wordt. We kunnen de cursor dat weer op de oude positie zetten

                    m_template.Content = rtbContent.Text;
                    m_template.Description = txtDescription.Text.Trim();
                    m_template.Redirect = txtRedirect.Text.Trim();
                    m_template.DocType = txtDocType.Text.Trim();
                    m_template.IsData = chkDataTemplate.Checked;
                    m_template.Name = txtName.Text.Trim();
                    m_template.ToSave = chkToSave.Checked;
                    m_template.Unattended = chkUnattended.Checked;

                    m_template.Roles.Clear();
                    foreach (string s in txtRights.Text.Split(';'))
                    {
                        int role;
                        if (int.TryParse(s, out role))
                            m_template.Roles.Add(role);
                    }

                    m_template.Save();

                    foreach (IXQuery query in m_queries)
                    {
                        if (query.ID == -1)
                        {
                            query.TemplateID = m_template.ID;
                            query.Save();
                        }
                    }
                    foreach (IXQuery query in m_modifiedQueries)
                    {
                        query.Save();
                    }

                    foreach (IXQuery query in m_deletedQueries)
                    {
                        m_manager.DeleteQuery(query.ID);
                    }

                    blnSaveSucces = true;   //Als we hier komen is het saven (voldoende) goed gegaan. ChangeTemplateID heeft z'n eigen foutafhandeling.

                    if (m_template.ID != txtID.IntValue && IsNewTemplateIDOK() && txtID.IntValue > -1)
                    {   //We hebben te maken met een gewijzigd templateid te maken EN hij is goedgekeurd (komt nog niet voor) EN het is geen nieuw template
                        ChangeTemplateID();
                    }

                    SetDirty(false);

                    //BindTemplatesGrid(-1);            // Do not refresh templates 
                    dgvTemplates.ClearSelection();

                    _BeingSaved = true;                 //Dit moet even aan en uitgezet worden, omdat anders de dgvTemplates_SelectionChanged niet werkt, doordat m_supressEvents aan staat
                    if (txtID.IntValue < 0)
                    {   //Het was een nieuw template en txtID is nooit ingevuld geweest
                        SelecteerRijOpID(m_template.ID);
                    }
                    else
                    {   //Het was geen nieuw template en de waarde in txtID is de nieuwste waarde, niet die in m_templateID
                        SelecteerRijOpID(txtID.IntValue);   //Er was een melding van Erwin, dat zijn template niet meer geselecteerd was in het grid, na saven, dit moet hiermee opgelost zijn
                    }
                    _BeingSaved = false;

                    if (_iPos > -1)
                    {
                        rtbContent.SelectionStart = _iPos;
                        UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
                    }
                }
                catch (Exception ex)
                {
                    blnSaveSucces = false;
                    string message;
                    string title;

                    if (ex.Message.ToLower().Contains("unique key constraint") && ex.Message.ToLower().Contains("duplicate key"))
                    {   //Unfortunately there is no (SQL) error code returned m_template.Save(), so I can olny filter on the message text
                        message = "Saving failed!\nThe database you are using does not allow duplicate TemplateNames!\n\n" + ex.Message;
                        title = "Duplicate TemplateNames not allowed";
                        //Example of the message text:
                        //(TemplateID=1062) Template cannot be saved
                        //Violation of UNIQUE KEY constraint 'IX_S_Templates'. Cannot insert duplicate key in object 'dbo.S_TEMPLATES'.
                        //The statement has been terminated.  (14:59:56.060)
                    }
                    else
                    {
                        message = "An error occured in the SaveTemplate routine.\n\n" + ex.Message;
                        title = "Error while saving";
                    }

                    MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                }
            }


            _blnAfterSave = false; //Even op de standaardwaarde zetten, we zijn niet langer bezig met saven.
            this.Cursor = Cursors.Arrow;
            m_supressUIEvents = false;

            return blnSaveSucces;   //De caller mag niet verder gaan met het laden van een ander template o.i.d. waardoor werk verloren gaat. 
        }

        private void ConnectionText() //Deze methode maakt een string met daarin de Initial Catalog en de Data Source van de gebruikte connection string, om hem af te beelden in de titel van het hoofdscherm
        {
            StringBuilder strConnection = new StringBuilder();

            try
            {   //Expirience teaches that if there is an error in the connection string, it will come to light here.
                _strMainConnString = ConfigurationManager.AppSettings["XDocConnectionString"];
                string[] strArray = _strMainConnString.Split(new Char[] { ';' });

                //<add key="XDocConnectionString" value="Provider=SQLOLEDB; Password=12345;Persist Security Info=True;User ID=sa;Initial Catalog=IRIS_test;Data Source=iristest" />
                foreach (string s in strArray)
                {
                    if (s.ToLower().Contains("source") || s.ToLower().Contains("catalog") || s.ToLower().Contains("initial") || s.ToLower().Contains("data"))
                    {
                        strConnection.Append("       -       ");
                        strConnection.Append(s.Trim());
                    }
                    else if (s.ToLower().Contains("sqloledb"))
                    {
                        CorrectConnectionString(strArray);
                    }
                }
                m_connectiontext = strConnection.ToString();
            }

            catch (Exception ex)
            {   //Ervaring leert dat als er een fout zit in de configfile, deze hier voor het eerst aan het licht komt en een niet-afgevangen fout gaf!
                string message = "Something is wrong with the XDocHQ.exe.config file.\n\nIs there a XDocConnectionString? Is there a syntax error?\n\n" +
                                    "If you want to add Rapporten, you need to define the section Rapporten. See config file in the deployment directory for this.\n\n" +
                                    ex.Message;
                //KubionLog kan niet aangeroepen worden, omdat deze een juist configfile nodig heeft om te werken!
                MessageBox.Show(message, "Error in configfile", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);    //Het form wordt nog opgestart, dus this.close en this.dispose werken niet goed.
            }
        }

        private void CorrectConnectionString(string[] strArray) //De nieuwe KubionDLL's werken niet meer met conncetionstrings die 'Provider=SQLOLEDB;' gebruiken. Corrigeer de app.config
        {
            StringBuilder strbConn = new StringBuilder();

            StringBuilder strbMessage = new StringBuilder("This is a one time only message!\n\n" +
                                                            "The connectionstring that was found in XDocHQ.exe.config was aangetroffen, " +
                                                            "does not work well with the latest KubionDLLs,\n" +
                                                        "'Provider=SQLOLEDB;' should be left out from now on.\n" +
                                                        "This error has been fixed automatically, the XDocHQ.exe.config has been adjusted.\n\n" +
                                                        "Old connectionstring: ");
            foreach (string str in strArray)
            {
                if (!str.ToLower().Contains("sqloledb"))
                {
                    strbConn.Append(str);
                    strbConn.Append(";");
                }
                strbMessage.Append(str);
                strbMessage.Append(";");
            }
            _strMainConnString = strbConn.ToString();   //Put the correct string in the global variable right away

            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.AppSettings.Settings.Remove("XDocConnectionString");
            config.AppSettings.Settings.Add("XDocConnectionString", _strMainConnString);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            strbMessage.Append("\nNew connectionstring: ");
            strbMessage.Append(_strMainConnString);
            strbMessage.AppendLine("\n\nIf you think that the new connectionstring is not correct, please adjust it manually.");

            MessageBox.Show(strbMessage.ToString(), "Error in connectionstring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void ExecuteProcedure(string key) //Deze methode voert de procedures uit, zoals create history triggers
        {
            ClientData dataManager = null;  //Even vantevoren aanmaken, zodat hij bestaat in de Finally en hij altijd opgeruimd kan worden.
            string[] strSqls;
            string[] separator = { "\tGO\t", "\tGO\n", "\tGO\r", "\tGO ", "\nGO\t", "\nGO\n", "\nGO\r", "\nGO ", "\rGO\t", "\rGO\n", "\rGO\r", "\rGO ", " GO\t", " GO\n", " GO\r", " GO ", }; // mogelijk \t \n \r\ " "
            string strTotalSql;
            string strTemp = "";

            try
            {
                    dataManager = new ClientData(_strMainConnString); //Een KubionDataNamespaceobject dat gegevens kan ophalen       
                    dataManager.BeginTransaction();     //Alleen doorvoeren als alles goed gaat

                    strTotalSql = _DynProceduresColl.GetValues(key)[0]; //Get chosen SQL from configfile

                    if (!string.IsNullOrEmpty(strTotalSql))
                    {
                        strSqls = strTotalSql.Split(separator, StringSplitOptions.RemoveEmptyEntries); //Splitsen, omdat GO niet is toegestaan
                    }
                    else
                    {
                        MessageBox.Show("The SQL-script belonging to the key '"+key+" could not be found in XDoc.exe.config. '." +
                                        "\n\nDid you add a node to <Procedures> of type <add key=\"Name\" value=\"SQL\">", "Error excuting procedure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        strSqls = new string[0];    //Er moet leeg array aan strSqls toegekend worden, zodat hij meteen goed uit onderstaande loop springt
                    }
                
                    foreach (string sQL in strSqls)
                    {                        
                        strTemp += dataManager.ExecuteNonQuery(sQL) + "\n"; //Alle stukje script een voor een uitvoeren.
                    }
                    dataManager.CommitTransaction();     //Alleen uitvoeren als alles goed gaat                                    

                    MessageBox.Show("The procedure was succesfully executed.\n\n Results from queries:\n" + strTemp, "Succes!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                dataManager.RollbackTransaction();
                string message = "An exception occured when executing the chosen procedure.\n\n" + ex.Message;
                MessageBox.Show(message, "Error executing procedure.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);               
            }
            finally
            {
                if (dataManager != null)
                {
                    dataManager.Dispose();                    
                }
            }
        }
      
        private bool HasHistTables()
        {
            bool retVal = false;
            string strSql = "DECLARE @Result BIT " +
                                " SET @Result = 0; " +
                                "  " +
                                " IF ( " +
                                " 	    EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Templates_Hist]') AND type in (N'U')) " +
                                " 	    AND " +
                                " 	    EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S_Queries_Hist]') AND type in (N'U')) " +
                                " 	) " +
                                " 	BEGIN " +
                                " 		SET @Result = 1; " +
                                " 	END " +
                                " SELECT @Result AS Result; ";
            ClientData dataManager = null;  //Even vantevoren aanmaken, zodat hij bestaat in de Finally en hij altijd opgeruimd kan worden.

            try
            {
                dataManager = new ClientData(_strMainConnString); //Een KubionDataNamespaceobject dat gegevens kan ophalen       

                string sTemp = dataManager.ExecuteQuery(strSql);

                if (sTemp == "1" || sTemp.ToLower() == "true")
                {
                    retVal = true;
                }
            }
            catch { }
            finally
            {
                if (dataManager != null)
                {
                    dataManager.Dispose();
                }
            }
            return retVal;
        }

        private void LoadReportsMenu()  //Deze methode vult het menu-item dynamisch met rapporten die door de gebruiker in XDocHQ.exe.config zijn toeggevoegd.
        {
            _DynReportsColl = (NameValueCollection)ConfigurationManager.GetSection("Rapporten");
            int j = 1;

            if (_DynReportsColl != null)
            {
                for (int i = 0; i < _DynReportsColl.Keys.Count; i++)
                {
                    string name = _DynReportsColl.Keys[i];

                    System.Windows.Forms.ToolStripMenuItem DynamicMenuItem = new System.Windows.Forms.ToolStripMenuItem();
                    DynamicMenuItem.Name = name;
                    DynamicMenuItem.Size = new System.Drawing.Size(172, 22);
                    DynamicMenuItem.Text = name;
                    if (j != 2)
                    {
                        DynamicMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("DynamicMenuItem.Image")));
                    }
                    else
                    {   //Voor het oog even wat wisselende icoontjes, anders ziet het er zo saai uit ;-)
                        DynamicMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("DynamicMenuItem2.Image")));
                        j = 0;
                    }
                    DynamicMenuItem.Click += new EventHandler(DynamicMenuItem_Click);
                    this.reportsToolStripMenuItem.DropDownItems.Add(DynamicMenuItem);

                    j += 1;
                }
            }
            else
            {
                string message = "A section in the XDocHQ.exe.config file is missing.\n" +
                                    "Since version 2.04 there is a section in the config file in which dynamic reports can be defined.\n" +
                                    "Now dynamic reports cannot be used.\n\n" +
                                    "Open XDocHQ.exe.config from release directory and copy the following lines to your own config file: \n" +
                                    "<configSections>\n" +
                                    "   <!-- Here the section 'Rapporten' is defined. Below you can add your own report with name and SQL.-->\n" +
                                    "   <section name=\"Rapporten\" type=\"System.Configuration.NameValueSectionHandler\" />\n" +
                                    "</configSections>\n\n" +
                                    "and also the section 'Rapporten':\n" +
                                    "<Rapporten>\n" +
                                    "   < ... >\n" +
                                    "   < ... >\n" +
                                    "</Rapporten>";
                KubionLog.WriteLine(message);
                //This mesage was not disired by Mihai, so taken out. I was not able to add the configsection programmatically
                //MessageBox.Show(message, "Missing config section 'Rapporten'", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadProceduresMenu()  //Deze methode vult het menu-item dynamisch met procedures die door de gebruiker in XDocHQ.exe.config zijn toeggevoegd.
        {
            _DynProceduresColl = (NameValueCollection)ConfigurationManager.GetSection("Procedures");
            int j = 1;

            if (_DynProceduresColl != null)
            {
                for (int i = 0; i < _DynProceduresColl.Keys.Count; i++)
                {
                    string name = _DynProceduresColl.Keys[i];

                    System.Windows.Forms.ToolStripMenuItem DynamicProcedureItem = new System.Windows.Forms.ToolStripMenuItem();
                    DynamicProcedureItem.Name = name;
                    DynamicProcedureItem.Size = new System.Drawing.Size(172, 22);
                    DynamicProcedureItem.Text = name;
                    if (j != 2)
                    {
                        DynamicProcedureItem.Image = ((System.Drawing.Image)(resources.GetObject("DynamicMenuItem.Image")));
                    }
                    else
                    {   //Voor het oog even wat wisselende icoontjes, anders ziet het er zo saai uit ;-)
                        DynamicProcedureItem.Image = ((System.Drawing.Image)(resources.GetObject("DynamicMenuItem2.Image")));
                        j = 0;
                    }
                    DynamicProcedureItem.Click += new EventHandler(DynamicProcedureItem_Click);
                    this.proceduresToolStripMenuItem.DropDownItems.Add(DynamicProcedureItem);

                    

                    j += 1;
                }
            }
            else
            {
                string message = "A section in the XDocHQ.exe.config file is missing.\n" +
                                    "Since version 2.04 there is a section in the config file in which dynamic reports can be defined.\n" +
                                    "Now dynamic reports cannot be used.\n\n" +
                                    "Open XDocHQ.exe.config from release directory and copy the following lines to your own config file: \n" +
                                    "<configSections>\n" +
                                    "   <!-- Here the section 'Procedures' is defined. Below you can add your own report with name and SQL.-->\n" +
                                    "   <section name=\"Procedures\" type=\"System.Configuration.NameValueSectionHandler\" />\n" +
                                    "</configSections>\n\n" +
                                    "and also the section 'Procedures':\n" +
                                    "<Procedures>\n" +
                                    "   < ... >\n" +
                                    "   < ... >\n" +
                                    "</Procedures>";
                KubionLog.WriteLine(message);
                //This mesage was not disired by Mihai, so taken out. I was not able to add the configsection programmatically
                //MessageBox.Show(message, "Missing config section 'Procedures'", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateRecTemplates() //Het dgvRecTemplatesgrid wordt geupdate nadat er een nieuw template geselecteerd is
        {
            string strID;
            string ID = string.Empty;
            string Name = string.Empty;
            int iAantalRec = _lstRecTemplates.Count;//Het aantal templates dat ooit geopend is
            int iStopIndex = 0;                     //Tot welke index gaan we aan de datagrid toevoegen?                
            bool blnNoSelection = true;             //Soms is er geen selectie in de recente lijst; andere logica

            if (m_template != null && m_template.ID > -1)
            {   //Er kan geen template geselecteerd zijn, als er net een gedelete is
                ID = m_template.ID.ToString();
                Name = m_template.Name;
                blnNoSelection = false;
            }

            m_supressUIEvents = true;               //Andere events mogen niks uitvoeren, zolang deze methode nog niet afgerond is
            dgvRecTemplates.SuspendLayout();

            if (iAantalRec > _iLstRecTempCapacity - 1)
            {   //Zorg dat hij niet groter wordt dan z'n originele capaciteit. Dit zou slecht voor performance zijn.                        
                _lstRecTemplates.Remove(_lstRecTemplates.Keys[0]);     //Haal het eerstgekozen template weg       
            }

            if (!blnNoSelection)
            {   //Voeg het gekozen template toe aan de lijst met aangeklikte templates:                 
                _lstRecTemplates.Remove(ID);    //Eerst verwijderen om de chronoligsche volgorde goed te houden
                _lstRecTemplates.Add(ID, Name);
            }

            iAantalRec = _lstRecTemplates.Count;    //Het aantal moet bijgesteld worden.

            dgvRecTemplates.ClearSelection();
            m_dtRecTemplates.Clear();

            if (iAantalRec > (_iLstRecTempCapacity - 10))    //De capaciteit van de lijst is 10 meer dan het getoonde aantal
            {
                iStopIndex = iAantalRec - (_iLstRecTempCapacity - 10);
            }

            for (int i = iAantalRec - 1; i >= iStopIndex; i--)
            {   //We lopen in omgekeerde volgorde door de lijst, zodat het laatstgekozen template bovenaan komt.
                strID = _lstRecTemplates.Keys[i];
                m_dtRecTemplates.Rows.Add(strID, _lstRecTemplates[strID]);
            }

            SchaalRecTempGrid();    //Schuif de splitter tussen Alle en Recente zodanig dat alle recente templates in beeld komen.

            dgvRecTemplates.DataSource = m_dtRecTemplates;
            if (blnNoSelection)
            {
                dgvRecTemplates.ClearSelection();
            }
            dgvRecTemplates.ResumeLayout();
            m_supressUIEvents = false;               //Events mogen weer af gaan.
        }

        private void BindRecTemplatesGrid()     //Maak de datatable aan die het datagrid van de recente templates vult en bind hem aan het datagrid
        {
            Int16 iAantalRecTemp = -1;

            m_dtRecTemplates = new DataTable();
            m_dtRecTemplates.Columns.Add("ID", typeof(int));
            m_dtRecTemplates.Columns.Add("Name", typeof(string));

            dgvRecTemplates.SuspendLayout();
            dgvRecTemplates.DataSource = m_dtRecTemplates;

            dgvRecTemplates.Columns["ID"].Width = 60;
            dgvRecTemplates.Columns["Name"].Width = 300;
            dgvRecTemplates.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgvRecTemplates.ResumeLayout();

            try
            {
                Int16.TryParse(ConfigurationManager.AppSettings["AantalRecenteTemplates"], out iAantalRecTemp);

                if (iAantalRecTemp > 0)
                {   //Stel de standaardgrootte van de lijst recente templates in op twee keer het aantal getoonde recente templates                    

                    //Er worden 10 meer recente templates bijgehouden dan er getoond worden. Dit is prettig, omdat bij het deleten van templates, 
                    //of het deleten van regels uit de recente lijst, er dan toch nog een gevulde recente lijst over blijft.                 
                    _iLstRecTempCapacity = (iAantalRecTemp + 10);
                }
                else
                {   //De key bestond niet, dus we geven een eenmalige melding en maken hem aan:
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings.Remove("AantalRecenteTemplates");
                    config.AppSettings.Settings.Add("AantalRecenteTemplates", "10");
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                    //Er worden 10 meer recente templates bijgehouden dan er getoond worden. Dit is prettig, omdat bij het deleten van templates, 
                    //of het deleten van regels uit de recente lijst, er dan toch nog een gevulde recente lijst over blijft.      
                    _iLstRecTempCapacity = 20;

                    MessageBox.Show("This is a one time only message!\n\n" +
                                        "The setting 'AantalRecenteTemplates' was missing in the XDocHQ.exe.config,\n" +
                                        "or the inserted value was smaller than 1, or it was not an integer.\n" +
                                        "The setting has been inserted and is set to 10", "Missing config setting", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                _lstRecTemplates = new NameValueCollection(_iLstRecTempCapacity);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while reading the 'AantalRecenteTemplates' key from the XDocHQ.exe.config.\n\nHas an integer value been filled in?\n\n" + ex.Message, "Error reading config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SchaalRecTempGrid()      //Het grid met recente templates moet herschaald worden. Kleine methode, maar wordt toch op 3 verschillende plekken aangeroepen.
        {
            int newSplitDist = this.Height - dgvRecTemplates.PreferredSize.Height - 52;
            if (newSplitDist >= 200)
            {   //Het getal is niet zo belangrijk. Maar altijd > 0 en met 200 blijven er bij een laag window ook altijd wat van de 'Alle templates' zichtbaar.
                splitAlleRecente.SplitterDistance = newSplitDist; //Schaal de ruimte voor het datagrid, zodat het er netjes uit ziet
            }
        }

        private bool IsNewTemplateNameOK() //In het nieuwe datamodel moet de templatenaam uniek zijn; XDocHQ dwingt het alvast af. Hieronder de controle op de zojuist ingevulde naam
        {
            string iNieuwTemplateName = txtName.Text.Trim().ToLower();
            bool blnNameCorrect = true;     //De nieuwe naam is goed, tenzij hij gevonden wordt in de lijst met templates.

            m_supressUIEvents = true;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                IDName[] templates = m_manager.GetTemplatesList();  //Haal de lijst met actuele templates op met de XDocengine

                foreach (IDName idName in templates)
                {
                    if (idName.Name.Trim().ToLower() == iNieuwTemplateName)
                    {
                        DialogResult dlgRes = MessageBox.Show("There already exists a template with the name '" + iNieuwTemplateName + "'.\n(case insensitive)" +
                            "\nThe name should be unique." +
                            "\n\nDo you want to keep the current non-unique name?", "TemplateName not unique", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

                        if (dlgRes == DialogResult.Cancel)
                        {
                            txtName.Text = _iOriginalTemplateName;  //Zet de vorige waarde terug, omdat de huidige niet toegestaan is                                
                            blnNameCorrect = false; //Don't try to save the template
                        }
                        else if (dlgRes == DialogResult.No)
                        {
                            txtName.Focus();
                            blnNameCorrect = false; //Don't try to save the template
                        }
                        else if (dlgRes == DialogResult.Yes)
                        {
                            blnNameCorrect = true;  //Altough not nice, it has been approved by the user
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                blnNameCorrect = false;
                string Message = "Error while checking the new templateID.\n\n" + ex.Message +
                                    "\n\nIs there a connection to the database?\n\nThe templateID now cannot be changed.";
                MessageBox.Show(Message, "Error while checking templateID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(Message);
            }
            finally
            {
                if (blnNameCorrect)
                {   //Het ID is correct gewijzigd, dus het template moet gesaved worden
                    SetDirty(true); //Let op! Het is hier verleidelijk om SetDirty(blnIDCorrect) te zeggen (zonder IF), maar we willen nooit SetDirty uitzetten, omdat er misschien al andere wijzigingen zijn gedaan!!!
                    _iOriginalTemplateName = txtName.Text;  //We zetten het originele template op de goedgekeurde nieuwe waarde. Hierdoor wordt deze routine niet nog een keer gedraaid en we houden geen oude waarde vast die ooit is ingevoerd
                }

                m_supressUIEvents = false;
                this.Cursor = Cursors.Arrow;
            }

            return blnNameCorrect;
        }

        private bool buildURLTryParse(ref string strURL, ref string[] strArguments)   //This method creates the URL view the current template in the browser.
        {
            bool blnSucces = true;
            StringBuilder sb = new StringBuilder(_URLTryPars, _URLTryPars.Length + 50); //Guess some length for an efficient use of the stringbuilder
            strArguments = null;
            if (_URLTryPars.EndsWith(".aspx"))
            {   //We have a normal URL to a XDocumentsapplication
                sb.Append("?templatename=");
                sb.Append(m_template.Name);
            }
            else if (_URLTryPars.EndsWith("/"))
            {   //We make the crude assumption that the user does not want to display his template, but wants to use it as a REST_service                    
                int iName = m_template.Name.IndexOf("_"); //We will display the GET_LIST Method, by asking the REST service for the Resource name
                sb.Append(m_template.Name.Substring(0, iName));
            }
            else if(_URLTryPars.ToLower().Equals("messagebox"))
            {
            }
            else
            {   //URL in configfile is not accepted.
                blnSucces = false;
                MessageBox.Show("An error occured while loading the URL to the XDocuments webapplication " +
                                    "<URLTryParse> from the config file.\n\n" +
                                    "The URL that was found did not end with \".aspx\", or with \"/\".\n" +
                                    "If you want the templates to be parsed as a normal XDoc-application, " +
                                    "fill in an URL ending with \".aspx\" (makeXdocument.aspx or MyIris.aspx).\n" +
                                    "If you want the templates to serve as a REST service, " +
                                    "fill in an URL ending with \"/\".\n\n" +
                                    "For example, if a webapplication has been created which works with the current database, " +
                                    "on this URL: 'http://localhost/xdoc_test/makexdocument.aspx?templateid=149', " +
                                    "then fill in under 'URLTryParse' in the XDocHQ.exe.config: 'http://localhost/xdoc_test/makexdocument.aspx'.\n\n" +
                                    "Or if a REST service exists which uses templates from the current database, " +
                                    "you can fill in the URL to the service, ending with \"/\", " +
                                    "for example: 'http://server/RestService/'.\n\n" +
                                    "Watch out! After changing XDocHQ.exe.config, XDocHQ must be restarted.",
                                "Error loading URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            strURL = sb.ToString();
            return blnSucces;
        }

        private void CheckXDocAttPath()
        {
            try
            {
                string strXDocAttPath = ConfigurationManager.AppSettings["XDocAttPath"];
                Char[] anyOf = { ':', '*', '?', '"', '<', '>', '|' };   //The path may not contain these characters                    

                if (string.IsNullOrEmpty(strXDocAttPath) || strXDocAttPath.IndexOfAny(anyOf) > -1)
                {
                    StringBuilder strbMessage = new StringBuilder("This is a one time only message!\n\n" +
                                                                "The XDocAttPath key was missing from the XDocHQ.exe.config.\n" +
                                                                "This setting tells the path of the XDoc-attachments.\n" +
                                                                "It has now gotten the standard value 'Attachments',\n" +
                                                                "and this directory has been created.\n\n" +
                                                                "This error has been fixed automatically, the XDocHQ.exe.config has been adjusted.\n\n");

                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    config.AppSettings.Settings.Remove("XDocAttPath");
                    config.AppSettings.Settings.Add("XDocAttPath", "Attachments");
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                    MessageBox.Show(strbMessage.ToString(), "Missing key in config file", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    strXDocAttPath = "Attachments";
                }

                if (!(strXDocAttPath.Contains("\\")))
                {   //Only try to create if it is a local relative path

                    DirectoryInfo dirInf = new DirectoryInfo(strXDocAttPath);
                    if (!(dirInf.Exists))
                    {
                        Directory.CreateDirectory(strXDocAttPath);
                    }
                }
            }
            catch
            {
                //No problem if it fails!
            }
        }

        private void CheckKubionTraceLevel()
        {
            try
            {
                string strKubionTraceLevel = ConfigurationManager.AppSettings["KubionTraceLevel"];

                if (string.IsNullOrEmpty(strKubionTraceLevel))
                {
                    string strMessage = "This is a one time only message!\n\n" +
                                                                "The KubionTraceLevel key was missing from the XDocHQ.exe.config.\n" +
                                                                "This setting tells the tracelevel used in the Kubion namespace.\n" +
                                                                "It has now gotten the standard value '9'.\n\n" +
                                                                "This error has been fixed automatically, the XDocHQ.exe.config has been adjusted.\n\n";

                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    config.AppSettings.Settings.Remove("KubionTraceLevel");
                    config.AppSettings.Settings.Add("KubionTraceLevel", "9");
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                    MessageBox.Show(strMessage, "Missing key in config file", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch
            {
                //No problem if it fails!
            }
        }

        private void CheckKubionLogFile()
        {
            try
            {       //<add key="KubionLogFile" value="Logs/app.log" />	                    
                string strKubionLogFile = ConfigurationManager.AppSettings["KubionLogFile"];

                if (string.IsNullOrEmpty(strKubionLogFile))
                {
                    string strMessage = "This is a one time only message!\n\n" +
                                            "The KubionLogFile key was missing from the XDocHQ.exe.config.\n" +
                                            "This setting tells the location of the Kubion log file.\n" +
                                            "It has now gotten the standard value 'Logs\\app.log',\n" +
                                            "and the directory has been created.\n\n" +
                                            "This error has been fixed automatically, the XDocHQ.exe.config has been adjusted.\n\n";

                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    config.AppSettings.Settings.Remove("KubionLogFile");
                    config.AppSettings.Settings.Add("KubionLogFile", "Logs\\app.log");
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");

                    MessageBox.Show(strMessage, "Missing key in config file", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    strKubionLogFile = "Logs\\app.log";
                }

                strKubionLogFile = strKubionLogFile.Replace('/', '\\');
                int iPosSlash = strKubionLogFile.IndexOf('\\');

                if (iPosSlash == strKubionLogFile.LastIndexOf('\\'))
                {   //Do not try to create a Dir if more than one subdir is mentioned in the path
                    if ((strKubionLogFile.Contains("\\")))
                    {
                        DirectoryInfo dirInf = new DirectoryInfo(strKubionLogFile.Substring(0, iPosSlash));
                        if (!(dirInf.Exists))
                        {
                            Directory.CreateDirectory(strKubionLogFile.Substring(0, iPosSlash));
                        }
                    }
                }
            }
            catch
            {
                //No problem if it fails!
            }
        }

        private void CheckDataModel()
        {             
            string strSql = "DECLARE @Result BIT " +
                                " SET @Result = 0; " +

                                " IF (  " +
                                "     EXISTS (SELECT Column_Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='s_templates' AND Column_Name='Rights')  " +
                                "     AND  " +
                                "     EXISTS (SELECT Column_Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='s_templates' AND Column_Name='DocType')  " +
                                " )  " +
                                " BEGIN  " +
                                " 	SET @Result = 1; " +
                                " END " +

                                " SELECT @Result AS Result; ";

            ClientData dataManager = null;  //Even vantevoren aanmaken, zodat hij bestaat in de Finally en hij altijd opgeruimd kan worden.

            try
            {
                dataManager = new ClientData(_strMainConnString); //Een KubionDataNamespaceobject dat gegevens kan ophalen       

                string sTemp = dataManager.ExecuteQuery(strSql);

                if (sTemp == "1" || sTemp.ToLower() == "true")
                {
                    _blnDocTypeRights = true;
                }
                else
                {
                    _blnDocTypeRights = false;
                }
            }
            catch { }
            finally
            {
                if (dataManager != null)
                {
                    dataManager.Dispose();
                }
            }                 
        }

        #endregion Standaardfunctionaliteit van XDocHQ

        #region Zoeken

        private void SelecteerTekstVerder(string strContent) //Hier wordt de zoekterm geselecteerd in de tekstbox:
        {
            int iLen = _strZoekterm.Length;
            int iPosTemp = -1;
            int iPosSearchEnd = strContent.Length - 1;  //The point were we stop searching. 
            //The first time around this will be the end of the document, second time, the first search starting point
            bool blnZoekVerderAll = false;
            bool blnSaveSucces = true;      //We gaan niet verder zoeken in andere templates als het saven mislukt.

            if (iPosSearchEnd > _iPos) //Hebben we de hele tekst al doorzocht?
            {
                try
                {
                    iPosTemp = strContent.IndexOf(_strZoekterm, _iPos + 1);

                    if (iPosTemp >= 0)
                    {   //De zoekterm komt voor in de templatetekst
                        _iPos = iPosTemp;
                        rtbContent.Select(_iPos, iLen); //Maak de juiste selectie
                        dgvQueries.ClearSelection();    //Clear the selected queries, otherwise it is not clear what is found: The selected text or the selected query
                        UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
                        rtbContent.ScrollToCaret(); //Dit zorgt ervoor dat de geselecteerde tekst ook zichtbaar wordt!

                        if (!rtbContent.Focused)
                        {   //Op verzoek Erwin: Bij Crtl+F -> Enter en er wordt iets gevonden,                                      
                            rtbContent.Focus();     //dan verwacht je je focus in de tekstbox en ben je de caret kwijt als hij hier niet op staat
                        }
                        else
                        {   //De focus was er nog, maar de caret is verplaatst, dus caret opnieuw maken; Ervaring leert dat hij weg kan zijn
                            rtbContent.CreateNewCaret();
                        }
                    }
                    else
                    {   //De zoekterm komt niet voor in de templatetekst, misschien in de queries?
                        if (!_blnSearchEndReached)
                        {   //Don't search the queries twice, if the end has already been passed
                            blnZoekVerderAll = !blnHitInQueriesVerder();
                        }
                        else
                        {   //Go straight to SearchEndForwards() or ZoekVerderAll()
                            blnZoekVerderAll = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error searching text", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(ex);
                }
            }
            else
            {
                if (!_blnSearchEndReached)
                {   //Don't search the queries twice, if the end has already been passed
                    blnZoekVerderAll = !blnHitInQueriesVerder();
                }
                else
                {   //Go straight to SearchEndForwards() or ZoekVerderAll()
                    blnZoekVerderAll = true;
                }
            }

            if (blnZoekVerderAll)
            {   //We reached the end of the text and also didn't find anything in the queries
                if (btnDoorzoekAlles.Checked)
                {
                    DialogResult dlgRes = DialogResult.Yes;

                    if (m_dirty) //We moeten eerst het huidige template opslaan, omdat we anders de zoektekst in het huidige template niet kunnen vinden, wanneer we rond zoeken en weer bij het huidige template aan komen.
                    {
                        dlgRes = MessageBox.Show("Save current template?", "Confirm save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                        if (dlgRes == DialogResult.Yes)
                        {
                            blnSaveSucces = SaveTemplate();
                        }
                        if (dlgRes == DialogResult.No)
                        {
                            m_dirty = false;
                        }
                    }
                    if (!(dlgRes == DialogResult.Cancel) && blnSaveSucces)
                    {   //We gaan niet verder zoeken wanneer de save gecanceled of mislukt is.
                        ZoekVerderAll();
                    }
                }
                else
                {
                    SearchEndForwards(strContent);
                }
            }
        }

        private void SelecteerTekstTerug(string strContent)//Hier wordt de zoekterm geselecteerd in de tekstbox, wanneer er teruguit gezocht wordt:
        {
            int iLen = _strZoekterm.Length;
            int iPosNext = 0; //Dit is de laatstgevonden positie
            int iPosTemp = 0;
            bool blnSaveSucces = true;      //We gaan niet verder zoeken in andere templates als het saven mislukt.

            if (strContent.Contains(_strZoekterm))//Als de zoekterm niet voor komt, dan is de eerste hit al geselecteerd
            {
                dgvQueries.ClearSelection();        //De zoekterm is nog gevonden in de templatetekst, dus de laatstegevonden query mag gedeselecteerd worden

                try
                {
                    while (iPosNext >= 0) //Er wordt gezocht totdat de laaste hit is bereikt.
                    {
                        iPosTemp = iPosNext;
                        iPosNext = strContent.IndexOf(_strZoekterm, iPosTemp + 1);
                    }
                    _iPos = iPosTemp;
                    rtbContent.Select(_iPos, iLen); //De selectie in de tekstbox wordt gemaakt.
                    UpdateCaretPositionLabels();     //Zet de Line- en Columnnumber op de juiste waarden
                    rtbContent.ScrollToCaret(); //Dit zorgt ervoor dat de geselecteerde tekst ook zichtbaar wordt!
                    if (!rtbContent.Focused)
                    {   //Op verzoek Erwin: Bij Crtl+F -> Enter en er wordt iets gevonden, 
                        rtbContent.Focus();     //dan verwacht je je focus in de tekstbox en ben je de caret kwijt als hij hier niet op staat
                    }
                    else
                    {   //De focus was er nog, maar de caret is verplaatst, dus caret opnieuw maken; Ervaring leert dat hij weg kan zijn
                        rtbContent.CreateNewCaret();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error searching text", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(ex);
                }
            }
            else
            {
                if (btnDoorzoekAlles.Checked)
                {   //We gaan nu teruguit zoeken door alle XDoc-templates
                    DialogResult dlgRes = DialogResult.Yes;

                    if (m_dirty) //We moeten eerst het huidige template opslaan, omdat we anders de zoektekst in het huidige template niet kunnen vinden, wanneer we rond zoeken en weer bij het huidige template aan komen.
                    {
                        dlgRes = MessageBox.Show("Save current template?", "Confirm save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                        if (dlgRes == DialogResult.Yes)
                        {
                            blnSaveSucces = SaveTemplate(); //Niet zoeken in andere templates als het saven mislukt, dan gaat er werk verloren.
                        }
                        if (dlgRes == DialogResult.No)
                        {
                            m_dirty = false;
                        }
                    }

                    if (!(dlgRes == DialogResult.Cancel) && blnSaveSucces)
                    {   //We gaan niet verder zoeken wanneer de save gecanceled of mislukt is.
                        ZoekTerugAll();
                    }
                }
                else
                {
                    SearchEndBackwards();
                }
            }
        }

        private void ZoekVerderAll() //Deze routine verzorgt het zoeken door ALLE XDoc-templates
        {
            int iPosStart = _iPos; //We gaan straks kijken of we niet op dezelfde positie zijn teruggekomen. Dan hebben we dus niks nieuws gevonden, maar hebben we opnieuwe onze oude selectie gevonden.
            IXTemplate temp_template = null;
            int templateID = 0;
            Boolean blnGevonden = false;
            int iTotaalRijen = dgvTemplates.Rows.Count; //Aantal rijen in de gridview
            int iRow = 0; //Huidige rij tijdens iteratie
            int iRowStart = 0; // We willen onthouden waar we begonnen met zoeken, omdat we anders te veel werk verrichten: we loopen dan door alle rijen en niet totaan waar we begonnen zijn
            Int16 iLoop = 0;
            Cursor curStand = Cursors.Arrow; //We maken een cursor aan die aan het einde van de routine ingesteld wordt.
            int iTemp = -1;

            //We don't want to remember Search Start en Search End (reached), since we are not searching in one template
            _blnSearchEndReached = false;   //At the next searchrequest, we can search again from the 
            _blnInSearchMode = false;       //Drop out of search mode and start the searching all over if nothing was ever searched.

            try
            {
                curStand = base.Cursor; //We slaan even op hoe de oude cursor er uit zag om hem later weer terug te zetten.
                base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                

                if (_blnZoekAllRecursive)
                {   //Het is verboden dat deze functie recursief wordt uitgevoerd (dit zou kunnen gebeuren, omdat SelecteerTekst*** wordt aangeroepen, welke deze functie weer aan kan roepen). We zouden dan een oneindige recursieve loop krijgen en die wil ik voorkomen. 
                    MessageBox.Show("An endless recursive loop has occured in ZoekVerderAll()." +
                                        "\n\nThe loop is now exited.", "Error searching", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _blnZoekAllRecursive = false;
                    iLoop = 3;
                }

                if (dgvTemplates.SelectedRows.Count > 0) //Is er wel een template geselecteerd?
                {
                    iRow = dgvTemplates.SelectedRows[0].Index + 1; //Dit is het volgnummer van rij die de geselecteerde rij opvolgt.
                    iRowStart = iRow; //We willen uit de loop stappen wanneer we voor de tweede keer deze rij tegenkomen.
                }
                else
                {
                    iRow = 0; //We beginnen met zoeken naar tekst in de eerste rij, omdat er niks geselecteerd is.
                    iRowStart = iRow; //We willen uit de loop stappen wanneer we voor de tweede keer deze rij tegenkomen.
                }

                while (iLoop < 2) //Een extra loop, omdat we bovenaan doorzoeken, wanneer we niks gevonden hebben.
                {
                    while ((!(blnGevonden)) && iRow < iTotaalRijen)
                    {
                        int.TryParse(dgvTemplates.Rows[iRow].Cells[0].Value.ToString(), out templateID);
                        try //We zetten hier een extra try-catch omheen, omdat het denkbaar is dat het datagrid niet meer up to date is en de gegeven templateID niet geladen kan worden.
                        {
                            temp_template = m_manager.LoadTemplate(templateID);
                            if (temp_template.Content.ToLower().Contains(_strZoekterm))
                            {   //zoekterm gevonden in de templatetekst
                                blnGevonden = true;
                            }
                            else if (blnHitInQueries(templateID))   //We hoeven dit niet uit te voeren als hij hierboven al gevonden is
                            {   //zoekterm gevonden in de queryteksten
                                blnGevonden = true;
                            }
                        }
                        catch { }
                        if ((iLoop > 0) && (iRow == iRowStart))
                        {
                            iRow = iTotaalRijen + 1; //We staan op het punt om voor de tweede keer de rij iRowStart te controleren. We hebben dus alle rijen een keer gehad en we gaan uit de loop.
                        }
                        iRow += 1; //We gaan zoeken in het document in de volgende rij 
                    }

                    if (blnGevonden)
                    {
                        SelecteerRijOpID(templateID); //De rij met gevonden tekst wordt geselecteerd                                
                        _blnZoekAllRecursive = true; //In selecteerdtekst*** kan deze functie weer aangeroepen wordt, maar dit mag nooit recursief gebeuren
                        SelecteerTekstVerder(rtbContent.Text.ToLower()); //Er wordt in de nieuwe tekst verder gezocht.
                        _blnZoekAllRecursive = false;
                        iLoop = 3; //We moeten wel uit de whilelloop van het hoogste niveau, wanneer er iets gevonden is

                        int.TryParse(dgvTemplates.SelectedRows[0].Index.ToString(), out iTemp);
                        if ((iTemp == iRowStart - 1) && (_iPos == iPosStart))
                        { //We zitten in hetzelfde template en op dezelfde positie:  We hebbenn dus geen nieuwe tekst gevonden: Melding hiervan geven.
                            MessageBox.Show("All templates have been searched.\n\n'" + _strZoekterm +
                                                "' only occurs once in all templates.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        iLoop += 1; //We willen nooit meer dan 2 keer door de lijst loopen.
                        if (iLoop > 1)
                        {
                            MessageBox.Show("The searchterm does not occur in the templates.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        iRow = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occured in ZoekVerderAll(). This is the routine which searches through all methods.\n\n" +
                                    ex.Message, "Error searching", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(ex);
            }
            finally
            {
                base.Cursor = curStand; //We zetten de cursor weer terug op het oude type.
                _blnZoekAllRecursive = false;
            }
        }

        private void ZoekTerugAll() //Deze routine verzorgt het TERUGUIT zoeken door ALLE XDoc-templates
        {
            int iPosStart = _iPos; //We gaan straks kijken of we niet op dezelfde positie zijn teruggekomen. Dan hebben we dus niks nieuws gevonden, maar hebben we opnieuwe onze oude selectie gevonden.
            IXTemplate temp_template = null;
            int templateID = 0;
            Boolean blnGevonden = false;
            int iTotaalRijen = dgvTemplates.Rows.Count; //Aantal rijen in de gridview
            int iRow = 0; //Huidige rij tijdens iteratie
            int iRowStart = 0; // We willen onthouden waar we begonnen met zoeken, omdat we anders te veel werk verrichten: we loopen dan door alle rijen en niet totaan waar we begonnen zijn
            Int16 iLoop = 0;
            Cursor curStand = base.Cursor; //We slaan even op hoe de oude cursor er uit zag om het later weer terug te zetten.
            int iTemp;

            try
            {
                base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.

                if (_blnZoekAllRecursive)
                {   //Het is verboden dat deze functie recursief wordt uitgevoerd (dit zou kunnen gebeuren, omdat SelecteerTekst*** wordt aangeroepen, welke deze functie wee aan kan roepen). We zouden dan een oneindige recursieve loop krijgen en die wil ik kosten wat kost voorkomen. 
                    MessageBox.Show("And endless loop has occured in ZoekTerugAll().\n\nThe loop is now exited.", "Error searching", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _blnZoekAllRecursive = false;
                    iLoop = 3;
                }

                if (dgvTemplates.SelectedRows.Count > 0) //Is er wel een template geselecteerd?
                {
                    iRowStart = dgvTemplates.SelectedRows[0].Index; //We willen uit de loop stappen wanneer we voor de tweede keer deze rij tegenkomen.
                    iRow = iRowStart - 1; //Dit is het volgnummer van rij die de geselecteerde rij opvolgt.  

                    if (iRow < 0)
                    {   //Als we bij de eerste rij waren (index=0), dan gaan we verder zoeken bij de laatste
                        iRow = iTotaalRijen - 1;
                    }
                }
                else
                {
                    iRow = iTotaalRijen - 1; //We beginnen met zoeken naar tekst in de laatste rij, omdat er niks geselecteerd is en we achteruit zoeken.
                    //iRowStart = iRow; //We willen uit de loop stappen wanneer we voor de tweede keer deze rij tegenkomen.
                    iRowStart = 0; //We beginnen onderaan, dus als we bij de eerste rij zijn, dan hebben we alles doorzocht.
                }

                while (iLoop < 2) //Een extra loop, omdat we onderaan doorzoeken, wanneer we niks gevonden hebben.
                {
                    while ((!(blnGevonden)) && iRow >= 0)
                    {
                        int.TryParse(dgvTemplates.Rows[iRow].Cells[0].Value.ToString(), out templateID);
                        try //We zetten hier een extra try-catch omheen, omdat het denkbaar is dat het datagrid niet meer up to date is en de gegeven templateID niet geladen kan worden.
                        {
                            temp_template = m_manager.LoadTemplate(templateID);
                            if (temp_template.Content.ToLower().Contains(_strZoekterm))
                            {   //zoekterm gevonden in de templatetekst
                                blnGevonden = true;
                            }
                            else if (blnHitInQueries(templateID))   //We hoeven dit niet uit te voeren als hij hierboven al gevonden is
                            {   //zoekterm gevonden in de queryteksten
                                blnGevonden = true;
                            }
                        }
                        catch { }

                        if ((iLoop > 0) && (iRow == iRowStart))
                        {
                            iRow = -3; //We staan op het punt om voor de tweede keer de rij iRowStart te controleren. We hebben dus alle rijen een keer gehad en we gaan uit de loop.
                        }
                        iRow -= 1; //We gaan zoeken in het document in de volgende rij 
                    }

                    if (blnGevonden)
                    {
                        SelecteerRijOpID(templateID); //De rij met gevonden tekst wordt geselecteerd                                

                        _blnZoekAllRecursive = true; //In SelecteerTekst*** kan deze functie weer aangeroepen worden, maar dit mag nooit recursief gebeuren
                        //SelecteerTekstTerug(rtbContent.Text.ToLower()); //Er wordt in de nieuwe tekst TERUGUIT gezocht.
                        btnZoekTerug_Click(null, null);
                        _blnZoekAllRecursive = false;

                        iLoop = 3; //We moeten wel uit de whilelloop van het hoogste niveau, wanneer er iets gevonden is
                        int.TryParse(dgvTemplates.SelectedRows[0].Index.ToString(), out iTemp);
                        if ((iTemp == iRowStart) && (_iPos == iPosStart))
                        { //We zitten in hetzelfde template en op dezelfde positie:  We hebbenn dus geen nieuwe tekst gevonden: Melding hiervan geven.
                            MessageBox.Show("All templates have been searched.\n\n\"" + _strZoekterm + "\" only occurs once in all templates.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        iLoop += 1; //We willen nooit meer dan 2 keer door de lijst loopen.
                        if (iLoop > 1)
                        {
                            MessageBox.Show("The searchterm does not occur in the templates.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        iRow = iTotaalRijen - 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occured in ZoekTerugrAll(). This is the routine which searches backwards through all templates.\n\n" + ex.Message, "Error searching", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(ex);
            }
            finally
            {
                base.Cursor = curStand; //We zetten de cursor weer terug op het oude type.
            }
        }

        private void SearchEndForwards(string strContent)    //Deze routine loopt wanneer bij het vooruitzoeken het einde is bereikt, en Doorzoek alles niet aan staat.
        {
            string strDeelTekst;

            if (!_blnSearchEndReached)
            {   //Be carefull for endless regression! This is recursive:
                _blnSearchEndReached = true;    //This makes sure we don't start searching from the beginning twice
                _iPos = -1;
                _GeneralToolTip.Show("End of document was reached. Now searching from the top, until search starting point is reached.", grpDetails, -5, -5, 6000);

                strDeelTekst = strContent.Substring(0, _iPosSearchStart);   //Only search from 0 to where we started!
                SelecteerTekstVerder(strDeelTekst);
            }
            else
            {
                _GeneralToolTip.Hide(grpDetails);   //Tooltip information has become obsolete
                MessageBox.Show("We've reached the starting point of your search.\n\nAll text & templates were searched.", "Search finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _blnSearchEndReached = false;   //At the next searchrequest, we can search again from the 
                _blnInSearchMode = false;       //Drop out of search mode and start the searching all over if nothing was ever searched.
            }
        }

        private void SearchEndBackwards()    //Deze routine loopt wanneer bij het vooruitzoeken het einde is bereikt, en Doorzoek alles niet aan staat.
        {
            DialogResult dlgRes = MessageBox.Show("The searchterm does not exist in template text, query names or query texts (while searching backwards)." +
                "\n\n(It might occur in the template name...)\n\nDo you want to continue searching backwards starting at the end of the document?",
                "End of search", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            if (dlgRes == DialogResult.OK)
            {
                rtbContent.SelectionStart = rtbContent.Text.Length + 1;     //Zet de caret aan het einde
                rtbContent.SelectionLength = 0;                             //Maak geen selectie
                _blnSearchEndReachedBackwards = true;                       //If this is true we will start searching in the queries
                btnZoekTerug_Click(null, null);                             //Zoek!                    
            }
        }

        private bool blnHitInQueries(int templateID) //Deze routine zoekt in de queries van het template dat in de m_manager is geladen (deze is nu niet geopend)                
        {
            bool blnHit = false;
            IXQuery iQ;
            IDName[] queryLijst = m_manager.GetQueriesList(templateID);

            foreach (IDName idNaam in queryLijst)
            {
                iQ = m_manager.LoadQuery(idNaam.ID);
                if (iQ.Name.ToLower().Contains(_strZoekterm) || iQ.QueryText.ToLower().Contains(_strZoekterm))
                {
                    blnHit = true;
                    break;
                }
            }

            return blnHit;
        }

        private bool blnHitInQueriesVerder() //Deze routine zoekt in de queries van het geopende template naar de zoekterm
        {
            bool blnHit = false;
            int iQueryAantal = m_queries.Count;
            int i = 0;
            if (dgvQueries.SelectedRows.Count > 0)
            {
                i = dgvQueries.SelectedRows[0].Index + 1;
            }
            IXQuery iQ;

            while (i < iQueryAantal)
            {
                iQ = (IXQuery)m_queries[i];
                if (iQ.Name.ToLower().Contains(_strZoekterm) || iQ.QueryText.ToLower().Contains(_strZoekterm))
                {
                    dgvQueries.ClearSelection();
                    dgvQueries.Rows[i].Selected = true;
                    dgvQueries.FirstDisplayedScrollingRowIndex = i; //We zorgen dat er naar de geselecteerde rij toegescrolld wordt, wanneer deze buiten beeld valt.
                    blnHit = true;
                    //iQueryHit = i;
                    break;
                }
                i += 1;
            }

            if (!blnHit)
            {
                //iQueryHit = -1;     //Er is niks (meer) gevonden in de queries. Bij de volgende zoekopdracht gaan we weer van bovenaan zoeken. Ook nodig voor het geval dat we naar een andere template gaan.                    
            }

            return blnHit;
        }

        private bool blnHitInQueriesTerug() //Deze routine zoekt ACHTERUIT in de queries van het geopende template naar de zoekterm
        {
            bool blnHit = false;
            int iQueryAantal = m_queries.Count;
            //int i = iQueryHit - 1;      //We selecteren de vorige query
            int i = iQueryAantal - 1;
            if (dgvQueries.SelectedRows.Count > 0)
            {
                i = dgvQueries.SelectedRows[0].Index - 1;
            }
            IXQuery iQ;

            while (i > -1)
            {
                iQ = (IXQuery)m_queries[i];
                if (iQ.Name.ToLower().Contains(_strZoekterm) || iQ.QueryText.ToLower().Contains(_strZoekterm))
                {
                    dgvQueries.ClearSelection();
                    dgvQueries.Rows[i].Selected = true;
                    dgvQueries.FirstDisplayedScrollingRowIndex = i; //We zorgen dat er naar de geselecteerde rij toegescrolld wordt, wanneer deze buiten beeld valt.                 
                    blnHit = true;
                    break;
                }
                i -= 1;
            }

            if (!blnHit)
            {
                //iQueryHit = -1;     //Er is niks (meer) gevonden in de queries. Bij de volgende zoekopdracht gaan we weer van onderaf zoeken. Ook nodig voor het geval dat we naar een andere template gaan.                    
            }

            return blnHit;
        }

        #endregion Zoeken

        #region Open Template in Visual Studio

        private void SaveToFile(string strURL) //Wij gaan de template tijdelijk als file opslaan.
        {   //Foutafhandeling in bovenliggende void!

            FileStream fs = new FileStream(strURL, FileMode.Create);
            TextWriter textWriter = new StreamWriter(fs);

            try
            {
                textWriter.Write(rtbContent.Text);
            }
            catch (Exception ex)
            {
                throw ; //Alle foutafhandeling vindt in bovenliggende void plaats!
            }
            finally
            {
                textWriter.Close();
                fs.Close();
            }
        }

        private void CreateReadmeFile() //Deze readmefile komt in %USER%\Application Data\XDocHQ\ te staan voor een beetje toelichting.
        {
            try
            {
                FileStream fs = new FileStream(_strXDocPath + _ReadMeFileName, FileMode.Create);
                TextWriter writeText = new StreamWriter(fs);
                writeText.WriteLine("The files in this folder are temporary files of XDocHQ.");
                writeText.WriteLine("These files are created if a template is opened in Visual Studio.");
                writeText.WriteLine("");
                writeText.WriteLine("Files which remain unchanged for a period longer than " + _BewaarDuur + " days, and who have an 'xdoc' extension, will be automatically removed when XDocHQ closes.");
                writeText.WriteLine("");
                writeText.WriteLine("It is completely safe to remove these files, or the entire folder, by hand.");
                writeText.WriteLine("");
                writeText.WriteLine("");
                writeText.WriteLine("Groet,");
                writeText.WriteLine("Lucien");
                writeText.Close();
                fs.Close();
            }
            catch { }
        }

        private void GetRespondingVS(ref string strURL, ref string strFile) //Hier wordt de oude VS gebruikt om een nieuw template te laden, als dit niet lukt, dan wordt er een nieuwe VS geopend
        {   //Foutafhandeling in bovenliggende void                       
            List<int> WrongVSProcesLijst = new List<int>();
            bool blnUseOldVSProcess = true;   //We gaan er vanuit dat er al een VS draait. Pas als blijkt dat dit niet zo is, dan gaan we er een opstarten.

            //We gaan het proces bepalen waarin we het template gaan laden:
            if (!(_VSProcessID == -1))
            {   //Er is een ProcessID beschikbaar van een eerder geopende VS. Dit is altijd de juiste! 
                try
                {
                    Process p = Process.GetProcessById(_VSProcessID);

                    if (p.Responding)
                    {
                        if (!(p.HasExited)) //Dit lijkt overbodig, maar is het niet: Als je te snel na het sluiten van VS op open in VS klikt, dan lijkt VS nog actief te zijn, terwijl hij bezig is met afsluiten
                        {
                            //We hebben een beschikbaar proces om de template in te gaan laden
                            //_VSWindowHandle = p.MainWindowHandle; //Deze hoort al goed gevuld te zijn, maar voor de zekerheid stellen we hem in, omdat we met dit proces aan de slag gaan.
                            blnUseOldVSProcess = true;
                        }
                        else
                        {   //Het process is gesloten of gestorven, of wat dan ook: Geen risico nemen; nieuwe VS opstarten 
                            _VSProcessID = -1;
                            _VSWindowHandle = IntPtr.Zero;
                            blnUseOldVSProcess = false;
                        }
                    }
                    else
                    {   //Het process is gesloten of gestorven, of wat dan ook: Geen risico nemen; nieuwe VS opstarten 
                        _VSProcessID = -1;
                        _VSWindowHandle = IntPtr.Zero;
                        blnUseOldVSProcess = false;
                    }
                }
                catch
                {   //Het process is gesloten of gestorven, of wat dan ook: Geen risico nemen; nieuwe VS opstarten 
                    _VSProcessID = -1;
                    _VSWindowHandle = IntPtr.Zero;
                    blnUseOldVSProcess = false;
                }
            }
            else
            {
                _VSProcessID = -1;
                _VSWindowHandle = IntPtr.Zero;
                blnUseOldVSProcess = false;
            }

            if (blnUseOldVSProcess)
            {   //We gaan het bestaande VS-proces gebruiken om de template te laden                     
                OpenTemplateInOldVS(ref strURL);
            }
            else
            {   //Er draait nog geen VS 'van ons', dus we gaan een nieuwe opstarten.
                //Eerst gaan we kijken of er -toevallig- al een VS draait, dit is nodig omdat we later niet het verkeerde proces aan willen spreken!
                foreach (Process VSProcess in Process.GetProcessesByName("devenv"))
                {   //We willen weten of er al VS's actief zijn
                    WrongVSProcesLijst.Add(VSProcess.Id);
                }

                OpenTemplateInNewVS(ref strURL);

                foreach (Process FoundProcess in Process.GetProcessesByName("devenv"))
                {   //We willen weten of er al VS's actief zijn
                    if (!(WrongVSProcesLijst.Contains(FoundProcess.Id)))
                    {   //Dit is ons proces, want het bestond aan het begin van deze procedure nog niet
                        _VSProcessID = FoundProcess.Id;                     //We slaan het processId                             
                        //_VSWindowHandle = FoundProcess.MainWindowHandle;    //en de Handle op, zodat we onze VS-instantie gemakkelijk kunnen hergebruiken en aanroepen
                        _blnAlternativeEditor = false;                      //We hebben niet te maken met een externe editor
                        _blnAlterWarningShown = false;
                    }
                }
                if (_VSProcessID == -1)   //  if ((_VSProcessID == -1) || (_VSWindowHandle == IntPtr.Zero))
                {   //Er is geen VS geopend. Is de .XDoc-extensie misschien aan een andere editor gekoppeld?
                    foreach (Process p in Process.GetProcesses())
                    {
                        if (p.MainWindowTitle.ToLower().Contains(strFile.ToLower()))
                        {
                            _blnAlternativeEditor = true;
                            if (!(_blnAlterWarningShown))
                            {
                                _blnAlterWarningShown = true;   //We tonen de boodschap maar een keer.
                                MessageBox.Show("Watch out!\n\nThe extension '.XDoc' has not been coupled to Visual Studio.\n" +
                                                    "The template has been loaded in an alternative editor.\n" +
                                                    "Therefore some functionality is not available.\n\n" +
                                                    "You yourself have to take care of entering your changes into XDocHQ.",
                                                    "Alternative editor used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                    }
                }
            }
        }

        private void OpenTemplateInOldVS(ref string strURL)
        {
            if (_VSWindowHandle == IntPtr.Zero)
            {
                GetVSWindowHandle();
            }

            if (_VSWindowHandle != IntPtr.Zero)
            {   //Er is een opgestartte VS bekend, zoals het hoort
                try
                {
                    if (SetForegroundWindow(_VSWindowHandle))
                    {   //VS staat nu op de voorgrond, we kunnen er commando's heen sturen:
                        Thread.Sleep(300);
                        SendKeys.SendWait("^o");        //Ctrl+O = Open File
                        Thread.Sleep(300);
                        SendKeys.SendWait(strURL);      //Welke file?
                        Thread.Sleep(300);
                        SendKeys.SendWait("{ENTER}");   //ENTER = Open 
                    }
                }
                catch (Exception ex)
                {
                    string message = "An error has occured while loading the template into the already opened Visual Studio window.\n\n" +
                                        "Go to Visual Studio, close it manually and try again." + ex.Message;
                    MessageBox.Show(message, "Error activating Visual Studio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                    _VSWindowHandle = IntPtr.Zero;
                    _VSProcessID = -1;
                }
            }
            else
            {
                string strError = "Activating the old Visual Studio window has not succeeded.\n\nDid it crash?\n\nClose Visual Studio and try again.";
                MessageBox.Show(strError, "Error activating Visual Studio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(strError);
                _VSWindowHandle = IntPtr.Zero;
                _VSProcessID = -1;
            }
        }

        private void OpenTemplateInNewVS(ref string strURL) //Er wordt een nieuwe VS opgestart en het template wordt hierin geopend
        {
            string devenv8 = @"C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe";
            string devenv9 = @"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe";

            try
            {
                Process.Start(strURL).WaitForInputIdle();       //Start het proces direct door alleen de XDoc op te geven.
            }
            catch
            {
                if (File.Exists(devenv9))
                {   //Als hier iets fout gaat, dan wordt dit correct afgevangen in de try-catch van bovenliggende methode
                    ProcessStartInfo startInf = new ProcessStartInfo(devenv9, "\"" + strURL + "\"");         //We gaan openen met VS2008
                    Process.Start(startInf).WaitForInputIdle();
                }
                else if (File.Exists(devenv8))
                {   //Als hier iets fout gaat, dan wordt dit correct afgevangen in de try-catch van bovenliggende methode
                    ProcessStartInfo startInf = new ProcessStartInfo(devenv8, "\"" + strURL + "\"");         //We gaan openen met VS2005
                    Process.Start(startInf).WaitForInputIdle();
                }
                else
                {
                    string message = "The filetype '.XDoc' has not been coupled to an external editor." +
                        "\n\nAlso Visual Studio was not found on the location expected:" +
                        "\n" + devenv9 + " or\n" + devenv8 + "\n" +
                        "\nSolve one of these problems!";
                    MessageBox.Show(message, "Error opening Visual Studio", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                }
            }
        }

        private void RestoreTemplateFromFile(string strURL) //De opgeslagen file wordt ingelezen in XDocHQ
        {
            FileStream fs = new FileStream(strURL, FileMode.Open);
            StreamReader sr = new StreamReader(fs);

            try
            {
                rtbContent.Text = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw ; //Alle foutafhandeling gaat naar de bovenliggende void
            }
            finally
            {
                sr.Close();
                fs.Close();
            }
        }

        private void ReloadFromVS(ref DialogResult dgRes, ref string strURL, ref string strFile) //De wijzigingen worden opgeslagen en hierna gaat de file weer ingelzen worden
        {
            if (_blnAlternativeEditor)
            {   //De template is niet in VS geladen, we hoeven dus alleen maar file in te laden. De gebruiker heeft een waarschuwing gehad dat hij verantwoordelijk is voor het opslaan van de file.
                if (dgRes.Equals(DialogResult.Yes))
                {
                    RestoreTemplateFromFile(strURL);
                }
            }
            else if (SaveTemplateInVS(ref strURL, ref strFile, ref dgRes) && dgRes.Equals(DialogResult.Yes)) //In SaveTemplateInVS wordt de template door VS opgeslagen
            {   //Tekstboxvak opnieuw vullen met gewijzigde waarde
                RestoreTemplateFromFile(strURL);
            }
        }

        private bool SaveTemplateInVS(ref string strURL, ref string strFile, ref DialogResult dgRes) //Deze functie vraagt VS om het geopende document te sluiten en eventueel op te slaan, vervolgens wordt VS weer geminimaliseerd
        {
            bool blnSucces = false;

            if (_VSWindowHandle == IntPtr.Zero)
            {
                GetVSWindowHandle();
            }

            if (_VSWindowHandle != IntPtr.Zero)
            {   //Er is een opgestartte VS bekend, zoals het hoort
                try
                {
                    Process p = Process.GetProcessById(_VSProcessID);

                    if (p.MainWindowTitle.ToLower().Contains(strFile.ToLower()))
                    {   //De template staat nog open in VS. Dus we moeten hem saven en sluiten.                        
                        if (SetForegroundWindow(_VSWindowHandle))
                        {   //VS staat nu op de voorgrond, er is om afsluiting gevraagd en we moeten alleen nog even Ja zeggen op de vraag of de wijzigingen opgeslagen moeten worden.                        
                            Thread.Sleep(300);
                            SendKeys.SendWait("^s");        //We slaan wijzigingen altijd in de file op. Of ze in XDocHQ worden ingeladen hangt af van het antwoord op de vraag in het voorgaande dialoogvenster.                 
                            Thread.Sleep(300);
                            SendKeys.SendWait("^{F4}");     //Vraag om het sluiten van het huidige document

                            //TODO: Moet hier misschien code om te controleren of het bestand ook echt weggeschreven is?          
                            blnSucces = true;
                        }
                    }
                    else
                    {
                        if (p.Responding)
                        {   //VS draait nog gewoon, maar de template is wel gesloten. We gaan er vanuit dat de gebruiker bewust het template gesloten heeft en al-dan-niet opgeslagen heeft en dat hij er bewust voor heeft gekozen om het opgeslagen template te laden in XDocHQ:
                            blnSucces = true;
                        }
                    }
                }
                catch
                {
                    blnSucces = false;
                }
                //finally { ShowWindow(_VSWindowHandle, 6);   //Functie uit user32.dll, code '6' betekent minimaliseren }
            }

            if (!(blnSucces))
            {
                _VSWindowHandle = IntPtr.Zero;  //Het aanspreken van het bestaande VS-window is niet goed gegaan, dus we halen de referenties hiernaar weg.
                _VSProcessID = -1;

                if (dgRes == DialogResult.Yes)
                {   //De gebruiker had graag de wijzigingen in VS in XDocHQ geladen, maar VS kon niet goed aangesproken worden, dus een duidelijk melding naar de gebruiker.
                    string strError = "Activating Visual Studio did not succeed.\n\nHas it been closed or did it crash?\n\n" +
                                        "Only close Visual Studio after XDocHQ has closed the template within Visual Studio.\n\n" +
                                        "Now NO template has been loaded in XDocHQ.\n" +
                                        "The last changes saved by Visual Studio, have been saved in this file: \n" + strURL;
                    MessageBox.Show(strError, "Error activating Visual Studio.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(strError);
                }
            }

            this.Focus();
            return blnSucces;
        }

        private void GetVSWindowHandle() //De MainWindowHandle van het Visual Studioproces wordt vastgelegd
        {
            try
            {
                Process p = Process.GetProcessById(_VSProcessID);
                _VSWindowHandle = p.MainWindowHandle;
            }
            catch
            {
                _VSWindowHandle = IntPtr.Zero;
            }
        }

        //Een externe methode om het Visual Studioscherm op de voorgrond te krijgen, om er vervolgens een SendKey naar toe te kunnen sturen:
        [System.Runtime.InteropServices.DllImport("user32.dll", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);

        #endregion Open Template in Visual Studio

        #region TemplateID Wijzigen

        private bool IsNewTemplateIDOK()
        {   //We gaan controleren of het zojuist ingevulde templateid al voor komt in de database
            int iNieuwTemplateID = txtID.IntValue;
            bool blnIDCorrect = true;

            m_supressUIEvents = true;

            try
            {
                this.Cursor = Cursors.WaitCursor;
                IDName[] templates = m_manager.GetTemplatesList();  //Haal de lijst met actuele templates op met de XDocenigine

                foreach (IDName idName in templates)
                {
                    if (idName.ID == iNieuwTemplateID)
                    {
                        txtID.IntValue = _iOriginalTemplateID;
                        MessageBox.Show("There already exists a template with ID " + iNieuwTemplateID + ".\n\nThe template ID must be unique.",
                                            "TemplateID not unique", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        blnIDCorrect = false;   //We hebben het ID teruggezet, dus we hoeven het template niet te saven
                        txtID.Focus();
                        break;
                    }
                }

                if (blnIDCorrect)
                {   //Het ID is correct gewijzigd, dus het template moet gesaved worden
                    SetDirty(true); //Let op! Het is hier verleidelijk om SetDirty(blnIDCorrect) te zeggen, maar we willen nooit SetDirty uitzetten, omdat er misschien al andere wijzigingen zijn gedaan!!!
                    _iOriginalTemplateID = txtID.IntValue;  //We zetten het originele template op de goedgekeurde nieuwe waarde. Hierdoor wordt deze routine niet nog een keer gedraaid en we houden geen oude waarde vast die ooit is ingevoerd
                }
            }
            catch (Exception ex)
            {
                string Message = "An error occured while checking the new template ID.\n\n" + ex.Message +
                                    "\n\nIs there a connection to the database?\n\nThe template ID could not be changed.";
                MessageBox.Show(Message, "Error checking template ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(Message);
                blnIDCorrect = false;
            }
            finally
            {
                m_supressUIEvents = false;
                this.Cursor = Cursors.Arrow;
            }

            return blnIDCorrect;
        }

        private void ChangeTemplateID() //Wijzig het templateID. Er zitten wat haken en ogen aan, omdat het ID een primary key is. Methode is een stuk uitgebreiden dan een simpele UPDATE
        {
            this.Cursor = Cursors.WaitCursor;
            bool blnRollBack = false;
            string strOldID = m_template.ID.ToString();
            string strNewID = txtID.Text;
            string strUniqueName = "omzeiling_unieke_naamseis" + DateTime.Now.Ticks.ToString(); //Er kan een eis van unieke naam op s_templates zitten. Tussentijds deze naam gebruiken.
            string strErrorMessage = "An error occured while changing the template ID.\n\nSaving of the template did succeed.";
            //Het resultaat van queries: het aantal aangepaste rijen
            int iResultCheckID = -1, iResultInsertOn = -1, iResultInsNewRow = -1, iResultDelOldRow = -1, iResultUpdateQRYs = -1, iResultInsertOff = -1, iResultRestoreRightName = -1;
            //Is de query succesvol geweest?
            bool blnResultCheckID = false, blnResultInsertOn = false, blnResultInsNewRow = false, blnResultDelOldRow = false, blnResultUpdateQRYs = false, blnResultInsertOff = false, blnResultRestoreRightName = false;

            //Een KubionDataNamespaceobject dat gegevens kan ophalen
            ClientData dataManager = new ClientData(_strMainConnString);

            try
            {
                //Even een snelle voorcontrole: bestaat het ID al?
                string sqlCheckID = " select count (*) from s_templates where templateid = " + strNewID;

                //Hier wordt ID editbaar gemaakt
                string sqlInsertOn = "SET IDENTITY_INSERT s_templates on ";

                //Er zijn helaas verschillende typen s_templatestabellen, dus we kunnen geen statische SQL gebruiken
                string sqlInsNewRow = CreateSQLInsertNewRow(ref dataManager, strOldID, strNewID, strUniqueName, ref strErrorMessage);

                //Hier wordt het oude record met het oude ID verwijderd                                
                string sqlDelOldRow = "delete FROM s_templates WHERE (TemplateID = " + strOldID + ") ";

                //Hier worden de referenties naar het oude ID in de querytabel vernieuwd
                string sqlUpdateQRYs = "update s_queries set templateid = " + strNewID + " where templateid = " + strOldID + " ";

                //Er kan een eis voor unieke naam op de tabel staan. Er is tijdelijk een unieke naam weggeschreven. Oude record is vewijderd. Hier de juiste naam weer terugzetten.
                string sqlRestoreRightName = "update s_templates set TemplateName = '" + txtName.Text.Trim() + "' where templateid = " + strNewID;

                //Dit is een extra query om de historie met templatenaam "omzeling_unieke_naamseis..." te verwijderen. Niet belangrijk, wel netjes.
                string sqlDeleteWrongHist = "delete from s_templates_hist where templatename = '" + strUniqueName + "'";

                string sqlInsertOff = "SET IDENTITY_INSERT s_templates off "; //Hier wordt het ID weer op niet-editbaar gezet

                dataManager.BeginTransaction();

                blnResultCheckID = Int32.TryParse(dataManager.ExecuteQuery(sqlCheckID), out iResultCheckID);

                if (blnResultCheckID && iResultCheckID == 0)    //We gaan de rest niet eens proberen als het ID al bestaat
                {   //Voer de queries uit en controleer of er de juiste getallen terugkomen
                    if (!_blnIsSQLite)  //ID is always insertable in SQLite, so no IDENTITY_INSERT ON/OFF
                    {
                        blnResultInsertOn = Int32.TryParse(dataManager.ExecuteNonQuery(sqlInsertOn), out iResultInsertOn);                      //Maak ID insertable
                    }
                    else
                    {
                        blnResultInsertOn = true;
                    }
                    blnResultInsNewRow = Int32.TryParse(dataManager.ExecuteNonQuery(sqlInsNewRow), out iResultInsNewRow);                   //Schrijf kopie met unieke naam en nieuw id weg
                    blnResultDelOldRow = Int32.TryParse(dataManager.ExecuteNonQuery(sqlDelOldRow), out iResultDelOldRow);                   //Delete oude rij
                    blnResultUpdateQRYs = Int32.TryParse(dataManager.ExecuteNonQuery(sqlUpdateQRYs), out iResultUpdateQRYs);                //Update de queries met het nieuwe templateid
                    blnResultRestoreRightName = Int32.TryParse(dataManager.ExecuteNonQuery(sqlRestoreRightName), out iResultRestoreRightName);  //Zet de juiste naam weer terug
                    if (HasHistTables())
                    {   //Alleen uitvoeren als er History tables zijn!
                        dataManager.ExecuteNonQuery(sqlDeleteWrongHist);                                                                        //Haal de foute historie met de unieke naam weg
                    }
                    if (!_blnIsSQLite)  //ID is always insertable in SQLite, so no IDENTITY_INSERT ON/OFF
                    {
                        blnResultInsertOff = Int32.TryParse(dataManager.ExecuteNonQuery(sqlInsertOff), out iResultInsertOff);                   //Maak het ID weer noninsertable       
                    }
                    else
                    {
                        blnResultInsertOff = true;
                    }
                }
            }
            catch (Exception ex)
            {
                blnRollBack = true;
                strErrorMessage += "\n\n" + ex.Message;  //Error will be shown below                    
            }
            finally
            {
                if (!blnRollBack && blnResultInsertOn & blnResultInsNewRow & blnResultDelOldRow & blnResultUpdateQRYs & blnResultInsertOff & blnResultRestoreRightName)
                {   //Alle queries hebben een getal teruggegeven, dus geen error of andere rare dingen
                    if ((iResultInsNewRow > 0) & (iResultDelOldRow > 0) & (iResultUpdateQRYs > -1) & (iResultRestoreRightName > 0))
                    {   //Er is 1 rij toegevoegd, 1 verwijderd en 0 of meer queries geupdate en de naam is 1 keer weer goedgezet
                        dataManager.CommitTransaction();    //Er is een uitgebreide controle op alle queries geweest, omdat ID wijzigen en rij verwijderen tricky is.
                    }
                    else
                    {
                        blnRollBack = true;
                    }
                }
                else
                {
                    blnRollBack = true;
                }

                if (blnRollBack)
                {
                    dataManager.RollbackTransaction();

                    MessageBox.Show(strErrorMessage, "Error changing TemplateID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(strErrorMessage);

                    txtID.Text = strOldID;  //Set the ID in the textbox back to the m_template.ID, otherwise there is a discrepancy between textbox and templatelist
                }

                dataManager.Dispose();  //ruim de datamanager op
            }
            this.Cursor = Cursors.Default;
        }

        private string CreateSQLInsertNewRow(ref ClientData dataManager, string strOldID, string strNewID, string strUniqueName, ref string strErrorMessage)
        //Be carefull! 
        //Errors are thrown to the caller! Only call this method from ChangeTemplateID()
        {
            DataTable dt;
            bool blnDocType = false;
            bool blnRights = false;
            bool blnTemplateSelectionContent = false;
            string sqlRetrieveColumns;
            string sqlInsNewRow;

            if (_blnIsSQLite)
            {   //We allready know we have columns DocType and Rights and not TemplateSelectionContent in the SQLite table, so no need for the checking logic.
                sqlInsNewRow = "insert into s_templates " +                    //Hier wordt een nieuw record met de oude waarden en een nieuw ID toegevoegd
                                            " ( [TemplateID] , [TemplateName] , [TemplateDescription] , [TemplateContent] , [TemplateToSave] " +
                                            " ,[TemplateToRedirect] , [Unattended] , [DataTemplate] , [DocType] , [Rights] ) " +
                                            " SELECT " + strNewID + " as [TemplateID], " +
                                            " ('" + strUniqueName + "') as [TemplateName], " + //Tijdelijke toevoeging om unieke naamseis te omzeilen
                                            " [TemplateDescription] , [TemplateContent] , [TemplateToSave], " +
                                            " [TemplateToRedirect] , [Unattended] , [DataTemplate] , [DocType] , [Rights] " +
                                            " FROM s_templates WHERE (TemplateID = " + strOldID + ") ";
            }
            else
            {
                sqlRetrieveColumns = " select Column_Name " +
                                                " from INFORMATION_SCHEMA.COLUMNS " +
                                                " where TABLE_NAME='s_templates' ";

                sqlInsNewRow = "insert into s_templates " +                    //Hier wordt een nieuw record met de oude waarden en een nieuw ID toegevoegd
                                            " ( [TemplateID] , [TemplateName] , [TemplateDescription] , [TemplateContent] , [TemplateSelectionContent] , [TemplateToSave] " +
                                            " ,[TemplateToRedirect] , [Unattended] , [DataTemplate] , [DocType] , [Rights] ) " +
                                            " SELECT " + strNewID + " as [TemplateID], " +
                                            " ('" + strUniqueName + "') as [TemplateName], " + //Tijdelijke toevoeging om unieke naamseis te omzeilen
                                            " [TemplateDescription] , [TemplateContent] , [TemplateSelectionContent] , [TemplateToSave], " +
                                            " [TemplateToRedirect] , [Unattended] , [DataTemplate] , [DocType] , [Rights] " +
                                            " FROM s_templates WHERE (TemplateID = " + strOldID + ") ";

                try
                {
                    dt = dataManager.DTExecuteQuery(sqlRetrieveColumns);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {   //We gaan kijken welke kolommen voorkomen. Als ze niet voorkomen dan moet de query aangepast worden
                        if (dt.Rows[i][0].ToString().ToLower() == "doctype")
                        {
                            blnDocType = true;
                        }
                        else if (dt.Rows[i][0].ToString().ToLower() == "rights")
                        {
                            blnRights = true;
                        }
                        else if (dt.Rows[i][0].ToString().ToLower() == "templateselectioncontent")
                        {
                            blnTemplateSelectionContent = true;
                        }
                    }

                    if (!blnDocType)
                    {
                        sqlInsNewRow = sqlInsNewRow.Replace(@" , [DocType]", " ");
                    }
                    if (!blnRights)
                    {
                        sqlInsNewRow = sqlInsNewRow.Replace(@" , [Rights]", "");
                    }
                    if (!blnTemplateSelectionContent)
                    {
                        sqlInsNewRow = sqlInsNewRow.Replace(@" , [TemplateSelectionContent]", "");
                    }
                }
                catch (Exception ex)
                {
                    //strErrorMessage is a reference to a string declared in the caller. Caller will handle the message.
                    strErrorMessage = "An error occured while retrieving the available colomns in s_templates.\n" +
                                            "Therefore changing the template ID could not proceed.\n\nSaving of the template did succeed.";
                    //Be carefull! Error thrown to the caller. Only call this method from ChangeTemplateID()!
                    throw ;   //Throw this exception to the caller, so all errors concerning the Changing of the template ID are shown by one message!
                }
            }

            return sqlInsNewRow;
        }

        #endregion TemplateID Wijzigen

        #endregion Methods

        #region Event handlers

        #region MainForm

        private void MainForm_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");

            try
            {
                BindRecTemplatesGrid();     //Deze moet eerst, omdat _lstRecTemplates in BindTemplatesGrid geinitialiseerd moet zijn. Het dgvRecTemplatesgrid wordt aangemaakt (vulling kan pas bij eerste geopende template)

                if (!BindTemplatesGrid(-1)) //The templatesgrid is being filled; This is the only method called here, which is also called from other methods.
                {                           //It has it's own errorhandling, so it will never return an error, 
                    Environment.Exit(0);    //so it will have to close the application itself and not through the Catch, below.                    
                }
                
                LoadConnections();

                LoadReportsMenu();          //Deze methode vult het menu-item rapporten
                LoadProceduresMenu();          //Deze methode vult het menu-item procedures

                ClearForm(false);

                if (_iComArgTempID > -1)    //Open the template that was given as a commandline argument
                {
                    SelecteerRijOpID(_iComArgTempID);
                }
            }
            catch (Exception ex)
            {
                string message = "There was a fatal error in MainForm_Load!\nThe application will be closed!\nAre there errors in the config file?\n\n" +
                                    ex.Message + "\n\nStacktrace:\n" + ex.StackTrace;
                MessageBox.Show(message, "Error in MainForm_Load", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(message);
                Environment.Exit(0);
            }
        }

        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.S)
            {   //Ctrl+S = Save
                btnSave_Click(sender, EventArgs.Empty);
            }
            else if (e.Shift == true && e.KeyCode == Keys.F3)
            {   //Shift+F3 = teruguitzoeken
                if ((rtbZoekterm.Text == string.Empty) && (rtbContent.SelectedText.Trim().Length > 0))
                {
                    rtbZoekterm.Text = rtbContent.SelectedText;
                    _iPos = rtbContent.SelectionStart;
                }
                btnZoekTerug_Click(sender, EventArgs.Empty);
            }
            else if (e.KeyCode == Keys.F3)
            {   //F3 = Vooruitzoeken
                if ((rtbZoekterm.Text == string.Empty) && (rtbContent.SelectedText.Trim().Length > 0))
                {
                    rtbZoekterm.Text = rtbContent.SelectedText;
                    _iPos = rtbContent.SelectionStart;
                }
                btnZoekVerder_Click(sender, EventArgs.Empty);
            }
            else if (e.KeyCode == Keys.F5)
            {   //F5 is TryParse: net zoals VS, F5= Start Debugging
                btnTryParse_Click(null, null);
            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                if (e.KeyCode == Keys.F)
                {   //Ctrl+F = Ga naar zoektermbox om te beginnen met zoeken
                    rtbZoekterm.Focus();    //Als eerste de focus zetten, omdat er soms na Ctrl+F getiepte tekens in het tekstveld kwamen en niet in het zoektermveld (melding Erwin)

                    if (rtbContent.SelectedText.Trim().Length > 0)
                    {
                        rtbZoekterm.Text = rtbContent.SelectedText;
                    }
                    rtbZoekterm.SelectAll();
                }
                else if (e.KeyCode == Keys.H)
                {   //Ctrl+H = zoek-en-vervang                
                    e.SuppressKeyPress = true;  //Normaal gesproken geeft Ctrl+H dezelfde KeyCode als Backspace. We moeten de backspace nu dus onderdrukken!
                    e.Handled = true;           //Het KeyEvent moet niet verder behandeld worden door andere eventhandlers

                    btnZoekVervang_Click(sender, EventArgs.Empty);
                }
                else if ((int)e.KeyCode == 109)
                {   //Dit is de - van het KeyPad
                    btnDecrFont_Click(null, null);
                }
                else if ((int)e.KeyCode == 107)
                {   //Dit is de + van het KeyPad
                    btnIncrFont_Click(null, null);
                }
            }
        }

        private void MainForm_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {   //XDocHQ wordt gesloten

            //We gaan de tijdelijke bestanden voor Visual Studio opruimen:
            DirectoryInfo dirInf = new DirectoryInfo(_strXDocPath);
            if (!(dirInf.Exists))
            {
                Directory.CreateDirectory(_strXDocPath);
            }

            if (!(File.Exists(_strXDocPath + _ReadMeFileName)))
            {   //Zet de ReadMefile in de map, voor uitleg, wanneer deze nog niet bestaat.
                CreateReadmeFile();
            }

            foreach (FileInfo fInf in dirInf.GetFiles())
            {
                TimeSpan duur = DateTime.Now - fInf.LastWriteTime;
                if (duur.TotalDays > (float)_BewaarDuur && fInf.Extension.ToLower().Contains("xdoc"))
                {   //Alle files, waar de afgelopen 17 dagen niet in geschreven is, worden verwijderd, bij het afsluiten van XDocHQ
                    fInf.Delete();
                }
            }
        }

        private void MainForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)   //Deze methode gaat als eerste af bij het sluiten van XDocHQ
        {
            if (m_dirty)
            {   //Als het template niet opgeslagen is, dan openen we een dialoog:
                DialogResult dlgRes = MessageBox.Show("Save current template?", "Closing XDocHQ: Confirm save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dlgRes == DialogResult.Yes)
                {   //Opslaan
                    if (!SaveTemplate())
                    {   //Als het saven niet gelukt is, dan moet XDocHQ niet afgesloten worden, omdat er anders werk verloren gaat.
                        e.Cancel = true;
                    }
                }
                if (dlgRes == DialogResult.No)
                {   //Niet opslaan en zorgen dat de vraag niet nogmaals gesteld wordt
                    m_dirty = false;
                }
                if (dlgRes == DialogResult.Cancel)
                {   //Annuleer het afsluiten van XDocHQ; Het draait gewoon verder!
                    e.Cancel = true;
                }
            }
        }

        private void MainForm_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {   //Als Ctrl + MouseWheel gebruikt wordt, dan moet het font verkleind of vergroot worden.
            Keys k = System.Windows.Forms.Control.ModifierKeys;

            int iTurn = e.Delta;

            if (k == Keys.Control && iTurn != 0)
            {
                if (iTurn > 0)
                {
                    btnIncrFont_Click(null, null);
                }
                if (iTurn < 0)
                {
                    btnDecrFont_Click(null, null);
                }
            }
        }

        private void MainForm_Resize(object sender, System.EventArgs e)
        {
            SchaalRecTempGrid();    //Schuif de splitter tussen Alle en Recente zodanig dat alle recente templates in beeld komen.
        }

        #endregion MainForm

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool blnFixScollbar = true;

            if (m_template.ID == -1)
            {   //We hebben met een nieuw, onopgeslagen template te maken, dus na het saven willen we scrollen naar dit template en de scrollpositie dus niet fixeren
                blnFixScollbar = false;
            }

            int iSelStart = rtbContent.SelectionStart;                              //De cursorpositie opslaan en na saven herstellen
            int iSelLen = rtbContent.SelectionLength;                               //De selectielengte opslaan en na saven herstellen            
            int iScrollPosHori = rtbContent.ScrollPos_H;                            //Haal de positie van de horizontale scrollbar op       
            int iScrollPosVert = rtbContent.ScrollPos_V;                            //Haal de positie van de verticale scrollbar op       
            int iScrollTemplates = dgvTemplates.FirstDisplayedScrollingRowIndex;    //Haal de postie van bovenste getoonde rij in dgvTemplates op

            SaveTemplate();     //Template opslaan;

            rtbContent.SelectionStart = iSelStart;      //Cursor terugzetten  
            rtbContent.SelectionLength = iSelLen;       //Selectie herstellen

            //Zet alle scrollbars terug naar hun oude positie:
            rtbContent.ScrollAdd_H = iScrollPosHori;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
            rtbContent.ScrollAdd_V = iScrollPosVert;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
            if (blnFixScollbar)
            {   //We hadden een bestaand template, we hoeven niet naar het nieuwe ID toe te scrollen: Fixeer scrollbar (herstel oude positie)
                dgvTemplates.FirstDisplayedScrollingRowIndex = iScrollTemplates;
            }

            UpdateCaretPositionLabels();                //Zorg dat de regel- en kolomnummers weer goed staan
        }

        private void btnNewTemplate_Click(object sender, EventArgs e)
        {
            m_supressUIEvents = true;

            ClearForm(btnNewTemplate.Checked);
            if (btnNewTemplate.Checked)
            {
                m_template = m_manager.NewTemplate();
                dgvTemplates.ClearSelection();          //Er is geen template meer geselecteerd in de lijsten
                dgvRecTemplates.ClearSelection();
                this.Text = m_caption + " (New template)" + m_connectiontext;

            }
            else
            {
                m_template = null;
            }

            btnNewTemplate.Checked = false;
            m_supressUIEvents = false;

            rtbContent.Focus();
        }

        private void btnEditQuery_Click(object sender, EventArgs e)
        {
            if (dgvQueries.SelectedRows.Count > 0 && m_template != null)
            {
                int id;
                int.TryParse(dgvQueries.SelectedRows[0].Cells[0].Value.ToString(), out id);
                IXQuery query = FindQuery(id);
                string BeginText = query.QueryText;
                string BeginName = query.Name;
                int BeginConn = query.ConnectionID;


                QueryForm frm = new QueryForm(m_connections, query, ref _strobjVervangterm, ref rtbZoekterm);

                if ((_SearchReplaceForm != null) && (!_SearchReplaceForm.IsDisposed))
                {
                    _SearchReplaceForm.Close();
                }

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (query.ID != -1)
                    {
                        m_modifiedQueries.Add(query);
                    }
                    BindQueriesGrid();

                    if (BeginConn != query.ConnectionID || BeginName != query.Name || BeginText != query.QueryText)
                    {
                        SetDirty(true); //query changed, so template(+ query) need to be saved.
                    }
                }
            }
        }

        private void btnAddQuery_Click(object sender, EventArgs e)
        {
            if (m_template == null)
                return;
            IXQuery query = m_manager.NewQuery();
            QueryForm frm = new QueryForm(m_connections, query, ref _strobjVervangterm, ref rtbZoekterm);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                m_queries.Add(query);
                BindQueriesGrid();
                SetDirty(true);
            }
        }

        private void btnDeleteQuery_Click(object sender, EventArgs e)
        {
            if (m_template == null)
                return;
            if (dgvQueries.SelectedRows.Count > 0)
            {
                ArrayList deletedRows = new ArrayList();
                foreach (DataGridViewRow row in dgvQueries.SelectedRows)
                {
                    int qID;
                    int.TryParse(row.Cells["ID"].Value.ToString(), out qID);
                    if (qID == -1)
                    {
                        int hashCode;
                        int.TryParse(row.Cells["HashCode"].Value.ToString(), out hashCode);
                        IXQuery toRemove = null;
                        foreach (IXQuery query in m_queries)
                            if (hashCode == query.GetHashCode())
                                toRemove = query;
                        if (toRemove != null)
                            m_queries.Remove(toRemove);
                    }
                    else
                    {
                        IXQuery query = FindQuery(qID);
                        m_deletedQueries.Add(query);
                        m_queries.Remove(query);
                    }

                    deletedRows.Add(row);
                }

                foreach (DataGridViewRow row in deletedRows)
                    dgvQueries.Rows.Remove(row);

                BindQueriesGrid();

                SetDirty(true);
            }
        }

        private void chkToSave_CheckedChanged(object sender, EventArgs e)
        {
            if (!m_supressUIEvents)
                SetDirty(true);
        }

        private void btnVS_Click(object sender, EventArgs e)
        {
            string strFile = "tempXDoc_ID="; //Naam van de file
            string strPath = _strXDocPath;
            string strURL;
            bool blnDirCreate = true;

            this.Cursor = Cursors.WaitCursor;

            if (!(Directory.Exists(strPath)))
            {
                try
                {
                    Directory.CreateDirectory(strPath);
                    CreateReadmeFile();                     //Zet een readmefile in de map voor een beetje toelichting
                }
                catch (Exception ex)
                {
                    string message = "Creating the directory '" + strPath + "' failed.\nIs there a rights issue?\n" +
                                        "It should be a personal folder like this '%USER%\\Application Data\\XDocHQ\'.\n\n" + ex.Message;
                    MessageBox.Show(message, "Error creating directory", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(message);
                    blnDirCreate = false;
                }
            }

            if (dgvTemplates.SelectedRows.Count > 0)
            {
                if (blnDirCreate)
                {
                    strFile += dgvTemplates.SelectedRows[0].Cells[0].Value.ToString() + ".xdoc";
                    strURL = strPath + strFile;

                    try
                    {
                        SaveToFile(strURL); //Save tempfile                    
                        GetRespondingVS(ref strURL, ref strFile);     //Open Visual Studio

                        //Messagebox: File teruglezen?
                        DialogResult dgRes = MessageBox.Show("Would you like to refresh the template text with the text that you wrote in Visual Studio?", "Retrieve template text?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        ReloadFromVS(ref dgRes, ref strURL, ref strFile);  //Save file in VS, herlaad desgewenst in XDocHQ en minimaliseer VS.
                    }
                    catch (Exception ex)
                    {   //Hier worden alle fouten opgevangen, ook uit onderliggende subs. In de subs van btnVS_Click vindt bewust geen foutafhandeling plaats!
                        if (ex.Message.Contains("associated with the specified file"))
                        {
                            MessageBox.Show("Make sure that Visual Studio is the standard application for opening files of the type '*.XDoc'.\n\n" +
                                                ex.Message, "Error starting Visual Studio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            MessageBox.Show("Editing the template in Visual Studio failed.\n\nThe path to the file was: '" + strURL + "'.\n\n" + ex.Message,
                                                "Editing in Visual Studio failed!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        KubionLog.WriteLine(ex);
                    }
                }
            }
            else
            {
                MessageBox.Show("Select a template!", "No template selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            this.Activate();
            this.Focus();
            this.Cursor = Cursors.Arrow;
        }

        private void btnZoekVervang_Click(object sender, EventArgs e)
        {
            if (rtbContent.SelectedText.Trim().Length > 0)
            {   //Show the same behaviour as when pressing Ctrl+F, so use selected text as searchterm, this will be carried to the search-and-replace form
                rtbZoekterm.Text = rtbContent.SelectedText;
            }

            _strZoekterm = rtbZoekterm.Text.ToLower();      //De zoekterm wordt uitgelezen en in de globale variabele geplaatst (net als bij alle overige zoekknoppen)

            try
            {
                if ((_SearchReplaceForm == null) || (_SearchReplaceForm.IsDisposed))
                {
                    _SearchReplaceForm = new SearchReplaceForm(ref _strobjVervangterm, ref rtbContent, ref rtbZoekterm, false);   //Geef Zoekterm, huidige tekstbox en vervangterm mee. Alle kunnen aangepast worden
                    _SearchReplaceForm.Show(this);
                }
                else
                {
                    _SearchReplaceForm.SetZoekTerm(_strZoekterm);       //Geef de huidige zoekterm door aan het zoek-en-vervangscherm
                    _SearchReplaceForm.Focus(false);
                    //This line has been disabled after a notice by Michael, that pressing Ctrl+H should only set the focus to the S&R-window
                    //_SearchReplaceForm.btnVolgende_Click(sender, e);    //Ga zoeken
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error showing the Search-and-Replace form.\n\n" + ex.Message, "Error opening Search-and-Replace form",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(ex);
            }
        }

        private void btnTryParse_Click(object sender, EventArgs e)
        {
            bool blnDoExecute = false;
            string strURL = string.Empty;
            string[] strArguments = null;

            if (m_dirty)
            {
                DialogResult dlgRes = MessageBox.Show("Do you want to save the current template before parsing it?\n\n" +
                                                        "If you answer 'No', the last saved version will be parsed and shown in the browser.\n" +
                                                        "No changes will be lost.", "Save before parsing?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (dlgRes == DialogResult.Yes)
                {   //Save and proceed!
                    btnSave_Click(null, null);  //Eerst saven om de laatste versie van het template te kunnen tonen in de browser (zelfde gedrag VS: save, compile, run)
                    blnDoExecute = true;
                }
                else if (dlgRes == DialogResult.No)
                {   //Don't save and proceed!
                    blnDoExecute = true;
                }
                else if (dlgRes == DialogResult.Cancel)
                {
                    blnDoExecute = false;
                }
            }
            else if (m_template== null || m_template.Name == null)
            {
                DialogResult dlgRes = MessageBox.Show("No template selected");
                blnDoExecute = false;
            }
            else
            {
                blnDoExecute = true;    //There was nothing to save: proceed!
            }

            if (blnDoExecute)
            {
                try
                {
                    if (string.IsNullOrEmpty(_URLTryPars))
                    {   //Vul de URL uit de App.config in
                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["URLTryParse"]))
                        {
                            _URLTryPars = ConfigurationManager.AppSettings["URLTryParse"].Trim().ToLower();  //Geen controle, onderstaande Catch vangt het af
                        }
                        else
                        {   //We maken de key aan in de XDocHQ.exe.config: "<add key="URLTryParse" value="http://localhost/xdoc_test/makexdocument.aspx" />"
                            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                            config.AppSettings.Settings.Remove("URLTryParse");
                            config.AppSettings.Settings.Add("URLTryParse", "http://server/site/makexdocument.aspx");
                            config.Save(ConfigurationSaveMode.Modified);
                            ConfigurationManager.RefreshSection("appSettings");
                        }
                    }

                    if (!string.IsNullOrEmpty(_URLTryPars))
                    {
                        if (buildURLTryParse(ref strURL, ref strArguments))
                        {
                            if (!_URLTryPars.ToLower().Equals("messagebox"))
                            {
                                Process.Start(strURL);
                            }
                            else
                            {
                                XDocManager xdoc = new XDocManager(ConfigurationManager.AppSettings["XDocConnectionString"].ToString());
                                IXTemplate xt = xdoc.LoadTemplate(m_template.Name);
                                IXDocument xd = xt.ProcessTemplate("");
                                MessageBox.Show(xd.Content);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("An error occured while loading the URL to the XDocuments webapplication " +
                                            "<URLTryParse> from the config file.\n\n" +
                                            "Has an URL be filled in under 'URLTryParse' in XDocHQ.exe.config?\n\n" +
                                            "If you want the templates to be parsed as a normal XDoc-application, " +
                                            "fill in an URL ending with \".aspx\" (makeXdocument.aspx or MyIris.aspx).\n" +
                                            "If you want the templates to serve as a REST service, " +
                                            "fill in an URL ending with \"/\".\n\n" +
                                            "For example, if a webapplication has been created which works with the current database, " +
                                            "on this URL: 'http://localhost/xdoc_test/makexdocument.aspx?templateid=149', " +
                                            "then fill in under 'URLTryParse' in the XDocHQ.exe.config: 'http://localhost/xdoc_test/makexdocument.aspx'.\n\n" +
                                            "Or if a REST service exists which uses templates from the current database, " +
                                            "you can fill in the URL to the service, ending with \"/\", " +
                                            "for example: 'http://server/RestService/'.\n\n" +
                                            "Watch out! After changing XDocHQ.exe.config, XDocHQ must be restarted.",
                                        "Error loading URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    string Message = "An error occured showing the current template in the browser.\nHas a template been loaded?\n" +
                                        "Has a correct URL been filled in under 'URLTryParse' in XDocHQ.exe.config?\n\n" + ex.Message;
                    MessageBox.Show(Message, "Error opening template in browser", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(Message);
                }
            }
        }

        private void btnZoekForm_Click(object sender, EventArgs e)   //Het zoekoverzichtscherm wordt geladen.
        {
            _strZoekterm = rtbZoekterm.Text.ToLower();      //De zoekterm wordt uitgelezen en in de globale variabele geplaatst (net als bij alle overige zoekknoppen)

            try
            {
                if ((_SearchForm == null) || (_SearchForm.IsDisposed))
                {   //Als het zoekscherm nog niet geinitieerd is, of als het weer gesloten is, dan moeten we het openen:
                    _SearchForm = new SearchForm(_strZoekterm);
                    _SearchForm.Show(this);
                }
                else
                {   //Als het zoekscherm al open staat, dan hoeven we het alleen maar te selecteren:                    
                    if (_strZoekterm.Trim().Length > 0)
                    {
                        _SearchForm.tbxZoekterm.Text = _strZoekterm;
                        //Er is op de 'Zoeken' geklikt terwijl het zoekscherm al open was, dus we kunnen meteen een zoekactie uitvoeren:
                        _SearchForm.btnZoek_Click(null, null);
                    }
                    _SearchForm.Focus();
                    _SearchForm.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured showing the search form.\n\n" + ex.Message, "Error opening search form.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                KubionLog.WriteLine(ex);
            }
        }

        private void btnZoekTerug_Click(object sender, EventArgs e)
        {
            _iPos = rtbContent.SelectionStart; //Zet de zoekstartpositie op de plaats van de huidige cursor
            string strDeelTekst = string.Empty;
            _strZoekterm = rtbZoekterm.Text.ToLower();      //De zoekterm wordt uitgelezen en in de globale variabele geplaatst (net als bij alle overige zoekknoppen)

            if (!(_strZoekterm.Length < 1))
            {
                if ((dgvQueries.SelectedRows.Count > 0) || ((rtbContent.SelectionLength < 1) && (_iPos == 0)) || _blnSearchEndReachedBackwards)
                {   //Query selected, or at the beginning of new loaded template, or we've reached the top while searching backwards
                    _blnSearchEndReachedBackwards = false;  //Reset if used; it is only used here.

                    if (!blnHitInQueriesTerug())
                    {   //Er is geen hit in de queries. Dus verder terugzoeken door de templatetekst                        
                        SelecteerTekstTerug(rtbContent.Text.ToLower());
                    }
                }
                else
                {   //Er was geen query geselecteerd: Dit is de status quo: Zoeken door de templatetekst.
                    if (_iPos >= 0)
                    {   //Als iPos < 0, dan is er geen template geladen, of er is nooit gezocht. We kunnen geen substring opbouwen:
                        strDeelTekst = rtbContent.Text.ToLower().Substring(0, _iPos);
                    }
                    SelecteerTekstTerug(strDeelTekst);
                }
            }
            else
            {
                MessageBox.Show("Enter a searchterm.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void btnZoekVerder_Click(object sender, EventArgs e)
        {
            rtbContent.LostFocus += new EventHandler(rtbContent_LostFocus_Handled); //Assign a different eventhandle temporarilly, because we want to handle differently
            rtbContent.LostFocus -= new EventHandler(rtbContent_LostFocus);

            _strZoekterm = rtbZoekterm.Text.ToLower();      //De zoekterm wordt uitgelezen en in de globale variabele geplaatst (net als bij alle overige zoekknoppen)

            if (!_blnInSearchMode)
            {   //We will reset the search startingpoint                    
                _blnInSearchMode = true;    //Don't reset startingpoint each search. _blnSearchMode will be set to false if we start doing something other than searching.
                _iPosSearchStart = rtbContent.SelectionStart;   //Remember the startingpoint of the search. We will loop round uptil here.                    
                _blnSearchEndReached = false;   //We've just dropped into search mode, so search until the end of the text (if true, search is from beginning to searchstartposition)
            }

            if (rtbContent.SelectionLength > 0)
            {   //Als er al een selectie is, dan beginnen we met zoeken vanaf 1 plaats verder dan het begin van de selectie (+1 wordt later gedaan)
                _iPos = rtbContent.SelectionStart; //Zet de zoekstartpositie op de plaats van de huidige cursor
            }
            else
            {   //Als er nog geen selectie is, dan beginnen we met zoeken vanaf de huidige cursorplaats (+1 wordt later gedaan)
                _iPos = rtbContent.SelectionStart - 1; //Zet de zoekstartpositie op de plaats van de huidige cursor
            }

            if (_strZoekterm.Length > 0)
            {
                if (dgvQueries.SelectedRows.Count > 0)
                {   //Er is een query geselecteerd, dus daar staat de focus, dus we gaan eerst door de queries zoeken...
                    if (!blnHitInQueriesVerder())
                    {   //Er is geen hit in de queries. Dus klaar met zoeken, of verder in ander template
                        if (btnDoorzoekAlles.Checked)
                        {
                            ZoekVerderAll();
                        }
                        else
                        {
                            SearchEndForwards(rtbContent.Text.ToLower());    //Toon bericht en vraag of er vanaf het begin verder gezocht moet worden.
                        }
                    }
                }
                else
                {   //Er was geen query geselecteerd: Dit is de status quo: Zoeken door de templatetekst.
                    if (!_blnSearchEndReached)
                    {   //Status quo
                        SelecteerTekstVerder(rtbContent.Text.ToLower());
                    }
                    else
                    {   //We are in SearchMode and we are in the second run, from top to Searchstartposition
                        SelecteerTekstVerder(rtbContent.Text.ToLower().Substring(0, _iPosSearchStart));   //Only search through the left over text
                    }
                }
            }
            else
            {
                MessageBox.Show("Enter a searchterm.", "End of search", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnDoorzoekAlles_Click(object sender, EventArgs e)
        {   //Deze methode vangt twee clicks af! Van btnDoorzoekAlles en van doorzoekAllesToolStripMenuItem!!!
            if (btnDoorzoekAlles.Checked)
            {   //Hij is aan, dus hij moet uit
                btnDoorzoekAlles.Checked = false;
                doorzoekAllesToolStripMenuItem.Checked = false;
            }
            else
            {   //Hij is uit, dus hij moet aan
                btnDoorzoekAlles.Checked = true;
                doorzoekAllesToolStripMenuItem.Checked = true;
            }
        }

        public void btnRefresh_Click(object sender, EventArgs e)
        {
            bool blnFixScollbar = true;

            if (m_template == null || m_template.ID == -1)
            {   //We hebben met een nieuw, onopgeslagen template te maken, dus na het saven willen we scrollen naar dit template en de scrollpositie dus niet fixeren
                blnFixScollbar = false;
            }

            int iSelStart = rtbContent.SelectionStart;                              //De cursorpositie opslaan en na saven herstellen
            int iSelLen = rtbContent.SelectionLength;                               //De selectielengte opslaan en na saven herstellen            
            int iScrollPosHori = rtbContent.ScrollPos_H;                            //Haal de positie van de horizontale scrollbar op       
            int iScrollPosVert = rtbContent.ScrollPos_V;                            //Haal de positie van de verticale scrollbar op       
            int iScrollTemplates = dgvTemplates.FirstDisplayedScrollingRowIndex;    //Haal de postie van bovenste getoonde rij in dgvTemplates op

            BindTemplatesGrid(-1);                      //Deze methode is de eigenlijke methode die het templatesgrid opnieuw vult.

            if (m_template != null && m_template.ID != -1)
            {
                SelecteerRijOpID(m_template.ID);
            }

            rtbContent.SelectionStart = iSelStart;      //Cursor terugzetten  
            rtbContent.SelectionLength = iSelLen;       //Selectie herstellen

            //Zet alle scrollbars terug naar hun oude positie:
            rtbContent.ScrollAdd_H = iScrollPosHori;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
            rtbContent.ScrollAdd_V = iScrollPosVert;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
            if (blnFixScollbar)
            {   //We hadden een bestaand template, we hoeven niet naar het nieuwe ID toe te scrollen: Fixeer scrollbar (herstel oude positie)
                dgvTemplates.FirstDisplayedScrollingRowIndex = iScrollTemplates;
            }

            UpdateCaretPositionLabels();                //Zorg dat de regel- en kolomnummers weer goed staan 

            rtbContent.Focus(); //We zetten de focus gewoon op het tekstveld, dit kan geen kwaad en het ziet er beten uit, omdat dan de caret niet verdwijnt, na het klikken op refresh;
        }

        private void DynamicMenuItem_Click(object sender, EventArgs e)
        {   //Dit zijn de rapporten die dynamisch als menu-item toegevoegd worden:

            foreach (string key in _DynReportsColl.AllKeys)
            {
                if (reportsToolStripMenuItem.DropDownItems[key].Pressed)
                {
                    ReportsForm rptFrm = new ReportsForm(ref _DynReportsColl, key, HasHistTables());
                    rptFrm.Show(this);
                    break;
                }
            }
        }
        
        private void DynamicProcedureItem_Click(object sender, EventArgs e)
        {   //Dit zijn de procedures die dynamisch als menu-item toegevoegd worden:

            foreach (string key in _DynProceduresColl.AllKeys)
            {
                if (proceduresToolStripMenuItem.DropDownItems[key].Pressed)
                {
                    //MessageBox.Show(_DynProceduresColl.GetValues(key)[0]);
                    ExecuteProcedure(key);
                    //ReportsForm rptFrm = new ReportsForm(ref _DynReportsColl, key, _HasHistTables);
                    //rptFrm.Show(this);
                    break;
                }
            }
        }

        private void btnIncrFont_Click(object sender, EventArgs e)
        {
            if (_fontSize < 14)
            {
                _fontSize = _fontSize + 1;
                rtbContent.Font = new Font(_fontFamily, _fontSize);
                rtbContent.SetCaretSize = Convert.ToInt16(_fontSize);   //Laat de caret meegroeien met het font
                if (rtbContent.Focused)
                {   //Als er geen focus is, dan krijgen we de caret op de oude positie in het luchtledige, dat ziet er raar uit
                    rtbContent.CreateNewCaret();
                }
            }
        }

        private void btnDecrFont_Click(object sender, EventArgs e)
        {
            if (_fontSize > 7)
            {
                _fontSize = _fontSize - 1;
                rtbContent.Font = new Font(_fontFamily, _fontSize);
                rtbContent.SetCaretSize = Convert.ToInt16(_fontSize);   //Laat de caret meekrimpen met het font
                if (rtbContent.Focused)
                {   //Als er geen focus is, dan krijgen we de caret op de oude positie in het luchtledige, dat ziet er raar uit
                    rtbContent.CreateNewCaret();
                }
            }
        }

        private void btnHideGrid_Click(object sender, System.EventArgs e)
        {
            //Schuif de splitter naar links:
            _oriGridSplitSize = splitTemplatesText.SplitterDistance;    //Onthoud de afstand, zodat we hem weer netjes terug kunnen zetten.
            splitTemplatesText.SplitterDistance = 1;
            splitTemplatesText.IsSplitterFixed = true;
            splitTemplatesText.FixedPanel = FixedPanel.Panel1;

            //Maak alle controls in de linker kolom onzichtbaar:
            btnRefresh.Visible = false;
            lblAlleTemplates.Visible = false;
            btnHideGrid.Visible = false;
            dgvTemplates.Visible = false;
            dgvRecTemplates.Visible = false;
            lblRecTemplates.Visible = false;

            //Maak het uitklapknopje zichtbaar:
            btnExpandGrid.Visible = true;
        }

        private void btnExpandGrid_Click(object sender, System.EventArgs e)
        {
            //Schuif de splitter naar rechts:
            splitTemplatesText.SplitterDistance = _oriGridSplitSize;
            splitTemplatesText.IsSplitterFixed = false;
            splitTemplatesText.FixedPanel = FixedPanel.None;

            //Maak alle controls in de linker kolom zichtbaar:
            btnRefresh.Visible = true;
            lblAlleTemplates.Visible = true;
            btnHideGrid.Visible = true;
            dgvTemplates.Visible = true;
            dgvRecTemplates.Visible = true;
            lblRecTemplates.Visible = true;

            //Maak het uitklapknopje onzichtbaar:
            btnExpandGrid.Visible = false;
        }

        private void ContextMenuItemNewWindow_Click(object sender, System.EventArgs e)
        {   //Dit is de eeste knop in het contextmenu (rechtermuisknop), van het dgvTemplates;                
            if (_IDinNewW > -1)
            {
                Process.Start("XDocHQ.exe", "templateid=" + _IDinNewW.ToString());
                _IDinNewW = -2;
            }
            else
            {
                MessageBox.Show("Could not retrieve a correct template ID from your mouse click.\nTry again.",
                                    "Failed to get ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ContextMenuItemRefresh_Click(object sender, System.EventArgs e)
        {   //Dit is de tweede knop in het contextmenu (rechtermuisknop), van het dgvTemplates;                
            btnRefresh_Click(null, null);
        }

        private void btnHideQueries_Click(object sender, EventArgs e)
        {
            if (_oriQuerySplitSize == -1)
            {
                _oriQuerySplitSize = splitTextQueries.Height - splitTextQueries.SplitterDistance;    //Onthoud de afstand, zodat we hem weer netjes terug kunnen zetten.
                splitTextQueries.SplitterDistance = this.Height;
                splitTextQueries.IsSplitterFixed = true;
                splitTextQueries.FixedPanel = FixedPanel.Panel2;
                //Hide content
                grpDetails.Visible = false;
                grpQueries.Visible = false;
                //Show other buttonimage and right hovertext
                this.btnHideQueries.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHideQueries.Up")));
                _strHideQueriesHoverText = "Click to expand";
            }
            else
            {

                if (splitTextQueries.Height - _oriQuerySplitSize >= 0)
                {   //Protect against negative splitterdistance:
                    splitTextQueries.SplitterDistance = splitTextQueries.Height - _oriQuerySplitSize;
                }
                else
                {   //Protect against negative splitterdistance:
                    splitTextQueries.SplitterDistance = (int)(splitTextQueries.Height / 2);
                }

                splitTextQueries.IsSplitterFixed = false;           //Manually sliding the splitter is prohibited
                splitTextQueries.FixedPanel = FixedPanel.None;
                _oriQuerySplitSize = -1;                            //Reset, so on next click it will end in the IF above.
                //Show content
                grpDetails.Visible = true;
                grpQueries.Visible = true;
                //Show right buttonimage and right hovertext
                this.btnHideQueries.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHideQueries.Down")));
                _strHideQueriesHoverText = "Click to hide";
            }
        }

        private void btnHideQueries_MouseEnter(object sender, System.EventArgs e)
        {
            _GeneralToolTip.Show(_strHideQueriesHoverText, btnHideQueries, 0, 10, 3000);
        }

        private void btnHideQueries_MouseLeave(object sender, System.EventArgs e)
        {
            _GeneralToolTip.Hide(btnHideQueries);
        }

        #endregion Buttons

        #region Textfields/DGV's

        private void dgvTemplates_Sorted(object sender, System.EventArgs e)
        {   //Na sorteren was de selectie veranderd, deze gaan we nu weer goed zetten
            int _iSortingPos;

            try
            {
                _iSortingPos = iSortingPos; //Haal vorige cursorpositie op; deze wordt zo overschreven door SelectionChanged                
                if (!m_supressUIEvents)
                {   //We voorkomen nu dat een fout template telkens weer geselecteerd wordt in een loop (b.v. na saven van rechten, waarna we geen toegang meer hebben)
                    SelecteerRijOpID(iSortingID);   //Selecteer weer de template die ook geselecteerd was voor het sorteren
                }
                rtbContent.SelectionStart = _iSortingPos;   //Zet de cursor op de plaats waar hij stond voor het sorteren
                UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
            }
            catch { }
            finally
            {
                _BeingSorted = false;   //Sorting has finished, because the current event is fired
            }
        }

        private void dgvTemplates_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTemplates.SelectedRows.Count > 0 && (!m_supressUIEvents || _BeingSaved) && !_BeingSorted) //Bij _BeingSaved moet deze code juist WEL uitgevoerd worden, bij _BeingSorted NIET
            {
                int templateID;
                int.TryParse(dgvTemplates.SelectedRows[0].Cells["ID"].Value.ToString(), out templateID);
                _BeingSaved = false;        //Tijdens het saven mag maar 1 keer geprobeerd worden om een template te tonen, anders kan er een loop komen, b.v. wanneer foute rights zijn gesaved.
                ShowTemplate(templateID);
            }
        }

        private void dgvTemplates_KeyPress(object sender, KeyPressEventArgs e)      //KeyPress is de ideale methode om het ingedrukte karakter af te vangen, 
        {                                                                               //omdat e.KeyChar meteen het juiste karakter geeft, ook met Shiftcombinaties
            char char1eLetter = char.ToLower(e.KeyChar);    //We proberen het eerstvolgende template te selecteren dat met het ingedrukte karakter begint.                                   
            int iStartSearch;
            int iRowCount = dgvTemplates.Rows.Count;
            int iMaxLoop = 0;
            int iLoopTeller = 0;
            int iIndex = 0;

            if (dgvTemplates.SelectedRows.Count == 1)
            {   //We hebben 1 selectie
                iMaxLoop = iRowCount - 1; //We loopen een keer minder dan het aantal regels, omdat de geselecteerde rij niet mee doet.
                if ((dgvTemplates.SelectedRows[0].Index + 1) != iRowCount)
                {   //Die selectie staat niet aan het einde      
                    iStartSearch = dgvTemplates.SelectedRows[0].Index + 1;  //Dus we beginnen te zoeken vanaf de eerstvolgende regel.
                }
                else
                {   //Selectie staat aan het einde, dus zoeken vanaf het begin:
                    iStartSearch = 0;
                }
            }
            else
            {   //In de gevallen 0 of meerdere geselecteerd gewoon vanaf het begin zoeken
                iMaxLoop = iRowCount;   //We loop door alle regels,
                iStartSearch = 0;       //vanaf het begin.
            }

            for (iIndex = iStartSearch; iLoopTeller < iMaxLoop; iIndex++) //We loopen tot we iets vinden, of alle regels gehad hebben (exclusief al geselecteerde regel).
            {
                iLoopTeller += 1;

                if (iIndex == iRowCount)
                {   //We zijn de laatste regel voorbij, dus we beginnen bij de eerste:
                    iIndex = 0;
                }

                if (char.ToLower(dgvTemplates.Rows[iIndex].Cells["Name"].Value.ToString().ToCharArray()[0]) == char1eLetter)
                {
                    dgvTemplates.ClearSelection();
                    dgvTemplates.Rows[iIndex].Selected = true;
                    dgvTemplates.FirstDisplayedCell = dgvTemplates.Rows[iIndex].Cells[0];
                    break;
                }
            }
        }

        private void dgvTemplates_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteTemplates();
            }
        }

        private void dgvTemplates_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DataGridView.HitTestInfo testInfo;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                testInfo = dgvTemplates.HitTest(e.X, e.Y);

                if (testInfo.RowIndex > -1)
                {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!

                    int.TryParse(dgvTemplates.Rows[testInfo.RowIndex].Cells[0].Value.ToString(), out _IDinNewW);   //Dit is het ID waarop geklikt was met de rechte muisknop

                    dgvTemplates.ContextMenu.Show(dgvTemplates, e.Location);     //Toon het contextmenu
                }
            }
            else if (e.Button == MouseButtons.Left)
            {
                testInfo = dgvTemplates.HitTest(e.X, e.Y);
                if (testInfo.Type == DataGridViewHitTestType.ColumnHeader)
                {   //The columnheader was clicked, so sorting will start!                        
                    _BeingSorted = true;
                    dgvTemplates.ClearSelection();              //Now no selectionchanged event will fire after sorting
                    if (m_template != null)
                    {
                        iSortingID = m_template.ID;                 //Remember the templateID that was loaded before sorting
                    }
                    else
                    {
                        iSortingID = -1;
                    }
                    iSortingPos = rtbContent.SelectionStart;    //Remember the caret position
                }
                else if (testInfo.Type == DataGridViewHitTestType.TopLeftHeader)
                { }
            }
        }

        private void dgvQueries_DoubleClick(object sender, EventArgs e)
        {
            btnEditQuery_Click(sender, EventArgs.Empty);
        }

        private void dgvQueries_SelectionChanged(object sender, EventArgs e)
        {
            btnEditQuery.Enabled = dgvQueries.SelectedRows.Count == 1;
            btnDeleteQuery.Enabled = dgvQueries.SelectedRows.Count > 0;

            if (dgvQueries.SelectedRows.Count != 0)
            {   //If we have a query selected, we have no text selected and vice versa, this is to know where searching will start and what is has found. 
                rtbContent.SelectionLength = 0;
            }
        }

        private void dgvQueries_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                btnDeleteQuery_Click(this, EventArgs.Empty);
            }
        }

        private void rtbZoekterm_DoubleClick(object sender, EventArgs e)
        {
            if (rtbContent.SelectedText.Trim().Length > 0)
            {
                rtbZoekterm.Text = rtbContent.SelectedText;
            }

            rtbZoekterm.SelectAll();
        } //Er is gedubbelklikt op het vakje van de zoekterm; nu wordt de geselecteerde tekst erheen gekopieerd en wordt alle tekst geselecteerd

        private void rtbZoekterm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)  //We gaan tekst zoeken, wanneer er in de zoektekstbox op Enter wordt gedrukt
        {
            if (e.KeyChar == 13)  //Proefondervindelijk; 13 is enter
            {   //Enter in zoektermbox = vooruitzoeken
                btnZoekVerder_Click(sender, EventArgs.Empty);
            }
        }

        private void rtbZoekterm_TextChanged(object sender, System.EventArgs e)
        {
            toolStriptbxZoekterm.Text = rtbZoekterm.Text;
        }

        private void rtbContent_GotFocus(object sender, System.EventArgs e)
        {
            if (!rtbContent.ReadOnly)
            {
                rtbContent.CreateNewCaret();    //Helaas vervalt de zelfingestelde caret bij het opnieuw focussen op de textbox, dus opnieuw opvragen   
            }
            else
            {
                rtbContent.HideCaret();
            }
        }

        private void rtbContent_MouseClick(object sender, MouseEventArgs e)
        {
            dgvQueries.ClearSelection();    //Als we in de tekst zitten, dan geen queryselectie -> Dus niet beginnen met zoeken in de queries
            UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
        }

        private void rtbContent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                if (e.KeyCode == Keys.F)
                {
                    //Ctrl+F = Ga naar zoektermbox om te beginnen met zoeken
                    rtbZoekterm.Focus();    //Als eerste de focus zetten, omdat er soms na Ctrl+F getiepte tekens in het tekstveld kwamen en niet in het zoektermveld (melding Erwin)

                    if (rtbContent.SelectedText.Trim().Length > 0)
                    {
                        rtbZoekterm.Text = rtbContent.SelectedText;
                    }
                    rtbZoekterm.SelectAll();
                }
                else if (e.KeyCode == Keys.D)
                {   //Write %dash% when ^D was pressed
                    int iPosA = rtbContent.SelectionStart;

                    rtbContent.SelectedText = "";   //overwrite selection if there is any
                    rtbContent.Text = rtbContent.Text.Insert(iPosA, "%dash%");  //insert the text at carretposition

                    rtbContent.SelectionStart = iPosA + "%dash%".Length; //return carret to position at end of inserted string
                }
            }

            if (e.KeyCode != Keys.F3)
            {
                dgvQueries.ClearSelection();    //Als we in de tekst zitten, dan geen queryselectie -> Dus niet beginnen met zoeken in de queries
            }

            if (e.KeyCode == Keys.A && e.Control == true)
            {
                rtbContent.SelectAll();
            }
        }

        private void rtbContent_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.F3)
            {
                dgvQueries.ClearSelection();    //Als we in de tekst zitten, dan geen queryselectie -> Dus niet beginnen met zoeken in de queries
            }
            UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
        }

        private void rtbContent_LostFocus(object sender, System.EventArgs e)    //This is the standard eventhandler; If Search Forward was clicked, the alternative one will run
        {
            _blnInSearchMode = false;       //We should start the searchloop all over and not remeber the last Search Start Point
            _blnSearchEndReached = false;
        }

        private void rtbContent_LostFocus_Handled(object sender, System.EventArgs e)    //This is an alternative handler. In case we pressed Search Forward
        {
            rtbContent.LostFocus += new EventHandler(rtbContent_LostFocus);  //Just reassign the old handler and do nothing!
            rtbContent.LostFocus -= new EventHandler(rtbContent_LostFocus_Handled);
        }

        private void rtbContent_TextChanged(object sender, EventArgs e)
        {
            dgvQueries.ClearSelection();    //Als we in de tekst zitten, dan geen queryselectie -> Dus niet beginnen met zoeken in de queries

            if (!m_supressUIEvents)
            {
                SetDirty(true);
                UpdateCaretPositionLabels();    //Zet de Line- en Columnnumber op de juiste waarden
            }

            //Our looping search has to start over again from the current position, since the text has changed
            _blnSearchEndReached = false;   //At the next searchrequest, we can search again from the search start point
            _blnInSearchMode = false;       //Drop out of search mode and start the searching all over if nothing was ever searched.
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if (!m_supressUIEvents)
            {
                SetDirty(true);
            }
        }

        private void txtName_GotFocus(object sender, System.EventArgs e)  //tekstbox van het veld ID van het template is geselecteerd
        {
            _iOriginalTemplateName = txtName.Text;  //We moeten het oude ID terug kunnen zetten, wanneer blijkt dat het al bestaat (cotrole in txtID_LostFocus)
        }

        private void txtName_LostFocus(object sender, System.EventArgs e)
        {
            if ((txtName.Text != _iOriginalTemplateName) && (txtName.Text != m_template.Name) && (!m_supressUIEvents))
            {   //De naam is gewijzid en het is niet naar het de naam van het geladen template teruggezet 
                IsNewTemplateNameOK();
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)      //tekst in het veld ID van het template is gewijzigd
        {
            if (!m_supressUIEvents)
            {   //Er is een aanpassing gedaan, dus het template is dirty.
                SetDirty(true);
            }
        }

        private void txtID_GotFocus(object sender, System.EventArgs e)  //tekstbox van het veld ID van het template is geselecteerd
        {
            _iOriginalTemplateID = txtID.IntValue;  //We moeten het oude ID terug kunnen zetten, wanneer blijkt dat het al bestaat (cotrole in txtID_LostFocus)
        }

        private void txtID_LostFocus(object sender, System.EventArgs e)
        {
            if ((txtID.IntValue != _iOriginalTemplateID) && (txtID.IntValue != m_template.ID) && (!m_supressUIEvents))
            {   //Het ID is gewijzid en het is niet naar het geladen templateID teruggezet 
                IsNewTemplateIDOK();
            }
        }

        private void dgvRecTemplates_SelectionChanged(object sender, System.EventArgs e)
        {
            int iTemp = -1;

            if (!m_supressUIEvents && dgvRecTemplates.SelectedRows.Count == 1)
            {
                int.TryParse(dgvRecTemplates.SelectedCells[0].OwningRow.Cells["ID"].Value.ToString(), out iTemp);
                SelecteerRijOpID(iTemp);        //Selecteer de rij ook in de dgvTemplates en laat de events het werk doen
            }
            if (!m_supressUIEvents && dgvRecTemplates.SelectedRows.Count > 1)
            {   //More than one row was selected. This is not disirable. Select only the one already loaded:
                dgvTemplates.ClearSelection();
                SelecteerRijOpID(m_template.ID);
            }
            else if (_lstRecTemplates.Count < 1)
            {   //De recente lijst is zojuist leeggemaakt
                SchaalRecTempGrid();    //Schuif de splitter tussen Alle en Recente zodanig dat alle recente templates in beeld komen.
            }

        }

        private void dgvRecTemplates_UserDeletingRow(object sender, System.Windows.Forms.DataGridViewRowCancelEventArgs e)
        {
            if (!m_supressUIEvents)
            {   //Er is een rij uit de recente templates verwijderd. Nu moet deze ook uit de lijst gehaald worden.
                _lstRecTemplates.Remove(e.Row.Cells["ID"].Value.ToString());
            }
        }

        private void dgvRecTemplates_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //We hebben een right mouseclick, dus toon: Open in new window:                                        
                DataGridView.HitTestInfo testInfo = dgvRecTemplates.HitTest(e.X, e.Y);

                if (testInfo.RowIndex > -1)
                {   //There was no row found under the mouseposition, so there is no use in showing the contextmenu!
                    int.TryParse(dgvRecTemplates.Rows[testInfo.RowIndex].Cells[0].Value.ToString(), out _IDinNewW);   //Dit is het ID waarop geklikt was met de rechte muisknop
                    dgvTemplates.ContextMenu.Show(dgvRecTemplates, e.Location);     //Rare truc: Ik gebruik het contextmenu van de ene control om in de andere te tonen.                   
                }
            }
        }

        private void lblRecTemplates_MouseHover(object sender, System.EventArgs e)
        {   //We show a tooltip text. This is consistent, since we also show a text on the All templates label
            _GeneralToolTip.Show("This frame shows the last " + (_iLstRecTempCapacity - 10).ToString() + " templates that have been loaded.\n" +
                        "The number of templates shown here can be adjusted in the XDocHQ.exe.config", lblRecTemplates, 20, 20, 7000);
        }

        private void lblRecTemplates_MouseLeave(object sender, System.EventArgs e)
        {   //The mouse has left the control, so hide the tooltip text
            _GeneralToolTip.Hide(lblRecTemplates);
        }

        private void cbxWordWrap_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxWordWrap.Checked)
            {
                rtbContent.WordWrap = true;
            }
            else
            {
                rtbContent.WordWrap = false;
            }
        }

        #endregion Textfields/DGV's

        #region Menu

        private void newTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnNewTemplate.Checked = true;
            btnNewTemplate_Click(sender, e);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.ShowDialog();
        }

        private void connectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectionsForm frm = new ConnectionsForm(m_connections, m_manager);
            if (frm.ShowDialog(this) == DialogResult.OK)
            {   //Als het antwoord Nee is, dan wordt er niks veranderd aan de templates; Als het Ja is dan moet het alles opnieuw geladen worden.
                int iSelTempl = 0;
                int iCurPos = 0;
                int iSelLen = 0;
                int iScrollPosHori = 0;
                int iScrollPosVert = 0;
                int iScrollTemplates = 0;

                bool blnTemplateSelected = false;

                if (dgvTemplates.SelectedRows.Count > 0)
                {   //We gaan eerste even opslaan welk template geladen was en waar de cursor stond. Zo kunnen we na het saven van de connections, de template opnieuw tonen.
                    int.TryParse(dgvTemplates.SelectedRows[0].Cells["ID"].Value.ToString(), out iSelTempl);
                    iCurPos = rtbContent.SelectionStart;
                    iSelLen = rtbContent.SelectionLength;
                    blnTemplateSelected = true;

                    iScrollPosHori = rtbContent.ScrollPos_H;                            //Haal de positie van de horizontale scrollbar op       
                    iScrollPosVert = rtbContent.ScrollPos_V;                            //Haal de positie van de verticale scrollbar op       
                    iScrollTemplates = dgvTemplates.FirstDisplayedScrollingRowIndex;    //Haal de postie van bovenste getoonde rij in dgvTemplates op
                }

                ClearForm(false);
                m_connections = frm.Connections;
                dgvTemplates.ClearSelection();

                if (blnTemplateSelected)
                {
                    SelecteerRijOpID(iSelTempl);            //Selecteer de template die geopend was voor het saven van de connections
                    rtbContent.SelectionStart = iCurPos;    //Zet de cursor op de oude positie
                    rtbContent.SelectionLength = iSelLen;   //Maak eventueel de oude selectie weer actief

                    //Zet alle scrollbars terug naar hun oude positie:
                    rtbContent.ScrollAdd_H = iScrollPosHori;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
                    rtbContent.ScrollAdd_V = iScrollPosVert;     //Na het opnieuw laden van de tekst staat de scrollbar op 0, dus optellen is okee
                    dgvTemplates.FirstDisplayedScrollingRowIndex = iScrollTemplates;

                    UpdateCaretPositionLabels();
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStriptbxZoekterm_Click(object sender, EventArgs e)
        {
            if (toolStriptbxZoekterm.Font.Italic)
            {
                toolStriptbxZoekterm.Text = "";
                toolStriptbxZoekterm.Font = new Font(toolStriptbxZoekterm.Font, FontStyle.Regular);
                toolStriptbxZoekterm.ForeColor = Color.Black;
            }

        }

        private void toolStriptbxZoekterm_TextChanged(object sender, System.EventArgs e)
        {
            rtbZoekterm.Text = toolStriptbxZoekterm.Text;
        }

        private void saveTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnSave_Click(sender, e);
        }

        private void refreshTemplatelistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnRefresh_Click(sender, e);
        }

        private void zoekTerugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnZoekTerug_Click(sender, e);
        }

        private void zoekVerderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnZoekVerder_Click(sender, e);
        }

        private void zoekEnVervangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnZoekVervang_Click(sender, e);
        }

        private void zoekoverzichtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnZoekForm_Click(sender, e);
        }

        private void dependenciesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor curStand = Cursors.Arrow; //We maken een cursor aan die aan het einde na het vullen van het nieuwe window ingesteld wordt.
            int iTemp = -1;

            if (dgvTemplates.SelectedRows.Count > 0)
            {
                try
                {
                    curStand = base.Cursor; //We slaan even op hoe de oude cursor er uit zag om hem later weer terug te zetten.
                    base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten. 

                    int.TryParse(dgvTemplates.SelectedRows[0].Cells[0].Value.ToString(), out iTemp);
                    IDName inSelected = new IDName(iTemp, dgvTemplates.SelectedRows[0].Cells[1].Value.ToString());
                    using (DependenciesForm frmDepForm = new DependenciesForm(inSelected))
                    {
                        frmDepForm.ShowDialog(this);
                        SelecteerRijOpID(frmDepForm.idClicked);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occured processing the dependencies form.\n\n" + ex.Message,
                                        "Error in dependencies form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(ex);
                }
                finally
                {
                    base.Cursor = curStand;
                }
            }
            else
            {
                MessageBox.Show("First select a template for which the dependencies should be shown!", "No template selected",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void openInVSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnVS_Click(sender, e);
        }

        private void tryParseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnTryParse_Click(sender, e);
        }

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)     //Open het historieform
        {
            string strMSG = "The history form can not be loaded.\n\nHas a template been opened?\nOr is a new template active?";

            if (HasHistTables())
            {
                if (m_template == null || m_template.ID == -1)
                {   //Er is geen template geladen of een nieuw template aangemaakt en nog niet opgeslagen.
                    MessageBox.Show(strMSG, "Cannot show history", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        HistoryForm frmHist = new HistoryForm(m_template.ID, m_template.Name, m_connections, _blnDocTypeRights, false, _blnIsSQLite);
                        frmHist.Show(this);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(strMSG + "\n\n" + ex.Message, "Error showing history", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        KubionLog.WriteLine(strMSG + "\n\n" + ex.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("It is not possible to show the history form\n" +
                                    "because the current database does not have history tables\n" +
                                    "(S_Templates_Hist & S_Queries_Hist).\n\n" +
                                    "History tables can be added from the procedures menu.", "Cannot show history form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void openTemplateInNewWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_template != null && m_template.ID > -1)
            {
                Process.Start("XDocHQ.exe", "templateid=" + m_template.ID.ToString());
            }
            else
            {
                MessageBox.Show("No saved template is active. Select a template or save your new template.",
                                    "No template selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void deletedTemplatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string strMSG = "The deleted form cannot be loaded.\n\n";

            if (HasHistTables())
            {
                try
                {
                    HistoryForm frmHist = new HistoryForm(-2, "Deleted templates", m_connections, _blnDocTypeRights, true, _blnIsSQLite);
                    frmHist.Show(this);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(strMSG + "\n\n" + ex.Message, "Error showing deleted form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    KubionLog.WriteLine(strMSG + "\n\n" + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("It is not possible to show the deleted form\n"+
                                    "because the current database does not have history tables\n"+
                                    "(S_Templates_Hist & S_Queries_Hist).\n\n" +
                                    "History tables can be added from the procedures menu.", "Cannot show deleted form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }






        #endregion Menu

        #endregion Event handlers

    } 
}