using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using KubionDataNamespace;
using KubionLogNamespace;
using XDocuments;
using System.Data.SqlClient;

namespace XDocHQ
{
    public partial class HistoryForm : Form
    {
        #region Members
            private int _tempID;        //Het templateId van het huidige template
            private string _tempName;   //De templateNaam van het huidige template
            private HistoryPreview frmPreview;  //Het form waarin het historische template bekeken kan worden
            private IDName[] _connections;    //Lijst met connections, zodat in het HistoryPreview de naam getoond kan worden            
            private bool _blnDocTypeRights;  //Hebben we te maken met een s_templates met DocType- & Rightskolommen?
            private bool _blnShowDeleted = false;   //Gaan we het historie van een template tonen, of een overzicht van deleted items?            
            private int _iChildForms = 0;            //Het aantal HistoryPreviewForms dat gestart is vanuit dit formulier.
            private bool _blnIsSQLite = false;
            /// <summary>
            /// Methode om bij te houden hoeveel HistoryPreviewForms er gestart zijn. 
            /// </summary>
            public int CountChildForms
            {         
                set
                {
                    _iChildForms = value;
                    
                    if (_iChildForms < 0)
                    {   //Waarden kleiner dan nul zijn fout; herstel:
                        _iChildForms = 0;
                    }                    
                }
                get
                {
                    return _iChildForms;
                }                
            }         
        #endregion

        #region Event Handlers
            public HistoryForm(int templateID, string templateName, IDName[] connections, bool blnDocTypeRights, bool blnShowDeleted, bool blnIsSQLite)
            {
                _blnDocTypeRights = blnDocTypeRights;
                _tempID = templateID;
                _tempName = templateName;
                _connections = connections; //Lijst met connections, zodat in het HistoryPreview de naam getoond kan worden
                _blnShowDeleted = blnShowDeleted;
                _blnIsSQLite = blnIsSQLite;
                
                InitializeComponent();
            }

            private void HistoryForm_Load(object sender, EventArgs e)
            {
                if (_blnShowDeleted)
                {
                    this.Text = "Deleted templates";
                    lblBeschrijvingBegin.Text  = "This shows an overview of deleted templates.";
                    lblBeschrijvingEind.Visible = false;
                    lblNiksGevonden.Text = "No deleted templates have been found.";
                }
                else
                {
                    lblBeschrijvingEind.Text = _tempName + ", ID = " + _tempID.ToString();
                }

                BindTemplates();    //Vul het grid met historische templates
            }

            private void HistoryForm_Resize(object sender, System.EventArgs e) //Het DGV moet bijgeschaald worden bij een resize
            {
                dgvHistory.Height = this.Height - 105;
                dgvHistory.Width = this.Width - 30;
            }

            private void btnToon_Click(object sender, EventArgs e)
            {
                int idClicked;

                if (dgvHistory.SelectedCells.Count > 0) //Er was goed geklikt en er is iets geselecteerd...
                {
                    int.TryParse(dgvHistory.SelectedCells[0].OwningRow.Cells["HistoryID"].Value.ToString(),out idClicked);

                    frmPreview = new HistoryPreview(idClicked, _tempID, _connections, _blnDocTypeRights, this.Right, this.Top, _blnIsSQLite);                    
                    frmPreview.Show(this);                    
                    this.Focus();       //Zet de focus terug op dit Historiescherm, zodat er meteen met de pijljes door de historische templates gewandeld kan worden. 
                                        //Het historypreviewform is toch niet interactief!
                    
                }
                else
                {
                    MessageBox.Show("Select a template first!", "No template selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }                
            }

            private void btnRestore_Click(object sender, EventArgs e)
            {
                if (dgvHistory.SelectedCells.Count > 0) //Er was goed geklikt en er is iets geselecteerd...
                {
                    int iRestoreID = -1;
                    int.TryParse(dgvHistory.SelectedCells[0].OwningRow.Cells["HistoryID"].Value.ToString(), out iRestoreID);
                    RestoreTempFromHist(iRestoreID);
                }
                else
                {
                    MessageBox.Show("Select a template first!", "No template selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }  
            }
        
            private void btnCancel_Click(object sender, EventArgs e)
            {
                this.Close();
            }

            private void dgvHistory_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
            {
                btnToon_Click(sender, e);
            }

            private void dgvHistory_SelectionChanged(object sender, System.EventArgs e)
            {
                if (dgvHistory.SelectedCells.Count > 0 && frmPreview != null && !frmPreview.IsDisposed) 
                {   //We kunnen het bestaande previewscherm gebruiken om de nieuwe selectie te tonen:
                    int iNewID;
                    int.TryParse(dgvHistory.SelectedCells[0].OwningRow.Cells["HistoryID"].Value.ToString(), out iNewID);

                    frmPreview.Reload(iNewID);
                    this.Focus();
                }
            }

        #endregion Event Handlers

        #region Methods
  
            private void BindTemplates() //We gaan het grid vullen met gezochte templates
            {
                ClientData cd = null;

                try
                {
                    base.Cursor = Cursors.WaitCursor;  //Deze routine duurt lang, zodat we de cursor op zandloper willen zetten.                
                    this.Owner.Cursor = Cursors.WaitCursor;

                    System.Windows.Forms.SortOrder sortOrder = dgvHistory.SortOrder;
                    DataGridViewColumn sortColumn = dgvHistory.SortedColumn;
                    DataTable dtFoundTemplates = new DataTable();
                                    
                    if (System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                    {
                        cd = new ClientData(System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                    
                        string sSQL = GetOverviewSQL();
                                        
                        dtFoundTemplates = cd.DTExecuteQuery(sSQL);                    //DataTable komt terug en kan getoond worden in datagrid
                    }
                    else
                    {
                        MessageBox.Show("The connection string XDocConnectionString was not found in the config file!", "Error in config file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //We zetten nog wat instellingen van het datagrid:
                    dgvHistory.SuspendLayout();
                    dgvHistory.DataSource = dtFoundTemplates;

                    LijnDgvUit();
                }
                catch (Exception ex)
                {
                    string message = "An error occured filling the data grid.\n\n" + ex.Message;
                    KubionLog.WriteLine(message);
                    MessageBox.Show(message, "Error filling data grid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                    this.Owner.Cursor = Cursors.Arrow;
                    if (cd != null)
                    {
                        cd.Dispose();
                    }
                }
            }

            private void LijnDgvUit()    //We gaan het DGV uitlijnen en opmaken.
            {
                int width, height;
                int iAantal = dgvHistory.Rows.Count;
            
                //Beschikbare kolommen:
                //HistoryID, Naam, Description, Startdatum, Einddatum, StartUser, EndUser, Actueel.

                dgvHistory.Columns["HistoryID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.Columns["Naam"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                dgvHistory.Columns["Startdatum"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                dgvHistory.Columns["Einddatum"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.Columns["StartUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.Columns["EndUser"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.Columns["Actueel"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvHistory.AutoSize = true;

                width = dgvHistory.Width;
                height = dgvHistory.Height;
                
                dgvHistory.AutoSize = false;

                if (height > 300)
                {   //standaard bepaalde hoogte van DGV. Het window kan opgerekt worden.
                    dgvHistory.Height = 300;        
                    dgvHistory.Width = width - 15;
                }
                else
                {
                    dgvHistory.Height = height - 30;
                    dgvHistory.Width = width - 30;
                }

                this.Width = dgvHistory.Width + 30;     //Zo blijkt het grid goed uit te komen; resize event zorgt voor uitlijning bij vervorming van window
                this.Height = dgvHistory.Height + 110;
                
                dgvHistory.ClearSelection();
                dgvHistory.ResumeLayout();

                if (iAantal < 1)
                {   //Is er historie beschikbaar?
                    lblNiksGevonden.Visible = true;
                    dgvHistory.Visible = false;
                }
                else
                {
                    if (!_blnShowDeleted)
                    {   //Welke boodschap komt onderaan? Meervoud/enkelvoud/deleted/historisch...
                        if (iAantal == 1)
                        {
                            lblAantalHist.Text = "1 historical record has been found.";
                        }
                        else
                        {
                            lblAantalHist.Text = iAantal.ToString() + " historical records have been found.";
                        }
                    }
                    else
                    {
                        if (iAantal == 1)
                        {
                            lblAantalHist.Text = "1 deleted template has been found.";
                        }
                        else
                        {
                            lblAantalHist.Text = iAantal.ToString() + " deleted templates have been found.";
                        }
                    }
                    lblAantalHist.Visible = true;
                }            
            }

            public void RestoreTempFromHist(int TempHistID)
            {
                string sqlTemplates, sqlDeleteQueries, sqlInsertQueries;
                int iResTemplates = -2, iResDeleteQueries = -2, iResInsertQueries = -2;
                bool blnResTemplates = false, blnResDeleteQueries = false, blnResInsertQueries = false;
                ClientData cd = null;
                
                try
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"] != null)
                    {
                        cd = new ClientData(System.Configuration.ConfigurationManager.AppSettings["XDocConnectionString"]); //Een KubionDataNamespaceobject dat gegevens kan ophalen                    

                        GetRestoreQueries(out sqlTemplates, out sqlDeleteQueries, out sqlInsertQueries, TempHistID);   //Vul de queriesstrings

                        cd.BeginTransaction();  //Meerdere queries die allen goed moeten gaan, dus Transaction

                        blnResTemplates = Int32.TryParse(cd.ExecuteNonQuery(sqlTemplates), out iResTemplates);
                        blnResDeleteQueries = Int32.TryParse(cd.ExecuteNonQuery(sqlDeleteQueries), out iResDeleteQueries);
                        blnResInsertQueries = Int32.TryParse(cd.ExecuteNonQuery(sqlInsertQueries), out iResInsertQueries);

                        if (blnResTemplates && blnResDeleteQueries && blnResInsertQueries && iResTemplates > -1 && iResDeleteQueries > -1 && iResInsertQueries > -1)
                        {   //Alles is okee, committen dus!
                            cd.CommitTransaction();                            
                            
                            MainForm hoofdscherm = (MainForm)this.Owner;    //We hebben het hoofdscherm nodig om de refreshknop aan te kunnen spreken.
                            hoofdscherm.btnRefresh_Click(null, null);

                            if (_blnShowDeleted)
                            {   //Als er niks geselecteerd was, dan willen we het gerestorde template openen:
                                if (hoofdscherm.m_template == null) 
                                {   //Er was geen template actief, dus we kunnen het gerestorde template selecteren, zonder iets te verliezen
                                    int iTemp;
                                    int.TryParse(dgvHistory.SelectedCells[0].OwningRow.Cells["TemplateID"].Value.ToString(), out iTemp);
                                    hoofdscherm.SelecteerRijOpID(iTemp);
                                }
                                else if (hoofdscherm.m_template.ID < 0 && hoofdscherm.m_dirty == false)
                                {   //Er was een nieuw template actief, maar nog zonder wijzigingen, dus we kunnen het gerestorde template selecteren, zonder iets te verliezen
                                    int iTemp;
                                    int.TryParse(dgvHistory.SelectedCells[0].OwningRow.Cells["TemplateID"].Value.ToString(), out iTemp);
                                    hoofdscherm.SelecteerRijOpID(iTemp);
                                }

                            }

                            MessageBox.Show("The template has been restored succesfully.", "Succes!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else 
                        {   //Er ging iets fout: Roll back!
                            cd.RollbackTransaction();
                            string Message = "An error occured while restoring the template.\n\nResponse from TemplatesSQL = " + iResTemplates.ToString() +
                                                "\nResponse from DeleteQueriesSQL = " + iResDeleteQueries.ToString() +
                                                "\nResponse from InsertQueriesSQL = " + iResInsertQueries.ToString();                            
                            KubionLog.WriteLine(Message);
                            MessageBox.Show(Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }                        
                    }
                    else
                    {
                        MessageBox.Show("Error finding connectionstring XDocConnectionString.", "Error in configfile", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    cd.RollbackTransaction();

                    string message = "An error occured while restoring the template.\n\n" + ex.Message;
                    if (_blnShowDeleted)
                    {   //We hebben met een restore van een deleted template te maken, dus de fout zou kunnen zijn dat het template al voor kwam:
                        message += GetTempIDExistsMessage(ref cd, blnResTemplates);     //Message uitbreiden bij de uitzondering dat het templateID al voor kwam!
                    }                    
                    KubionLog.WriteLine(message);
                    MessageBox.Show(message, "Error restoring template", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (cd != null)
                    {   //Ruim je dataobject op:
                        cd.Dispose();
                    }
                }
            }

            private void GetRestoreQueries(out string sqlTemplates, out string sqlDeleteQueries, out string sqlInsertQueries, int TempHistID)
            {
                StringBuilder sbTemplates = new StringBuilder();



                if (!_blnShowDeleted)
                {   //We hebben te maken met een normaal historyoverview, dus een update
                    if (_blnIsSQLite)
                    {
                        sbTemplates.Append(" UPDATE    S_Templates SET " +
                                        " TemplateName = (SELECT TemplateName FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " TemplateDescription = (SELECT TemplateDescription FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " TemplateContent = (SELECT TemplateContent FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " TemplateToSave = (SELECT TemplateToSave FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " TemplateToRedirect = (SELECT TemplateToRedirect FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")),  " +
                                        " Unattended = (SELECT Unattended FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " DataTemplate = (SELECT DataTemplate FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " Rights = (SELECT Rights FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")), " +
                                        " DocType = (SELECT DocType FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")) " +
                                        " WHERE		(TemplateID = (SELECT TemplateID FROM S_Templates_Hist WHERE (TemplateHistID = " + TempHistID + ")	)); ");
                    }
                    else
                    {
                        sbTemplates.Append("UPDATE    S_Templates " +
                                        "SET        " +
                                        "		S_Templates.TemplateName = S_Templates_Hist.TemplateName, " +
                                        "		S_Templates.TemplateDescription = S_Templates_Hist.TemplateDescription, " +
                                        "		S_Templates.TemplateContent = S_Templates_Hist.TemplateContent, " +
                                        "		S_Templates.TemplateToSave = S_Templates_Hist.TemplateToSave, " +
                                        "		S_Templates.TemplateToRedirect = S_Templates_Hist.TemplateToRedirect, " +
                                        "		S_Templates.Unattended = S_Templates_Hist.Unattended, " +
                                        "		S_Templates.DataTemplate = S_Templates_Hist.DataTemplate ");
                        if (_blnDocTypeRights)
                        {
                            sbTemplates.Append(",  S_Templates.Rights = S_Templates_Hist.Rights, " +
                                            " 	S_Templates.DocType = S_Templates_Hist.DocType ");
                        }
                        sbTemplates.Append("FROM S_Templates_Hist " +
                                        "WHERE   (S_Templates_Hist.TemplateHistID = " + TempHistID + ") " +
                                        "AND		 " +
                                        "		(S_Templates.TemplateID =  " +
                                        "			( " +
                                        "				SELECT S_Templates_Hist.TemplateID WHERE (S_Templates_Hist.TemplateHistID = " + TempHistID + ") " +
                                        "			) " +
                                        "		) ;  ");

                        if (ExpExists()) //Here we will add another conditional update statement, to set the TemplateContentExpanede column to NULL, if present.
                        {   //To bad this cannot be done by a simple IF in SQL, because the SQL-syntax within the IF must be executable, even if the conditions are never met.
                            sbTemplates.Append(" 	UPDATE   S_Templates  " +
                                        " 	SET  S_Templates.TemplateContentExpanded = NULL " +
                                        " 	WHERE (S_Templates.TemplateID =	( SELECT TemplateID FROM S_Templates_Hist WHERE (S_Templates_Hist.TemplateHistID = " + TempHistID + " ) ) ) ; " );                                        
                        }
                    }
                }
                else
                {   //We hebben te maken met een DELETEDoverview, dus INSERTquery
                    if (!_blnIsSQLite)
                    {   //SQLite allows Identity Insert anyway
                        sbTemplates.Append(" SET IDENTITY_INSERT s_templates on ");
                    }
                    sbTemplates.Append(" insert into s_templates  " +
                                    " ( [TemplateID] , [TemplateName] , [TemplateDescription] , [TemplateContent] ,  " +
                                    " [TemplateToSave] ,[TemplateToRedirect] , [Unattended] , [DataTemplate] ");
                    if (_blnDocTypeRights)
                    {   //Als deze kolommen bestaan dan meenemen:
                        sbTemplates.Append(" , [Rights] , [DocType] ");
                    }
                    sbTemplates.Append(" ) " +
                                    " SELECT [TemplateID], [TemplateName], [TemplateDescription], [TemplateContent] , " +
                                    " [TemplateToSave] ,[TemplateToRedirect] , [Unattended] , [DataTemplate] ");
                    if (_blnDocTypeRights)
                    {
                        sbTemplates.Append(" , [Rights], [DocType] ");
                    }
                    sbTemplates.Append(" FROM s_templates_hist WHERE (TemplateHistID = " + 
                                    TempHistID.ToString() + ") " );
 
                    if (!_blnIsSQLite)
                    {   //SQLite allows Identity Insert anyway
                        sbTemplates.Append(   " SET IDENTITY_INSERT s_templates off  ");
                    }
                }

                sqlTemplates = sbTemplates.ToString();

                sqlDeleteQueries = "DELETE FROM S_Queries WHERE TemplateID = " + _tempID;

                sqlInsertQueries = "INSERT INTO S_Queries (TemplateID, QueryName, QueryText, ConnID) " +
	                                "SELECT TemplateID, QueryName, QueryText, ConnID " +
	                                "FROM S_Queries_Hist " +
                                    "WHERE (S_Queries_Hist.TemplateHistID = " + TempHistID + ") ";
            }

            private string GetOverviewSQL()
            {
                string sSQL;

                if (!_blnShowDeleted)
                {   //Normale historieoverzicht:
                    if (!_blnIsSQLite)
                    {
                        sSQL = "SELECT Top 10000 TemplateHistID as HistoryID, TemplateName as Naam, TemplateDescription as Description, " +
                                "StartDate as Startdatum, EndDate as Einddatum, StartUser,EndUser, Actual as Actueel " +
                                "FROM S_Templates_Hist " +
                                "WHERE (TemplateID = " + _tempID.ToString() + ") " +
                                "AND  " +
                                "( " +
                                "(DATEDIFF(day, ISNULL(StartDate, 1), ISNULL(EndDate, ISNULL(StartDate, 1) + 2)) > 1) " + //Eerst een DateDiff op basis van 'day'. Deze voorkomt een overflow van milliseconden in de DateDiff hieronder. Hier worden meteen de NULL voor StartDate en EndDate afgevangen, zodat dit hieronder niet meer hoeft.  
                                "OR " +
                                "(DATEDIFF(ms, StartDate, EndDate) > 100) " +  //Tijdsverschil tussen begin- en einddatum van de templatehistory moet meer dan 17 ms zijn, anders is het een history die door de onvolkomenheid van de trigger extra is aangemaakt
                                ") " +
                                "ORDER BY TemplateHistID DESC ";
                    }
                    else
                    {
                        sSQL = "SELECT TemplateHistID as HistoryID, " +
                                " TemplateName as Naam,  " +
                                " TemplateDescription as Description,  " +
                                " DateTime(StartDate) as Startdatum,  " +
                                " DateTime(EndDate) as Einddatum, StartUser,EndUser,  " +
                                " Actual as Actueel  " +
                                " FROM S_Templates_Hist  " +
                                " WHERE TemplateID = " + _tempID.ToString() +
                                " AND  (julianday(EndDate)-julianday(StartDate) > 0.0000025 " +
                                "       OR Actual = 1) " +                                
                                " ORDER BY TemplateHistID DESC;  ";
                    }
                }
                else
                {  //Alle deleted items tonen: 
                    if (_blnIsSQLite)
                    {
                        sSQL = "SELECT TemplateHistID as HistoryID, TemplateID, TemplateName as Naam, TemplateDescription as Description, " +
                                "DateTime(StartDate) as Startdatum, DateTime(EndDate) as Einddatum, StartUser,EndUser, Actual as Actueel " +
                                "FROM S_Templates_Hist " +
                                "WHERE (EndDate IS NOT NULL) AND Actual = 1 " +
                                "ORDER BY enddate desc, templateid asc ";
                    }
                    else
                    {
                        sSQL = "SELECT Top 10000 TemplateHistID as HistoryID, TemplateID, TemplateName as Naam, TemplateDescription as Description, " +
                                "StartDate as Startdatum, EndDate as Einddatum, StartUser,EndUser, Actual as Actueel " +
                                "FROM S_Templates_Hist " +
                                "WHERE (EndDate IS NOT NULL) AND Actual = 1 " +
                                "ORDER BY enddate desc, templateid asc ";
                    }
                }                

                return sSQL;
            }

            private string GetTempIDExistsMessage(ref ClientData cd, bool blnResTemplates)                        
            {
                string strQOutput;
                int iTemp = -1;
                string strReturn = "";                
                string id = dgvHistory.SelectedCells[0].OwningRow.Cells["TemplateID"].Value.ToString();
                string sqlCountTempID = "SELECT count(templateid) FROM s_templates WHERE (TemplateID = " + id + ")";

                if (!blnResTemplates && _blnShowDeleted)
                    {   //Fout bij het restoren van deleted template
                        try
                        {
                            strQOutput = cd.ExecuteQuery(sqlCountTempID);
                            if (!string.IsNullOrEmpty(strQOutput))
                            {
                                int.TryParse(strQOutput, out iTemp);
                                if (iTemp > 0)
                                {   //Het ID kwam al voor. Dit is een grote uitzondering, dus even uitleg:
                                    strReturn = "\n\nThe templateID already existed in the database.\n" +
                                                    "When the history triggers work correctly this can only occur\n" +
                                                    "when another application has added a template with ID " + id + ",\n" +
                                                    "after opening the Deleted overview.";                                                                                      
                                }
                            }
                        }
                        catch { }                        
                    }
                    return strReturn;                       
            }

            private bool ExpExists()
            {
                bool blnRetVal = false ;

                string strSql = " IF (  " +
                                    "     EXISTS (SELECT Column_Name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='s_templates' AND Column_Name='TemplateContentExpanded')  " +
                                    " )  SELECT 1 AS Result; \n" +
                                    " ELSE SELECT 0 AS Result; ";

                ClientData dataManager = null;  //Even vantevoren aanmaken, zodat hij bestaat in de Finally en hij altijd opgeruimd kan worden.

                try
                {
                    dataManager = new ClientData(((MainForm)this.Owner)._strMainConnString); //Een KubionDataNamespaceobject dat gegevens kan ophalen       

                    string sTemp = dataManager.ExecuteQuery(strSql);

                    if (sTemp == "1" || sTemp.ToLower() == "true")
                    {
                        blnRetVal = true;
                    }
                    else
                    {
                        blnRetVal = false;
                    }
                }
                catch { }
                finally
                {
                    if (dataManager != null)
                    {
                        dataManager.Dispose();
                    }
                }

                return blnRetVal;
            }
                
        #endregion Methods        

    }
}